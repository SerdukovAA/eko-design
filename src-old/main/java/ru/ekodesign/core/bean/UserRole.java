package ru.ekodesign.core.bean;

/**
 * Created by serdukov on 04.05.16.
 * Роль пользователя в системе. Каждая роль подразумевает за собой определенные привелегии
 */
public class UserRole {

    private byte id;
    private String name;

    /*---------------getters and setters-----------------------------*/
    public byte getId() {
        return id;
    }
    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
