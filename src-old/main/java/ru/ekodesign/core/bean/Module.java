package ru.ekodesign.core.bean;

/**
 * Created by serdukov on 04.05.16.
 * Модуль расчета. Это справочник модулей в БД.
 */
public class Module {

    private int id;
    private String name;
    private String description;
    /*---------------getters and setters-----------------------------*/

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

}
