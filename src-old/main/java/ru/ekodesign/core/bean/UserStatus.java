package ru.ekodesign.core.bean;

/**+
 * У каждого пользователя выставляется текущий статус в системе. Пользователь может быт ьзаблокирован или активен. Или аналоги.
 */
public class UserStatus {

    //1-активен
    private byte id;
    private String name;

    /*--------getters and setters-----*/
    public byte getId() {
        return id;
    }
    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
