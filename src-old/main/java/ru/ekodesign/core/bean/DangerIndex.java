package ru.ekodesign.core.bean;

/**
 * Created by serdukov on 22.05.16.
 */
public abstract class DangerIndex {

    private long id;
    private String bookPage;
    private User userOwner;
    private DangerIndexKind dangerIndexKind;
    private double value;
    private BookSource bookSource;

    public abstract int returnBall(double val);

/*---------------getters and setters-----------------------------*/

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getBookPage() {
        return bookPage;
    }
    public void setBookPage(String bookPage) {
        this.bookPage = bookPage;
    }

    public User getUserOwner() {
        return userOwner;
    }
    public void setUserOwner(User userOwner) {
        this.userOwner = userOwner;
    }

    public DangerIndexKind getDangerIndexKind() {
        return dangerIndexKind;
    }
    public void setDangerIndexKind(DangerIndexKind dangerIndexKind) {
        this.dangerIndexKind = dangerIndexKind;
    }

    public double getValue() {
        return value;
    }
    public void setValue(double value) {
        this.value = value;
    }

    public BookSource getBookSource() {
        return bookSource;
    }
    public void setBookSource(BookSource bookSource) {
        this.bookSource = bookSource;
    }
}
