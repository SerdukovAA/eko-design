package ru.ekodesign.core.bean;

/**
 * Created by serdukov on 22.05.16.
 */
public class Concentration {

    private String name;
    private double value;
    private String ident;

    /*------getters and setters---------*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }
}
