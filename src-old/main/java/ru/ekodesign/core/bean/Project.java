package ru.ekodesign.core.bean;

import ru.ekodesign.core.moduls.Calculation;

import java.util.Set;

/**
 * Created by serdukov on 04.05.16.
 * У каждого пользователя есть некоторое колличество проектов. Каждый проект принадлежит конкретному пользователю.
 */
public class Project {

    private long id;
    private String name;
    private String description;
    private User user;
    private Set<Calculation> calculations;
/*--------------------getters and setters-----------------------------------*/
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public Set<Calculation> getCalculations() {
        return calculations;
    }
    public void setCalculations(Set<Calculation> calculations) {
        this.calculations = calculations;
    }

}
