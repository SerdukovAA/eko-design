package ru.ekodesign.core.bean;


/**
 * У каждого расчета контролируется статус. Который определяет может ли модуль быть расчитан
 */
public class CalculationStatus {

    private byte id;
    private String name;

    /*--------getters and setters-----*/
    public byte getId() {
        return id;
    }
    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
