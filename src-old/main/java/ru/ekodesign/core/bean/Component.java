package ru.ekodesign.core.bean;



import java.util.Set;

/**
 * Created by serdukov on 22.05.16.
 * ВЕЩЕСТВО.Таблица с компонентами создает справочник веществ
 */
public class Component {

    private long id;
    private String name;
    private String description;
    private Set<DangerIndex> dangerIndexes;

    /*---------------getters and setters-----------------------------*/
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<DangerIndex> getDangerIndexes() {
        return dangerIndexes;
    }

    public void setDangerIndexes(Set<DangerIndex> dangerIndexes) {
        this.dangerIndexes = dangerIndexes;
    }
}
