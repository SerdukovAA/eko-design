package ru.ekodesign.core.dao;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.ekodesign.core.bean.User;

/**
 * Created by serdukov on 05.05.16.
 */
public interface UserDAO {


    public User getUserByEmail(String email) throws UsernameNotFoundException;
    public int create(User user) throws Exception;
    public void update(User user) throws Exception;
    public void delete(User user) throws Exception;


}
