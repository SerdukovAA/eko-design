package ru.ekodesign.core.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ekodesign.core.bean.User;
import ru.ekodesign.core.bean.UserRole;
import ru.ekodesign.core.dao.UserDAO;

import java.sql.*;

/**
 * Created by serdukov on 05.05.16.
 */
@Repository
public class UserDAOImpl implements UserDAO{

    final String newUserSql =
            "insert into eko.users (email, date_create, user_status_id) VALUES ( ?, NOW(), 1)";

    final String updateSql =
            "update eko.users set email = ?, user_status_id = ?, password = ? where id = ?";

    final String deleteSql = "delete from eko.users where id = ?";
    final String insetrUserRoleSql = "insert into eko.user_role_ref(user_id, role_id)  values (?,?)";
    final String deleteUserRoleSql = "delete from eko.user_role_ref where user_id = ?";


    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Transactional(readOnly = true, rollbackFor = Throwable.class)
    public User getUserByEmail(String email) throws UsernameNotFoundException {

        String sql = "select * from eko.users where email = ?;";

        User user = (User)jdbcTemplate.queryForObject(
                sql, new Object[] { email },
                new BeanPropertyRowMapper(User.class));

        return user;
    }


    @Transactional(rollbackFor = Throwable.class)
    public int create(final User user) throws Exception{
        KeyHolder holder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection)
                    throws SQLException {
                PreparedStatement ps = connection.prepareStatement(newUserSql.toString(), new String[]{"id"});
                ps.setString(1, user.getEmail());
                return ps;
            }
        }, holder);

        int user_id = holder.getKey().intValue();

        //записываем роли
        for(UserRole userRole : user.getUserRoles()){
            jdbcTemplate.update(insetrUserRoleSql, user_id, userRole.getId());
        }

        return user_id;
    }


    public void update(final User user) throws Exception{
        jdbcTemplate.update(updateSql, user.getEmail(), user.getUserStatus().getId(), user.getPassword(), user.getId());
    }

    @Transactional(rollbackFor = Throwable.class)
    public void delete(User user) throws Exception{
        jdbcTemplate.update(deleteSql, user.getId());
        //удаляем роли
        jdbcTemplate.update(deleteUserRoleSql, user.getId());
    }
}
