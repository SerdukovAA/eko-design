package ru.ekodesign.core.moduls;


import ru.ekodesign.core.bean.CalculationStatus;
import ru.ekodesign.core.bean.Module;
import ru.ekodesign.core.bean.Project;

/**
 * Это абстрактрый класс который объединяет все расчетные модули. Содержит минимальный функцтонал для
 * расчтеных модулей и абстракции которые модули должны реализовать
 */
public abstract class Calculation{

    private long id;
    private String name;
    private String description;
    private Project project;
    private Module modul;
    private CalculationStatus calculationStatus;

    //каждый расчетныймодуль знает свою папку, куда он попадет при выводе в UI
    private String folderName;

    /*-------------------getters and setters----------------------*/


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Project getProject() {
        return project;
    }
    public void setProject(Project project) {
        this.project = project;
    }

    public Module getModul() {
        return modul;
    }

    public void setModul(Module modul) {
        this.modul = modul;
    }

    public CalculationStatus getCalculationStatus() {
        return calculationStatus;
    }

    public void setCalculationStatus(CalculationStatus calculationStatus) {
        this.calculationStatus = calculationStatus;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }
}
