package ru.ekodesign.core.moduls.danger_class_modul.bean.prInd;

import ru.ekodesign.core.bean.DangerIndex;

/**
 * Created by serdukovaa on 01.06.2015.
 * Класс опасности в почве
 */
public class DangerClassGround extends DangerIndex {


    @Override
    public int returnBall(double val) {

     if (val==1.0){
         return 1;
     }else if(val==2.0){
         return 2;
     }else if(val==3.0){
         return 3;
     }else if(val==-1){ //значение -1 соответствует - "не установлен"
         return 4;
     }else{
         throw new IllegalArgumentException("Переданное значение первичного показателя не может быть использовано для нахождения балла");
     }

    }
}
