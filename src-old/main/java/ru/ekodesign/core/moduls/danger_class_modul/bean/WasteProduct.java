package ru.ekodesign.core.moduls.danger_class_modul.bean;

import ru.ekodesign.core.bean.Component;
import ru.ekodesign.core.bean.Concentration;
import ru.ekodesign.core.bean.DangerIndex;

import java.util.Set;

/**
 * Created by serdukov on 22.05.16.
 * КОМПОНЕНТ. В реальном мире это как бы компонент отхода. Совокупность компонента из каталога и концентрации. Плюс он содержит в себе пересчитанные показатели по отходу Wi, Ki,
 Также у каждого компонента две концентрации. Каждая пересчитывается автоматически. Процентная и массовая.
 */
public class WasteProduct {

    private long id;
    private String name;
    private String description;
    private Concentration massConcentration;
    private Concentration percentageConcentration;
    private Component component;
    private Set<DangerIndex> dangerIndexSet;



    /*---------------getters and setters-----------------------------*/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Concentration getMassConcentration() {
        return massConcentration;
    }

    public void setMassConcentration(Concentration massConcentration) {
        this.massConcentration = massConcentration;
    }

    public Concentration getPercentageConcentration() {
        return percentageConcentration;
    }

    public void setPercentageConcentration(Concentration percentageConcentration) {
        this.percentageConcentration = percentageConcentration;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    public Set<DangerIndex> getDangerIndexSet() {
        return dangerIndexSet;
    }

    public void setDangerIndexSet(Set<DangerIndex> dangerIndexSet) {
        this.dangerIndexSet = dangerIndexSet;
    }

}
