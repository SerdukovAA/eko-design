package ru.ekodesign.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * Created by serdukov on 05.05.16.
 */

@Configuration
@EnableTransactionManagement
@ComponentScan({"ru.ekodesign.core.*"})
//@PropertySource("classpath:applicationDEV.properties")
@PropertySource("classpath:application${profile}.properties")
public class RootConfig {


    @Autowired
    public Environment env;




    //Конфигурация базу данных приложения
    @Bean(name = "dataSource", destroyMethod = "close")
    public javax.sql.DataSource getDataSource() {
        org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUrl(env.getProperty("db.url"));
        ds.setUsername(env.getProperty("db.username"));
        ds.setPassword(env.getProperty("db.password"));
        ds.setInitialSize(5);
        ds.setTestOnBorrow(true);
        ds.setValidationInterval(34000);
        ds.setValidationQuery("SELECT 1");
        ds.setMaxActive(10);
        ds.setMaxIdle(5);
        ds.setMinIdle(1);
        System.out.println("Start Primary DataSource");
        return ds;
    }


    @Bean(name = "jdbcTemplate")
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(getDataSource());
    }

    //Spring manager transaction
    @Bean
    public PlatformTransactionManager txManager() {
        System.out.println("Start Transaction Manager");
        return new DataSourceTransactionManager(getDataSource());
    }


}
