package ru.ekodesign.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by serdukov on 05.05.16.
 */


@EnableWebMvc  //Аннотация разрешает использовать MVC
@Configuration //Объявляем конфигурационный класс
@ComponentScan({"ru.ekodesign.core.*"})
public class ServletConfig {





}
