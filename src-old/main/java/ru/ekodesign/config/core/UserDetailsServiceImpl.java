package ru.ekodesign.config.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekodesign.core.bean.User;
import ru.ekodesign.core.bean.UserRole;
import ru.ekodesign.core.dao.UserDAO;

import java.util.ArrayList;
import java.util.List;


@Service("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService{


    @Autowired
    protected UserDAO userDAO;


    @Transactional(readOnly=true)
    public UserDetails loadUserByUsername(String email)  throws UsernameNotFoundException {
        User user =userDAO.getUserByEmail(email);
        if(user==null){
            System.out.println("User not found");
            throw new UsernameNotFoundException("Username not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                user.getUserStatus().getId() == 1 , true, true, true, getGrantedAuthorities(user));
    }


    private List<GrantedAuthority> getGrantedAuthorities(User user){
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for(UserRole userRole : user.getUserRoles()){
            authorities.add(new SimpleGrantedAuthority("ROLE_"+userRole.getName()));
        }
        return authorities;
    }

}