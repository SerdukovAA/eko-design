package ru.ekodesign.config.core;



import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.ekodesign.config.RootConfig;
import ru.ekodesign.config.ServletConfig;

import javax.servlet.ServletRegistration;


public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {RootConfig.class  };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
        return new Class[] {ServletConfig.class  };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/api/*" };
	}

	@Override
	protected void customizeRegistration(ServletRegistration.Dynamic registration) {
		boolean done = registration.setInitParameter("throwExceptionIfNoHandlerFound", "true"); // -> true
		if(!done) throw new RuntimeException();
	}

}