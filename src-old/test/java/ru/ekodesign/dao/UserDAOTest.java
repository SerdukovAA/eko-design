package ru.ekodesign.dao;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.ekodesign.config.RootConfig;
import ru.ekodesign.config.ServletConfig;
import ru.ekodesign.core.bean.User;
import ru.ekodesign.core.bean.UserRole;
import ru.ekodesign.core.bean.UserStatus;
import ru.ekodesign.core.dao.UserDAO;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by serdukov on 05.05.16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
//@Transactional
@ContextConfiguration(classes={RootConfig.class, ServletConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserDAOTest {


    @Autowired
    UserDAO userDAO;




    @Test
    public void a_create() throws Exception{
        UserRole ur1 = new UserRole();
        ur1.setId((byte) 1);

        UserRole ur2 = new UserRole();
        ur2.setId((byte) 2);

        Set<UserRole> set = new HashSet<UserRole>();
        set.add(ur1);
        set.add(ur2);

        User user = new User();
        user.setEmail("login2");
        user.setUserRoles(set);
        int id = userDAO.create(user);
        System.out.printf("Получили id " + id);
    }

    @Test
    public void b_getUserByEmail(){
        User user = userDAO.getUserByEmail("login2");
        System.out.println(user.getDateCreate() + " " + user.getPassword());
    }

    @Test
    public void c_update() throws Exception {
        UserStatus userStatus = new UserStatus();
        userStatus.setId((byte)1);
        User user = userDAO.getUserByEmail("login2");
        user.setUserStatus(userStatus);
        user.setEmail("login3");
        userDAO.update(user);
    }

    @Test
    public void d_delete() throws Exception {
       User user = userDAO.getUserByEmail("login3");;
       userDAO.delete(user);
    }
}