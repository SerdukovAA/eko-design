package ru.ekodesign.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.ekodesign.beans.User;

import java.util.List;

/**
 * Created by serdukov on 07.07.16.
 */
public interface UserRepositoryImpl extends MongoRepository<User, String> {

    public User findByEmail(String email);

    public List<User> findAllByEmail(String email);

    public void deleteByEmail(String email);



}
