package ru.ekodesign.services;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;
import ru.ekodesign.beans.ErrorList;
import ru.ekodesign.beans.User;
import ru.ekodesign.beans.UserRole;
import ru.ekodesign.beans.UserStatus;
import ru.ekodesign.config.Settings;
import ru.ekodesign.config.TextMasseges;
import ru.ekodesign.controllers.requests.RegistrationRequest;
import ru.ekodesign.dao.UserRepositoryImpl;
import ru.ekodesign.mail.MailManager;
import ru.ekodesign.util.GenereteHashUtil;
import ru.ekodesign.util.UserValidator;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by serdukov on 08.07.16.
 */
@Service
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserValidator userValidator;

    @Autowired
    UserRepositoryImpl userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    VelocityEngine velocityEngine;

    @Autowired
    MailManager mailManager;

    @Autowired
    Settings settings;



    public void saveNewUser(RegistrationRequest reg, ErrorList errorList) throws Exception{
        Date date = new Date();

        User user = new User();
        user.setDateCreate(date);
        user.setPassword(passwordEncoder.encode(reg.getPas1().trim()));
        user.setEmail(reg.getEmail().trim().toLowerCase());
        user.setUserRole(UserRole.USER);
        user.setUserStatus(UserStatus.NOT_ACTIVE);
        userValidator.validateNewUser(user, errorList);
        //если сконструированный пользователь валиден, идем далее
        if(!errorList.isHaveErrors()){
            logger.info("Пользователь {} прошел валидацию сервиса ", user);
            user.setActivateCode(GenereteHashUtil.genHash(user.getEmail()+date.getTime()));
            //сохраняем пользователя в БД
            saveUser(user);
            //генерим и отправляем ссылку на активацию аккаунта
            sendActivationLinkForUser(user);

        }else{
            logger.info("Пользователь {} не прошел валидацию сервиса: \n {} ", user, errorList.getErrors());
        }

    }

    //метод работает как апдейт
    private void saveUser(User user){
        logger.info("Сохраняем пользователя: {}", user);
        userRepository.save(user);
    }


    public void tryActivateUser(String email, String activationCode) throws Exception{

        //TODO необходим тест кайес. мало ли что
        List<User> users = userRepository.findAllByEmail(email);
        if(users == null || users.size() == 0){
            throw new Exception("Пользователь c email "+email+" не найден");
        }

        boolean finded = false;
        for(User u : users){
            if(u.getActivateCode().equals(activationCode)){
                finded = true;
                logger.info("Перевод пользователя {} в статус {}", u, UserStatus.ACTIVE);
                u.setUserStatus(UserStatus.ACTIVE);
                userRepository.save(u);
            }
        }

        for(User deletUser : users) {
            if(deletUser.getUserStatus() != UserStatus.ACTIVE){
               userRepository.delete(deletUser);
            }
        }

        if(!finded){
            //если никто не найден и не активирован
            throw new Exception("Код активации не подходит");
        }

    }


    private void sendActivationLinkForUser(User user){
        String link = settings.getSiteDomain()+"/act-user/"+user.getEmail()+"/"+user.getActivateCode();
        Map model = new HashMap();
        model.put("settings", settings);
        model.put("link", link);
        String mailContent = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "vm_templates/activate_mail.vm", "UTF-8", model);
        mailManager.sendMail(user.getEmail(), TextMasseges.ACTIVATION_TEMA_HEADER.getMessage(), mailContent);
    }

}
