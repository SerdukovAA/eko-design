package ru.ekodesign.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import ru.ekodesign.config.Settings;

import javax.mail.internet.MimeMessage;

@Service
public class MailManager{

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    Settings settings;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public void sendMail(String toEmail, String tema, String message){
         if(!settings.isMailSendlerActive()){
             logger.info("Почтовый менеджер выключен");
             return;
         }
         try{
             MimeMessage mailMessage = javaMailSender.createMimeMessage();
             MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true, "UTF-8");
             helper.setFrom(settings.getMailSendlerFromSign());
             helper.setTo(toEmail);
             helper.setSubject(tema);
             helper.setText(message, true);
             javaMailSender.send(mailMessage);
             logger.info("Сообщение с темой: "+tema+", на адрес "+toEmail+" отправлено" );
           }catch (Exception e){
             e.printStackTrace();
             logger.error("Ошибка отправки email сообщения {} \n {}",e.getMessage(), e);
           }
    }

}
