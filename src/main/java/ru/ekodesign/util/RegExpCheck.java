package ru.ekodesign.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Serdukov on 27.07.2015.
 */
public class RegExpCheck {

    public static final Pattern cyrillicNamePattern = Pattern.compile("^[а-яА-ЯЁьъ\\s]{3,20}$");
    public static final Pattern passwordPattern = Pattern.compile("^[a-zA-Z0-9_-]{0,100}$");
    public static final Pattern loginPattern = Pattern.compile("^[a-zA-Z0-9_-]{4,30}$");






   public static boolean cyrillicNameRegExCheck(String cyrillicName){
        Matcher matcher = cyrillicNamePattern.matcher(cyrillicName);
        return matcher.matches();
    }
   public static boolean passwordRegExCheck(String password){
        Matcher matcher = passwordPattern.matcher(password);
        return matcher.matches();
   }
   public static boolean loginRegExCheck(String login){
        Matcher matcher = loginPattern.matcher(login);
        return matcher.matches();
   }

    public static boolean fieldRegExCheck(String regEx ,String value){
        Matcher matcher = Pattern.compile(regEx).matcher(value);
        return matcher.matches();
    }




}
