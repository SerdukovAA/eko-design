package ru.ekodesign.util;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ekodesign.beans.*;
import ru.ekodesign.beans.Error;
import ru.ekodesign.dao.UserRepositoryImpl;

/**
 * Created by serdukov on 08.07.16.
 */
@Service
public class UserValidator {


    @Autowired
    UserRepositoryImpl userRepository;

    public void validateNewUser(User user, ErrorList errorList){
        //проверяем валидность email адреса
        if(!EmailValidator.getInstance().isValid(user.getEmail())){
            errorList.addError(Error.EMAIL_NOT_VALIDE);
        }

        //проверяем что адрес ранее не зарегистрирован
        User u = userRepository.findByEmail(user.getEmail());
        if(u != null && u.getUserStatus() == UserStatus.ACTIVE){
            errorList.addError(Error.EMAIL_ALREADY_IN_USE);
        }
    }





}
