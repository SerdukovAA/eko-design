package ru.ekodesign.controllers.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by serdukov on 24.01.16.
 */
public class RecaptchaResponse {

    @JsonProperty
    private Boolean success;

    @JsonProperty("error-codes")
    private List<String> errorCodes;

    @JsonProperty("challenge_ts")
    private String challengeTs;


    @JsonProperty("hostname")
    private String hostname;

    //---------------------setters and getters----------------------------------------------
    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<String> getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(List<String> errorCodes) {
        this.errorCodes = errorCodes;
    }


    public String getChallengeTs() {
        return challengeTs;
    }

    public void setChallengeTs(String challengeTs) {
        this.challengeTs = challengeTs;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }


}
