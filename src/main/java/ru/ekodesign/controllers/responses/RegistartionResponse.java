package ru.ekodesign.controllers.responses;

import ru.ekodesign.beans.ErrorList;

/**
 * Created by serdukov on 07.07.16.
 */
public class RegistartionResponse {


    String status;
    ErrorList errorList;



    /*---------------getters and setters-----------------------------*/



    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public ErrorList getErrorList() {
        return errorList;
    }
    public void setErrorList(ErrorList errorList) {
        this.errorList = errorList;
    }

}
