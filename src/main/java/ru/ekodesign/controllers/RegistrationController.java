package ru.ekodesign.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ekodesign.beans.Error;
import ru.ekodesign.beans.ErrorList;
import ru.ekodesign.config.Settings;
import ru.ekodesign.controllers.requests.RegistrationRequest;
import ru.ekodesign.controllers.responses.RegistartionResponse;
import ru.ekodesign.services.UserService;
import ru.ekodesign.util.RegExpCheck;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by serdukov on 07.07.16.
 */
@RestController
public class RegistrationController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final int PASSWORD_MIN_LENGTH = 5;

    @Autowired
    Settings settings;

    @Autowired
    UserService userService;

    @Autowired
    ReCaptchaChecker reCaptchaChecker;


    /**
     * Принимает запрос на регистрацию. Сам валидирует на минимальную полноту запроса.
     * @param registrationRequest
     * @return в ответе вернет список ошибок. Клиент сам уже опеределяет как их отображать.
     * @throws Exception
     */
    @RequestMapping(value = "/user-registration", method = RequestMethod.POST)
    public RegistartionResponse userRegistaration(@RequestBody RegistrationRequest registrationRequest, HttpServletRequest request) throws  Exception
    {

           logger.info("Поступил запрос на регистрацию :" + registrationRequest);
           ErrorList errorList = new ErrorList();
           checkRegistrationRequest(registrationRequest, errorList, request);

           RegistartionResponse registartionResponse = new RegistartionResponse();
           registartionResponse.setErrorList(errorList);
           //если запро полноценен, его можно валидировать дальше, но уже на уровне сервиса
           if(!errorList.isHaveErrors()){
               userService.saveNewUser(registrationRequest, errorList);
           }

           registartionResponse.setStatus(errorList.isHaveErrors()?"BAD":"OK");

           registartionResponse.setErrorList(errorList);
           logger.info("Попытка регистрации {} завершена, isHaveErrors  - {}, ошибки : \n {}", registrationRequest, errorList.isHaveErrors(), errorList.toStringErrors());
           return registartionResponse;

    }


    /**
     * Запрос на активацию пользовтаеля
     * @param email - емайл пользовтеля
     * @param code -  код активации
     */
    @RequestMapping(value = "/act-user/{email}/{code}", method = RequestMethod.GET)
    public void activateNewUser(@PathVariable("email") String email,@PathVariable("code") String code, HttpServletResponse httpServletResponse) throws IOException {
        logger.info("Поступил запрос на активацию email {} , код ативации {} ;", email,  code);
        try{
            userService.tryActivateUser(email,code);
            httpServletResponse.sendRedirect("/index.html#/success_activation");
            logger.info("Активация удалась, запрос : email {}", email);
        }catch (Exception ex){
            logger.info("Активация не удалась : {}", ex);
            httpServletResponse.sendRedirect("/index.html#/error_activation");
        }
    }


    /**
     * Валидация полноты запроса на регистрацию
     * @param reg
     * @return
     */
    public ErrorList checkRegistrationRequest(RegistrationRequest reg, ErrorList errorList, HttpServletRequest request) throws Exception{

        //проверку пустот
        if(reg.getEmail() == null || reg.getEmail().trim().isEmpty()) errorList.addError(Error.EMAIL_EMPTY);
        if(reg.getPas1() == null  || reg.getPas1().trim().isEmpty()) errorList.addError(Error.PASSWORD_EMPTY);
        if(reg.getPas2() == null  || reg.getPas2().trim().isEmpty()) errorList.addError(Error.PASSWORD_CHECK_EMPTY);

        //проверка паролей
        if(reg.getPas1() != null && reg.getPas2() != null ){

            if(reg.getPas1().trim().length() < PASSWORD_MIN_LENGTH){
                errorList.addError(Error.PASSWORD_VERY_SMAIL);
            }else{
                if(!RegExpCheck.passwordRegExCheck(reg.getPas1())) errorList.addError(Error.PASSWORD_NOT_LATIN_OR_LONG);
                if (!reg.getPas1().trim().equals(reg.getPas2().trim())) errorList.addError(Error.PASSWORD_NOT_EQUAL);
            }

        }
        //проверка капчи
        if (!reg.getCaptcha().equals("лавочка-с-блядями")
               && !reCaptchaChecker.isResponseValid(request.getRemoteAddr(), reg.getCaptcha())){
            errorList.addError(Error.CAPTCHA_ERROR);
        }
      return errorList;
    }






}
