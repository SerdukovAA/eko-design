package ru.ekodesign.controllers;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.ekodesign.config.Settings;
import ru.ekodesign.controllers.responses.RecaptchaResponse;

/**
 * Created by serdukov on 01.10.15.
 */

public class ReCaptchaChecker {

    @Autowired
    Settings settings;

    private final String SECRET_KEY;
    private final String RECAPTCHA_VERIF_URL;


    public ReCaptchaChecker(String secretKey, String checkURL){
        SECRET_KEY = secretKey;
        RECAPTCHA_VERIF_URL = checkURL;
    }


    public boolean isResponseValid(String remoteIp, String response) throws Exception {

        RecaptchaResponse recaptchaResponse;
        RestTemplate restTemplate = new RestTemplate();
        try {
            recaptchaResponse = restTemplate.postForObject(RECAPTCHA_VERIF_URL, createBody(SECRET_KEY , remoteIp, response), RecaptchaResponse.class);
        } catch (RestClientException e) {
            throw new Exception("Recaptcha API не доступно, ошибка: \n", e);
        }
        return recaptchaResponse.getSuccess();
    }

    private MultiValueMap<String, String> createBody(String secret, String remoteIp, String response) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("secret", secret);
        map.add("response", response);
        map.add("remoteip", remoteIp);
        return map;
    }

}
