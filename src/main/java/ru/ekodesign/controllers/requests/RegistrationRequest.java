package ru.ekodesign.controllers.requests;

/**
 * Created by serdukov on 07.07.16.
 */
public class RegistrationRequest {

    private String email;
    private String pas1;
    private String pas2;
    private String captcha;

    /*---------------getters and setters-----------------------------*/

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPas1() {
        return pas1;
    }

    public void setPas1(String pas1) {
        this.pas1 = pas1;
    }

    public String getPas2() {
        return pas2;
    }

    public void setPas2(String pas2) {
        this.pas2 = pas2;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }



    @Override
    public String toString() {
        return String.format(
                "RegistrationRequest[email=%s, pas1='%s', pas2='%s', captcha='%s']",
               email, pas1, pas2, captcha );
    }


}
