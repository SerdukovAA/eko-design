package ru.ekodesign.beans;

/**+
 * У каждого пользователя выставляется текущий статус в системе. Пользователь может быт ьзаблокирован или активен. Или аналоги.
 */
public enum UserStatus {

    NOT_ACTIVE((byte)0, "Не активирован", "NOT_ACTIVE"),
    ACTIVE((byte)1, "Активирован", "ACTIVE"),
    BLOCKED((byte)2, "Заблокирован", "BLOCKED"),
    DELETED((byte)3, "Удален", "DELETED");


    private byte id;
    private String name;
    private String status;


    UserStatus(byte id, String name, String status){
      this.id = id;
      this.name = name;
      this.status = status;
    }


    /*--------getters and setter------*/

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }




    @Override
    public String toString() {
        return String.format(
                "UserStatus[id=%s, name='%s']",
                id, name);
    }


    }
