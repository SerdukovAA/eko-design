package ru.ekodesign.beans;

/**
 * Created by serdukov on 07.07.16.
 */
public enum Error {

    EMAIL_NOT_VALIDE(1, "Адрес электронной почты не валиден"),
    PASSWORD_EMPTY(2, "Не передан пароль"),
    PASSWORD_CHECK_EMPTY(3, "Не передан проверочный пароль"),
    PASSWORD_NOT_EQUAL(4, "Проверочный пароль не совпадает"),
    EMAIL_EMPTY(5, "Не передан адрес электронной почты"),
    EMAIL_ALREADY_IN_USE(6, "Адрес электронной почты уже зарегистирован"),
    CAPTCHA_ERROR(7, "Проверка капчи не пройдена"),
    PASSWORD_VERY_SMAIL(8, "Пароль слишком короткий"),
    PASSWORD_NOT_LATIN_OR_LONG(9, "Пароль слишком длинный или содержит не допустимые символы");




    int id;
    String message;

    Error(int id, String message){
        this.id = id;
        this.message = message;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public String toString() {
        return "Error{" +
                "id=" + id +
                ", message='" + message + '\'' +
                '}';
    }

}
