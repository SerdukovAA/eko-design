package ru.ekodesign.beans;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by serdukov on 07.07.16.
 */
public class ErrorList {

    boolean haveErrors;
    Set<Error> errors = new HashSet<>();





    public Set<Error> getErrors() {
        return this.errors;
    }


    public boolean haveError(Error error){
        for(Error e : errors){
            if(e.getId() == error.getId()){
                return true;
            }
        }
        return false;
    }

    public void addErrorList(ErrorList errorList) {
        haveErrors = errorList.isHaveErrors();
        errors.addAll(errorList.getErrors());
    }

    public void addError(Error error) {
        haveErrors = true;
        errors.add(error);
    }

    public boolean isHaveErrors() {
        return haveErrors;
    }

    public String toStringErrors(){
        StringBuilder sb = new StringBuilder();
        for(Error er : errors){
            sb.append(er).append("\n");
        }
        return sb.toString();
    }
}
