package ru.ekodesign.beans;

/**
 * Created by serdukov on 04.05.16.
 * Роль пользователя в системе. Каждая роль подразумевает за собой определенные привелегии
 */
public enum UserRole{

    ADMIN ((byte)0, "Администратор", "ADMIN"),
    USER ((byte)1, "Пользователь", "USER");


    private byte id;
    private String name;
    private String role;


    UserRole(byte id, String name, String role){
        this.id = id;
        this.name = name;
        this.role = role;
    }

    /*---------------getters and setters-----------------------------*/
    public byte getId() {
        return id;
    }
    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }


    @Override
    public String toString() {
        return String.format(
                "UserRole[id=%s, name='%s']",
                id, name);
    }


}
