package ru.ekodesign.beans;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;
import java.util.Set;

/**
 * Created by serdukov on 04.05.16.
 * Пользователь системы
 */
@TypeAlias("user")
public class User {


    private String id;
    @Indexed
    private String email; //используется как логин в том числе

    private String password;
    private String activateCode;

    private Date dateCreate;
    private UserStatus userStatus;
    private UserRole userRole;

    /*---------------getters and setters-----------------------------*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }


    public String getActivateCode() {
        return activateCode;
    }
    public void setActivateCode(String activateCode) {
        this.activateCode = activateCode;
    }

    public Date getDateCreate() {
        return dateCreate;
    }
    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }


    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return String.format(
                "Usre[id=%s, email='%s', user_role ='%s', user_status ='%s']",
                id, email, userRole.toString(), userStatus.toString());
    }



}
