package ru.ekodesign.config;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by serdukov on 24.01.16.
 */
public class Settings {


    private String siteName; //наименование сайта, используется в письмах и ответах
    private String siteDomain; //домен сайта, используется при формировании ссылок
    private String mailAcceprotEmail; //адрес почты для обратной связи
    private String mailSendlerFromSign; //адрес почты для отправки сообщений связи
    private boolean mailSendlerActive; //отправка сообщений работает только если активен


    private String recaptchaSecretKey;

    /*----------------getters and setters--------------------------------*/


    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getMailAcceprotEmail() {
        return mailAcceprotEmail;
    }

    public void setMailAcceprotEmail(String mailAcceprotEmail) {
        this.mailAcceprotEmail = mailAcceprotEmail;
    }



    public boolean isMailSendlerActive() {
        return mailSendlerActive;
    }

    public void setMailSendlerActive(boolean mailSendlerActive) {
        this.mailSendlerActive = mailSendlerActive;
    }


    public String getMailSendlerFromSign() {
        return mailSendlerFromSign;
    }

    public void setMailSendlerFromSign(String mailSendlerFromSign) {
        this.mailSendlerFromSign = mailSendlerFromSign;
    }

    public String getSiteDomain() {
        return siteDomain;
    }

    public void setSiteDomain(String siteDomain) {
        this.siteDomain = siteDomain;
    }



    public String getRecaptchaSecretKey() {
        return recaptchaSecretKey;
    }

    public void setRecaptchaSecretKey(String recaptchaSecretKey) {
        this.recaptchaSecretKey = recaptchaSecretKey;
    }

}
