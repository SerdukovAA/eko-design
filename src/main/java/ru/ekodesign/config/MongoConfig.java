package ru.ekodesign.config;

import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

/**
 * Created by serdukov on 07.07.16.
 */
@Configuration
public class MongoConfig {

    @Autowired
    Environment env;


    @Bean
    public MongoDbFactory mongoDbFactory() throws Exception {
        MongoClient mongo = new MongoClient(env.getProperty("mongo.host"), Integer.parseInt(env.getProperty("mongo.port")));
        return new SimpleMongoDbFactory(mongo, env.getProperty("mongo.dbname"));
    }

}


