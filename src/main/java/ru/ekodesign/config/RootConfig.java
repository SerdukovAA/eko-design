package ru.ekodesign.config;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import ru.ekodesign.controllers.ReCaptchaChecker;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by serdukov on 09.07.16.
 */
@Configuration
public class RootConfig {


    @Autowired
    Environment env;



    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", true);
        mailProperties.put("mail.smtp.starttls.enable", true);
        mailProperties.put("mail.smtp.ssl.trust", "smtp.yandex.ru");
        mailProperties.put("mail.transport.protocol", "smtp");
        mailProperties.put("mail.encoding", "UTF-8");
        mailProperties.put("mail.smtp.host", "smtp.yandex.ru");
        mailProperties.put("mail.smtp.port", "465");
        mailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        mailProperties.put("mail.smtp.socketFactory.port", "465");
        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setUsername(env.getProperty("mail.sendler.email"));
        mailSender.setPassword(env.getProperty("mail.sendler.password"));
        return mailSender;
    }



    @Bean
    public Settings getSettings(){

        Settings s = new Settings();
        s.setSiteName(env.getProperty("settings.siteName"));
        s.setMailSendlerFromSign(env.getProperty("mail.sendler.from")); //подпись отправки сообщений
        s.setMailAcceprotEmail(env.getProperty("mail.accepter.email")); //прием сообщений обратной связи
        s.setSiteDomain(env.getProperty("settings.siteDomain"));
        s.setMailSendlerActive(Boolean.parseBoolean(env.getProperty("mail.sendler.active")));

        s.setRecaptchaSecretKey(env.getProperty("recaptcha.secretKey"));

        return s;
    }



    @Bean
    public VelocityEngine velocityEngine() throws VelocityException, IOException {
        VelocityEngineFactoryBean factory = new VelocityEngineFactoryBean();
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader." +
                        "ClasspathResourceLoader");
        factory.setVelocityProperties(props);

        return factory.createVelocityEngine();
    }

    @Bean
    public ReCaptchaChecker getReCaptchaChecker(){
        ReCaptchaChecker reCaptchaChecker = new ReCaptchaChecker(env.getProperty("recaptcha.secretKey"), env.getProperty("recaptcha.checkURL"));
        return reCaptchaChecker;
    }


}
