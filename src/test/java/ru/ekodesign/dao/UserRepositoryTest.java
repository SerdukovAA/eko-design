package ru.ekodesign.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ekodesign.CoreApplication;
import ru.ekodesign.beans.User;
import ru.ekodesign.beans.UserRole;
import ru.ekodesign.beans.UserStatus;

/**
 * Created by serdukov on 07.07.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CoreApplication.class)
public class UserRepositoryTest {


    @Autowired
    private UserRepositoryImpl repository;


    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    public void save() {
        User user = new User();
        user.setUserRole(UserRole.USER);
        user.setUserStatus(UserStatus.NOT_ACTIVE);
        user.setEmail("faergi@mail.ru");
        System.out.println("Сконструировали пользователя: \n - " + user);
        repository.save(user);
        User user1 = repository.findByEmail(user.getEmail());
        System.out.println("Получили пользователя из базы: \n - " + user1);
        assert user1 != null;
        repository.delete(user);
    }


    @Test
    public void save2() {
        User user = new User();
        user.setUserRole(UserRole.USER);
        user.setUserStatus(UserStatus.NOT_ACTIVE);
        user.setEmail("faergi@mail.ru");
        System.out.println("Сконструировали пользователя: \n - " + user);

        repository.save(user);

        User u = repository.findByEmail("faergi@mail.ru");
        assert u != null;

        user.setEmail("faergi@mail21.ru");
        repository.save(user);

        User u2= repository.findByEmail("faergi@mail21.ru");
        assert u2 != null;


        assert repository.findAllByEmail("faergi@mail.ru").size() == 0;
        assert repository.findAllByEmail("faergi@mail21.ru").size() == 1;

        assert user != null;
        repository.delete(user);
    }





}
