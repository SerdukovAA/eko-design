package ru.ekodesign.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ekodesign.CoreApplication;
import ru.ekodesign.beans.*;
import ru.ekodesign.beans.Error;
import ru.ekodesign.controllers.requests.RegistrationRequest;
import ru.ekodesign.controllers.responses.RegistartionResponse;
import ru.ekodesign.dao.UserRepositoryImpl;
import ru.ekodesign.mail.MailManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by serdukov on 09.07.16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CoreApplication.class)
public class RegistrationControllerTest {

    @Autowired
    RegistrationController registrationController;

    @Autowired
    UserRepositoryImpl userRepository;



    @Test
    public void registrationAndActivateUser() throws Exception{

        HttpServletResponse response = new MockHttpServletResponse();
        HttpServletRequest request = new MockHttpServletRequest();
        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("   sErd89@Mail.Ru        ");
        reg.setPas1("passwed");
        reg.setPas2("passwed");
        reg.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert !registartionResponse.getErrorList().isHaveErrors();

        User user = userRepository.findByEmail("serd89@mail.ru");

        assert user.getUserStatus() == UserStatus.NOT_ACTIVE;

        registrationController.activateNewUser(user.getEmail(), user.getActivateCode(), response);

        System.out.println(response.getLocale());

        User userA = userRepository.findByEmail("serd89@mail.ru");

        assert userA.getUserStatus() == UserStatus.ACTIVE;

        userRepository.delete(userA);

        User userD = userRepository.findByEmail("serd89@mail.ru");

        assert userD == null;
    }



    @Test
    public void succesRegistrationAndActivateUserNo() throws Exception{

        HttpServletResponse response = new MockHttpServletResponse();
        HttpServletRequest request = new MockHttpServletRequest();
        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("   sErd89@Mail.Ru        ");
        reg.setPas1("passwed");
        reg.setPas2("passwed");
        reg.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert !registartionResponse.getErrorList().isHaveErrors();

        User user = userRepository.findByEmail("serd89@mail.ru");

        assert user.getUserStatus() == UserStatus.NOT_ACTIVE;

        registrationController.activateNewUser(user.getEmail(), user.getActivateCode()+"1", response);

        System.out.println(response.getLocale());

        User userA = userRepository.findByEmail("serd89@mail.ru");

        assert userA.getUserStatus() == UserStatus.NOT_ACTIVE;

        userRepository.delete(userA);

        User userD = userRepository.findByEmail("serd89@mail.ru");

        assert userD == null;
    }





    @Test
    public void passwordNotEquals() throws Exception {

        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("   serd89@mail.ru  ");
        reg.setPas1("pa3WWsswed");
        reg.setPas2("passwed");
        reg.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_NOT_EQUAL);

        User userD = userRepository.findByEmail("serd89@mail.ru");

        assert userD == null;
    }


    @Test
    public void password1Null() throws Exception {
        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("   serd89@mail.ru  ");
        reg.setPas1(null);
        reg.setPas2("passwed");
        reg.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_EMPTY);

        User userD = userRepository.findByEmail("serd89@mail.ru");

        assert userD == null;
    }


    @Test
    public void password2Null() throws Exception {

        HttpServletRequest request = new MockHttpServletRequest();
        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("   serd89@mail.ru  ");
        reg.setPas1("fds2wwe");
        reg.setPas2(null);
                reg.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_CHECK_EMPTY);

        User userD = userRepository.findByEmail("serd89@mail.ru");

        assert userD == null;
    }


    @Test
    public void emailEmptyandpassandpass2() throws Exception {


        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("  ");
        reg.setPas1(" ");
        reg.setPas2(null);
        reg.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_EMPTY);
        assert registartionResponse.getErrorList().haveError(Error.EMAIL_EMPTY);
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_CHECK_EMPTY);

        User userD = userRepository.findByEmail("serd89@mail.ru");

        assert userD == null;
    }


    @Test
    public void emailEmptyAndP1AndP2() throws Exception {


        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail(null);
        reg.setPas1(null);
        reg.setPas2(null);
        reg.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_EMPTY);
        assert registartionResponse.getErrorList().haveError(Error.EMAIL_EMPTY);
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_CHECK_EMPTY);

        User userD = userRepository.findByEmail("serd89@mail.ru");

        assert userD == null;
    }


    @Test
    public void captchaNotValid() throws Exception {


        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail(null);
        reg.setPas1(null);
        reg.setPas2(null);
        reg.setCaptcha("лавочка-с-блядями2"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.CAPTCHA_ERROR);

        User userD = userRepository.findByEmail("serd89@mail.ru");
        assert userD == null;
    }



    @Test
    public void emailNotValid() throws Exception {


        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("111123.rr");
        reg.setPas1("passs");
        reg.setPas2("passs");
        reg.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.EMAIL_NOT_VALIDE);

        User userD = userRepository.findByEmail("111123.rr");
        assert userD == null;
    }



    @Test
    public void passwordSmal() throws Exception {

        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("dfdfd@ffjjf.rr");
        reg.setPas1("pas");
        reg.setPas2("pas");
        reg.setCaptcha("лавочка-с-блядями");

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_VERY_SMAIL);

        User userD = userRepository.findByEmail("dfdfd@ffjjf.rr");
        assert userD == null;
    }


    @Test
    public void passwordNOTLatin() throws Exception {

        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("dfdfd@ffjjf.rr");
        reg.setPas1("pasЯSЫ");
        reg.setPas2("pasЫЫВЦ");
        reg.setCaptcha("лавочка-с-блядями");

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_NOT_LATIN_OR_LONG);

        User userD = userRepository.findByEmail("dfdfd@ffjjf.rr");
        assert userD == null;
    }


    @Test
    public void passwordToLong() throws Exception {

        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("dfdfd@ffjjf.rr");
        reg.setPas1("12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieoQ12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieoQ");
        reg.setPas2("12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieoQ12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieo12fdjhkjfdwieoQ");
        reg.setCaptcha("лавочка-с-блядями");

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert registartionResponse.getErrorList().isHaveErrors();
        assert registartionResponse.getErrorList().haveError(Error.PASSWORD_NOT_LATIN_OR_LONG);

        User userD = userRepository.findByEmail("dfdfd@ffjjf.rr");
        assert userD == null;
    }



    @Test
    public void emailAlredyInUSe() throws Exception {

        userRepository.deleteByEmail("serd389@mail.ru");

        HttpServletRequest request = new MockHttpServletRequest();

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("seRD389@Mail.ru");
        reg.setPas1("passs");
        reg.setPas2("passs");
        reg.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse = registrationController.userRegistaration(reg, request);

        //ошибок быть не должно
        assert !registartionResponse.getErrorList().isHaveErrors();


        User userD = userRepository.findByEmail("serd389@mail.ru");
        assert userD != null;
        userD.setUserStatus(UserStatus.ACTIVE);
        userRepository.save(userD);




        RegistrationRequest reg2 = new RegistrationRequest();
        reg2.setEmail("serd389@mail.ru");
        reg2.setPas1("passs22");
        reg2.setPas2("passs22");
        reg2.setCaptcha("лавочка-с-блядями"); //TODO реализовать проверку капчи

        RegistartionResponse registartionResponse2 = registrationController.userRegistaration(reg, request);



        //ошибок быть не должно
        assert registartionResponse2.getErrorList().isHaveErrors();
        assert registartionResponse2.getErrorList().haveError(Error.EMAIL_ALREADY_IN_USE);


        userRepository.deleteByEmail("serd389@mail.ru");

        User userD2 = userRepository.findByEmail("serd389@mail.ru");
        assert userD2 == null;

    }


}