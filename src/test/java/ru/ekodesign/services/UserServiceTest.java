package ru.ekodesign.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ekodesign.CoreApplication;
import ru.ekodesign.beans.ErrorList;
import ru.ekodesign.beans.User;
import ru.ekodesign.beans.UserRole;
import ru.ekodesign.beans.UserStatus;
import ru.ekodesign.controllers.requests.RegistrationRequest;
import ru.ekodesign.dao.UserRepositoryImpl;

/**
 * Created by serdukov on 07.07.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CoreApplication.class)
public class UserServiceTest {


    @Autowired
    private UserService userService;

    @Autowired
    private UserRepositoryImpl userRepository;

    @Test
    public void saveUser() throws Exception{

        RegistrationRequest reg = new RegistrationRequest();
        reg.setEmail("serd89@mail.ru");
        reg.setPas1("passwed");
        reg.setPas2("passwed");
        reg.setCaptcha("captchaCode");
        ErrorList errorList = new ErrorList();
        userService.saveNewUser(reg, errorList);

        assert errorList.isHaveErrors() == false;

        userRepository.deleteByEmail("serd89@mail.ru");
   }


}
