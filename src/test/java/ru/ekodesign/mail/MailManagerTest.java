package ru.ekodesign.mail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ekodesign.CoreApplication;

/**
 * Created by serdukov on 09.07.16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CoreApplication.class)
public class MailManagerTest {

    @Autowired
    MailManager mailManager;

    @Test
    public void sendMailTest(){
      mailManager.sendMail("serd89@mail.ru", "тестовая отправка сообщения", "какой то обычное письмо с <b>html</b> <i>разметкой</i>");
    }





}