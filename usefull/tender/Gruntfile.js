module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		webapp:'src/main/webapp',
		browserSync: {
			bsFiles: {
				src: ['<%= webapp %>/res/css/*.css',
					'<%= webapp %>/res/js/*.js',
					'<%= webapp %>/res/js/*/*/*.js',
					'<%= webapp %>/res/js/*/*/*.html',
					'<%= webapp %>/index.html']
			},
			options: {
				server: {
					baseDir: "<%= webapp %>/"
				}
			}
		},



		protractor_webdriver: {
			start: {
				options: {
					path: './node_modules/protractor/bin/',
					command: 'webdriver-manager start'
				},
			},
		},
		protractor: {
			e2e: {
				options: {
					configFile: "e2e/conf.js", // Target-specific config file
					args: {
						specs: ['e2e/new_tender-spec.js']
					} // Target-specific arguments
				}
			},
		}

	});
	//grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-protractor-runner');
	grunt.loadNpmTasks('grunt-protractor-webdriver');

	grunt.registerTask('default', ['protractor_webdriver:start','protractor:e2e']);
};
