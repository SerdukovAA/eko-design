module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

		view_dev:"dev/view_dev",
		view:"src/main/webapp/WEB-INF/view",
		res:"src/main/webapp/WEB-INF/resources",
		jsSourcePath: "src/main/webapp/WEB-INF/resources/api",
        concat: {
		    index: {
				src: ['<%= res %>/js/lib/angular.min.js',
					'<%= res %>/js/lib/angular-route.min.js',
					'<%= res %>/js/lib/angular-animate.min.js',
					'<%= res %>/js/lib/ui-bootstrap-tpls-1.1.0.min.js',
					'<%= jsSourcePath %>/ang_m/index/js/*.js',
					'<%= jsSourcePath %>/ang_m/general/general.js'],
				dest: '<%= jsSourcePath %>/ang_m/index/js/app-index.js'
			},

			userpage:{
				src: ['<%= res %>/js/lib/angular.min.js',
					'<%= res %>/js/lib/angular-route.min.js',
					'<%= res %>/js/lib/angular-animate.min.js',
					'<%= res %>/js/lib/ui-bootstrap-tpls-1.1.0.min.js',
					'<%= jsSourcePath %>/ang_m/user_page/js/*.js',
					'<%= jsSourcePath %>/ang_m/general/general.js'],
				dest: '<%= jsSourcePath %>/ang_m/user_page/js/app-user-page.js'
			},

			tender:{
				src: ['<%= res %>/js/lib/jquery-2.1.1.min.js',
					'<%= res %>/js/lib/jquery-ui.min.js',
					'<%= res %>/js/lib/datepicker-ru.js',
					'<%= res %>/js/bootstrap.min.js',
     				'<%= res %>/js/lib/jquery.maskedinput.min.js',
					'<%= res %>/js/lib/angular.min.js',
					'<%= res %>/js/lib/angular-route.min.js',
					'<%= res %>/js/lib/angular-animate.min.js',
					'<%= jsSourcePath %>/ang_m/tender/js/*.js',
					'<%= jsSourcePath %>/ang_m/general/general.js'],
				dest: '<%= jsSourcePath %>/ang_m/tender/js/app-tender.js'
			},
			css: {
				src: ['<%= res %>/css/bootstrap.min.css',
					'<%= res %>/css/jquery-ui.theme.min.css',
					'<%= res %>/css/jquery-ui.min.css',
					'<%= res %>/css/style.css'],
				dest: '<%= res %>/css/app-style.css'
			}
        },
		uglify: {
			index: {
				src: '<%= jsSourcePath %>/ang_m/index/js/app-index.js',
				dest: '<%= jsSourcePath %>/ang_m/index/js/app-index.min.js'
			},
			userpage:{
				src: '<%= jsSourcePath %>/ang_m/user_page/js/app-user-page.js',
				dest: '<%= jsSourcePath %>/ang_m/user_page/js/app-user-page.min.js'
			},
			tender:{
				src: '<%= jsSourcePath %>/ang_m/tender/js/app-tender.js',
				dest: '<%= jsSourcePath %>/ang_m/tender/js/app-tender.min.js'
			}


		},
		cssmin: {
			css: {
				src: '<%= res %>/css/app-style.css',
				dest: '<%= res %>/css/app-style.min.css' //данный файл сразу указан
			}
		},

		clean: {
			old: {
				src: ['<%= jsSourcePath %>/ang_m/index/js/app-index.js',
					  '<%= jsSourcePath %>/ang_m/user_page/js/app-user-page.js',
					  '<%= jsSourcePath %>/ang_m/tender/js/app-tender.js',
					  '<%= res %>/css/app-style.css']
			}
		}

    });
    grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-css');



	grunt.registerTask('default', ['concat','uglify','cssmin','clean']);

};
