/**
 * Created by serdukov on 06.03.16.
 */
/**
 * Created by serdukov on 05.01.16.
 */
angular.module('app').controller('submitOfferController', ['$scope','$http', '$location','$anchorScroll',
    'tenderService', '$filter','UserSession',
    function( $scope, $http,  $location, $anchorScroll, tenderService , $filter, UserSession) {


        var tScope = this;

        tScope.NewOffer = {};
        tScope.SubmitOfferForm = {};
        tScope.submitOfferMap = {};
        tScope.juridicalInfo =  UserSession.User().Row.juridicalInfo;

        tScope.submitOffer = function(newOfferForm) {
            newOfferForm.$submitted = true;

            if (!newOfferForm.$valid) return false;


                tScope.SubmitOfferForm.tender_id = tScope.Tender.id;
                tScope.SubmitOfferForm.adress = tScope.NewOffer.adress;
                tScope.SubmitOfferForm.offerDescription = tScope.NewOffer.offerDescription;
                tScope.SubmitOfferForm.dateBegin = $filter('date')(tScope.NewOffer.offerDateBegin, "dd.MM.yyyy");
                tScope.SubmitOfferForm.dateEnd = $filter('date')(tScope.NewOffer.offerDateEnd, "dd.MM.yyyy");
                tScope.SubmitOfferForm.price = tScope.NewOffer.price;
                $http.post("/provider_new_offer", tScope.SubmitOfferForm)
                    .then(function (message) {
                        var data = message.data;
                        if (data.status == '3000') {
                            tScope.SubmitOfferForm = {};
                            $scope.$emit('errorAlertMessage', {
                                text: "Предложение по тендеру успешно добавлено",
                                status: "success"
                            });
                            $location.path("/user_page");
                        } else if (data.status == '1') {
                            var html = "<p><b>" + data.text + "</b></p>";
                            if (data.hasErrors = 'true') {
                                angular.forEach(data.errors, function (val, key) {
                                    html += "<p>" + val.errorText + "</p>";
                                });
                            }
                            $scope.$emit('errorAlertMessage', {text: html, status: "warning"});
                        } else {
                            var html = "<p><b>" + data.text + "</b></p>";
                            if (data.hasErrors = 'true') {
                                angular.forEach(data.errors, function (val, key) {
                                    html += "<p>" + val.errorText + "</p>";
                                });
                            }
                            $scope.$emit('errorAlertMessage', {text: html, status: "warning"});
                        }
                    });


        };



        tenderService.promiseTender().then(function() {



            tScope.Tender = tenderService.getTender();

            tScope.NewOffer.adress = {
                fullAdress: tScope.juridicalInfo.factAdress,
                latitude: tScope.juridicalInfo.factAdrLatitude,
                longitude: tScope.juridicalInfo.factAdrLongitude,
                region: tScope.juridicalInfo.regionCode,
                rayon: tScope.juridicalInfo.rayonCode,
                city: tScope.juridicalInfo.cityCode,
                street: tScope.juridicalInfo.street,
                houseNumber: tScope.juridicalInfo.houseNumber
            };

            tScope.setMapLang =  function(){

                //указываем позицию тендера
                tScope.Tender.tenderPositionG = new google.maps.LatLng(tScope.Tender.tenderPlaceLatitude, tScope.Tender.tenderPlaceLongitude);
                var image2 = '/res/img/icons/marker1.png';
                new google.maps.Marker({
                    position: tScope.Tender.tenderPositionG,
                    map: tScope.submitOfferMap,
                    icon: image2
                });

                var providerLocation = new google.maps.LatLng(tScope.juridicalInfo.factAdrLatitude, tScope.juridicalInfo.factAdrLongitude);
                tScope.submitOfferMap.marker = new google.maps.Marker({
                    map: tScope.submitOfferMap,
                    position: providerLocation
                });

            };


        });

    }]);

