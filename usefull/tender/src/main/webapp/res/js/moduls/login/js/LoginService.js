app.factory('LoginService', ['$http', function ($http) {
    return {
        login: function(username, password, httpHandler) {
                $http.post('/login', 'username=' + username + '&password=' + password, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).then(httpHandler);
        },
        logout: function(successHandler, errorHandler) {
                $http.post('/logout', '', {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).success(successHandler).error(errorHandler);
        }
    };
}]);




