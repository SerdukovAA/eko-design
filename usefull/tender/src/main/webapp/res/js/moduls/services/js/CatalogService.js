/**
 * Created by serdukov on 07.04.16.
 */
/**
 * Created by serdukov on 01.12.15.
 */
angular.module('app').factory('CatalogService', ['$http',function($http) {


    var RegionCollection = {};

    var promiseRegionCollection = $http.get("/info_get_all_regions")
        .success(function (regions) {
            RegionCollection = regions;
        });




    return {
        promiseRegionCollection:promiseRegionCollection,

       getRegionCollection : function(){
            return RegionCollection;
       }

    }

}]);
