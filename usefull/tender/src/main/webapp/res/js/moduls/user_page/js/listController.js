/**
 * Created by serdukov on 19.11.15.
 */



angular.module('app').controller('listController', ['$scope', '$stateParams', 'userPageService',
    function ($scope, $stateParams, userPageService) {




        var lScope = this;
        //список тендеров которые будут отображены
        lScope.TenderListForView = {};

        var renderSelectorValue = function (selectorV) {
            var values = " ";
            for (var v = 0, vlen = selectorV.values.length; v < vlen; v++) {
                values += selectorV.values[v];
                if (vlen > 1 && v < (vlen - 1)) {
                    values += ", ";
                }

            }
            selectorV.valuesHtml = values;
        }


        var generateSelectorValueList = function (tender) {

            var selectorValuesArray = new Array();
            var selectorValueList = tender.selectorValueList;
            ///пройтись по каждому и передать собрать однотипные селекторы

            for (var i = 0, len = selectorValueList.length; i < len; i++) {
                var selectorId = selectorValueList[i].selectorId;

                if (typeof selectorValuesArray[selectorId] === 'undefined') {
                    var selectorV = {};
                    selectorV.name = selectorValueList[i].selectorName;
                    selectorV.values = new Array();
                    selectorV.values.push(selectorValueList[i].selectorValue);
                    selectorValuesArray[selectorId] = selectorV;
                }
                else {
                    selectorValuesArray[selectorId].values.push(selectorValueList[i].selectorValue);
                }
            }
            selectorValuesArray.forEach(function (item, index, array) {
                renderSelectorValue(item);
            });

            tender.SelectorValueList = selectorValuesArray;
        };



        //по параметру решаем какой из листов грузить
        if ($stateParams.ten == 'tenders_my_cat') {
            lScope.listTema = "Тендеры Вашей категории участия";
            userPageService.promiseTendersForProvider().then(function () {
                lScope.TenderListForView = userPageService.getTendersMyCat();
                angular.forEach(lScope.TenderListForView, function(tender, key) {
                    generateSelectorValueList(tender);
                });
            });
        }



        if ($stateParams.ten == 'run_tenders') {
            lScope.listTema = "Тендеры, в которых Вы учавствуете";
            userPageService.promiseTendersWithOfferForProvider().then(function () {
                lScope.TenderListForView = userPageService.getTendersWithOfferForProvider();
                angular.forEach(lScope.TenderListForView, function(tender, key) {
                    generateSelectorValueList(tender);
                });
            });
        }


        if ($stateParams.ten == 'win_tenders') {
            lScope.listTema = "Тендеры, в которых Вы победили";
            userPageService.promiseTendersWinForProvider().then(function () {
                lScope.TenderListForView = userPageService.getTendersWinForProvider();
                angular.forEach(lScope.TenderListForView, function(tender, key) {
                    generateSelectorValueList(tender);
                });
            });
        }

        if ($stateParams.ten == 'my_tenders') {
            lScope.listTema = "Мои тендеры";
            userPageService.promiseTendersForConsumerOwner().then(function () {
                lScope.TenderListForView = userPageService.getTendersMy();
                angular.forEach(lScope.TenderListForView, function(tender, key) {
                    generateSelectorValueList(tender);
                });
            });
        }

        if ($stateParams.ten == 'actual_tenders') {
            lScope.listTema = "Актуальные тендеры";
            userPageService.promiseTendersForConsumerOwner().then(function () {
                lScope.TenderListForView = userPageService.getTendersMyActual();
                angular.forEach(lScope.TenderListForView, function(tender, key) {
                    generateSelectorValueList(tender);
                });
            });
        }

        if ($stateParams.ten == 'ended_tenders') {
            lScope.listTema = "Завершенные тендеры";
            userPageService.promiseTendersForConsumerOwner().then(function () {
                lScope.TenderListForView = userPageService.getTendersMyEnded();
                angular.forEach(lScope.TenderListForView, function(tender, key) {
                    generateSelectorValueList(tender);
                });
            });
        }


    }]);
