/**
 * Created by serdukov on 01.12.15.
 */
angular.module('app').factory('tenderEditService', ['$http','$q','$window','$stateParams', function($http, $q,$window, $stateParams) {

    var Tender = {};
    var RegionCollection = {};



    var InputCatalogCollections = [];
    var ParameterRelations = [];
    var RefferalCatalog = [];


    var cleanParametersRelation = function (parameterRelations) {
        var newParameterRelations = [];
        for (var i = 0; i < parameterRelations.length; i++) {

            var objectParameter = parameterRelations[i].objectParameter;


            if (newParameterRelations.length == 0) {
                newParameterRelations.push(parameterRelations[i]);
            }else {

                var duplicateIndex = -1;
                var neewLen = newParameterRelations.length;

                for (var k = 0; k < neewLen; k++) {
                    if (newParameterRelations[k].objectParameter.id == objectParameter.id) {
                        duplicateIndex = k;
                    }
                }

                if (duplicateIndex == -1) {
                    newParameterRelations.push(parameterRelations[i]);
                } else {
                    if (newParameterRelations[duplicateIndex].required == false && parameterRelations[i].required == true) {
                        newParameterRelations[duplicateIndex] = parameterRelations[i];
                    }
                }

            }
        }

        return newParameterRelations;
    };






    var getCollectionFunction = function (needCollect, nCount, i) {
        var curMap = needCollect[i].objectParameter.parameterIdent;

        return $http.get(needCollect[i].objectParameter.catalogLink)
            .success(function (collection) {
                InputCatalogCollections[curMap] = collection;
                InputCatalogCollections.length++;
            });

    };







    return {
        updateParametersObject:function(){

            var catSelectors = Tender.selectorValueList;

            var parameterRequestList = new Array();

            for (var i = 0; i < catSelectors.length; i++) {
                var selector = catSelectors[i];
                parameterRequestList.push({
                    selectorId: selector.selectorId,
                    optionId: selector.valueId
                });
            };

            return $http.post("/info_get_object_parameters", parameterRequestList)
                .success(function (parameterRelations) {
                    ParameterRelations = cleanParametersRelation(parameterRelations);
                });

        },

        updateInputCoolections:function () {

            return $q(function(resolve, reject) {

                var rC = 0;

                var parameterRelations = ParameterRelations;

                var needInsertInCollection = [];
                for (var i = 0; i < parameterRelations.length; i++) {
                    if (parameterRelations[i].objectParameter.varType == 'catalog' && parameterRelations[i].objectParameter.ref == false) {
                        needInsertInCollection.push(parameterRelations[i]);
                    }
                }
                var needCount = needInsertInCollection.length;

                if(needCount == 0)resolve('!');

                for (var i = 0; i < needCount; i++) {
                    getCollectionFunction(needInsertInCollection, needCount, i).then(function(){
                        rC++;
                        if(rC == needCount) resolve('!');
                    });
                }
            });
        },


        promiseTender: function(){
            return  $http({
                url: "/consumer_get_tender", method: "GET", params: {tender_id: $stateParams.tender_id}
            }).success(function (tender) {
                Tender = tender;
            });
        },

        getTender : function(){
            return Tender;
        },
        getInputCatalogCollections : function(){
            return InputCatalogCollections;
        },
        getRefferalCatalog : function(){
            return RefferalCatalog;
        },
        getParameterRelations : function(){
            return ParameterRelations;
        }
    }

}]);
