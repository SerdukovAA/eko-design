    var app = angular.module('app', ['ui.router', 'ngAnimate','ui.bootstrap', 'vcRecaptcha','spring-security-csrf-token-interceptor']);

    app.config(['$stateProvider', '$urlRouterProvider', 'csrfProvider', 'USER_ROLES',
        function ($stateProvider, $urlRouterProvider, csrfProvider, USER_ROLES ) {

        csrfProvider.config({
            url: '/login',
            csrfHttpType: 'options',
            maxRetries: 3,
        });

        $urlRouterProvider.otherwise("/home");

        $stateProvider

            // route for index modul
            .state('home', {
                url: "/home",
                templateUrl: '/res/js/moduls/index/index_home.html',
                controller: 'homeController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('quality_and_warranty', {
                url: "/quality_and_warranty",
                templateUrl: '/res/js/moduls/index/quality_and_warranty.html',
                controller: 'qualityWarrantyController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('about', {
                url: "/about",
                templateUrl: '/res/js/moduls/index/about_system.html',
                controller: 'aboutController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('how_it_works', {
                url: "/how_it_works",
                templateUrl: '/res/js/moduls/index/how_it_works.html',
                controller: 'howItWorkController',
                controllerAs:'howIt',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })



            //route for login in
            .state('login', {
                url: "/login",
                templateUrl: '/res/js/moduls/login/login.html',
                controller: 'loginController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('password_recovery', {
                url: "/password_recovery",
                templateUrl: '/res/js/moduls/login/password_recovery.html',
                controller: 'loginController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })


            .state('password_recovery_step2', {
                url: "/password_recovery_step2/:checkId/:hash",
                templateUrl: '/res/js/moduls/login/password_recovery_step2.html',
                controller: 'loginController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })



            //route for registration
            .state('registration_start', {
                url: "/registration/start",
                templateUrl: '/res/js/moduls/registration/start_page.html',
                controller: 'regStartController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('registration_consumer', {
                url: "/registration_consumer",
                templateUrl: '/res/js/moduls/registration/consumer_registration.html',
                controller: 'consumerController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('registration_consumer_convention', {
                url: "/registration_consumer_convention",
                templateUrl: '/res/js/moduls/registration/con/consumer_convention.html',
                controller: 'consumerController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('registration_provider', {
                url: "/registration_provider",
                templateUrl: '/res/js/moduls/registration/provider_registration.html',
                controller: 'providerController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('registration_provider_convention', {
                url: "/registration_provider_convention",
                templateUrl: '/res/js/moduls/registration/con/provider_convention.html',
                controller: 'providerController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('registration_offretite_contract', {
                url: "/registration_offretite_contract",
                templateUrl: '/res/js/moduls/registration/con/offretite_contract.html',
                controller: 'providerController',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('end_registration', {
                url: "/end_registration",
                templateUrl: '/res/js/moduls/registration/end_page.html',
                controller: 'endUserRegController',
                controllerAs: 'endContr',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('success_email', {
                url: "/success_email",
                templateUrl: '/res/js/moduls/registration/success_email.html',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('error_email', {
                url: "/error_email",
                templateUrl: '/res/js/moduls/registration/error_email.html',
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            //route for tender_registration
            .state('new-tender-start', {
                url: "/new-tender-start",
                templateUrl: '/res/js/moduls/tender_registration/ten_reg_start_page.html',
                controller: 'tenderRegStartController',
                controllerAs: 'tCtrl',
                resolve: {
                    'RegService': function (tenderRegService) {
                        return tenderRegService.promisetypeObjects;
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.GUEST,USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('new_tender_objects', {
                url: "/new_tender_objects/:way",
                templateUrl: '/res/js/moduls/tender_registration/ten_reg_object_page.html',
                controller: 'tenderRegObjectController',
                controllerAs: 'tCtrl',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('common_tender_parameters', {
                url: "/common_tender_parameters",
                templateUrl: '/res/js/moduls/tender_registration/ten_reg_common_page.html',
                controller: 'tenderRegCommonController',
                controllerAs: 'tCtrl',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('end_new_tender', {
                url: "/end_new_tender",
                templateUrl: '/res/js/moduls/tender_registration/end_page.html',
                controller: 'endController',
                controllerAs: 'endContr',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            //route for user_page
            .state('user_page', {
                url: "/user_page",
                templateUrl: '/res/js/moduls/user_page/user_page.html',
                controller: 'mainUserController',
                controllerAs: 'uScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('user_page.tender_list', {
                url: "/tender_list/:ten",
                templateUrl: '/res/js/moduls/user_page/tender_list.html',
                controller: 'listController',
                controllerAs: 'lScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('user_page.settings', {
                url: "/settings",
                templateUrl: '/res/js/moduls/user_page/settings_menu.html',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('user_page.money', {
                url: "/money",
                templateUrl: '/res/js/moduls/user_page/money.html',
                controller: 'moneyController',
                controllerAs: 'mScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER]
                }
            })

            .state('user_page.money_transaction', {
                url: "/money_transaction",
                templateUrl: '/res/js/moduls/user_page/money_transaction.html',
                controller: 'moneyController',
                controllerAs: 'mScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER]
                }
            })
            .state('user_page.provider_info', {
                url: "/provider_info",
                templateUrl: '/res/js/moduls/user_page/provider_info.html',
                controller: 'providerInfoController',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER]
                }
            })


            .state('user_page.user_delete', {
                url: "/user_delete",
                templateUrl: '/res/js/moduls/user_page/user_delete.html',
                controller: 'settingsController',
                controllerAs: 'stScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('user_page.password_change', {
                url: "/password_change",
                templateUrl: '/res/js/moduls/user_page/change_pass.html',
                controller: 'settingsController',
                controllerAs: 'stScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('user_page.mail_setting', {
                url: "/mail_setting",
                templateUrl: '/res/js/moduls/user_page/mail_settings.html',
                controller: 'settingsController',
                controllerAs: 'stScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER]
                }
            })
            .state('user_page.provider_filter', {
                url: "/provider_filter",
                templateUrl: '/res/js/moduls/user_page/filter_settings.html',
                controller: 'providerFilterController',
                controllerAs: 'stScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER]
                }
            })
            .state('user_page.region_settings', {
                url: "/region_settings",
                templateUrl: '/res/js/moduls/user_page/region_settings.html',
                controller: 'settingsController',
                controllerAs: 'stScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER]
                }
            })




            //route for one tender
            .state('tender', {
                url: "/tender/:tender_id",
                templateUrl: '/res/js/moduls/tender/tender_template.html',
                controller: 'tenderController',
                controllerAs:'tScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('edit_tender', {
                url: "/edit_tender/:tender_id",
                templateUrl: '/res/js/moduls/tender_edit/tender_edit_template.html',
                controller: 'tenderEditController',
                controllerAs:'tScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })


            .state('cancel_tender', {
                url: "/cancel_tender/:tender_id",
                templateUrl: '/res/js/moduls/tender/cancel_tender.html',
                controller: 'cancelTenderController',
                controllerAs:'tScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
             //todo поменять имена скопов
            .state('show_offer', {
                url: "/show_offer/:tender_id/:offer_id",
                templateUrl: '/res/js/moduls/tender/show_offer.html',
                controller: 'showOfferController',
                controllerAs:'tScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

            .state('cons_change_provider', {
                url: "/cons_change_provider/:tender_id",
                templateUrl: '/res/js/moduls/tender/cons_change_provider.html',
                controller: 'changerProviderController',
                controllerAs:'tScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('cons_over_tender', {
                url: "/cons_over_tender/:tender_id",
                templateUrl: '/res/js/moduls/tender/cons_over_tender.html',
                controller: 'consumerOverTenderController',
                controllerAs:'tScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })
            .state('provider_over_tender', {
                url: "/provider_over_tender/:tender_id",
                templateUrl: '/res/js/moduls/tender/provider_over_tender.html',
                controller: 'providerOverTenderController',
                controllerAs:'tScope',
                data: {
                    requireLogin: true
                }
            })

            .state('submit_offer', {
                url: "/submit_offer/:tender_id",
                templateUrl: '/res/js/moduls/tender/submit_offer.html',
                controller: 'submitOfferController',
                controllerAs:'tScope',
                data: {
                    authorizedRoles: [USER_ROLES.ADMIN,USER_ROLES.PROVIDER,USER_ROLES.CONSUMER]
                }
            })

        ///admin_page
            .state('admin', {
                url: "/admin",
                templateUrl: '/res/js/moduls/admin/admin.html',
                data: {
                    authorizedRoles: USER_ROLES.ADMIN
                }
            })
            .state('admin.providers', {
                url: "/providers",
                templateUrl: '/res/js/moduls/admin/providers.html',
                controller: 'infoUsersController',
                controllerAs: 'iU'
            })
            .state('admin.tenders', {
                url: "/tenders",
                templateUrl: '/res/js/moduls/admin/tenders.html',
                controller: 'tendersController',
                controllerAs: 'iU'
            })
            .state('admin.consumers', {
                url: "/consumers",
                templateUrl: '/res/js/moduls/admin/consumers.html',
                controller: 'infoUsersController',
                controllerAs: 'iU'
            });



    }]);

    app.factory('401Interceptor', ['$q','$timeout','$injector',
        function ($q,$timeout,$injector) {

            var LoginHandler, $http, $state, UserSession;

            // this trick must be done so that we don't receive
            // `Uncaught Error: [$injector:cdep] Circular dependency found`
            $timeout(function () {
                LoginHandler = $injector.get('LoginHandler');
                $http = $injector.get('$http');
                $state = $injector.get('$state');
                UserSession = $injector.get('UserSession');
            });

            return {
                responseError: function (rejection) {

                    if (rejection.status !== 401) {
                        return $q.reject(rejection);
                    }

                    if(rejection.config.url == '/login' && rejection.config.method == 'POST' && rejection.status === 401){
                        return $q.reject(rejection);
                    }

                    var stateName = $state.current.name;
                    var stateParams = $state.params;

                    if(UserSession.User().isAuthenticated){
                        UserSession.sessionReset();
                        UserSession.sessionExpired();
                    }

                    event.preventDefault();
                    LoginHandler.show().then(function () {
                        return $state.go(stateName, stateParams);
                    })
                    .catch(function () {
                        return $state.go('home');
                    });

                    return $q.reject(rejection);
                }
            };
        }]);

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('401Interceptor');
    }]);



    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.withCredentials = true; //Чтобы браузер передал вместе с запросом куки и HTTP-авторизацию, нужно поставить запросу xhr.withCredentials = true:
        $httpProvider.defaults.xsrfCookieName = 'CSRF-TOKEN'; // The name of the cookie sent by the server
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRF-TOKEN'; // The default header name picked up by Spring Security

    }]);

    app.run(['LoginHandler','$rootScope','$state','UserSession', function (LoginHandler, $rootScope, $state, UserSession) {

        var toStateName;
        var toStateParams;

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {




           //получаем необходимые роли
           var authorizedRoles = toState.data.authorizedRoles;


           if (!UserSession.isAuthorized(authorizedRoles)){
               toStateName = toState.name;
               toStateParams = toParams;
               //попытка перехода на защищенный ресурс
               event.preventDefault();
               LoginHandler.show().then(function () {
                   return $state.go(toStateName, toStateParams);
               })
                   .catch(function () {
                       UserSession.sessionReset();
                       return $state.go('home');
               });
            }





        });

    }]);

