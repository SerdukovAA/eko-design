/**
 * Created by serdukov on 01.12.15.
 */
angular.module('app').factory('userPageService', ['$http', function($http) {


    var User = {};

    var ProviderBalance = {};



    var UserParameters = {};


    var RegionCollection = {};




    //это список тендеров который подходят для данного провайдера, регулируются через роутинг параметрами в запросе
    //будет массив с номерами
    var TendersForProvider = new Array();
    var TendersMyCat = new Array();
    //пока не принимает параметры но будет принимать, параметры постраничного отображения

    var TendersWinForProvider = new Array();
    //пока не принимает параметры но будет принимать, параметры постраничного отображения

    var TendersWithOfferForProvider = new Array();


    var TendersForConsumerOwner = new Array();

    var TendersMy = new Array();
    var TendersMyActual = new Array();
    var TendersMyEnded = new Array();

    //пока не принимает параметры но будет принимать, параметры постраничного отображения


    var TypeObjectsCollection = new Array();

    return {
         promiseUserInfo: function(){
           return $http.get("/info_get_user_info")
                    .success(function (user) {
                        User = user;
                    });
        },

        promiseTendersForProvider:
            function(){
                 return $http.get("/provider_tenders_for_provider")
                    .success(function (tenders_for_provider) {
                        TendersForProvider = tenders_for_provider;
                        TendersMyCat = [];
                        angular.forEach(TendersForProvider, function(tender, key) {

                            if (tender.tenderStatus.id == 2 || tender.tenderStatus.id == 3 || tender.tenderStatus.id == 4) {
                                TendersMyCat.push(tender);
                            }

                        });


                    });
        } ,

        promiseTendersWinForProvider: function(){

                return $http.get("/provider_tenders_win")
                .success(function (tenders_win_for_provider) {
                    TendersWinForProvider = tenders_win_for_provider;
                });

        },




        promiseTendersWithOfferForProvider:function(){

                 return $http.get("/provider_tenders_with_offer")
                    .success(function (tenders_with_offer_for_provider) {
                        TendersWithOfferForProvider = tenders_with_offer_for_provider;
                    });

        },



        promiseProviderBalance:function(){

                 return $http.get("/provider_get_balance")
                    .success(function (balance) {
                        ProviderBalance = balance;

                    });

        },




        promiseUserParameters: function(){

                return $http.get("/provider_get_parameter_values")
                    .success(function (settings) {
                        UserParameters = settings;

                    });

        },



        promiseRegionCollection: function(){
            return $http.get("/info_get_all_regions")
                .success(function (regions) {
                    RegionCollection = regions;
                });
        },

        promiseTendersForConsumerOwner: function(){
                return $http.get("/consumer_tenders_for_user_owner")
                    .success(function (consumer_tenders_for_user_owner) {
                        TendersForConsumerOwner = consumer_tenders_for_user_owner;

                        TendersMy = [];
                        TendersMyActual = [];
                        TendersMyEnded = [];

                    angular.forEach(TendersForConsumerOwner, function(tender, key) {

                            if (tender.tenderStatus.id == 2 || tender.tenderStatus.id == 3 || tender.tenderStatus.id == 4){
                                TendersMy.push(tender);
                            }

                            if ((tender.tenderStatus.id == 2 || tender.tenderStatus.id == 3) && tender.ownerCloseTender==false){
                                TendersMyActual.push(tender);
                            }

                            if (tender.tenderStatus.id == 4 || tender.ownerCloseTender==true ){
                                TendersMyEnded.push(tender);
                            }

                        });

                    });


        },


        promiseUpdateTypeCollections: function() {
            return $http.get("/info_get_all_type_objects")
                .success(function (all_type_objects) {
                    TypeObjectsCollection = all_type_objects;
                });
        },

        getUserInfo : function(){
            return User;
        },
        getTendersMyCat : function(){
        return TendersMyCat;
        },
        getTendersWinForProvider : function(){
        return TendersWinForProvider;
        },
        getTendersWithOfferForProvider : function(){
            return TendersWithOfferForProvider;
        },
        getTendersMy : function(){
            return TendersMy;
        },
        getTendersMyActual : function(){
            return TendersMyActual;
        },
        getTendersMyEnded : function(){
            return TendersMyEnded;
        },
        getTypeObjectsCollection : function(){
            return TypeObjectsCollection;
        },
        getProviderBalance : function(){
            return ProviderBalance;
        },
        getUserParameters : function(){
            return UserParameters;
        },
        getRegionCollection : function(){
            return RegionCollection;
        }

    }

}]);
