/**
 * Created by serdukov on 24.02.16.
 */
angular.module('app').directive('scrollToMe',['$anchorScroll', '$location', function ($anchorScroll, $location) {
    return {
        restrict: 'A',
        scope: {
            scrollToMe: '@'
        },
        link: function(scope, $elm) {
            $location.hash(scope.scrollToMe);
            $anchorScroll();
        }
    }
}]);

angular.module('app')
    .filter('trustedHTML', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
}]);



angular.module('app').directive("errorbox",['$rootScope','$timeout',function($rootScope, $timeout) {
    return {
        template: '<div class="alert_fon"></div><div class="custom_alert alert alert-{{error.status}} text-center col-xs-12 col-sm-4" role="alert"  ng-bind-html="error.text | trustedHTML"></div></div>',
        scope: {
            error: '=error'
        },
        link: function (scope, element) {
            $rootScope.$on('errorAlertMessage', function(event,error){
                scope.error = error;
                element.css({display:'block'});
                element.css({opacity:'1'});
                $timeout(function(){
                    element.css({opacity:'0'});
                    $timeout(function(){
                        element.css({display:'none'});
                    }, 800);
                }, 5000);

            });
        }
    };
}]);


angular.module('app').directive("messagebox",['$rootScope','$timeout',function($rootScope, $timeout) {
    return {
        template: '<div ng-show="message.show"><div class="message_fon" ng-click="message.show = false"></div><div class="custom_message text-center col-xs-12 col-sm-4" role="alert">' +
        '<button type="button" class="close close-message" ng-click="message.show = false" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{message.text}}</div></div></div>',
        scope: {
            message: '=message'
        },
        link: function (scope, element) {
            $rootScope.$on('commentMessage', function(event,message){
                scope.message = message;
                scope.message.show=true;
                $timeout(function(){
                    scope.message.show=false;
                }, 35000);
            });
        }
    };
}]);



angular.module('app').filter('letername', function() {
    return function(user_id, tender_id) {
        var k = parseInt(user_id);
        var t = parseInt(tender_id);
        var result = '';
        var words = 'АБВГДЕЖИКЛМНОПРСТ';
        for (var i = 1; i < 6; ++i) {
            var position = (i * k * t * 14) % 17;
            result = result + words.substring(position, position + 1);
        }
        return result;
    };
});


angular.module('app').filter('moneyr', function() {
    return function(number) {
        if(number == undefined) return;
        var str = ""+number;
        return str.replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ");
    };
});






//проверка даты на валидность
angular.module('app').directive('datevalidation', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$validators.datevalidation = function (modelValue, viewValue) {
                var date = viewValue || modelValue;
                if(date === undefined) return;
                var d_arr = date.split('.');
                var d = new Date(d_arr[2]+'/'+d_arr[1]+'/'+d_arr[0]+''); // дата в формате 2014/12/31
                if (d_arr[2]!=d.getFullYear()||d_arr[1]!= d.getMonth()+ 1|| d_arr[0]!=d.getDate())
                {
                    return false; // неккоректная дата
                };
                return true;
            };
            scope.$watch('datevalidation', function(newVal, oldVal) {
                ngModel.$validate();
            });
        }
    };
});

///--------------------------------Общие дерективы модуля-----------------------------------------------

//проверка логина на уникальность
angular.module('app').directive('uniqlogin', function($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.bind('blur', function (e) {

                if(!ngModel.$modelValue) return;
                $http.get("/info_check_login", {
                    params: {login: ngModel.$modelValue}
                }).success(function (data) {
                    if (data) {
                        if (data.status != '3013') {
                            ngModel.$setValidity('uniqlogin', false);
                        } else {
                            ngModel.$setValidity('uniqlogin', true);
                        }
                    }
                });

            });
        }
    };
});



//проверка логина на уникальность
angular.module('app').directive('uniqeditlogin', function($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.bind('blur', function (e) {

                if(!ngModel.$modelValue) return;
                $http.get("/info_edit_check_login", {
                    params: {login: ngModel.$modelValue}
                }).success(function (data) {
                    if (data) {
                        if (data.status != '3013') {
                            ngModel.$setValidity('uniqeditlogin', false);
                        } else {
                            ngModel.$setValidity('uniqeditlogin', true);
                        }
                    }
                });

            });
        }
    };
});





//проверка емайла на уникальность

angular.module('app').directive('uniqemail', function($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.bind('blur', function (e) {

                if(!ngModel.$modelValue) return;
                $http.get("/info_check_email", {
                    params: {email: ngModel.$modelValue}
                }).success(function (data) {
                    if (data) {
                        if (data.status != '3014') {
                            ngModel.$setValidity('uniqemail', false);
                        } else {
                            ngModel.$setValidity('uniqemail', true);
                        }
                    }
                });

            });
        }
    };
});


angular.module('app').directive('uniqeditemail', function($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.bind('blur', function (e) {

                if(!ngModel.$modelValue) return;
                $http.get("/info_edit_check_email", {
                    params: {email: ngModel.$modelValue}
                }).success(function (data) {
                    if (data) {
                        if (data.status != '3014') {
                            ngModel.$setValidity('uniqeditemail', false);
                        } else {
                            ngModel.$setValidity('uniqeditemail', true);
                        }
                    }
                });

            });
        }
    };
});


//проверка номера телефона науникальность
angular.module('app').directive('uniqphone', function($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.bind('blur', function (e) {
                if(!ngModel.$modelValue) return;
                $http.get("/info_check_phone", {
                    params: {phone: ngModel.$modelValue}
                }).success(function (data) {
                    if (data) {
                        if (data.status != '3014') {
                            ngModel.$setValidity('uniqphone', false);
                        } else {
                            ngModel.$setValidity('uniqphone', true);
                        }
                    }
                });

            });
        }
    };
});


angular.module('app').directive('uniqeditphone', function($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.bind('blur', function (e) {
                if(!ngModel.$modelValue) return;
                $http.get("/info_edit_check_phone", {
                    params: {phone: ngModel.$modelValue}
                }).success(function (data) {
                    if (data) {
                        if (data.status != '3014') {
                            ngModel.$setValidity('uniqeditphone', false);
                        } else {
                            ngModel.$setValidity('uniqeditphone', true);
                        }
                    }
                });

            });
        }
    };
});


//проверка номера ИНН науникальность
angular.module('app').directive('uniqinn', function($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.bind('blur', function (e) {
                if(!ngModel.$modelValue) return;
                $http.get("/info_check_inn", {
                    params: {inn: ngModel.$modelValue}
                }).success(function (data) {
                    if (data) {
                        if (data.status != '3015') {
                            ngModel.$setValidity('uniqinn', false);
                        } else {
                            ngModel.$setValidity('uniqinn', true);
                        }
                    }
                });

            });
        }
    };
});


angular.module('app').directive('uniqeeditinn', function($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.bind('blur', function (e) {
                if(!ngModel.$modelValue) return;
                $http.get("/info_edit_check_inn", {
                    params: {inn: ngModel.$modelValue}
                }).success(function (data) {
                    if (data) {
                        if (data.status != '3015') {
                            ngModel.$setValidity('uniqeeditinn', false);
                        } else {
                            ngModel.$setValidity('uniqeeditinn', true);
                        }
                    }
                });

            });
        }
    };
});



//проврека проверочного пароля на совпадение с основным
angular.module('app').directive('equalspas', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            equalspas: '='
        },
        link: function (scope, elem, attrs, ctrl) {


            if(!ctrl) return;

            ctrl.$validators.equalspas = function(modelValue, viewValue) {
                return modelValue === scope.equalspas;
            };

            scope.$watch('equalspas', function(newVal, oldVal) {
                ctrl.$validate();
            });


        }
    };
});

//проверка что чекбокс отмечен
angular.module('app').directive('checkRequired', function(){
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$validators.checkRequired = function (modelValue, viewValue) {
                return modelValue || viewValue;
            };
            scope.$watch('checkRequired', function(newVal, oldVal) {
                ngModel.$validate();
            });
        }
    };
});


angular.module('app').directive('phonemask', ['$filter', function($filter) {
   return {
        link: function (scope, element, attributes, ngModelCtrl) {
            if(!ngModelCtrl) return;

            scope.$watch(attributes.ngModel, function(newValue) {
                newValue = String(newValue);
                var number = newValue.replace(/[^0-9]+/g, '');
                var viewValue = $filter('phonenumber')(number);

                ngModelCtrl.$viewValue = viewValue;
                ngModelCtrl.$render();
             });

            element.bind('blur', function (e) {
                if(!ngModelCtrl.$modelValue) return;

                var newValue = String(ngModelCtrl.$modelValue);
                var number = newValue.replace(/[^0-9]+/g, '');
                var viewValue = $filter('phonenumber')(number);
                ngModelCtrl.$viewValue = viewValue;
                ngModelCtrl.$render();
            });
        },
        require: 'ngModel',
        restrict: 'A'
    };
}])




angular.module('app').filter('phonenumber', function() {

        return function (number) {
               if (!number) { return ''; }

            number = String(number);
          var formattedNumber = number;

            var c = (number[0] == '1') ? '1 ' : '';
            number = number[0] == '1' ? number.slice(1) : number;

            // # (###) ###-#### as c (area) front-end
            var area = number.substring(0,3);
            var front = number.substring(3, 6);
            var end = number.substring(6, 10);

            if (front) {
                formattedNumber = (c + "(" + area + ") " + front);
            }
            if (end) {
                formattedNumber += ("-" + end);
            }
            return formattedNumber;
        };
    });


angular.module('app').directive('requiredOne', function($timeout){
    return {
        scope: {
            catSelector: '=requiredOne'
        },
        restrict: 'A',
        link: function (scope, element, attrs, ngModel) {

            var initializing = true;

            function validate(newVal, oldVal){
                if(newVal.consumerSelectorType == 'radio'){
                    if (angular.isUndefined(scope.catSelector.checkedOptionId) || scope.catSelector.checkedOptionId == null|| scope.catSelector.checkedOptionId == '0') {
                        scope.catSelector.invalid = true;
                        return;
                    }else{
                        scope.catSelector.invalid = false;
                        return;
                    }
                }
                if(newVal.consumerSelectorType == 'checkbox'){

                    var options  = newVal.selectorOptions;
                    for (var i = 0, len = options.length; i < len; i++) {
                        if(options[i].checked === undefined) continue;
                        if(options[i].checked === true){
                            scope.catSelector.invalid = false;
                            return;
                        }
                    }

                    scope.catSelector.invalid = true;
                    return;
                }
            };



            scope.$on('secondBlockTry', function(){
                validate(scope.catSelector, scope.catSelector);
            });

            scope.$watch('catSelector', function (newVal, oldVal) {
                if (initializing) {
                    $timeout(function() { initializing = false; });
                } else {
                    validate(newVal, oldVal);
                }
            }, true);



        }
    };
});

angular.module('app').directive("filer",['$http',function ($http) {
    return {

        scope: {
            objectFotos: '=filer'
        },

        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function() {
                    // scope.tenderObject.fotoInput = new Array();
                    var files = changeEvent.target.files;
                    if(files == null) return false;
                    var formData = new FormData();
                    for(var i=0,len = files.length; i<len; i++ ){
                        formData.append('objectFotos', files[i]);
                    }


                    $http.post("/upload_foto", formData, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).then(function (response) {

                        if(response.status !== 200) return;
                        var listUrls = response.data;
                        angular.forEach(listUrls, function(value, key) {
                            scope.objectFotos.push({
                                fotoLink: value,
                                fotoMinLink: key
                            });
                            scope.objectFotos.small = false;
                        });

                    });

                });
            });
        }
    }
}]);


angular.module('app').directive("addfiler",['$http',function ($http) {
    return {

        scope: {
            tenderFileList: '=addfiler'
        },

        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function() {

                    var files = changeEvent.target.files;
                    if(files == null) return false;
                    var formData = new FormData();
                    for(var i=0,len = files.length; i<len; i++ ){
                        formData.append('additionalFiles', files[i]);
                    }


                    $http.post("/upload_file", formData, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).then(function (response) {


                        if(response.status !== 200) return;

                        var mapUrls = response.data;
                        angular.forEach(mapUrls, function(value, key) {
                            scope.tenderFileList.push({
                                fileLink: value,
                                originName: key
                            });
                        });
                        scope.tenderFileList.small = false;

                    });

                });
            });
        }
    }
}]);



angular.module('app').directive('locationset',['$timeout', '$http', '$q', function($timeout, $http, $q) {
    return {
        restrict: 'A',
        scope: {
            maplink:'=maplink',
            adressparent: '=adressparent',
            callback:'=callback'
        },

        templateUrl: '/res/js/moduls/templates/locationset.html',
        link: function (scope, element, attrs, ngModel) {

            var geocoder;
            var deferred;

            $timeout(function(){
                geocoder = new google.maps.Geocoder();
                scope.maplink = new google.maps.Map(document.getElementById('googleMap'), {zoom: 8});
                scope.maplink.setCenter(new google.maps.LatLng(55.7522, 37.6155)); //Москва
                google.maps.event.addListenerOnce(scope.maplink, 'idle', function(){
                    if(typeof(scope.callback) == "function"){
                        scope.callback();
                    }
                });
                google.maps.event.trigger(scope.maplink, "resize");
            });


            scope.getLocation = function(val) {
               deferred = $q.defer();
               geocoder.geocode({'address': val}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        deferred.resolve(results);
                    }else{
                        deferred.reject();
                    }
                });
               return deferred.promise;
            };


            scope.onSelect = function ($item) {

                scope.adressparent.adress = {};
                scope.adressparent.adress.fullAdress = $item.formatted_address;
                scope.adressparent.adress.latitude = $item.geometry.location.lat();
                scope.adressparent.adress.longitude = $item.geometry.location.lng();

                angular.forEach($item.address_components, function(value, key) {

                    if(value.types[0] == "route"){
                        scope.adressparent.adress.street = value.short_name
                    }

                    if(value.types[0] == "locality"){
                        scope.adressparent.adress.city = value.short_name
                    }


                    if(value.types[0] == "administrative_area_level_1"){
                        scope.adressparent.adress.region = value.short_name
                    }

                    if(value.types[0] == "administrative_area_level_2"){
                        scope.adressparent.adress.rayon = value.short_name
                    }

                    if(value.types[0] == "street_number"){
                        scope.adressparent.adress.houseNumber = value.short_name
                    }
                });

                if(scope.maplink.marker) {
                    scope.maplink.marker.setMap(null);
                    scope.maplink.marker = null;
                }


                scope.maplink.setCenter($item.geometry.location);

                scope.maplink.marker = new google.maps.Marker({
                    map: scope.maplink,
                    position: $item.geometry.location
                });
                google.maps.event.trigger(scope.maplink, "resize");

            };

        }
    };
}]);



angular.module('app').directive('preferregion',['$timeout', '$http', '$q', function($timeout, $http, $q) {
    return {
        restrict: 'A',
        scope: {
            adressparent: '=adressparent',
            callback:'=callback'
        },

        templateUrl: '/res/js/moduls/templates/preferregion.html',
        link: function (scope, element, attrs, ngModel) {


            var deferred;

            var geocoder = new google.maps.Geocoder();

            scope.getLocation = function(val) {
                deferred = $q.defer();
                geocoder.geocode({'address': val}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        deferred.resolve(results);
                    }else{
                        deferred.reject();
                    }
                });
                return deferred.promise;
            };


            scope.onSelect = function ($item) {
                angular.forEach($item.address_components, function(value, key) {

                    if(value.types[0] == "locality"){
                        scope.adressparent.cityCode = value.short_name
                    }

                    if(value.types[0] == "administrative_area_level_1"){
                        scope.adressparent.regionCode = value.short_name
                    }

                    if(value.types[0] == "administrative_area_level_2"){
                        scope.adressparent.rayonCode = value.short_name
                    }

                });

            };

        }
    };
}]);
