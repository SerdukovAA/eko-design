/**
 * Created by serdukov on 19.11.15.
 */
angular.module('app').controller('tenderRegObjectController', ['$window','$scope', '$http', '$timeout', '$location', '$anchorScroll',
    'tenderRegService', '$stateParams',
    function ($window, $scope, $http, $timeout, $location, $anchorScroll, tenderRegService, $stateParams) {


        var tCtrl = this;

        tCtrl.token = {};

        tCtrl.TenderFormObject = tenderRegService.getTender();
        if(tCtrl.TenderFormObject.typeService == undefined || tCtrl.TenderFormObject.typeService.categorySelectors == undefined) $location.path("/new-tender-start");



if($stateParams.way == 'forward'){

    tenderRegService.updateParametersObject().then(function () {
        tCtrl.TenderFormObject.tenderObjects=new Array();
        tCtrl.TenderFormObject.tenderObjects.push({
            objectFotos: new Array(),
            parameterRelations: angular.copy(tCtrl.TenderFormObject.ParameterRelations)
        });
        tenderRegService.updateInputCoolections();
        tCtrl.InputCatalogCollections = tenderRegService.getInputCatalogCollections();
        tCtrl.RefferalCatalog = tenderRegService.getRefferalCatalog;



    });
}
if($stateParams.way == 'back'){
//ничего
    tCtrl.InputCatalogCollections = tenderRegService.getInputCatalogCollections();
    tCtrl.RefferalCatalog = tenderRegService.getRefferalCatalog;

}




//todo если возвращаемся назад то нужно сохранить существубщие объекты
        //если переходим на вторую то нужно обновить только параметры объекты



        tCtrl.thirdBlockTry = function (ngForm) {
            ngForm.$submitted = true;
            for(var i=0,len = tCtrl.TenderFormObject.tenderObjects.length; i<len; i++ ){
                if(tCtrl.TenderFormObject.tenderObjects[i].objectFotos.length < 1){
                    tCtrl.TenderFormObject.tenderObjects[i].objectFotos.small = true;
                    $scope.$broadcast('errorAlertMessage',{text:"Необходимо передать хотя бы одно фото для объекта", status: "warning"} );
                    return false;
                }
            }
            if (ngForm.$valid) {
                tenderRegService.setTender(tCtrl.TenderFormObject);
                $location.path('/common_tender_parameters');
            }
        };


        tCtrl.removeObject = function (index) {
            if (tCtrl.TenderFormObject.tenderObjects.length > 1) {
                tCtrl.TenderFormObject.tenderObjects.splice(index, 1);
            } else {
                $scope.$emit('errorAlertMessage',{text:"Необходимо передать информацию хотя бы по одному объекту тендера", status: "warning"});
            }
        };


        tCtrl.removeFoto = function(objectFotos,index){
            if (objectFotos.length > 1) {
                objectFotos.splice(index, 1);
            } else {
                $scope.$emit('errorAlertMessage', {text:"Необходимо передать хотя бы по одно фото для объекта", status: "warning"});
            }
        };




        tCtrl.addObject = function () {
            tCtrl.TenderFormObject.tenderObjects.push({
                objectFotos: new Array(),
                parameterRelations: angular.copy(tCtrl.TenderFormObject.ParameterRelations)
            });
        };

        tCtrl.years = function (start) {
            var nowDate = new Date();
            var year = nowDate.getFullYear();
            var result = [];

            for (var i = year; i > start; i--) {
                result.push(i);
            }

            return result;
        }


        tCtrl.showInputFoto = function (id) {
            $(id).click();
            //angular.element(document.querySelector(id))[0].click();
        }

        tCtrl.sendFotoFiles = function (input) {

            var files = input.files;
            if (files == null) return false;
            var formData = new FormData();
            for (var i = 0, len = files.length; i < len; i++) {
                formData.append('objectFotos', files[i]);
            }

        }


    }]);

