angular.module('app').controller('tendersController', ['adminService', function( adminService) {




    var iU = this;

    iU.TendersCollection = {};

    iU.Pagination = {};
    iU.Pagination.maxSize = 10;
    iU.Pagination.page_num = 1;

    adminService.tendersCount().then(function(http){
        iU.Pagination.TotalTenders =  http.data;
    });


    var refreshCollections = function (){
        adminService.getTendersCollection(iU.Pagination.page_num).then(function(http){
            iU.TendersCollection = http.data;
        });
    };


    refreshCollections();

    iU.pageChanged = function() {
        refreshCollections();
    };




}]);
/**
 * Created by serdukov on 22.03.16.
 */
