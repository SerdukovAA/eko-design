/**
 * Created by serdukov on 01.12.15.
 */
angular.module('app').factory('registrationService', function() {

    var Consumer = {
        phoneCheckID:"##",
        recaptchaResponse:"##"
    };

    var Provider =  {
        recaptchaResponse:"##"
    };;



    return {
        getConsumer : function(){
            return Consumer;
        },
        getProvider : function(){
            return Provider;
        }
    }

});
