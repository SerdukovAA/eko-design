angular.module('app').factory('UserSession',  ['USER_ROLES','$http','$rootScope',
function(USER_ROLES, $http,$rootScope) {
//объект хранит информацию о текущем пользователе


    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    $rootScope.Personal = {
        InText:"Войти",
        showReg: true,
        showLogout: false,
        isRegClass: "marginLeft38"
    };

    var User = {
        isAuthenticated : false,
        userRole : USER_ROLES.GUEST
    };

    return {

        User: function(){
            return User;
        },

        isAuthorized:function (authorizedRoles) {
          if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
          }

            var dd = authorizedRoles.indexOf(USER_ROLES.GUEST);
            var check1 = authorizedRoles.indexOf(USER_ROLES.GUEST) !== -1;


          return  check1 || (User.isAuthenticated && authorizedRoles.indexOf(User.userRole) !== -1);
        },
        sessionExpired : function(){
            $rootScope.$broadcast('errorAlertMessage', {text: "Ваша сессия изтекла", status: "danger"});
        },
        sessionReset: function(){
            User = {
                Row : {},
                isAuthenticated : false,
                userRole : USER_ROLES.GUEST
            };
            $rootScope.Personal = {
                InText:"Войти",
                showReg: true,
                showLogout: false,
                isRegClass: "marginLeft38"
            };
            delete getCookie('JSESSIONID');
            console.info('The user has been logged out!');
        },
        getUserInfo: function(callback){
                return $http.get("/info_get_user_info")
                    .success(function (user) {
                        //заполняем инфу по пользователю
                        User.Row = user;
                        User.isAuthenticated = true;
                        User.userRole = user.userRole.user_role;
                        User.isJuridical = user.juridical;
                        $rootScope.Personal = {
                            InText:"Личное меню",
                            showReg: false,
                            showLogout: true,
                            isRegClass: ""
                        };
                        $rootScope.$broadcast('errorAlertMessage', {text: "Вы удачно авторизированны", status: "success"});
                        if(typeof callback === 'function'){
                            callback();
                        }
                    });
        }

    }
}]);



app.constant('USER_ROLES', {
    GUEST: 'GUEST',
    CONSUMER: 'USER_CONSUMER',
    PROVIDER: 'USER_PROVIDER',
    ADMIN: 'ADMIN'


});
