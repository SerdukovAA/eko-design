/**
 * Created by serdukov on 19.11.15.
 */
angular.module('app').controller('endController',['$scope', '$location', '$anchorScroll', 'serverMessageService','tenderRegService',
    function($scope, $location, $anchorScroll, serverMessageService, tenderRegService) {

    var tCtrl = {};

    tCtrl.TenderFormObject = tenderRegService.getTender();

    $scope.serverMessage = serverMessageService.getServerMessage();

}]);
