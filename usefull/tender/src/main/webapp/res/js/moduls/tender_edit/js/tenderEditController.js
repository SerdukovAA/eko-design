/**
 * Created by serdukov on 05.01.16.
 */
angular.module('app').controller('tenderEditController', ['$compile','$scope','$http','$timeout', '$location','$anchorScroll',
    'tenderEditService','$filter', '$rootScope',
    function($compile, $scope, $http, $timeout, $location, $anchorScroll, tenderEditService , $filter, $rootScope) {


        var tScope = this;



        tScope.initParameter = function(parameterRelation, tenderObject){

            var objectParameterValues = tenderObject.objectParameterValues;

            if(parameterRelation.objectParameter.fieldType == 'range' && parameterRelation.objectParameter.varType == 'date' && parameterRelation.objectParameter.stopRange == null){
                objectParameterValues.forEach(function (item1, index, array) {
                    if(item1.parameterId == parameterRelation.objectParameter.id) {
                        parameterRelation.objectParameter.value = parseInt(item1.parameterValue);
                    }
                });
            }

            if(parameterRelation.objectParameter.fieldType == 'input' && parameterRelation.objectParameter.varType =='text'){
                objectParameterValues.forEach(function (item2, index, array) {
                    if(item2.parameterId == parameterRelation.objectParameter.id) {
                        parameterRelation.objectParameter.value = item2.parameterValue;
                    }
                });
            }


            if(parameterRelation.objectParameter.varType == 'catalog' && parameterRelation.objectParameter.ref == false) {
                objectParameterValues.forEach(function (item3, index, array) {
                    if(item3.parameterId == parameterRelation.objectParameter.id) {
                        tScope.InputCatalogCollections[parameterRelation.objectParameter.parameterIdent].forEach(function (it, index, array) {
                             if(it[parameterRelation.objectParameter.catalogNameField] == item3.parameterValue){
                                 parameterRelation.objectParameter.value = it;
                                 var id =  tenderObject.id;
                                 tScope.RefferalCatalog[tenderObject.id] = new Array();
                                 tScope.RefferalCatalog[tenderObject.id][parameterRelation.objectParameter.parameterIdent] = {};
                                 tScope.RefferalCatalog[tenderObject.id][parameterRelation.objectParameter.parameterIdent] = it;
                             }
                         });
                    }
                });
            }

            if(parameterRelation.objectParameter.varType == 'catalog' && parameterRelation.objectParameter.ref == true) {
                objectParameterValues.forEach(function (item4, index, array) {
                    if(item4.parameterId == parameterRelation.objectParameter.id) {
                        var obj = {};
                        obj[parameterRelation.objectParameter.catalogNameField] = item4.parameterValue;
                        parameterRelation.objectParameter.value = obj;
                    }
                });
            }
        };


        tenderEditService.promiseTender().then(function() {
            tScope.Tender = tenderEditService.getTender();
            //прописываем даты
            var begin = new Date();
            begin.setTime(tScope.Tender.dateBegin);
            var end = new Date();
            end.setTime(tScope.Tender.dateEnd);
            tScope.dateBeginTender = begin;
            tScope.dateEndTender = end;


            tScope.Tender.adress = {
                fullAdress: tScope.Tender.fullAdress,
                latitude: tScope.Tender.tenderPlaceLatitude,
                longitude: tScope.Tender.tenderPlaceLongitude,
                region: tScope.Tender.regionCode,
                rayon: tScope.Tender.rayonCode,
                city: tScope.Tender.cityCode,
                street: tScope.Tender.street,
                houseNumber: tScope.Tender.houseNumber
            };






            generateSelectorValueList();

            tenderEditService.updateParametersObject().then(function () {

                tScope.ParameterRelations = tenderEditService.getParameterRelations();
                tScope.Tender.tenderObjects.forEach(function (item5, index, array) {
                    item5.parameterRelations = angular.copy(tScope.ParameterRelations);
                });


                tenderEditService.updateInputCoolections().then(function(){
                    tScope.InputCatalogCollections = tenderEditService.getInputCatalogCollections();
                    tScope.RefferalCatalog = tenderEditService.getRefferalCatalog();

                    tScope.Tender.tenderObjects.forEach(function (tenderObject, index, array) {
                        tenderObject.parameterRelations.forEach(function (parameterRelation, index, array) {
                            tScope.initParameter(parameterRelation, tenderObject);
                        });
                    });
                });


                tScope.Tender.typeService.commonParameters.forEach(function (commonParameter, index, array) {
                    tScope.Tender.commonValueList.forEach(function (comVal, index, array) {
                        if(comVal.commonId == commonParameter.id){
                            commonParameter.value = comVal.parameterValue;
                        }
                    });
                });


                tScope.setMapLang = function(){
                    //Показать адресс тендера в любом случае
                    tScope.Tender.tenderPositionG = new google.maps.LatLng(tScope.Tender.tenderPlaceLatitude, tScope.Tender.tenderPlaceLongitude);
                    tScope.TenderMap.marker = new google.maps.Marker({
                        position: tScope.Tender.tenderPositionG,
                        map: tScope.TenderMap,
                    });
                    tScope.TenderMap.setCenter(tScope.Tender.tenderPositionG);
                };




            });

        });

        tScope.showInputFile = function () {
            $('#addFileInput').click();
            //angular.element(document.querySelector('#addFileInput'))[0].click();
        };

        tScope.removeAddFile = function(index){
            if (tScope.Tender.tenderFileList.length > 1) {
                tScope.Tender.tenderFileList.splice(index, 1);
            } else {
                $scope.$broadcast('errorAlertMessage', {text:"Необходимо передать хотя бы  один дополнительный файл по тендеру", status: "warning"});
            }
        };

        var generateSelectorValueList = function () {

            var selectorValuesArray = new Array();
            var selectorValueList = tScope.Tender.selectorValueList;
            ///пройтись по каждому и передать собрать однотипные селекторы

            for (var i = 0, len = selectorValueList.length; i < len; i++) {
                var selectorId = selectorValueList[i].selectorId;

                if (typeof selectorValuesArray[selectorId] === 'undefined') {
                    var selectorV = {};
                    selectorV.name = selectorValueList[i].selectorName;
                    selectorV.values = new Array();
                    selectorV.values.push(selectorValueList[i].selectorValue);
                    selectorValuesArray[selectorId] = selectorV;
                }
                else {
                    selectorValuesArray[selectorId].values.push(selectorValueList[i].selectorValue);
                }
            }
            selectorValuesArray.forEach(function (item6, index, array) {
                renderSelectorValue(item6);
            });

            tScope.SelectorValueList = selectorValuesArray;
        };

        var renderSelectorValue = function (selectorV) {
            var values = " ";
            for (var v = 0, vlen = selectorV.values.length; v < vlen; v++) {
                values += selectorV.values[v];
                if (vlen > 1 && v < (vlen - 1)) {
                    values += ", ";
                }

            }
            selectorV.valuesHtml = values;
        };

        tScope.removeFoto = function(objectFotos,index){
            if (objectFotos.length > 1) {
                objectFotos.splice(index, 1);
            } else {
                $scope.$emit('errorAlertMessage', {text:"Необходимо передать хотя бы по одно фото для объекта", status: "warning"});
            }
        };

        tScope.showInputFoto = function (id) {
            $(id).click();
            //angular.element(document.querySelector(id)).click();
        }

        tScope.years = function (start) {
            var nowDate = new Date();
            var year = nowDate.getFullYear();
            var result = [];

            for (var i = year; i > start; i--) {
                result.push(i);
            }

            return result;
        }

        tScope.addObject = function(){
          tScope.Tender.tenderObjects.push({
                    objectFotos: new Array(),
                    parameterRelations: angular.copy(tScope.ParameterRelations)
          });
        }

        tScope.removeObject = function (index) {
            if (tScope.Tender.tenderObjects.length > 1) {
                tScope.Tender.tenderObjects.splice(index, 1);
            } else {
                $scope.$emit('errorAlertMessage',{text:"Необходимо передать информацию хотя бы по одному объекту тендера", status: "warning"});
            }
        };



        tScope.submitEditedTender =  function(editTenderForm){



            if(editTenderForm.$invalid) return false;

            //начинаем подоготовку тендера к отправке, многие поля нам просто не нужны. например справочники и т.п
            var EditTenderForm = {};

            EditTenderForm.id = tScope.Tender.id;
            EditTenderForm.tenderName = tScope.Tender.tenderName;

            EditTenderForm.tenderDescription = tScope.Tender.tenderDescription;
            EditTenderForm.typeObject = tScope.Tender.typeObject;
            EditTenderForm.typeService = tScope.Tender.typeService;

            EditTenderForm.adress = tScope.Tender.adress;

            EditTenderForm.dateBegin =  $filter('date')(tScope.dateBeginTender, "dd.MM.yyyy");
            EditTenderForm.dateEnd =  $filter('date')(tScope.dateEndTender, "dd.MM.yyyy");

            EditTenderForm.tenderFileList = tScope.Tender.tenderFileList;

            EditTenderForm.tenderFileList.forEach(function (file, index, array) {
                delete file.id;
            });


            EditTenderForm.needCredit = tScope.Tender.needCredit;
            EditTenderForm.expectedPrice = tScope.Tender.expectedPrice;

            EditTenderForm.commonValueList = new Array();



            var commonParameters =  tScope.Tender.typeService.commonParameters;
            for(var i= 0, len = commonParameters.length; i<len; i++) {

                if(commonParameters[i].varType =='text'){
                    EditTenderForm.commonValueList.push({
                        commonId: commonParameters[i].id,
                        parameterName: commonParameters[i].parameterName,
                        valueId: null,
                        parameterValue: commonParameters[i].value
                    });
                }
            }




            EditTenderForm.tenderObjects = tScope.Tender.tenderObjects;

            for (var i = 0; i < EditTenderForm.tenderObjects.length; i++) {
                var tenderObject = EditTenderForm.tenderObjects[i];

                delete tenderObject.id;

                tenderObject.objectFotos.forEach(function (foto, index, array) {
                    delete foto.id;
                });

                for (var p = 0; p < tenderObject.parameterRelations.length; p++) {
                    var parameterRelation = tenderObject.parameterRelations[p];
                    if(parameterRelation.objectParameter.varType == 'catalog'){
                        parameterRelation.objectParameter.value = parameterRelation.objectParameter.value[parameterRelation.objectParameter.catalogNameField];
                    }
                }

                tenderObject.objectParameterValues = new Array();

                var parRelations =  tenderObject.parameterRelations;
                for(var p = 0, pl=parRelations.length; p<pl ; p++ ){
                    tenderObject.objectParameterValues.push({
                        parameterId: parRelations[p].objectParameter.id,
                        parameterName: parRelations[p].objectParameter.parameterName,
                        valueId: null,
                        parameterValue: parRelations[p].objectParameter.value
                    });
                }


            }


            $http.post("/consumer_edit_tender", EditTenderForm)
                .then(function (http) {
                    var data = http.data;
                    if (data.status == '3000') {
                        editTenderForm.$invalid = true;
                        $scope.$emit('errorAlertMessage', {text: "Изменения успешно сохранены", status: "success"});
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function (val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {
                            text: "Не удалось сохранить изменения: " + html,
                            status: "warning"
                        });
                    }
                });






        }


    }]);

