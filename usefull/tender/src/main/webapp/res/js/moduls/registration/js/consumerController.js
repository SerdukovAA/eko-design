/**
 * Created by serdukov on 19.11.15.
 */


angular.module('app').controller('consumerController',['$scope', '$http', '$timeout', '$location', '$anchorScroll', 'serverMessageService'
    , 'registrationService',
    function($scope, $http, $timeout, $location, $anchorScroll, serverMessageService, registrationService) {

    $scope.phoncked = false;


    $scope.consumer = registrationService.getConsumer();


    $scope.captchaResponse = function(response){
        $scope.consumer.recaptchaResponse = response;
    };


    $scope.sendCheckCode = function (){

        if($scope.consumer.recaptchaResponse=='##'){
            $scope.$emit('errorAlertMessage', {text: "<p>Необходимо подтвердить, что вы не робот</p>", status: "warning"});
            return false;
        }

         var phoneNumber = $scope.consumer.phoneNumber;
        $http.get("/check_phone", {
            params: {phoneNumber: phoneNumber, captchaResponse: $scope.consumer.recaptchaResponse}
        }).success(function (data) {
            if (data.status == '3000') {
                $scope.consumer.phoneCheckID = data.phoneCheckID;
                $scope.sendCheckButtonBlock =true;
                $timeout(function() {
                    $scope.sendCheckButtonBlock =false;
                }, 10000);
                $scope.$emit('errorAlertMessage', {text:"Проверочный код выслан", status: "success"});
            } else {
                var html = "<p><b>" + data.text + "</b></p>";
                if (data.hasErrors = 'true') {
                    angular.forEach(data.errors, function(val, key) {
                        html += "<p>" + val.errorText + "</p>";
                    });
                }
                $scope.$emit('errorAlertMessage', {text: html, status: "warning"});
            }
        });
    }

    $scope.submitRegistrationConsumer = function (invalid) {
        if(invalid || $scope.consumer.phoneCheckID == "##") return false;
        $http.post("/registration_consumer", $scope.consumer)
            .success(function(serverMessage, status) {
                serverMessageService.setServerMessage(serverMessage)
                $location.path('/end_registration');
                $location.hash('end');
                $anchorScroll();
            });

    }


}]);

