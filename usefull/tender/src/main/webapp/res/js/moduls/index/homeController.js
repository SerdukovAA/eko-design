/**
 * Created by serdukov on 24.02.16.
 */
angular.module('app').controller('homeController', ['$scope', '$http' , '$interval', 'appParams','$rootScope', 'LoginService',
    '$location','UserSession','$state',
    function($scope, $http ,$interval, appParams, $rootScope, LoginService , $location, UserSession, $state) {

        var home = this;

    home.tender = {};


        $http.get(appParams.INFO_GET_COUNTER)
        .success(function (counter) {
            $scope.Counter = counter;
        });


    $http.get(appParams.INFO_GET_ALL_TYPE_OBJECTS)
        .success(function (typeObjects) {
            home.TypeObjectsCollection = typeObjects;
        });


    $http.get(appParams.INFO_GET_NEW_LAST_TENDERS)
        .success(function(tenders) {
            home.tenderList = tenders;
            $rootScope.dataLoaded = true;

        });



        home.logout = function() {
            UserSession.sessionReset();
            LoginService.logout(function (data, status, headers, config) {
                $location.url('/home');
            }, function(data, status, headers, config) {
                console.info('Something went wrong while trying to logout... ', data, status, headers, config);
            });
        };


        $scope.brod = function (){
        home.$broadcast('commentMessage', {text:'Возникли проблемы с определением местоположения'});
        }

        home.isCurrentPath = function (path) {
            if($location.path() == path) return true;
        };

    $rootScope.dataLoaded = true;

}]);
