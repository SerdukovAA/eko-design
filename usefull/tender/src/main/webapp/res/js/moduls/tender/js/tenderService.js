/**
 * Created by serdukov on 01.12.15.
 */
angular.module('app').factory('tenderService', ['$http','$stateParams',function($http, $stateParams) {

    var Tender = {};
    var RegionCollection = {};


    var promiseRegionCollection = $http.get("/info_get_all_regions")
        .success(function (regions) {
            RegionCollection = regions;
        });


    return {
        promiseTender:function(){
            return  $http({
                url: "/info_get_tender", method: "GET", params: {tender_id: $stateParams.tender_id}
            }).success(function (tender) {
                Tender = tender;
                $http({
                    url: "/provider_tender_winner_by_tender_id", method: "GET", params: {tender_id: Tender.id}
                }).success(function (userOvnerInfo) {
                    Tender.userOvnerInfo = userOvnerInfo;
                });
            });
        },
        promiseRegionCollection:promiseRegionCollection,
        getRegionCollection : function(){
            return RegionCollection;
        },
        getTender : function(){
            return Tender;
        }
    }

}]);
