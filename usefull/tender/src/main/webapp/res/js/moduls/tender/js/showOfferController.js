/**
 * Created by serdukov on 06.03.16.
 */
/**
 * Created by serdukov on 05.01.16.
 */
angular.module('app').controller('showOfferController', ['$scope','$http', '$location','$anchorScroll',
    'tenderService', '$stateParams',
    function( $scope, $http,  $location, $anchorScroll, tenderService , $stateParams) {


        var tScope = this;
        tScope.AcceptOffer ={};
        tScope.OfferModal = {};

        tenderService.promiseTender().then(function() {

            tScope.Tender = tenderService.getTender();

            angular.forEach(tScope.Tender.offerList, function(offer, key) {
                if (parseInt(offer.id) == parseInt($stateParams.offer_id)) {

                    tScope.OfferModal.title = 'Предложение от компании: '+offer.provider.juridicalInfo.companyName ;
                    tScope.OfferModal.offerDescription = offer.offerDescription;
                    tScope.OfferModal.dateBegin = offer.dateBegin;
                    tScope.OfferModal.dateEnd = offer.dateEnd;
                    tScope.OfferModal.priceOffer = offer.price;
                    tScope.OfferModal.offerId = offer.id;
                    tScope.OfferModal.fullAdress = offer.fullAdress;




                    tScope.OfferModal.mapOneOffer = new google.maps.Map(document.getElementById('mapOneOffer'), {zoom: 13});
                    tScope.OfferModal.markerOffer = new google.maps.Marker({map: tScope.OfferModal.mapOneOffer});
                    var latlng = new google.maps.LatLng(offer.offerPlaceLatitude, offer.offerPlaceLongitude);
                    tScope.OfferModal.markerOffer.setPosition(latlng);


                    google.maps.event.trigger(tScope.OfferModal.mapOneOffer, "resize");
                    tScope.OfferModal.mapOneOffer.setCenter(tScope.OfferModal.markerOffer.getPosition());

                    return;
                }
            });


        });


        tScope.acceptOffer = function(offer_id){
            tScope.AcceptOffer.tender_id =  tScope.Tender.id;
            tScope.AcceptOffer.offer_id =  offer_id;

            $http.post("/consumer_accept_offer", tScope.AcceptOffer)
                .then(function(message) {
                    var data = message.data;
                    if (data.status == '3000') {
                        tScope.AcceptOffer ={};
                        tScope.Tender.tenderStatus.id = '3';
                        $scope.$emit('errorAlertMessage', {text:"Предложение по тендеру принято успешно", status: "success"});
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function(val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {text: html, status: "warning"});
                    }
                    $location.path("/user_page");
                });

        };

    }]);

