/**
 * Created by serdukov on 06.03.16.
 */
/**
 * Created by serdukov on 05.01.16.
 */
angular.module('app').controller('providerOverTenderController', ['$scope','$http', '$location','$anchorScroll',
    'tenderService',
    function( $scope, $http,  $location, $anchorScroll, tenderService ) {


        var tScope = this;
        tScope.ProviderOverTender ={};


        //завершение тендера исполнителем
        tScope.provOverTender = function() {


            tenderService.promiseTender().then(function() {
            tScope.ProviderOverTender.tender_id = tenderService.getTender().id;


            $http.post("/provider_over_tender", tScope.ProviderOverTender)
                .then(function(message) {
                    var data = message.data;
                    if (data.status == '3000') {
                        tScope.ProviderOverTender ={};
                        $scope.$emit('errorAlertMessage', {text:"Тендер успешно завершен", status: "success"});
                        $location.path("/user_page");
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function(val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {text: html, status: "warning"});
                    }

                });
            });

        };






    }]);

