/**
 * Created by serdukov on 19.11.15.
 */


angular.module('app').controller('mainUserController', ['$scope', '$http', '$location','userPageService',
    function($scope, $http,  $location, userPageService) {






        var uScope = this;




        userPageService.promiseUserInfo().then(function() {
            uScope.User = userPageService.getUserInfo();
            if(uScope.User.provider) {
                uScope.User.consumer = false;
                $location.path('/user_page/tender_list/tenders_my_cat');

                userPageService.promiseTendersForProvider().then(function(){
                    uScope.TendersMyCat = userPageService.getTendersMyCat();
                });

                userPageService.promiseTendersWinForProvider().then(function(){
                    uScope.TendersWinForProvider = userPageService.getTendersWinForProvider();
                });

                userPageService.promiseTendersWithOfferForProvider().then(function(){
                    uScope.TendersWithOfferForProvider = userPageService.getTendersWithOfferForProvider();
                });

                userPageService.promiseTendersForConsumerOwner().then(function(){
                    uScope.TendersMy = userPageService.getTendersMy();
                    uScope.TendersMyActual = userPageService.getTendersMyActual();
                    uScope.TendersMyEnded = userPageService.getTendersMyEnded();
                });


            }else{
                uScope.User.consumer = true;
                $location.path('/user_page/tender_list/my_tenders');

                userPageService.promiseTendersForConsumerOwner().then(function(){
                    uScope.TendersMy = userPageService.getTendersMy();
                    uScope.TendersMyActual = userPageService.getTendersMyActual();
                    uScope.TendersMyEnded = userPageService.getTendersMyEnded();
                });

            }

        });





        uScope.isCurrentPath = function (path) {
            if($location.path() == path) return true;
        };


    }]);


