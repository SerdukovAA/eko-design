/**
 * Created by serdukov on 19.11.15.
 */
angular.module('app').controller('providerFilterController', ['$scope', 'userPageService','$http',
    function ($scope, userPageService, $http) {



        var stScope = this;
        stScope.TypeObjectsCollection = {};
        userPageService.promiseUpdateTypeCollections().then(function () {
            stScope.TypeObjectsCollection = userPageService.getTypeObjectsCollection();
            stScope.ckeckProviderFilters();
        });





        stScope.checkAll = function(selector){
            angular.forEach(selector.selectorOptions, function(option, key) {
                option.checked = true;
            });
        };


        stScope.unCheckAll = function(selector){
            angular.forEach(selector.selectorOptions, function(option, key) {
                option.checked = false;
            });
        };




        //заполение селекторов
        stScope.ckeckProviderFilters = function(){
            userPageService.promiseUserInfo().then(function(){
                var providerFilterValues = userPageService.getUserInfo().juridicalInfo.providerFilterValues;
                angular.forEach(providerFilterValues, function(filter, key) {
                    angular.forEach(stScope.TypeObjectsCollection, function(typeObject, key) {
                        angular.forEach(typeObject.typeServices, function(typeService, key) {
                            angular.forEach(typeService.categorySelectors, function(selector, key) {
                                //если селектор наш, то заходим в него
                                if(filter.selectorId == selector.id){
                                    angular.forEach(selector.selectorOptions, function(option, key) {
                                        if(filter.valueId == option.optionId) option.checked = true;
                                    });
                                }
                            });
                        });
                    });
                });
            });

        };

        stScope.getCkeckedProviderFiltersOptions = function(){
            var parametersRequestList = new Array();
            angular.forEach(stScope.TypeObjectsCollection, function(typeObject, key) {
                angular.forEach(typeObject.typeServices, function(typeService, key) {
                    angular.forEach(typeService.categorySelectors, function(selector, key) {
                        //если селектор наш, то заходим в него
                        angular.forEach(selector.selectorOptions, function(option, key) {
                            if(option.checked) {
                                parametersRequestList.push({
                                    selectorId: selector.id,
                                    optionId: option.optionId,
                                    typeServiceId: typeService.id
                                })
                            }
                        });

                    });
                });
            });

            return parametersRequestList;
        };

        stScope.saveProviderFilterSettings = function() {

            var ParamRequestList = stScope.getCkeckedProviderFiltersOptions();

            $http.post("/provider_set_filter_parameters", ParamRequestList)
                .then(function (http) {
                    var data = http.data;
                    if (data.status == '3000') {
                        $scope.$emit('errorAlertMessage', {text: "Изменения успешно сохранены", status: "success"});
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function (val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {
                            text: "Не удалось сохранить изменения: " + html,
                            status: "warning"
                        });
                    }
                });

        };

    }]);
