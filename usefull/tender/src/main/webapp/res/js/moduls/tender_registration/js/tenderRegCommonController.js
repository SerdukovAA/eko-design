/**
 * Created by serdukov on 19.11.15.
 */
angular.module('app').controller('tenderRegCommonController', ['$scope','$http','$timeout', '$location','$anchorScroll',
    'tenderRegService', 'serverMessageService','$filter', 'UserSession',  function($scope, $http, $timeout, $location, $anchorScroll,
                                                                          tenderRegService, serverMessageService, $filter, UserSession) {


       var tCtrl = this;


        tCtrl.UserisJuridical = UserSession.User().isJuridical;


        tCtrl.TenderFormObject = tenderRegService.getTender();

        if(tCtrl.TenderFormObject.typeService == undefined || tCtrl.TenderFormObject.typeService.categorySelectors == undefined) {
            $location.path("/new-tender-start");
        }




        tCtrl.showInputFile = function () {
            $('#addFileInput').click();
            //angular.element(document.querySelector('#addFileInput'))[0].click();
        }


        tCtrl.removeAddFile = function(index){
            if (tCtrl.TenderFormObject.tenderFileList.length > 0) {
                tCtrl.TenderFormObject.tenderFileList.splice(index, 1);
            }
        };


        tCtrl.submitNewTender = function (invalid) {

            if(invalid) return false;

            //начинаем подоготовку тендера к отправке, многие поля нам просто не нужны. например справочники и т.п
            var NewTender = {}; //tCtrl.TenderFormObject;
            NewTender.tenderName = tCtrl.TenderFormObject.tenderName;
            NewTender.tenderDescription = tCtrl.TenderFormObject.tenderDescription;

            NewTender.adress = tCtrl.TenderFormObject.adress;

            NewTender.typeObject = angular.copy(tCtrl.TenderFormObject.typeObject);
            NewTender.typeObject.typeServices =  null;

            NewTender.typeService = tCtrl.TenderFormObject.typeService;

            NewTender.typeService.categorySelectors.forEach(function (categorySelector, index, array) {
                delete categorySelector.showHelp;
                categorySelector.selectorOptions.forEach(function (selectorOption, index, array) {
                    delete selectorOption.showHelp;
                });
            });

            NewTender.dateBegin=  $filter('date')(tCtrl.TenderFormObject.dateBegin, "dd.MM.yyyy");
            NewTender.dateEnd  =  $filter('date')(tCtrl.TenderFormObject.dateEnd, "dd.MM.yyyy");


            NewTender.tenderFileList = tCtrl.TenderFormObject.tenderFileList;

            NewTender.needCredit = tCtrl.TenderFormObject.needCredit;
            NewTender.expectedPrice = tCtrl.TenderFormObject.expectedPrice;

            NewTender.responsibleInfo = tCtrl.TenderFormObject.Responsible;
            NewTender.needResponsible = tCtrl.TenderFormObject.needResponsible;


            NewTender.commonValueList = new Array();



            var commonParameters = tCtrl.TenderFormObject.typeService.commonParameters;
            for(var i= 0, len = commonParameters.length; i<len; i++) {

                if(commonParameters[i].varType =='text'){
                    NewTender.commonValueList.push({
                        commonId: commonParameters[i].id,
                        parameterName: commonParameters[i].parameterName,
                        valueId: null,
                        parameterValue: commonParameters[i].value
                    });
                }
            }


            NewTender.selectorValueList = new Array();
            var selectors = tCtrl.TenderFormObject.typeService.categorySelectors

            for(var s= 0, slen = selectors.length; s<slen; s++) {
                var categoriaSelector = selectors[s];
                if(categoriaSelector.consumerSelectorType == 'radio' && categoriaSelector.depended == false){
                    var options  = categoriaSelector.selectorOptions;
                    for (var i = 0, len = options.length; i < len; i++) {
                         if(options[i].optionId == categoriaSelector.checkedOptionId ){
                             NewTender.selectorValueList.push({
                                 selectorId: categoriaSelector.id,
                                 selectorName: categoriaSelector.selectorName,
                                 valueId: options[i].optionId,
                                 selectorValue: options[i].optionName
                             });
                         }
                    }
                }

                if(categoriaSelector.consumerSelectorType == 'checkbox' && categoriaSelector.depended == false){
                    var options  = categoriaSelector.selectorOptions;
                    for (var i = 0, len = options.length; i < len; i++) {
                        if(options[i].checked === undefined) continue;
                        if(options[i].checked == true) {
                            NewTender.selectorValueList.push({
                                selectorId: categoriaSelector.id,
                                selectorName: categoriaSelector.selectorName,
                                valueId: options[i].optionId,
                                selectorValue: options[i].optionName
                            });
                        }
                    }
                }
////////////////////зависимые селекторы/////////
                var depSelectors = categoriaSelector.dependedSelectors;

                for(var d= 0, dlen = depSelectors.length; d<dlen; d++) {
                    var depCategoriaSelector = depSelectors[d];
                    if (depCategoriaSelector.consumerSelectorType == 'radio') {
                        var options = depCategoriaSelector.selectorOptions;
                        for (var i = 0, len = options.length; i < len; i++) {
                            if (options[i].optionId == depCategoriaSelector.checkedOptionId) {
                                NewTender.selectorValueList.push({
                                    selectorId: depCategoriaSelector.id,
                                    selectorName: depCategoriaSelector.selectorName,
                                    valueId: options[i].optionId,
                                    selectorValue: options[i].optionName
                                });
                            }
                        }
                    }

                    if (depCategoriaSelector.consumerSelectorType == 'checkbox') {
                        var options = depCategoriaSelector.selectorOptions;
                        for (var i = 0, len = options.length; i < len; i++) {
                            if (options[i].checked === undefined) continue;
                            if (options[i].checked == true) {
                                NewTender.selectorValueList.push({
                                    selectorId: depCategoriaSelector.id,
                                    selectorName: depCategoriaSelector.selectorName,
                                    valueId: options[i].optionId,
                                    selectorValue: options[i].optionName
                                });
                            }
                        }
                    }
                }
                 ////Заканчиваем проход по зависимым селекторам/////////////////////
            }








            NewTender.tenderObjects = tCtrl.TenderFormObject.tenderObjects;




            for (var i = 0; i < NewTender.tenderObjects.length; i++) {
                var tenderObject = NewTender.tenderObjects[i];

                for (var p = 0; p < tenderObject.parameterRelations.length; p++) {
                        var parameterRelation = tenderObject.parameterRelations[p];
                        if(parameterRelation.objectParameter.varType == 'catalog'){
                            parameterRelation.objectParameter.value = parameterRelation.objectParameter.value[parameterRelation.objectParameter.catalogNameField];
                        }
                }

                tenderObject.objectParameterValues = new Array();

                var parRelations =  tenderObject.parameterRelations;
                for(var p = 0, pl=parRelations.length; p<pl ; p++ ){
                    tenderObject.objectParameterValues.push({
                            parameterId: parRelations[p].objectParameter.id,
                            parameterName: parRelations[p].objectParameter.parameterName,
                            valueId: null,
                            parameterValue: parRelations[p].objectParameter.value
                        });
                }


            }


            $http.post("/tender_registration", NewTender)
                .then(function(http) {
                    var data = http.data;
                    if (data.status == '3000') {
                        serverMessageService.setServerMessage(data);
                        tenderRegService.setTender(
                            {
                                tenderFileList:new Array(),
                                tenderObjects:new Array()
                            }
                        );
                        $location.path('/end_new_tender');
                        $location.hash('end_page');
                        $anchorScroll();
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function (val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {
                            text: "Не удалось сохранить: " + html,
                            status: "warning"
                        });

                    }
                });











        }



    }]);

