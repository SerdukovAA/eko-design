/**
 * Created by serdukov on 19.11.15.
 */
angular.module('app').controller('providerInfoController', ['$scope', 'UserSession', '$http',
    function ($scope, UserSession, $http) {

        $scope.provider = {};

        var juridicalInfo =  UserSession.User().Row.juridicalInfo;
        var privateInfo =  UserSession.User().Row.privateInfo;


        $scope.provider.firstNameContactPerson = privateInfo.firstName;
        $scope.provider.lastNameContactPerson = privateInfo.lastName;
        $scope.provider.p_login = UserSession.User().Row.login;
        $scope.provider.emailContactPerson = privateInfo.email;
        $scope.provider.phoneNumberContactPerson = privateInfo.phoneNumber;
        $scope.provider.companyName = juridicalInfo.companyName;
        $scope.provider.inn = juridicalInfo.inn;
        $scope.provider.companyDescription = juridicalInfo.companyDescription;


        $scope.provider.adress = {
            fullAdress: juridicalInfo.factAdress,
            latitude: juridicalInfo.factAdrLatitude,
            longitude: juridicalInfo.factAdrLongitude,
            region: juridicalInfo.regionCode,
            rayon: juridicalInfo.rayonCode,
            city: juridicalInfo.cityCode,
            street: juridicalInfo.street,
            houseNumber: juridicalInfo.houseNumber
        };





        $scope.submitProvider = function (invalid) {

            if(invalid) return false;
                //подготовка исполнителя
                $http.post("/provider_edit_info", $scope.provider)
                    .then(function(http) {
                        var data = http.data;
                        if (data.status == '3000') {
                            $scope.$emit('errorAlertMessage', {text: "Изменения успешно сохранены", status: "success"});
                        } else {
                            var html = "<p><b>" + data.text + "</b></p>";
                            if (data.hasErrors = 'true') {
                                angular.forEach(data.errors, function (val, key) {
                                    html += "<p>" + val.errorText + "</p>";
                                });
                            }
                            $scope.$emit('errorAlertMessage', {
                                text: "Не удалось сохранить изменения: " + html,
                                status: "warning"
                            });
                        }
                        UserSession.getUserInfo();
                    });

        }

    }]);
