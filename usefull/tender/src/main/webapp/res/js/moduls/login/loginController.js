/**
 * Created by serdukov on 24.02.16.
 */
app.controller('loginController',['$scope', 'LoginService', '$location', '$stateParams', '$http','LoginHandler',
    function ($scope, LoginService, $location, $stateParams, $http, LoginHandler) {

    $scope.login = function () {
        LoginService.login($scope.credentials.username, $scope.credentials.password, function (http) {
            if(http.status === 200){
                LoginHandler.success();
            }else{
                $scope.$emit('errorAlertMessage', {text: "Неправильный логин или пароль", status: "danger"});
              //  LoginHandler.reject();
                console.info('Something went wrong while trying to login... ', http);
            }
        });
    };





            $scope.sendRecovery = function(form){
        if(form.$invalid) return false;
        var loginMail =  this.loginEmail;

        $http({
            method: 'post',
            url: "/info_password_recovery",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: 'loginMail='+loginMail
        }).success(function(serverMessage, status) {
            if (serverMessage.status == '3000') {
                $scope.passwordRecoveryAnsver = true;
            }
        });

    };


        $scope.changePassButton = function(form){
            if(form.$invalid) return false;
            $http({
                method: 'post',
                url: "/info_change_pas_recovery",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: 'hash='+$stateParams.hash+'&recovery_id='+$stateParams.checkId+'&password='+this.passwordChange
            }).success(function(serverMessage, status) {
                if (serverMessage.status == '3000') {
                    $scope.$emit('errorAlertMessage',{text:"Пароль изменен", status: "success"});
                    $location.path('/home');
                }else {
                    var html = "<p><b>" + serverMessage.text + "</b></p>";
                    if (serverMessage.hasErrors = 'true') {
                        angular.forEach(serverMessage.errors, function (val, key) {
                            html += "<p>" + val.errorText + "</p>";
                        });
                    }
                    $scope.$emit('errorAlertMessage', {
                        text: "Не удалось сохранить изменения: " + html,
                        status: "warning"
                    });
                }
            });

        };

}]);
