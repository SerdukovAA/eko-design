/**
 * Created by serdukov on 19.11.15.
 */
angular.module('app').controller('endUserRegController', function($scope, $location, $anchorScroll, serverMessageService) {

    $scope.serverMessage = serverMessageService.getServerMessage();

});


angular.module('app').service('serverMessageService', function() {

    var serverMessage = {};

    return {
        setServerMessage  : function(sm) {
            serverMessage = sm;
        },
        getServerMessage : function(){
            return serverMessage;
        }
    };

});
