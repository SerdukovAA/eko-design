app.service('LoginHandler',['$q','$state','$rootScope','UserSession', function ($q,$state,$rootScope,UserSession) {
    var deferred;
    return {
        show: function(){
            $state.go('login');
            deferred = $q.defer();
            return deferred.promise;
        },
        reject: function(){
            UserSession.sessionReset();
            if(deferred != undefined){
                return deferred.reject();
            }
        },
        success: function(){
            UserSession.getUserInfo(function(){
                if(deferred != undefined){
                    return deferred.resolve();
                }
            });
        }
    }
}]);


