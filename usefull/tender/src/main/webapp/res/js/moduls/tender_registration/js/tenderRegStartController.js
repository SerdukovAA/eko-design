/**
 * Created by serdukov on 19.11.15.
 */


angular.module('app').controller('tenderRegStartController', ['$scope','$http','$timeout', '$location','$anchorScroll', 'tenderRegService',
    function($scope, $http, $timeout, $location, $anchorScroll, tenderRegService) {

        var tCtrl = this;

        tCtrl.TenderFormObject = tenderRegService.getTender();



        tCtrl.TypeObjectsCollection =  tenderRegService.getTypeObjectsCollection();



       tCtrl.showDependetSelector = function(categoriaSelector, dependedSelector){

           var parentValues = dependedSelector.parentSelectorValues;


            if(categoriaSelector.consumerSelectorType == 'radio'){
                var values = parentValues.split(",");
                for (var v = 0, vlen = values.length; v < vlen; v++) {
                    if (categoriaSelector.checkedOptionId == values[v]) {
                        return true;
                    }

                }
                dependedSelector.invalid = false;
                return false;
            }
            if(categoriaSelector.consumerSelectorType == 'checkbox'){
                var options  = categoriaSelector.selectorOptions;
                for (var i = 0, len = options.length; i < len; i++) {
                    if(options[i].checked === undefined) continue;
                        var values = parentValues.split(",");
                        for (var v = 0, vlen = values.length; v < vlen; v++) {
                           if(options[i].checked == true && options[i].optionId == values[v]) return true;
                        }
                }
                dependedSelector.invalid = false;
                return false;

            }
        };


        tCtrl.secondBlockTry = function(){
            $scope.$broadcast('secondBlockTry');
            if(tCtrl.isValidSelectors()){
                tenderRegService.setTypeObjectsCollection(tCtrl.TypeObjectsCollection);
                tenderRegService.setTender(tCtrl.TenderFormObject);
                $location.path('/new_tender_objects/forward');
            }else{
                $scope.$emit('errorAlertMessage', {text:"Заполните все необходимые поля", status: "danger"});
            }
        };

        tCtrl.isValidSelectors = function() {
            var selectors = tCtrl.TenderFormObject.typeService.categorySelectors;
            for (var i = 0, len = selectors.length; i < len; i++) {
                if(selectors[i].invalid == true ){
                    //форма не отсылается
                    return false;
                }else{
                    var depended =  selectors[i].dependedSelectors;
                    for (var d = 0, dlen = depended.length; d < dlen; d++) {
                        if(depended[d].invalid ==true){
                           return false;
                        }
                    }
                }
            }
            return true;
        };



    }]);

