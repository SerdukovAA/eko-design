/**
 * Created by serdukov on 05.01.16.
 */


angular.module('app').controller('tenderController', ['$compile','$scope','$http','$timeout', '$location','$anchorScroll',
    'tenderService', '$filter',
    function($compile, $scope, $http, $timeout, $location, $anchorScroll, tenderService ,$filter) {

        lightbox.option({
            'resizeDuration': 500,
            'positionFromTop': 200,
            'wrapAround': true
        });

        var tScope = this;

        tScope.geocoder = new google.maps.Geocoder();

        tScope.NewComment = {
            isAnswer:false
        };

        tScope.shoWmapOffersDiv = false;
        tScope.shoWOffersListTable = true;

        var renderSelectorValue = function (selectorV) {
            var values = " ";
            for (var v = 0, vlen = selectorV.values.length; v < vlen; v++) {
                values += selectorV.values[v];
                if (vlen > 1 && v < (vlen - 1)) {
                    values += ", ";
                }

            }
            selectorV.valuesHtml = values;
        };

        tScope.dateCreate = function (timestamp) {
            var date = new Date(timestamp),dd = date.getDate(), mm = date.getMonth() + 1, yyyy = date.getFullYear();
            if (dd < 10) {dd = '0' + dd}
            if (mm < 10) {mm = '0' + mm}
            return dd + '.' + mm + '.' + yyyy;
        };


        var generateSelectorValueList = function () {

            var selectorValuesArray = new Array();
            var selectorValueList = tScope.Tender.selectorValueList;
            ///пройтись по каждому и передать собрать однотипные селекторы

            for (var i = 0, len = selectorValueList.length; i < len; i++) {
                var selectorId = selectorValueList[i].selectorId;

                if (typeof selectorValuesArray[selectorId] === 'undefined') {
                    var selectorV = {};
                    selectorV.name = selectorValueList[i].selectorName;
                    selectorV.values = new Array();
                    selectorV.values.push(selectorValueList[i].selectorValue);
                    selectorValuesArray[selectorId] = selectorV;
                }
                else {
                    selectorValuesArray[selectorId].values.push(selectorValueList[i].selectorValue);
                }
            }
            selectorValuesArray.forEach(function (item, index, array) {
                renderSelectorValue(item);
            });

            tScope.SelectorValueList = selectorValuesArray;
        };






        tScope.geocode = function (){
            tScope.Tender.tenderPositionG = new google.maps.LatLng(tScope.Tender.tenderPlaceLatitude, tScope.Tender.tenderPlaceLongitude);
            tScope.geocoder.geocode({"latLng": tScope.Tender.tenderPositionG}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                   if (results[1]) {
                      return results[1].formatted_address;
                   } else {
                      return " ";
                   }
            } else {
                return " ";
            }
         });
       }


       tenderService.promiseTender().then(function(){
            tScope.Tender = tenderService.getTender();
            //список комментариев
            tScope.TenderMessageList = tScope.Tender.messageList;
            tScope.ready = true;
            generateSelectorValueList();
            $timeout(function() {

                //Показать адресс тендера в любом случае
                tScope.Tender.tenderPositionG = new google.maps.LatLng(tScope.Tender.tenderPlaceLatitude, tScope.Tender.tenderPlaceLongitude);
                //////////////////прогружаем карту и наносим тендер/////////////////////////////////////////
                tScope.mapTender = new google.maps.Map(document.getElementById('mapTenderDiv'), {
                    zoom: 13,
                    center: tScope.Tender.tenderPositionG
                });
                //создаем маркер тендера со специфичной иконкой
                var image2 = '/res/img/icons/marker1.png';
                var tenderMarker = new google.maps.Marker({
                    position: tScope.Tender.tenderPositionG,
                    map: tScope.mapTender,
                    icon: image2
                });
                //создаем карту и помещаем ее на страницу
                tScope.mapOffers = new google.maps.Map(document.getElementById('mapOffersDiv'), {
                    zoom: 13,
                    center: tScope.Tender.tenderPositionG
                });

                //сздаем маркер тендера со специфичной иконкой
                var image = '/res/img/icons/marker1.png';

                tScope.tenderMarker = new google.maps.Marker({
                    position: tScope.Tender.tenderPositionG,
                    map: tScope.mapOffers,
                    icon: image
                });

                tScope.offerMarkers = [];

                angular.forEach(tScope.Tender.offerList, function(offer, key) {

                    var compiled = $compile(getInfoTemplate (offer))($scope);

                    var infowindow = new google.maps.InfoWindow({content: compiled[0] });

                    tScope.offerMarkers[key] = new google.maps.Marker({
                        position: new google.maps.LatLng(offer.offerPlaceLatitude, offer.offerPlaceLongitude),
                        map: tScope.mapOffers
                    });

                    google.maps.event.addListener( tScope.offerMarkers[key], 'click', function () {
                      infowindow.open(tScope.mapOffers,  tScope.offerMarkers[key]);
                    });

                });

            });
        });


        var getInfoTemplate = function(offer){

            var html = ["<div>",
                "<h4 style='word-wrap: break-word;'> Предложение от: ",offer.provider.juridicalInfo.companyName,
                "</h4>",
                "<p> ",offer.offerDescription,"</p>",
                "<p><b>Сроки: </b>", tScope.dateCreate(offer.dateBegin), " - ",tScope.dateCreate(offer.dateEnd),"</p>",
                "<a class='btn btn-info btn-xs'  href='#/show_offer/",tScope.Tender.id,"/",offer.id,"' >Посмотреть предложение</a>",
                "</div>"].join("");

            return html;
        }


        tScope.showOffersList = function(event){
            event.preventDefault();
            tScope.shoWmapOffersDiv = false;
            tScope.shoWOffersListTable = true;
            return true;
        };

        tScope.showOffersMap = function() {
            tScope.shoWmapOffersDiv = true;
            tScope.shoWOffersListTable = false;
            window.setTimeout(function(){
                google.maps.event.trigger( tScope.mapOffers, "resize");
                tScope.mapOffers.setCenter(tScope.Tender.tenderPositionG);
            },100);
            return true;
        };


        tScope.addAsverForComment = function(message_id, user_id){
            var result = $filter('letername')(user_id, tScope.Tender.id);
            tScope.NewComment.message_id = message_id;
            tScope.NewComment.text = result+', ';
            tScope.NewComment.isAnswer = true;
            $location.hash('new_comment');
            $anchorScroll();

        };


        tScope.submitNewComment = function() {
            tScope.NewComment.tender_id = tScope.Tender.id;
            $http.post("/add_comment", tScope.NewComment)
                .then(function(message) {
                    tScope.Tender.messageList.push(message.data);
                    tScope.TenderMessageList = tScope.Tender.messageList;
                    tScope.NewComment = {
                        isAnswer:false
                    };
                    $location.hash('lastComment');
                    $anchorScroll();
                });
        };




    }]);

