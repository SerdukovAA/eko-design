/**
 * Created by serdukov on 19.11.15.
 */
angular.module('app').controller('providerController',['$scope', '$http', '$location', '$anchorScroll',
    'serverMessageService' ,'registrationService', 'vcRecaptchaService',
    function($scope, $http, $location, $anchorScroll, serverMessageService, registrationService, vcRecaptchaService) {

    $scope.provider = registrationService.getProvider();

    $scope.captchaResponse = function(response){
        $scope.provider.recaptchaResponse = response;
    };

    $scope.submitRegistrationProvider = function (invalid) {

        if($scope.provider.recaptchaResponse=='##'){
            $scope.$emit('errorAlertMessage', {text: "<p>Необходимо подтвердить, что вы не робот</p>", status: "warning"});
            return false;
        }

            if(invalid) return false;

            $http.post("/registration_provider", $scope.provider)
                .then(function(http) {
                    var data = http.data;
                    if (data.status == '3000') {
                        serverMessageService.setServerMessage(data);
                        $location.path('/end_registration');
                        $location.hash('end');
                        $anchorScroll();
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function (val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {
                            text: "Не удалось сохранить изменения: " + html,
                            status: "warning"
                        });
                        vcRecaptchaService.reload(0);
                    }
                });


    }

}]);
