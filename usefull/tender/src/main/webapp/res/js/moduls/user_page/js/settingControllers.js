/**
 * Created by serdukov on 19.11.15.
 */


angular.module('app').controller('settingsController', ['$scope','$http','$location','userPageService',
    function($scope, $http, $location, userPageService) {

        var stScope = this;

        stScope.ChangePassRequest = {};
        stScope.NewLocate = {};

        stScope.UserParameters = {};
        userPageService.promiseUserParameters().then(function () {
            stScope.UserParameters = userPageService.getUserParameters();
        });


        stScope.PreferRegions = userPageService.getUserInfo().userPreferedRegions;

        stScope.addRegionToPreffer =  function(newRegionForm){
            newRegionForm.$submitted = true;
            if (newRegionForm.$invalid) return false;
            stScope.PreferRegions.push(stScope.NewLocate);
        };

        stScope.deleteRegionFromPrefered = function(index){
            stScope.PreferRegions.splice(index, 1);
        };

        stScope.savePrefferRegios = function(){
            $http.post("/provider_set_prefer_regions", stScope.PreferRegions)
                .then(function (http) {
                    var data = http.data;
                    if (data.status == '3000') {
                        $scope.$emit('errorAlertMessage', {text: "Изменения успешно сохранены", status: "success"});
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function (val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {
                            text: "Не удалось сохранить изменения: " + html,
                            status: "warning"
                        });
                    }
                });

        };






        stScope.userDeleteBySelf = function(){

            $http.post("/consumer_delete_byself", null)
                .then(function(http) {
                    var data  = http.data;
                    if (data.status == '3000') {
                        $scope.$emit('errorAlertMessage', {text:"Ваш акаунт успешно удален", status: "success"});
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function(val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {text: "Не удалось удалить пользователя:"+html, status: "warning"});
                    }
                });

        };




        stScope.saveMailSetting = function() {

            $http.post("/provider_save_mail_settings", stScope.UserParameters)
                .then(function (http) {
                    var data = http.data;
                    if (data.status == '3000') {
                        $scope.$emit('errorAlertMessage', {text: "Изменения успешно сохранены", status: "success"});
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function (val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {
                            text: "Не удалось сохранить изменения: " + html,
                            status: "warning"
                        });
                    }
                });

        };




        stScope.changePassword = function(form) {

            form.$submitted = true;
            if (form.$invalid) return false;

            $http.post("/consumer_change_password", stScope.ChangePassRequest)
                .then(function (http) {
                    var data = http.data;
                    if (data.status == '3000') {
                        $scope.$emit('errorAlertMessage', {text: "Пароль успешно изменен", status: "success"});
                    } else {
                        var html = "<p><b>" + data.text + "</b></p>";
                        if (data.hasErrors = 'true') {
                            angular.forEach(data.errors, function (val, key) {
                                html += "<p>" + val.errorText + "</p>";
                            });
                        }
                        $scope.$emit('errorAlertMessage', {
                            text: "Не удалось изменить пароль: " + html,
                            status: "warning"
                        });
                    }
                });
         };



    }]);




