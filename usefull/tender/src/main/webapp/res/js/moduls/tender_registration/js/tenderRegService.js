/**
 * Created by serdukov on 01.12.15.
 */
angular.module('app').factory('tenderRegService', ['$http',function($http) {

    var TenderFormObject = {
        tenderFileList:new Array(),
        tenderObjects:new Array()
    };

    var TypeObjectsCollection = new Array();

    var InputCatalogCollections = [];
    var RefferalCatalog = [];




    var promisetypeObjects = $http.get("/info_get_all_type_objects")
        .success(function (typeObjects) {
            TypeObjectsCollection = typeObjects;
    });









     var updateParametersObject = function(){

      try {
          var catSelectors = TenderFormObject.typeService.categorySelectors;

          var parameterRequestList = new Array();

          for (var i = 0; i < catSelectors.length; i++) {
              var selector = catSelectors[i];

              if (selector.depended) continue;

              if (selector.consumerSelectorType == 'radio') {
                  if (!selector.checkedOptionId) continue;
                  parameterRequestList.push({
                      selectorId: selector.id,
                      optionId: selector.checkedOptionId
                  });
              }
              if (selector.consumerSelectorType == 'checkbox') {
                  var options = selector.selectorOptions;
                  for (var o = 0; o < options.length; o++) {
                      var option = options[o];
                      if (option.checked) {
                          parameterRequestList.push({
                              selectorId: selector.id,
                              optionId: option.optionId
                          });
                      }
                  }
              }


              for (var ds = 0; ds < selector.dependedSelectors.length; ds++) {
                  var dependedSelector = selector.dependedSelectors[ds];

                  if (dependedSelector.consumerSelectorType == 'radio') {
                      if (!dependedSelector.checked) continue;
                      parameterRequestList.push({
                          selectorId: dependedSelector.id,
                          optionId: dependedSelector.checked
                      });
                  }
                  if (dependedSelector.consumerSelectorType == 'checkbox') {
                      var options = dependedSelector.selectorOptions;
                      for (var o = 0; o < options.length; o++) {
                          var option = options[o];
                          if (option.checked) {
                              parameterRequestList.push({
                                  selectorId: dependedSelector.id,
                                  optionId: option.optionId
                              });
                          }
                      }
                  }
              }
          }

          return $http.post("/info_get_object_parameters", parameterRequestList)
              .success(function (parameterRelations) {
                  TenderFormObject.ParameterRelations = cleanParametersRelation(parameterRelations);
              });
      }catch(e){
          //ewerwerwer
      }
    };

    var updateInputCoolections = function () {

        var parameterRelations = TenderFormObject.ParameterRelations;

        var needInsertInCollection = [];
        for (var i = 0; i < parameterRelations.length; i++) {
            if (parameterRelations[i].objectParameter.varType == 'catalog' && parameterRelations[i].objectParameter.ref == false) {
                needInsertInCollection.push(parameterRelations[i]);
            }
        }
        var needCount = needInsertInCollection.length;
        for (var i = 0; i < needCount; i++) {
            getCollectionFunction(needInsertInCollection, needCount, i);
        }
    };


    var getCollectionFunction = function (needCollect, nCount, i) {
        var curMap = needCollect[i].objectParameter.parameterIdent;

        $http.get(needCollect[i].objectParameter.catalogLink)
            .success(function (collection) {
                InputCatalogCollections[curMap] = collection;
                InputCatalogCollections.length++;
            });

    };







    var cleanParametersRelation = function (parameterRelations) {
        var newParameterRelations = [];
        for (var i = 0; i < parameterRelations.length; i++) {

            var objectParameter = parameterRelations[i].objectParameter;


            if (newParameterRelations.length == 0) {
                newParameterRelations.push(parameterRelations[i]);
            }else {

                var duplicateIndex = -1;
                var neewLen = newParameterRelations.length;

                for (var k = 0; k < neewLen; k++) {
                    if (newParameterRelations[k].objectParameter.id == objectParameter.id) {
                        duplicateIndex = k;
                    }
                }

                if (duplicateIndex == -1) {
                    newParameterRelations.push(parameterRelations[i]);
                } else {
                    if (newParameterRelations[duplicateIndex].required == false && parameterRelations[i].required == true) {
                        newParameterRelations[duplicateIndex] = parameterRelations[i];
                    }
                }

            }
        }

        return newParameterRelations;
    };










    return {
        promisetypeObjects:promisetypeObjects,
        updateParametersObject:updateParametersObject,
        updateInputCoolections:updateInputCoolections,
        getTender : function(){
            return TenderFormObject;
        },
        setTender : function(Tender){
            TenderFormObject = Tender;
        },
        setTypeObjectsCollection : function(typeObjCollection){
            TypeObjectsCollection = typeObjCollection;
        },
        getTypeObjectsCollection : function(){
            return TypeObjectsCollection;
        },
        getInputCatalogCollections : function(){
            return InputCatalogCollections;
        },
        getRefferalCatalog : function(){
            return RefferalCatalog;
        }

    }

}]);
