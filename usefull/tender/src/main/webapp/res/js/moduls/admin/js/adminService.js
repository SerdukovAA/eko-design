/**
 * Created by serdukov on 01.12.15.
 */
angular.module('app').factory('adminService', ['$http',function($http) {

    return {
        getProvidersCollection: function (page_num) {
            return  $http({
                url: "/admin_show_providers",
                method: "GET",
                params: {page_num:page_num}
            });
        },
        getTendersCollection: function (page_num) {
            return  $http({
                url: "/admin_show_tenders",
                method: "GET",
                params: {page_num:page_num}
            });
        },
        getConsumersCollection: function (page_num) {
            return  $http({
                url: "/admin_show_consumers",
                method: "GET",
                params: {page_num:page_num}
            });
        },
        tendersCount: function () {
            return  $http.get("/admin_tenders_count");
        },
        providersCount: function () {
            return  $http.get("/admin_providers_count");
        },
        consumersCount: function () {
            return $http.get("/admin_consumers_count");
        },
    }

}]);
