/**
 * Created by serdukov on 06.03.16.
 */
/**
 * Created by serdukov on 05.01.16.
 */
angular.module('app').controller('cancelTenderController', ['$scope','$http', '$location','$anchorScroll',
    'tenderService',
    function( $scope, $http,  $location, $anchorScroll, tenderService ) {



        var tScope = this;

        tScope.cancelTenderCheck ={};
        tScope.CancelTender={};


        tScope.consCancelTender = function(form){

            if(form.$invalid) return false;

            tenderService.promiseTender().then(function(){

               tScope.CancelTender.tender_id = tenderService.getTender().id;
               $http.post("/consumer_cancel_tender", tScope.CancelTender)
                    .then(function(message) {
                        var data = message.data;
                        if (data.status == '3000') {
                            tScope.CancelTender ={};
                            $scope.$emit('errorAlertMessage', {text:"Вы успешно отменили проведение тендера", status: "success"});
                            $location.path("/user_page");
                        } else {
                            var html = "<p><b>" + data.text + "</b></p>";
                            if (data.hasErrors = 'true') {
                                angular.forEach(data.errors, function(val, key) {
                                    html += "<p>" + val.errorText + "</p>";
                                });
                            }
                            $scope.$emit('errorAlertMessage', {text: html, status: "warning"});
                        }

                    });
             });

        }



    }]);

