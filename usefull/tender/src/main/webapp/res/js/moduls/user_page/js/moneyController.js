/**
 * Created by serdukov on 19.11.15.
 */



angular.module('app').controller('moneyController', ['$scope', 'userPageService','$http',
    function ($scope, userPageService, $http) {





        var mScope = this;
        //список тендеров которые будут отображены

        mScope.YandexForm = {};



        userPageService.promiseProviderBalance().then(function () {
            mScope.ProviderBalance = userPageService.getProviderBalance();
        });

        mScope.submitYandexForm = function(yaForm){

            if(yaForm.$invalid) return false;

            var YandexTransBegin ={
                paymentType: mScope.YandexForm.paymentType,
                sum: mScope.YandexForm.sum
            };

            $http({method: 'OPTIONS',url: "/provider_begin_transaction",cache: false}).then(function(){
                $http.post("/provider_begin_transaction", YandexTransBegin)
                    .then(function(http) {
                        if(http.status == '200'){
                            mScope.YandexForm.customerNumber = http.data.user.user_id
                            mScope.YandexForm.paymentType = http.data.paymentType
                            mScope.YandexForm.t_uniq = http.data.id
                            document.getElementById("YandexForm").submit();
                        }
                    });
            });

        }

    }]);
