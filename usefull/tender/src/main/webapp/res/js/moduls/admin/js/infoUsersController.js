angular.module('app').controller('infoUsersController', ['adminService', function( adminService) {




    var iU = this;

    iU.ProvidersCollection = {};
    iU.ConsumersCollection = {};

    iU.Pagination = {};


    iU.Pagination.maxSize = 10;

    iU.Pagination.page_num = 1;

    adminService.providersCount().then(function(http){
        iU.Pagination.TotalProviders =  http.data;
    });
    adminService.consumersCount().then(function(http){
        iU.Pagination.TotalConsumers = http.data;
    });

    var refreshCollections = function (){
        adminService.getProvidersCollection(iU.Pagination.page_num).then(function(http){
            iU.ProvidersCollection = http.data;
        });
        adminService.getConsumersCollection(iU.Pagination.page_num).then(function(http){
            iU.ConsumersCollection = http.data;
        });


    };
    refreshCollections();

    iU.pageChanged = function() {
        refreshCollections();
    };




}]);
