/**
 * Created by serdukov on 06.03.16.
 */
/**
 * Created by serdukov on 05.01.16.
 */
angular.module('app').controller('consumerOverTenderController', ['$scope','$http', '$location','$anchorScroll',
    'tenderService',
    function( $scope, $http,  $location, $anchorScroll, tenderService ) {



        var tScope = this;

        tScope.OverTender = {
            rating:"",
            comment:""
        };
        tScope.overTenderCheck={};

        tScope.ratingStarList = [
            {rating:"1", css: ""},
            {rating:"2", css: ""},
            {rating:"3", css: ""},
            {rating:"4", css: ""},
            {rating:"5", css: ""},
        ];


        tScope.ratingStarClick = function(star, $index){
            tScope.OverTender.rating = star.rating;
            angular.forEach(tScope.ratingStarList, function(star, key) {
                if(key<= $index){
                    star.css = "ratingStarChacked";
                }
                else{
                    star.css = "";
                }
            });
            tScope.overTenderCheck.emptyRating = false;
        };


        tScope.consOverTender = function (form){


            if(tScope.OverTender.rating == "") {
                tScope.overTenderCheck.emptyRating = true;
            }

            if(form.$invalid || tScope.overTenderCheck.emptyRating) return false;

            tenderService.promiseTender().then(function() {

                tScope.OverTender.tender_id = tenderService.getTender().id;


                $http.post("/consumer_over_tender", tScope.OverTender)
                    .then(function (message) {
                        var data = message.data;
                        if (data.status == '3000') {
                            tScope.OverTender = {};
                            $scope.$emit('errorAlertMessage', {text: "Тендер успешно завершен", status: "success"});
                            $location.path("/user_page");
                        } else {
                            var html = "<p><b>" + data.text + "</b></p>";
                            if (data.hasErrors = 'true') {
                                angular.forEach(data.errors, function (val, key) {
                                    html += "<p>" + val.errorText + "</p>";
                                });
                            }
                            $scope.$emit('errorAlertMessage', {text: html, status: "warning"});
                        }

                    });

            });
        };




    }]);

