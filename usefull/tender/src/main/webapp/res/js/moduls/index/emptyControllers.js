/**
 * Created by serdukov on 24.02.16.
 */
app.controller('qualityWarrantyController',['$scope', '$location', '$anchorScroll' , function ($scope, $location, $anchorScroll) {
    $location.hash('quality');
    $anchorScroll();
}]);


app.controller('howItWorkController',['$scope', '$location', '$anchorScroll' , '$interval', function ($scope, $location, $anchorScroll, $interval) {
    $location.hash('how_it_works');
    $anchorScroll();


    var howIt = this;
    
    howIt.Shemalist = new Array();

    howIt.Shemalist.push({
        id:"1",
        message: "Делаете фотографии или можете вызвать нашего <a href='mailto:UBETEN&lt;info@ubeten.com&gt;?subject=Вызов эксперта'>бесплатного эксперта</a>",
        img:"/res/img/shema/what0.png",
        selected: true
    });

    howIt.Shemalist.push({
        id:"2",
        message:"Вы заполняете заявку на тендер",
        img:"/res/img/shema/what1.png",
        selected: false
    });

    howIt.Shemalist.push({
        id:"3",
        message:"Получаете предложения от различных исполнителей",
        img:"/res/img/shema/what2.png",
        selected: false
    });

    howIt.Shemalist.push({
        id:"4",
        message:"Вы сравниваете предложения по рейтингу исполнителей и цене",
        img:"/res/img/shema/what3.png",
        selected: false
    });

    howIt.Shemalist.push({
        id:"5",
        message:"Вы делаете окончательный выбор и принимаете предложение. Выбранный исполнитель свяжется с Вами. ",
        img:"/res/img/shema/what4.png",
        selected: false
    });

    howIt.Shemalist.push({
        id:"6",
        message:"Исполнитель приступает к работе и по факту выполнения тендера Вы подтверждает завершение тендера",
        img:"/res/img/shema/what5.png",
        selected: false
    });

    howIt.currentShemaItem = howIt.Shemalist[0];

    howIt.setShemaItem = function($index){
        howIt.currentShemaItem.selected = false;
        howIt.currentShemaItem = howIt.Shemalist[$index];
        howIt.currentShemaItem.selected = true;
    }


    var indexShema = 1;


    howIt.setShemaItemView = function($index){
        howIt.setShemaItem($index);
        indexShema=$index;
    }


    $interval(function(){
        howIt.setShemaItem(indexShema);
        indexShema++;
        if(indexShema == howIt.Shemalist.length){
            indexShema = 0;
        }
    }, 4000);

    
}]);

app.controller('aboutController',['$scope', '$location', '$anchorScroll' , function ($scope, $location, $anchorScroll) {
    $location.hash('about');
    $anchorScroll();
}]);


