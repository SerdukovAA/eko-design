package com.tender.webapp.models;

import javax.persistence.*;

/**
 * Created by Serdukov on 05.06.2015.
 */
@Entity
@Table(name = "user_status")
public class UserStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "user_status", unique = true, nullable = false)
    private String user_status;

    /*--------getters and setters-----*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

}
