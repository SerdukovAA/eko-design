package com.tender.webapp.models.journal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "refil_transaction_journal",uniqueConstraints= @UniqueConstraint(columnNames={"t_uniq", "user_id","transaction_status_id"}))
public class RefillTransactionJournal {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "t_uniq")
    private RefillTransactionJournal tUniq;


    @Column(name = "date_create", unique = false, nullable = true)
    private Date dateCreate;

    @ManyToOne
    @JoinColumn(name = "transaction_status_id")
    private TransactionStatus transactionStatus;

    @Column(name = "order_sum_amount", unique = false, nullable = true)
    private BigDecimal orderSumAmount;

    @Column(name = "order_money_code", unique = false, nullable = true)
    private int orderSumCurrencyPaycash;

    @Column(name = "shop_sum_amount", unique = false, nullable = true)
    private BigDecimal shopSumAmount;

    @Column(name = "shop_money_code", unique = false, nullable = true)
    private int shopSumCurrencyPaycash;


    @Column(name = "invoice_id", unique = false, nullable = true)
    private long invoiceId;

    //customerNumber
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


    @Column(name = "payment_type", unique = false, nullable = true)
    private String paymentType;



    /*--getters and setters-*/


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RefillTransactionJournal gettUniq() {
        return tUniq;
    }

    public void settUniq(RefillTransactionJournal tUniq) {
        this.tUniq = tUniq;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public BigDecimal getOrderSumAmount() {
        return orderSumAmount;
    }

    public void setOrderSumAmount(BigDecimal orderSumAmount) {
        this.orderSumAmount = orderSumAmount;
    }

    public int getOrderSumCurrencyPaycash() {
        return orderSumCurrencyPaycash;
    }

    public void setOrderSumCurrencyPaycash(int orderSumCurrencyPaycash) {
        this.orderSumCurrencyPaycash = orderSumCurrencyPaycash;
    }

    public BigDecimal getShopSumAmount() {
        return shopSumAmount;
    }

    public void setShopSumAmount(BigDecimal shopSumAmount) {
        this.shopSumAmount = shopSumAmount;
    }

    public int getShopSumCurrencyPaycash() {
        return shopSumCurrencyPaycash;
    }

    public void setShopSumCurrencyPaycash(int shopSumCurrencyPaycash) {
        this.shopSumCurrencyPaycash = shopSumCurrencyPaycash;
    }

    public long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }


}
