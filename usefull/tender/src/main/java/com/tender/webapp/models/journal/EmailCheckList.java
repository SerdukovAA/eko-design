package com.tender.webapp.models.journal;

import com.tender.webapp.models.PrivateInfo;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "email_check_list")
public class EmailCheckList {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(name = "user_email", unique = false, nullable = false)
    private String userEmail;

    @Column(name = "check_hash", unique = false, nullable = false)
    private String checkHash;

    @OneToOne
    @JoinColumn(name = "private_id")
    private PrivateInfo privateInfo;

    @Column(name = "is_email_check", nullable = true)
    private boolean emailCheck;

    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;


    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;



    /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getCheckHash() {
        return checkHash;
    }

    public void setCheckHash(String checkHash) {
        this.checkHash = checkHash;
    }

    public PrivateInfo getPrivateInfo() {
        return privateInfo;
    }

    public void setPrivateInfo(PrivateInfo privateInfo) {
        this.privateInfo = privateInfo;
    }

    public boolean isEmailCheck() {
        return emailCheck;
    }

    public void setEmailCheck(boolean emailCheck) {
        this.emailCheck = emailCheck;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }

}
