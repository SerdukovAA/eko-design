package com.tender.webapp.biznes;


import com.tender.webapp.controllers.service_bean.CancelTenderRequest;
import com.tender.webapp.controllers.service_bean.OverTenderRequest;
import com.tender.webapp.controllers.service_bean.TenderFormObject;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Created by Serdukov on 05.06.2015.
 */
public interface ServiceTender {

    public Tender getTenderByTenderId(User user, int tender_id)throws Exception;
    public Tender getUniqTenderByTenderId(int tender_id)throws Exception;

    public Tender getMyTenderByTenderId(int user_id, int tender_id)throws Exception;

    public List<Tender> getTendersForOwner(int user_owner_id)throws Exception;

    public List<Tender> getTendersForProvider(int user_id)throws Exception;

    public List<Tender> getTendersWinForProvider(int provider_id)throws Exception;
    public List<Tender> getTendersWithOfferProvider(int provider_id)throws Exception;

    public void createAndSaveNewTender(TenderFormObject tenderFormObject, HttpServletRequest request) throws Exception;
    public void editExistTender(TenderFormObject tenderFormObject, HttpServletRequest request) throws Exception;

    public void cancelTender(CancelTenderRequest cancelTenderRequest, User user) throws Exception;

    public void consumerOverTender(OverTenderRequest overTenderRequest, User user) throws Exception;

    public void providerOverTender( OverTenderRequest overTenderRequest, User user) throws Exception;





}
