package com.tender.webapp.dao;

import com.tender.webapp.models.User;
import com.tender.webapp.models.handbook.TenderStatus;
import com.tender.webapp.models.journal.PasswordRecoveryList;

/**
 * Created by serdukov on 31.10.15.
 */
public interface PasswordRecoveryListDAO {

    public PasswordRecoveryList save(User user, String hash) throws  Exception;
    public PasswordRecoveryList getById(int rec_id) throws  Exception;
    public void updateDate(PasswordRecoveryList passwordRecoveryList) throws  Exception;
}
