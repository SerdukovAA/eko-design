package com.tender.webapp.models.handbook;

import com.tender.webapp.models.User;

import javax.persistence.*;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "user_parameter")
public class UserParameter {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    /**
     * Желает ли пользователь в принципе получать почту от сервиса
     */
    @Column(name = "mail_all", unique = false)
    private Boolean mailAll;

    /**
     * Желает ли получать уведомления о новых комментариях
     */
    @Column(name = "mail_comment", unique = false)
    private Boolean mailComment;

    /**
     * Желает ли получать уведомления о новых тендерах
     */
    @Column(name = "mail_tender", unique = false)
    private Boolean mailTender;


    /**
     * Желает ли получать уведомления о новых предложениях по тендерам
     */
    @Column(name = "mail_offer", unique = false)
    private Boolean mailOffer;


 ///-------------getters and setters------------------------------/


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean isMailAll() {
        return mailAll;
    }

    public void setMailAll(Boolean mailAll) {
        this.mailAll = mailAll;
    }

    public Boolean isMailComment() {
        return mailComment;
    }

    public void setMailComment(Boolean mailComment) {
        this.mailComment = mailComment;
    }

    public Boolean isMailTender() {
        return mailTender;
    }

    public void setMailTender(Boolean mailTender) {
        this.mailTender = mailTender;
    }

    public Boolean isMailOffer() {
        return mailOffer;
    }

    public void setMailOffer(Boolean mailOffer) {
        this.mailOffer = mailOffer;
    }



}
