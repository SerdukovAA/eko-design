package com.tender.webapp.biznes;


import com.tender.webapp.models.Offer;
import com.tender.webapp.models.PrivateInfo;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;

/**
 * Created by Serdukov on 05.06.2015.
 */
public interface EmailService {

    public void createNewEmailCheckNote(PrivateInfo privateInfo) throws Exception;
    public void createEmailAboutCredit(Tender tender, User  userOwner ) throws Exception;
    public void senMailOnPasswordReset(String email , String hash, int checkId) throws  Exception;
    public void sendAccepOfferMailToOfferProvider(Tender tender, Offer offer) throws Exception;
}
