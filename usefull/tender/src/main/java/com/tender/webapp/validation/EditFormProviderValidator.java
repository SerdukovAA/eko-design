package com.tender.webapp.validation;


import com.tender.webapp.biznes.ValidateService;
import com.tender.webapp.controllers.service_bean.ProviderFormObject;
import com.tender.webapp.models.service.ServerMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Serdukov on 06.06.2015.
 */
@Component
public class EditFormProviderValidator {

    @Autowired
    ValidateService validateService;


    //TODO нужно объединить два валидатора что бы рефакторить бщие методы. Пусть будет один ProviderFormValidator. И два метода валидации для нового и для редакции

    public void validate(ProviderFormObject providerFormObject, ServerMessage serverMessage) throws Exception {


        if (providerFormObject.getFirstNameContactPerson() == null || providerFormObject.getFirstNameContactPerson().trim().isEmpty()) {
            serverMessage.addError(30, "Не передано имя контактоного лица компании");
            if (!RegExpCheck.cyrillicNameRegExCheck(providerFormObject.getFirstNameContactPerson())) {
                serverMessage.addError(31, "Передаваемое имя контактного лица не корректно. Используйте кирилицу");
            }
        }


        if (providerFormObject.getLastNameContactPerson() == null || providerFormObject.getLastNameContactPerson().trim().isEmpty()) {
            serverMessage.addError(32, "Не передана фамилия контактного лица компании");
            if (!RegExpCheck.cyrillicNameRegExCheck(providerFormObject.getLastNameContactPerson())) {
                serverMessage.addError(33, "Передаваемая фамилия контактного лица не корректна. Используйте кирилицу");
            }
        }



        if (providerFormObject.getPhoneNumberContactPerson() == null || providerFormObject.getPhoneNumberContactPerson().trim().isEmpty()) {
            serverMessage.addError(35, "Не передан номер телефона для контактного лица");
            if (!RegExpCheck.phoneRegExCheck(providerFormObject.getPhoneNumberContactPerson())) {
                serverMessage.addError(36, "Передаваемый номер телефона не действителен");
            }
        }

        if (providerFormObject.getCompanyName() == null || providerFormObject.getCompanyName().trim().isEmpty()) {
            serverMessage.addError(37, "Не передано наименование компании");
        }


        if (providerFormObject.getInn() == null || providerFormObject.getInn().trim().isEmpty()) {
            serverMessage.addError(40, "Не передан ИНН компании");
        } else {
            if (!RegExpCheck.innRegExCheck(providerFormObject.getInn())) {
                serverMessage.addError(57, "ИНН не валидный");
            }
        }

        if(providerFormObject.getAdress() ==null) {
            serverMessage.addError(42, "Не передан адрес компании");
        }else{
            if (providerFormObject.getAdress().getRegion() == null ) {
                serverMessage.addError(42, "Не передан регион фактического адреса компании");
            }

            if (providerFormObject.getAdress().getStreet() == null || providerFormObject.getAdress().getStreet().trim().isEmpty()) {
                serverMessage.addError(42, "Не передана улица фактического адреса компании");
            }

            if (providerFormObject.getAdress().getHouseNumber() == null || providerFormObject.getAdress().getHouseNumber().trim().isEmpty()) {
                serverMessage.addError(42, "Не передан номер дома фактического адреса компании");
            }


            if(providerFormObject.getAdress().getLongitude()==0.0d){
                serverMessage.addError(42, "Не передана Longitude адреса компании");
            }
            if(providerFormObject.getAdress().getLatitude()==0.0d){
                serverMessage.addError(42, "Не передана Latitude адреса компании");
            }


            if (providerFormObject.getAdress().getFullAdress() == null || providerFormObject.getAdress().getFullAdress().trim().isEmpty()) {
                serverMessage.addError(42, "Не передан фактический адрес компании");
            }

        }


        if (providerFormObject.getP_login() == null || providerFormObject.getP_login().isEmpty()) {
            serverMessage.addError(9, "Передан пустой логин");
        } else {

            String login = providerFormObject.getP_login().trim().toLowerCase();
            if (!RegExpCheck.loginRegExCheck(login)) {
                serverMessage.addError(11, "Переданый логин некоректный. Используйте латиницу");
            }
        }

        if (providerFormObject.getEmailContactPerson() == null || providerFormObject.getEmailContactPerson().trim().isEmpty()) {
            serverMessage.addError(54, "Не передан адрес электронной почты контактного лица компании");
        } else {
            String email = providerFormObject.getEmailContactPerson().toLowerCase();
            if (!RegExpCheck.emailRegExCheck(email)) {
                serverMessage.addError(56, "Переданый email не дейстивтелен");
            }
        }
    }
}