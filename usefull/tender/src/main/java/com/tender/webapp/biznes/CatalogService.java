package com.tender.webapp.biznes;


import com.tender.webapp.models.base.AutoBrand;
import com.tender.webapp.models.base.Region;

import java.util.List;

/**
 * Created by Serdukov on 05.06.2015.
 */
public interface CatalogService {

   public List<AutoBrand> getModelBrands() throws Exception;
   public List<Region> getRegions() throws Exception;


}
