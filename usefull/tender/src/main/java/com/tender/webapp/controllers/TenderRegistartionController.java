package com.tender.webapp.controllers;


import com.tender.webapp.biznes.FotoService;
import com.tender.webapp.biznes.ServiceTender;
import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.controllers.service_bean.TenderFormObject;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;
import com.tender.webapp.models.service.Error;
import com.tender.webapp.models.service.ServerMessage;
import com.tender.webapp.utils.impl.GenereteHashUtil;
import com.tender.webapp.validation.EditTenderValidator;
import com.tender.webapp.validation.TenderValidator;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Serdukov on 14.06.2015.
 */
@Controller
@PropertySource("classpath:tenderApp.properties")
public class TenderRegistartionController {

    @Autowired
    Environment env;
    @Autowired
    TenderValidator tenderValidator;
    @Autowired
    EditTenderValidator editTenderValidator;
    @Autowired
    ServiceTender serviceTender;
    @Autowired
    ServiceUser serviceUser;

    @Autowired
    FotoService fotoService;


    private static final Logger log = Logger.getLogger(TenderRegistartionController.class);


    /**
     * Контроллер запроса тендера на редактирование
     * @param request
     * @return
     */
    @RequestMapping(value = "/consumer_get_tender", method = RequestMethod.GET)
    public @ResponseBody
    Tender showTenderById(HttpServletRequest request) {
        int tender_id = Integer.parseInt(request.getParameter("tender_id"));
        User user = (User) request.getSession().getAttribute("user");
        try{
            Tender tender = serviceTender.getTenderByTenderId(user, tender_id);
            if(user.getUser_id() != tender.getUserOwner().getUser_id() && user.getUser_id() !=0 ){
                throw new Exception("Вы не имеете прав на редактирование данного тендера id"+ user.getUser_id());
            }
            return tender;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Произошла ошибка при запросе юзером тендера. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", e);
            return null;
        }
    }








    @RequestMapping(value = "/upload_foto", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> uploadFoto(HttpServletRequest request, @RequestParam(value = "objectFotos") List<MultipartFile> objectFotos) throws Exception {
        System.out.println("server id" + env.getProperty("server.id"));
        log.info(" id сессии: "+request.getSession().getId()+". Загрузка фото к тендеру");
        return  fotoService.uploadTenderFoto(objectFotos);
    }




    @RequestMapping(value = "/upload_file", method = RequestMethod.POST)
    public @ResponseBody
    Map<String,String> uploadFile(HttpServletRequest request, @RequestParam(value = "additionalFiles") List<MultipartFile> additionalFiles) throws Exception {

        Map<String,String> urlFileList = new HashMap<String, String>();
        System.out.println("server id" +env.getProperty("server.id"));
        log.info(" id сессии: "+request.getSession().getId()+". Загрузка файлов к тендеру");
        int size = additionalFiles.size();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM");
        String date = sdf.format(new Date());
        for (int i = 0; i < size; i++) {
            MultipartFile file = additionalFiles.get(i);
            File dir = new File(System.getProperty("user.home") + "/tender_files/files_for_tenders/"+ date+"/");
            if (!dir.exists())
            {
                dir.mkdirs();
            }

            String fileName = GenereteHashUtil.genHash(file.getOriginalFilename() + i + new Date()) + "." + FilenameUtils.getExtension(file.getOriginalFilename());

            if (!file.isEmpty()) {
                try {
                    byte[] bytes = file.getBytes();
                    BufferedOutputStream stream =
                            new BufferedOutputStream(new FileOutputStream(new File(System.getProperty("user.home") + "/tender_files/files_for_tenders/"+ date+"/" + fileName)));
                    stream.write(bytes);
                    stream.close();
                    String fileLink = env.getProperty("server.id")+"/tender_files/files_for_tenders/"+ date+"/" + fileName;
                    urlFileList.put(file.getOriginalFilename(), fileLink);
                } catch (IOException e) {
                    throw new Exception("Ошибка сохранения файла. - " + e.getMessage());
                }
            } else {
                throw new Exception("Переданный файл пуст");
            }
        }
        return urlFileList;
    }




   @RequestMapping(value = "/tender_registration", method = RequestMethod.POST)
    public @ResponseBody ServerMessage  doRegistrationTender(@RequestBody TenderFormObject tenderFormObject, HttpServletRequest request) throws Exception {

     ServerMessage serverMessage = new ServerMessage();
     log.info(" id сессии: " + request.getSession().getId() + ".Регистрация нового тендера");

     tenderValidator.validate(tenderFormObject, request, serverMessage);
          if(serverMessage.isHasErrors()){
            serverMessage.setStatus(1);
            serverMessage.setText("Переданы некорректные данные в заявке на тендер");
            log.info("Переданы некорректные данные в заявке на тендер. "+request.getSession().getId());
            for (Error error:serverMessage.getErrors()){
                log.info(" id сессии: "+request.getSession().getId()+" - "+error.getErrorText());
            }
            return serverMessage;
          }else {
            try {
                serviceTender.createAndSaveNewTender(tenderFormObject, request);
                serverMessage.setStatus(3000);
                serverMessage.setText("Новый тендер успешно отгружен.");
                log.info(" id сессии: "+request.getSession().getId()+". Новый тендер успешно отгружен.");
                return serverMessage;
            } catch (Exception e) {
                e.printStackTrace();
                serverMessage.setStatus(3);
                serverMessage.setText("Произошла ошибка при сохранении тендера на сервере");
                serverMessage.addError(1, "Ошибка сервера: " + e.getMessage());
                log.error("Произошла ошибка при сохранении тендера на сервере. id сессии: ", e);
                log.info("Произошла ошибка при сохранении тендера на сервере. id сессии: "+request.getSession().getId()+" - Ошибка сервера: " + e.getMessage());
                return serverMessage;
            }
          }
    }



//редактирование тендера
@RequestMapping(value = "/consumer_edit_tender", method = RequestMethod.POST)
public @ResponseBody ServerMessage  doEditTender(@RequestBody TenderFormObject tenderFormObject, HttpServletRequest request) throws Exception {

    ServerMessage serverMessage = new ServerMessage();
    log.info(" id сессии: " + request.getSession().getId() + ".Редатирование тендера нового тендера");

    editTenderValidator.validate(tenderFormObject, request,serverMessage);

    if(serverMessage.isHasErrors()){
        serverMessage.setStatus(1);
        serverMessage.setText("Переданы некорректные данные при редактировании тендера");
        log.info("Переданы некорректные данные в заявке на тендер. "+request.getSession().getId());
        for (Error error:serverMessage.getErrors()){
            log.info(" id сессии: "+request.getSession().getId()+" - "+error.getErrorText());
        }
        return serverMessage;
    }else {
        try {
            serviceTender.editExistTender(tenderFormObject, request);
            serverMessage.setStatus(3000);
            serverMessage.setText("Изменения успешно сохранены");
            log.info(" id сессии: " + request.getSession().getId() + ". Изменения по тендеру успешно сохранены.");
            return serverMessage;
        } catch (Exception e) {
            e.printStackTrace();
            serverMessage.setStatus(3);
            serverMessage.setText("Произошла ошибка при редактировании тендера на сервере");
            serverMessage.addError(1,"Ошибка сервера: " + e.getMessage());
            log.info("Произошла ошибка при сохранении тендера на сервере. id сессии: "+request.getSession().getId()+" - Ошибка сервера: " + e.getMessage());
            return serverMessage;
        }
    }
}





    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }



}









