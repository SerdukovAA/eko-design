package com.tender.webapp.biznes;


import com.tender.webapp.controllers.service_bean.NewCommentRequest;
import com.tender.webapp.models.Message;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by Serdukov on 05.06.2015.
 */
public interface ServiceMessage {


    public Message createAndSaveNewMessage(NewCommentRequest newCommentRequest, HttpServletRequest  request) throws Exception;

}
