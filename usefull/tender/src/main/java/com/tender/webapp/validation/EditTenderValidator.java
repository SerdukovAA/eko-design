package com.tender.webapp.validation;

import com.tender.webapp.biznes.SelectorsService;
import com.tender.webapp.controllers.service_bean.ParametersRequest;
import com.tender.webapp.controllers.service_bean.TenderFormObject;
import com.tender.webapp.models.*;
import com.tender.webapp.models.handbook.*;
import com.tender.webapp.models.service.ServerMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Serdukov on 06.06.2015.
 */
@Component
public class EditTenderValidator {


    @Autowired
    SelectorsService selectorsService;

    private static final Logger log = Logger.getLogger(EditTenderValidator.class);

    public void validate(TenderFormObject tenderFormObject, HttpServletRequest request, ServerMessage serverMessage) {

        try {

            if(tenderFormObject.getId() == 0){
                serverMessage.addError(2, "Не передан идентификатор тендера для редактирования");
            }

            if (tenderFormObject.getTenderName().trim().isEmpty()) {
                serverMessage.addError(2, "Не передан заголовок для тендера");
            }

            if (tenderFormObject.getTenderDescription().trim().isEmpty()) {
                serverMessage.addError(2, "Не передано описание тендера");
            }

            //проверка общих параметров по тендеру
            for (CommonValue commonValue : tenderFormObject.getCommonValueList()) {
                if (commonValue.getParameterName().trim().isEmpty())
                    serverMessage.addError(2, "Не передано наименование одного из общих параметров тендера");
                if (commonValue.getCommonId() == 0)
                    serverMessage.addError(2, "Не передан идентификатор одного из общих параметров тендера");
                if (commonValue.getParameterValue().trim().isEmpty())
                    serverMessage.addError(2, "Не передан значение одного из общих параметров тендера");
            }




            for (TenderFile tenderFile : tenderFormObject.getTenderFileList()) {
                if (tenderFile.getFileLink().trim().isEmpty())
                    serverMessage.addError(2, "Не передано ссылка на один из дополнительных файлов по тендеру");
                if (tenderFile.getFileName().trim().isEmpty())
                    serverMessage.addError(2, "Не передан наименование одного из дополнительных файлов по тендеру");
            }


            try {
                BigDecimal exPrice = new BigDecimal(tenderFormObject.getExpectedPrice().replaceAll(" ", ""));
                exPrice = null;
            } catch (Exception e) {
                serverMessage.addError(2, "Ожидаемая цена по тендеру не валидна. Значение должно содержать числовое значение в рублях.");
            }



            if(tenderFormObject.getAdress() ==null) {
                serverMessage.addError(42, "Не передан адрес тендера");
            }else{
                if (tenderFormObject.getAdress().getRegion() == null ) {
                    serverMessage.addError(42, "Не передан регион адреса тендера");
                }

                if (tenderFormObject.getAdress().getStreet() == null || tenderFormObject.getAdress().getStreet().trim().isEmpty()) {
                    serverMessage.addError(42, "Не передана улица адреса тендера");
                }

                if (tenderFormObject.getAdress().getHouseNumber() == null || tenderFormObject.getAdress().getHouseNumber().trim().isEmpty()) {
                    serverMessage.addError(42, "Не передан номер дома адреса тендера");
                }

                if(tenderFormObject.getAdress().getLongitude()==0.0d){
                    serverMessage.addError(42, "Не передана Longitude адреса тендера");
                }
                if(tenderFormObject.getAdress().getLatitude()==0.0d){
                    serverMessage.addError(42, "Не передана Latitude адреса тендера");
                }

                if (tenderFormObject.getAdress().getFullAdress() == null || tenderFormObject.getAdress().getFullAdress().trim().isEmpty()) {
                    serverMessage.addError(42, "Не передан полный адрес  тендера");
                }

            }





            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            sdf.setLenient(false);
            Date dBeng = null;
            Date dEnd = null;

            String dateBegin = tenderFormObject.getDateBegin().trim();
            if (dateBegin.isEmpty()) {
                serverMessage.addError(2, "Не передана дата начала сроков проведения тендера");
            } else {
                try {
                    dBeng = sdf.parse(dateBegin);
                } catch (ParseException e) {
                    serverMessage.addError(2, "Переданая дата начала сроков проведения тендера не подходит по формату ");
                }
            }

            String dateEnd = tenderFormObject.getDateEnd().trim();
            if (dateEnd.isEmpty()) {
                serverMessage.addError(2, "Не передана дата окончания сроков проведения тендера");
            } else {
                try {
                    dEnd = sdf.parse(dateEnd);
                } catch (ParseException e) {
                    serverMessage.addError(2, "Переданая дата окончания сроков проведения тендера не подходит по формату ");
                }
            }

            if (dBeng != null && dEnd != null) {
                if (dBeng.compareTo(dEnd) > 0) {
                    serverMessage.addError(2, "Дата начала проведения тендера позже даты окончания.");
                }
                dBeng = null;
                dEnd = null;
            }



            //перед валидацией каждого объекта нужно забрать параметрыпо значениям селекторов и по эти параметра валидировать.
            // используются только не зависимые селекторы поэтому здесь просто


            List<ParametersRequest> parRequest = new ArrayList<ParametersRequest>();

            for (CategorySelector categorySelector : tenderFormObject.getTypeService().getCategorySelectors()) {
                if (categorySelector.getConsumerSelectorType() == "radio" && categorySelector.isDepended() == false) {
                    ParametersRequest parametersRequest = new ParametersRequest();
                    parametersRequest.setSelectorId(categorySelector.getId());
                    parametersRequest.setOptionId(categorySelector.getCheckedOptionId());
                    parRequest.add(parametersRequest);
                }
                if (categorySelector.getConsumerSelectorType() == "checkbox" && categorySelector.isDepended() == false) {

                    for(SelectorOption selectorOption : categorySelector.getSelectorOptions()){
                        if(selectorOption.isChecked()){
                            ParametersRequest parametersRequest = new ParametersRequest();
                            parametersRequest.setSelectorId(categorySelector.getId());
                            parametersRequest.setOptionId(selectorOption.getOptionId());
                            parRequest.add(parametersRequest);
                        }
                    }
                }
            }

            //зависимые параметры для объектов задаются на основании основных селекторов
            List<ParameterRelation> parameterRelations = selectorsService.getParametersRelations(parRequest);


            for (TenderObject tenderObject : tenderFormObject.getTenderObjects()) {

                    for (ObjectFoto objectFoto : tenderObject.getObjectFotos()) {
                        if (objectFoto.getFotoLink() == null || objectFoto.getFotoLink().trim().isEmpty()){
                            serverMessage.addError(2, "Не передана ссылка на фото по объекту тендера");
                        }

                    }

                    for(ParameterRelation parameterRelation : parameterRelations) {
                        for (ParameterRelation objectParameterRelation : tenderObject.getParameterRelations()) {
                            if (parameterRelation.getObjectParameter().getId() == objectParameterRelation.getObjectParameter().getId()) {

                                if (parameterRelation.isRequired()) {
                                    if (objectParameterRelation.getObjectParameter().getValue() == null || objectParameterRelation.getObjectParameter().getValue() .trim().isEmpty()) {
                                        serverMessage.addError(52, "Не передана обязательный параметр тендера");
                                    }
                                    if (!RegExpCheck.fieldRegExCheck(parameterRelation.getObjectParameter().getParameterRegEx(),objectParameterRelation.getObjectParameter().getValue())) {
                                        serverMessage.addError(53, "Значение переданного параметра ("+parameterRelation.getObjectParameter().getParameterName()+") для объекта не валидно");
                                    }

                                } else {
                                    if (!objectParameterRelation.getObjectParameter().getValue().trim().isEmpty()) {
                                        if (!RegExpCheck.fieldRegExCheck(parameterRelation.getObjectParameter().getParameterRegEx(), objectParameterRelation.getObjectParameter().getValue())) {
                                            serverMessage.addError(54, "Значение переданного параметра объекта не валидно");
                                        }
                                    }
                                }

                            }
                        }
                    }
            }


        } catch (Exception ex) {
            ex.printStackTrace();
            serverMessage.setHasErrors(true);
            log.info(" id сессии: " + request.getSession().getId() + ". Ошибка при валидации нового тендера: " + ex.getMessage());
        }
    }


}