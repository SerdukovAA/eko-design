package com.tender.webapp.controllers.service_bean;



/**
 * Created by Serdukov on 03.09.2015.
 */

/**
 * Объект завершения тендера. Когда заказчик получает услугу по тендеру он завершает тендер. Выставляя исполнителю рейтинг и комментируя его работу
 */
public class OverTenderRequest {



    private int tender_id = 0;
    private String rating;
    private String comment;


/*-------getters and setters------------------------*/

    public int getTender_id() {
        return tender_id;
    }

    public void setTender_id(int tender_id) {
        this.tender_id = tender_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }



}
