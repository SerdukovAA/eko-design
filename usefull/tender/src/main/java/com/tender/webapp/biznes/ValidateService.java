package com.tender.webapp.biznes;


/**
 * Created by Serdukov on 05.06.2015.
 */
public interface ValidateService {

    public boolean checkPhoneNumberExist(String phoneNumber) throws Exception;

    public boolean checkLoginExist(String login) throws Exception;
    public boolean checkEmailExist(String email) throws Exception;
    public boolean checkINNExist(String inn) throws Exception;
    public boolean checkPhoneCode(String id, String code) throws Exception;


}
