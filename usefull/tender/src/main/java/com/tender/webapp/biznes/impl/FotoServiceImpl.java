package com.tender.webapp.biznes.impl;


import com.tender.webapp.biznes.FotoService;
import com.tender.webapp.utils.impl.GenereteHashUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Created by Serdukov on 05.06.2015.
 */
public class FotoServiceImpl implements FotoService {

    @Autowired
    Environment env;

    private final String FOTO_FOLDER ="/tender_files/foto_for_tenders/";


    public Map<String, String> uploadTenderFoto(List<MultipartFile> objectFotos) throws Exception{

        Map<String, String> urlFotoList = new HashMap<String, String>();

        int size = objectFotos.size();


        //проверяем существование папки для загрузки фото если нет то создаем ее
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM");
        String date = sdf.format(new Date());

        String date_foto_folder = FOTO_FOLDER+date+"/";

        String srcDir = System.getProperty("user.home")+date_foto_folder;
        File dir = new File(srcDir);
        if (!dir.exists())
        {
            dir.mkdirs();
        }


        for (int i = 0; i < size; i++) {
            MultipartFile file = objectFotos.get(i);


            if (!file.isEmpty()) {

                String fileName = GenereteHashUtil.genHash(file.getOriginalFilename() + i + new Date()) + "." + FilenameUtils.getExtension(file.getOriginalFilename());
                String fileNameMin = GenereteHashUtil.genHash(file.getOriginalFilename() + i + new Date()) + "_min." + FilenameUtils.getExtension(file.getOriginalFilename());

                String fullImagePath = srcDir + fileName;
                String minImagePath = srcDir + fileNameMin;
                try {

                    //сохраняем фотку0
                    byte[] bytes = file.getBytes();
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(fullImagePath)));
                    stream.write(bytes);
                    stream.close();
                    String fileLink = env.getProperty("server.id")+date_foto_folder+ fileName;
                    String fileLinkMin = env.getProperty("server.id")+date_foto_folder+ fileNameMin;

                    //теперь когда фотка сохранена мы создаем минифицированную версию
                    resize(fullImagePath,minImagePath);
                    //если все прошло нормально то мы возвращаем  ссылки на фотки
                    urlFotoList.put(fileLinkMin, fileLink);
                } catch (IOException e) {
                    throw new Exception("Ошибка сохранения фотограффии. - " + e.getMessage());
                }
            } else {
                throw new Exception("Переданный файл пуст");
            }
        }
        return urlFotoList;

    }

    public static void resize(String inputImagePath, String outputImagePath) throws IOException {

        File inputFile = new File(inputImagePath);
        //230 нужно
        int scaledHeight = 230;

        BufferedImage inputImage = ImageIO.read(inputFile);
        double percent = (double)scaledHeight / (double) inputImage.getHeight();
        int scaledWidth = (int) (inputImage.getWidth() * percent);

        resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
    }


    public static void resize(String inputImagePath, String outputImagePath, int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);

        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        // writes to output file
        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }


}
