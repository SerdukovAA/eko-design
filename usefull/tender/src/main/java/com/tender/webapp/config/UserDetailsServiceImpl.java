package com.tender.webapp.config;
import java.util.ArrayList;
import java.util.List;

import com.tender.webapp.models.User;
import com.tender.webapp.models.UserRole;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService{


    @Resource
    protected SessionFactory sessionFactory;

    @Transactional(readOnly=true)
    public UserDetails loadUserByUsername(String login)  throws UsernameNotFoundException {

        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from User where login = :login");
        query.setParameter("login", login.trim().toLowerCase());
        User user = (User) query.uniqueResult();
        session.close();
        if(user==null){
            System.out.println("User not found");
            throw new UsernameNotFoundException("Username not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(),
                user.getUserStatus().getId()== 1 , true, true, true, getGrantedAuthorities(user));
    }


    private List<GrantedAuthority> getGrantedAuthorities(User user){
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        UserRole userRole = user.getUserRole();
        authorities.add(new SimpleGrantedAuthority("ROLE_"+userRole.getUser_role()));
        return authorities;
    }

}