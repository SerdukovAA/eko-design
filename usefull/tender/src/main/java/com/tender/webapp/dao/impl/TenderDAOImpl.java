package com.tender.webapp.dao.impl;

import com.tender.webapp.dao.TenderDAO;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.handbook.TenderStatus;
import org.hibernate.*;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by serdukov on 01.11.15.
 */
public class TenderDAOImpl implements TenderDAO {

    @Resource
    protected SessionFactory sessionFactory;

    //ид статуса активных тендров
    final List<Integer> ACTIVE_TENDER_STATUS =   Arrays.asList(2,3,4);


    public List<Integer> getACTIVE_TENDER_STATUS(){
        return  ACTIVE_TENDER_STATUS;
    }

    //------------отмена тендера--------------------////
    public Tender cancelTender(int user_id, int tender_id) throws Exception{
        Session session = sessionFactory.getCurrentSession();

        Tender tender = (Tender) session.get(Tender.class, tender_id);
        if (tender.getUserOwner().getUser_id() != user_id) {
            throw new Exception("Попытка отмены не своего тендера");
        }
        if(tender.getTenderStatus().getId() == 6){
            throw new Exception("Тендер был ранее отозван");
        }

        if(tender.getTenderStatus().getId() == 3 ){
            throw new Exception("Нельзя отозвать исполняемый тендер");
        }

        //если проверки пройдены, отменяем тендер
        tender.setDateChange(new Date());
        tender.setTenderStatus((TenderStatus) session.get(TenderStatus.class, 6));
        session.update(tender);
        return tender;
    }


    public Tender getActiveTenderById(int tender_id)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        Criteria sc = session.createCriteria(Tender.class);
        sc.add(Restrictions.eq("id", tender_id));
        sc.add(Restrictions.in("tenderStatus.id", ACTIVE_TENDER_STATUS));
        sc.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
       return (Tender)sc.uniqueResult();
    }


    /**
     * Возращаем активные тендеры для собственника
     *
     * @param user_id
     * @return
     * @throws Exception
     */
    public List<Tender> getActivTendersByUserId(int user_id)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        Criteria sc = session.createCriteria(Tender.class);
        sc.add(Restrictions.eq("userOwner.user_id", user_id));
        sc.add(Restrictions.in("tenderStatus.id", ACTIVE_TENDER_STATUS));
        sc.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        List<Tender> tenders = sc.list();
        return tenders;
    }

    public List<Tender> getAllNotMyRunTenders(int my_user_id){
        Session session = sessionFactory.getCurrentSession();
        Criteria sc = session.createCriteria(Tender.class);
        sc.add(Restrictions.ne("userOwner.user_id", my_user_id));
        sc.add(Restrictions.eq("tenderStatus.id", 2));
        sc.addOrder(Order.desc("dateCreate"));
        sc.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        List<Tender> tenders = sc.list();
        return tenders;
    }

    public List<Tender> getAllRunTenders(int page_num){
        Session session = sessionFactory.openSession();
        page_num--;
        Query query = session.createQuery("from Tender where tenderStatus.id in (1,2,3,4)  ORDER BY date_create_tender");
        query.setFirstResult(0 + 10 * page_num);
        query.setMaxResults(10);
        List<Tender> tenders = query.list();
        session.close();
        return tenders;
    }

    public Long getAllRunTendersCount(){
        Session session = sessionFactory.openSession();
        Criteria sc = session.createCriteria(Tender.class);
        sc.add(Restrictions.in("tenderStatus.id", ACTIVE_TENDER_STATUS));
        sc.setProjection(Projections.rowCount());
        Long res = (Long) sc.uniqueResult();
        session.close();
        return res;
    }


    public List<Tender> getTendersWinForProvider(int provider_id){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Tender where tender_id in (select offer.tender from Offer offer where offer.provider.user_id = :provider_id and offer.offerStatus.id =2) and tender_status_id = 3 and user_owner_id != :provider_id");
        query.setParameter("provider_id", provider_id);
        List<Tender> tenderList = query.list();
        return tenderList;
    }

    /**
     * Возвращаем тендеры для исполнителя, в который исполнитель принимает участие. Т.е тендеры для которых данный исполнительн сделал офферы
     * @param provider_id
     * @return
     */
    public List<Tender> getTendersWithOfferProvider(int provider_id){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Tender where tender_id in(select offer.tender from Offer offer where offer.provider.user_id = :provider_id and offer.offerStatus.id =1 ) and tender_status_id = 2 and user_owner_id != :provider_id");
        query.setParameter("provider_id", provider_id);
        List<Tender> tenderList = query.list();
        return tenderList;
    }




    public List<Tender> getTendersForProvider(int provider_id)throws Exception{
        //единственный параметр только идентификатор исполнителя
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("SELECT * FROM get_tenders_for_provider(:provider_id)");
        query.setInteger("provider_id", provider_id);
        List<Integer> tenders_id = query.list();

        if(tenders_id.size() == 0) return  new ArrayList<Tender>();

        Query query2 = session.createQuery("FROM Tender t where t.id in (:t_ids)");
        query2.setParameterList("t_ids", tenders_id);
        List<Tender> tendersList = query2.list();
        return tendersList;
    }



}
