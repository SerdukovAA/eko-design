package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.OfferStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Serdukov on 02.06.2015.
 */
@Entity
@Table(name="offers")
public class Offer {

    @Id
    @Column(name="offer_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;


    @Column(name = "offer_uniq", unique = false, nullable = true)
    private String offerUniq;

    @Column(name = "offer_description",columnDefinition="TEXT", unique = false, nullable = true)
    private String offerDescription;

    @Column(name = "bonus_description",columnDefinition="TEXT", unique = false, nullable = true)
    private String bonusDescription;

    @Column(name="have_bonus",unique=false)
    private boolean haveBonus;

    @ManyToOne
    @JoinColumn(name="tender_id",nullable=false)
    @JsonIgnore
    private Tender tender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="provider_id",nullable=true)
    private User provider;

    @Column(name = "date_create_offer", unique = false, nullable = true)
    private Date dateCreate;

    @Column(name = "date_change_offer", unique = false, nullable = true)
    private Date dateChange;

    @Column(name = "date_begin_offer", unique = false, nullable = true)
    private Date dateBegin;

    @Column(name = "date_end_offer", unique = false, nullable = true)
    private Date dateEnd;

    @Column(name = "price_sum_offer", unique = false, nullable = true)
    private BigDecimal price;


    @Column(name = "work_price_offer", unique = false, nullable = true)
    private BigDecimal priceWork;

    @Column(name = "material_price_offer", unique = false, nullable = true)
    private BigDecimal priceMaterial;


    ///адресация тендера
    @Column(name = "full_adress", unique = false)
    private String fullAdress;

    @Column(name = "region_code", unique = false)
    private String regionCode;


    @Column(name = "rayon_code", unique = false)
    private String rayonCode;


    @Column(name = "city_code", unique = false)
    private String cityCode;


    @Column(name = "street", unique = false)
    private String street;

    @Column(name = "house_number", unique = false)
    private String houseNumber;



    @ManyToOne
    @JoinColumn(name = "offer_status_id")
    private OfferStatus offerStatus;

    @Column(name = "offer_place_latitude", unique = false, nullable = true)
    private double offerPlaceLatitude;

    @Column(name = "offer_place_longitude", unique = false, nullable = true)
    private double offerPlaceLongitude;

    @Column(name = "is_correction", unique = false, nullable = false)
    private boolean isCorrection;

    /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getOfferUniq() {
        return offerUniq;
    }

    public void setOfferUniq(String offerUniq) {
        this.offerUniq = offerUniq;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }

    public User getProvider() {
        return provider;
    }

    public void setProvider(User provider) {
        this.provider = provider;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public OfferStatus getOfferStatus() {
        return offerStatus;
    }

    public void setOfferStatus(OfferStatus offerStatus) {
        this.offerStatus = offerStatus;
    }

    public double getOfferPlaceLatitude() {
        return offerPlaceLatitude;
    }

    public void setOfferPlaceLatitude(double offerPlaceLatitude) {
        this.offerPlaceLatitude = offerPlaceLatitude;
    }

    public double getOfferPlaceLongitude() {
        return offerPlaceLongitude;
    }

    public void setOfferPlaceLongitude(double offerPlaceLongitude) {
        this.offerPlaceLongitude = offerPlaceLongitude;
    }


    public boolean isHaveBonus() {
        return haveBonus;
    }

    public void setHaveBonus(boolean haveBonus) {
        this.haveBonus = haveBonus;
    }

    public String getBonusDescription() {
        return bonusDescription;
    }

    public void setBonusDescription(String bonusDescription) {
        this.bonusDescription = bonusDescription;
    }


    public boolean isCorrection() {
        return isCorrection;
    }

    public void setIsCorrection(boolean isCorrection) {
        this.isCorrection = isCorrection;
    }

    public BigDecimal getPriceWork() {
        return priceWork;
    }

    public void setPriceWork(BigDecimal priceWork) {
        this.priceWork = priceWork;
    }

    public BigDecimal getPriceMaterial() {
        return priceMaterial;
    }

    public void setPriceMaterial(BigDecimal priceMaterial) {
        this.priceMaterial = priceMaterial;
    }

    public String getFullAdress() {
        return fullAdress;
    }

    public void setFullAdress(String fullAdress) {
        this.fullAdress = fullAdress;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }


    public String getRayonCode() {
        return rayonCode;
    }

    public void setRayonCode(String rayonCode) {
        this.rayonCode = rayonCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }
}
