package com.tender.webapp.controllers;


import com.tender.webapp.biznes.*;
import com.tender.webapp.controllers.service_bean.ParametersRequest;
import com.tender.webapp.models.*;
import com.tender.webapp.models.base.AutoBrand;
import com.tender.webapp.models.base.AutoModel;
import com.tender.webapp.models.base.Region;
import com.tender.webapp.models.handbook.ParameterRelation;
import com.tender.webapp.models.handbook.TenderStatus;
import com.tender.webapp.models.handbook.TypeObject;
import com.tender.webapp.models.journal.Counter;
import com.tender.webapp.models.service.ServerMessage;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serdukov on 20.06.2015.
 */
@Controller
public class InfoController {

    @Resource
    protected SessionFactory sessionFactory;

    @Autowired
    InfoService infoService;

    @Autowired
    ServiceUser serviceUser;

    @Autowired
    CatalogService catalogService;

    @Autowired
    SelectorsService selectorsService;

    @Autowired
    ValidateService validateService;



    private static final Logger log = Logger.getLogger(InfoController.class);



    /**
     * Запрос на ассинхронную проверку телефонного номера при регистрации
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/info_check_phone", method = RequestMethod.GET)
    public @ResponseBody ServerMessage checkPhone(HttpServletRequest request) throws  Exception{
        ServerMessage serverMessage = new ServerMessage();
        if(validateService.checkPhoneNumberExist(request.getParameter("phone").trim().toLowerCase())){
            serverMessage.setStatus(44);
            serverMessage.setText("Данный телефон уже зарегистрирован в системе");
        }else{
            serverMessage.setStatus(3014);
            serverMessage.setText("Телефон свободен.");
        }
        return serverMessage;
    }

    @RequestMapping(value = "/info_edit_check_phone", method = RequestMethod.GET)
    public @ResponseBody ServerMessage checkEditPhone(HttpServletRequest request) throws  Exception{
        String phone = request.getParameter("phone").trim().toLowerCase();
        ServerMessage serverMessage = new ServerMessage();
        User user = (User)request.getSession().getAttribute("user");
        if(user.getPrivateInfo().getPhoneNumber().equals(phone)){
            serverMessage.setStatus(3014);
            serverMessage.setText("Телефон свободен.");
            return serverMessage;
        }
        if(validateService.checkPhoneNumberExist(phone)){
            serverMessage.setStatus(44);
            serverMessage.setText("Данный телефон уже зарегистрирован в системе");
        }else{
            serverMessage.setStatus(3014);
            serverMessage.setText("Телефон свободен.");
        }
        return serverMessage;
    }

    /**
     * Запрос на ассинхронную проверку свободного логина при регистрации
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/info_check_login", method = RequestMethod.GET)
    public @ResponseBody ServerMessage checkLogin(HttpServletRequest request) throws Exception{

        ServerMessage serverMessage = new ServerMessage();
        if(validateService.checkLoginExist(request.getParameter("login").trim().toLowerCase())){
            serverMessage.setStatus(43);
            serverMessage.setText("К сожалению, данный логин уже занят. Используйте другой.");
        }else{
            serverMessage.setStatus(3013);
            serverMessage.setText("Логин свободен.");
        }
        return serverMessage;
    }

    @RequestMapping(value = "/info_edit_check_login", method = RequestMethod.GET)
    public @ResponseBody ServerMessage checkEditLogin(HttpServletRequest request) throws Exception{
        String login = request.getParameter("login").trim().toLowerCase();
        ServerMessage serverMessage = new ServerMessage();

        User user = (User)request.getSession().getAttribute("user");
        if(user.getLogin().equals(login)){
            serverMessage.setStatus(3013);
            serverMessage.setText("Логин свободен.");
            return serverMessage;
        }

        if(validateService.checkLoginExist(login)){
            serverMessage.setStatus(43);
            serverMessage.setText("К сожалению, данный логин уже занят. Используйте другой.");
        }else{
            serverMessage.setStatus(3013);
            serverMessage.setText("Логин свободен.");
        }
        return serverMessage;
    }




    /**
     * Запрос на ассинхронную проверку свободного email при регистрации
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/info_check_email", method = RequestMethod.GET)
    public @ResponseBody ServerMessage checkEmail(HttpServletRequest request) throws Exception{

        ServerMessage serverMessage = new ServerMessage();
        if(validateService.checkEmailExist(request.getParameter("email").trim().toLowerCase())){
            serverMessage.setStatus(44);
            serverMessage.setText("К сожалени, данный email уже зарегистрирован. Используйте другой.");
        }else{
            serverMessage.setStatus(3014);
            serverMessage.setText("Email свободен.");
        }
        return serverMessage;
    }

    @RequestMapping(value = "/info_edit_check_email", method = RequestMethod.GET)
    public @ResponseBody ServerMessage checkEditEmail(HttpServletRequest request) throws Exception{

        String email = request.getParameter("email").trim().toLowerCase();
        ServerMessage serverMessage = new ServerMessage();

        User user = (User)request.getSession().getAttribute("user");
        if(user.getPrivateInfo().getEmail().equals(email)){
            serverMessage.setStatus(3014);
            serverMessage.setText("Email свободен.");
            return serverMessage;
        }
        if(validateService.checkEmailExist(email)){
            serverMessage.setStatus(44);
            serverMessage.setText("К сожалени, данный email уже зарегистрирован. Используйте другой.");
        }else{
            serverMessage.setStatus(3014);
            serverMessage.setText("Email свободен.");
        }
        return serverMessage;
    }

    /**
     * Запрос на проверку уже зарегистрированного ИНН
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/info_check_inn", method = RequestMethod.GET)
    public @ResponseBody ServerMessage checkInn(HttpServletRequest request) throws Exception{
        ServerMessage serverMessage = new ServerMessage();
        if(validateService.checkINNExist(request.getParameter("inn").trim())){
            serverMessage.setStatus(44);
            serverMessage.setText("Юр. лицо с таким ИНН уже зарегистрированно в системе");
        }else{
            serverMessage.setStatus(3015);
            serverMessage.setText("ИНН свободен.");
        }
        return serverMessage;
    }

    @RequestMapping(value = "/info_edit_check_inn", method = RequestMethod.GET)
    public @ResponseBody ServerMessage checkEditInn(HttpServletRequest request) throws Exception{
        String inn = request.getParameter("inn").trim().toLowerCase();
        ServerMessage serverMessage = new ServerMessage();

        User user = (User)request.getSession().getAttribute("user");
        if(user.getJuridicalInfo().getInn().equals(inn)){
            serverMessage.setStatus(3015);
            serverMessage.setText("ИНН свободен.");
            return serverMessage;
        }
        if(validateService.checkINNExist(inn)){
            serverMessage.setStatus(44);
            serverMessage.setText("Юр. лицо с таким ИНН уже зарегистрированно в системе");
        }else{
            serverMessage.setStatus(3015);
            serverMessage.setText("ИНН свободен.");
        }
        return serverMessage;
    }

    /**
     * Запрос на необходимые поля для заполнения по каждому объекту при регистрации нового тендера
     * @param parametersRequestList
     * @return
     * @throws Exception
     */
    @RequestMapping(value ="/info_get_object_parameters", method = RequestMethod.POST)
    public @ResponseBody List<ParameterRelation> getObjectParameters(@RequestBody List<ParametersRequest> parametersRequestList) throws Exception {
        if(parametersRequestList.size()<1) throw new Exception("Не корректный запрос парметров для объекта");
        return selectorsService.getParametersRelations(parametersRequestList);
    }


    /**
     * Запрос на необходимые поля для заполнения по каждому объекту при регистрации нового тендера
     * @return
     * @throws Exception
     */
    @RequestMapping(value ="/info_get_counter", method = RequestMethod.GET)
    public @ResponseBody
    Counter getCounter() throws Exception {
        return infoService.getCounter();
    }



    /**
     * Запрос на справочник марои моделей
     * @return
     */
    @RequestMapping(value = "/info_get_model_brands", method = RequestMethod.GET)
    public @ResponseBody  List<AutoBrand> getAllBrands() throws Exception{
       return catalogService.getModelBrands();
    }

    /**
     * Запрос на регионов и городов
     * @return
     */
    @RequestMapping(value = "/info_get_all_regions", method = RequestMethod.GET)
    public @ResponseBody  List<Region> getAllRegion() throws Exception{
        return catalogService.getRegions();
    }


    /**
     * Перехват всех ошибок которые могут возникать от методов котроллера и направление их на логирование
     * @param exception
     */
    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("InfoController exceptionHandler description: ", exception);
    }

    /**
     * Запрос роли пользователя, заказчик он или исполнитель. Используется для формирования внешнего вида личного кабинета в веб-интерфейсе
     * @param request
     * @return
     */
    @RequestMapping(value = "/info_get_user_info", method = RequestMethod.GET)
    public @ResponseBody User getUserInfo(HttpServletRequest request) throws Exception{
        User user = (User) request.getSession().getAttribute("user");
        if(user == null) return null;
        return serviceUser.getUserByUserId(user.getUser_id());
    }



        /*-----------------------не проверенно----------------------------------------------------------------------------*/






    @RequestMapping(value = "/info_get_all_type_objects", method = RequestMethod.GET)
    public @ResponseBody List<TypeObject> getAllCategories() {
        Session session = sessionFactory.openSession();
        Criteria sc = session.createCriteria(TypeObject.class);
        sc.add(Restrictions.eq("active", 1));
        sc.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        @SuppressWarnings("unchecked")
        List<TypeObject> typeObjects = sc.list();
        session.close();
        return typeObjects;
    }


    @RequestMapping(value = "/info_last_new_tenders", method = RequestMethod.GET)
    public
    @ResponseBody List<Tender> getLastNewTenders(HttpServletRequest request) {
        Session session = sessionFactory.openSession();
        Query q = session.createQuery("select distinct t from Tender t join t.tenderStatus as ts where ts.id=2 order by t.dateCreate DESC");
        q.setMaxResults(5);
        List<Tender> tendersList = q.list();
        for(Tender tender : tendersList){
            tender.setOfferList(null);
            tender.setUserOwner(null);
        }
        session.close();
        return tendersList;
    }


   /////--------------------------------------До переделок




    @RequestMapping(value = "/get_all_model_list", method = RequestMethod.GET)
    public
    @ResponseBody  List<AutoModel> getAllModelsByBrandId(HttpServletRequest request) {
        int brand_id = Integer.parseInt(request.getParameter("brand_id"));
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from AutoModel where brand_id = :brand_id order by model_ident");
        query.setParameter("brand_id",brand_id);
        List<AutoModel> listAutoModel = query.list();
        session.close();
        return listAutoModel;
    }







    @RequestMapping(value = "get_modelById", method = RequestMethod.POST)
    public
    @ResponseBody
    String getModelBrandById(HttpServletRequest request) {
        int model_id = Integer.parseInt(request.getParameter("model_id"));
/*        AutoModel autoModel = autoModelBrandService.returnAutoModelById(model_id);

        return autoModel.getAutoBrand().getBrandName() + " " + autoModel.getModelIdent();*/
        return null;
    }


    @RequestMapping(value = "/get_all_parameters_for_sub", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ParameterRelation> getParametersBySubId(HttpServletRequest request) {
        int sub_id = Integer.parseInt(request.getParameter("sub_id"));
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from ParameterRelation where sub_id =:sub_id");
        query.setParameter("sub_id", sub_id);
        List<ParameterRelation> listRel = query.list();
        session.close();
        return listRel;
    }







}
