
package com.tender.webapp.models.handbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(
        name = "type_service"
)
public class TypeService {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private int id;
    @Column(
            name = "service_name_provider",
            unique = false,
            nullable = false
    )
    private String serviceNameProvider;
    @Column(
            name = "service_name_consumer",
            unique = false,
            nullable = false
    )
    private String serviceNameConsumer;
    @OneToMany(
            mappedBy = "typeService",
            fetch = FetchType.EAGER
    )
    @OrderBy("id ASC")
    @Fetch(FetchMode.SUBSELECT)
    private List<CategorySelector> categorySelectors;
    @OneToMany(
            mappedBy = "typeService",
            fetch = FetchType.EAGER
    )
    @OrderBy("id ASC")
    @Fetch(FetchMode.SUBSELECT)
    private List<CommonParameter> commonParameters;
    @Column(
            name = "add_file_comment",
            unique = false
    )
    private String addFileComment;
    @ManyToOne
    @JoinColumn(
            name = "type_obj_id"
    )
    @JsonIgnore
    private TypeObject typeObject;

    public TypeService() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceNameConsumer() {
        return this.serviceNameConsumer;
    }

    public void setServiceNameConsumer(String serviceNameConsumer) {
        this.serviceNameConsumer = serviceNameConsumer;
    }

    public String getServiceNameProvider() {
        return this.serviceNameProvider;
    }

    public void setServiceNameProvider(String serviceNameProvider) {
        this.serviceNameProvider = serviceNameProvider;
    }

    public TypeObject getTypeObject() {
        return this.typeObject;
    }

    public void setTypeObject(TypeObject typeObject) {
        this.typeObject = typeObject;
    }

    public List<CategorySelector> getCategorySelectors() {
        return this.categorySelectors;
    }

    public void setCategorySelectors(List<CategorySelector> categorySelectors) {
        this.categorySelectors = categorySelectors;
    }

    public List<CommonParameter> getCommonParameters() {
        return this.commonParameters;
    }

    public void setCommonParameters(List<CommonParameter> commonParameters) {
        this.commonParameters = commonParameters;
    }

    public String getAddFileComment() {
        return this.addFileComment;
    }

    public void setAddFileComment(String addFileComment) {
        this.addFileComment = addFileComment;
    }
}
