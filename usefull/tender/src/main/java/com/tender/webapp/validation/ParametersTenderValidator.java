package com.tender.webapp.validation;

import com.tender.webapp.models.handbook.ParameterRelation;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Serdukov on 06.06.2015.
 */
@Component
public class ParametersTenderValidator implements Validator {
    @Resource
    protected SessionFactory sessionFactory;

    public boolean supports(Class clazz) {
        return HttpRequest.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {


        Pattern pattern;
        Matcher matcher;

        HttpServletRequest request = (HttpServletRequest) obj;
        ParameterRelation p_rel;
        try {

            Session session = sessionFactory.openSession();


            System.out.println("validator " + request.getParameter("subCategoria"));

            if(request.getParameter("subCategoria") == null){
                throw new IllegalArgumentException ("Не выбрана категория тендера");
            }
            int sub_id= Integer.parseInt(request.getParameter("subCategoria"));

            Query query = session.createQuery("from ParameterRelation where parameter_id = :par_id and sub_id = :sub_id ");
            query.setParameter("sub_id", sub_id);



            //валидация ВИН
            query.setParameter("par_id", 8);
            p_rel = (ParameterRelation) query.uniqueResult();

            if (p_rel != null) {

                if(request.getParameter("vin") == null){
                    throw new IllegalArgumentException ("Для данной категории необходимо указать VIN");
                }
                pattern = Pattern.compile("^[A-Z0-9]{3,100}");
                String vin = request.getParameter("vin").toUpperCase().trim();
                matcher = pattern.matcher(vin);
                System.out.println(vin + "|  VIN.");
                if(!matcher.matches()){
                    e.reject("vin.not_valid", "VIN не валидный");
                }

            }


            //валидация Мощности
            query.setParameter("par_id", 7);
            p_rel = (ParameterRelation) query.uniqueResult();

            if (p_rel != null) {
                if(request.getParameter("powerAuto") == null){
                    throw new IllegalArgumentException ("Для данной категории необходимо указать мощность автомобиля");
                }
                pattern = Pattern.compile("^[0-9]{1,4}");
                String power = request.getParameter("powerAuto").trim();
                matcher = pattern.matcher(power);
                System.out.println(power + "|  POWER.");
                if(!matcher.matches()){
                    e.reject("powerAuto.not_valid", "Значение мощности не валидно");
                }

            }


            //валидация Пробега
            query.setParameter("par_id", 10);
            p_rel = (ParameterRelation) query.uniqueResult();

            if (p_rel != null) {
                if(request.getParameter("run") == null){
                    throw new IllegalArgumentException ("Для данной категории необходимо указать пробег автомобиля");
                }
                pattern = Pattern.compile("^[0-9]{1,9}");
                String run = request.getParameter("run").trim();
                matcher = pattern.matcher(run);
                System.out.println(run + "|  RUN.");
                if(!matcher.matches()){
                    e.reject("run.not_valid", "Значение пробега автомобиля не валидно");
                }

            }


            //валидация Объема двигателя
            query.setParameter("par_id", 11);
            p_rel = (ParameterRelation) query.uniqueResult();

            if (p_rel != null) {

                if(request.getParameter("engineСapacity") == null){
                    throw new IllegalArgumentException ("Для данной категории необходимо указать объем двигателя автомобиля");
                }
            }

        }catch (Exception ex){
            ex.printStackTrace();
            e.rejectValue("subCategoria", "subCategoria.null", ex.getMessage());
        }



    }
}