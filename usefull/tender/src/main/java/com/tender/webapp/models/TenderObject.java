package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.ParameterRelation;
import com.tender.webapp.models.handbook.TypeObject;
import com.tender.webapp.models.handbook.TypeService;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Serdukov on 02.06.2015.
 */
@Entity
@Table(name="tender_objects")
public class TenderObject {

    @Id
    @Column(name="object_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(mappedBy = "tenderObject", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ObjectFoto> objectFotos;

    @OneToMany(mappedBy = "tenderObject", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ObjectParameterValue> objectParameterValues;


    @Transient
    private List<ParameterRelation> parameterRelations;


    @ManyToOne
    @JoinColumn(name="tender_id",nullable=false)
    @JsonIgnore
    private Tender tender;

    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;

    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;

    //категория тендера
    @ManyToOne
    @JoinColumn(name = "type_obj_id")
    private TypeObject typeObject;


  /*--------------getters and setters--------------------------*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public List<ObjectParameterValue> getObjectParameterValues() {
        return objectParameterValues;
    }

    public void setObjectParameterValues(List<ObjectParameterValue> objectParameterValues) {
        this.objectParameterValues = objectParameterValues;
    }


    public List<ObjectFoto> getObjectFotos() {
        return objectFotos;
    }

    public void setObjectFotos(List<ObjectFoto> objectFotos) {
        this.objectFotos = objectFotos;
    }



    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }

    public TypeObject getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(TypeObject typeObject) {
        this.typeObject = typeObject;
    }

    public List<ParameterRelation> getParameterRelations() {
        return parameterRelations;
    }

    public void setParameterRelations(List<ParameterRelation> parameterRelations) {
        this.parameterRelations = parameterRelations;
    }
}
