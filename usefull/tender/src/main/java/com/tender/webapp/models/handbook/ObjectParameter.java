package com.tender.webapp.models.handbook;

import javax.persistence.*;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "object_parameters")
public class ObjectParameter {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "parameter_name", unique = false, nullable = false)
    private String parameterName;

    @Column(name = "parameter_ident", unique = false, nullable = false)
    private String parameterIdent;

    /*selector , range , input*/
    @Column(name = "field_type", unique = false, nullable = false)
    private String fieldType;

    /*text , catalog , Integer*/
    @Column(name = "var_type", unique = false, nullable = false)
    private String varType;

    /*идентификатор поля от которого идет зависимость, на него вешается eventListener*/
    @Column(name = "post_fix", unique = false, nullable = true)
    private String postFix;

    @Column(name = "parameter_regex", unique = false, nullable = true)
    private String parameterRegEx;

    /*если тип fielType = range и varType = Integer, то здесь указывают интервалы*/
    @Column(name = "start_range", unique = false, nullable = true)
    private Integer startRange;
    @Column(name = "stop_range", unique = false, nullable = true)
    private Integer stopRange;


    /*зависит ли данное поле от другого поля */
    @Column(name = "is_ref", unique = false, nullable = true)
    private boolean ref;

    /*идентификатор поля от которого идет зависимость, на него вешается eventListener*/
    @Column(name = "ref_parameter", unique = false, nullable = true)
    private String refParameter;

    /*если catalog, то url  по которому его забирать*/
    @Column(name = "catalog_link", unique = false, nullable = true)
    private String catalogLink;

    /*если catalog, в оптионс какое поле подставлять в значение*/
    @Column(name = "catalog_value_field", unique = false, nullable = true)
    private String catalogValueField;

    /*если catalog, в оптионс какое поле подставлять какое в название*/
    @Column(name = "catalog_name_field", unique = false, nullable = true)
    private String catalogNameField;


    /*указания как нужно заполнять поле*/
    @Column(name = "comment", unique = false, nullable = true)
    private String comment;

    @Transient
    private String value;


    /*--getters and setters-*/


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterIdent() {
        return parameterIdent;
    }

    public void setParameterIdent(String parameterIdent) {
        this.parameterIdent = parameterIdent;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getVarType() {
        return varType;
    }

    public void setVarType(String varType) {
        this.varType = varType;
    }

    public String getParameterRegEx() {
        return parameterRegEx;
    }

    public void setParameterRegEx(String parameterRegEx) {
        this.parameterRegEx = parameterRegEx;
    }

    public Integer getStartRange() {
        return startRange;
    }

    public void setStartRange(Integer startRange) {
        this.startRange = startRange;
    }

    public Integer getStopRange() {
        return stopRange;
    }

    public void setStopRange(Integer stopRange) {
        this.stopRange = stopRange;
    }

    public boolean isRef() {
        return ref;
    }


    public void setRef(boolean ref) {
        this.ref = ref;
    }


    public String getRefParameter() {
        return refParameter;
    }

    public void setRefParameter(String refParameter) {
        this.refParameter = refParameter;
    }


    public String getCatalogLink() {
        return catalogLink;
    }

    public void setCatalogLink(String catalogLink) {
        this.catalogLink = catalogLink;
    }

    public String getCatalogNameField() {
        return catalogNameField;
    }

    public void setCatalogNameField(String catalogNameField) {
        this.catalogNameField = catalogNameField;
    }

    public String getCatalogValueField() {
        return catalogValueField;
    }

    public void setCatalogValueField(String catalogValueField) {
        this.catalogValueField = catalogValueField;
    }


    public String getPostFix() {
        return postFix;
    }

    public void setPostFix(String postFix) {
        this.postFix = postFix;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
