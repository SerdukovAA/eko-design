package com.tender.webapp.models.handbook;

import com.tender.webapp.models.handbook.TypeService;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(
        name = "type_objects"
)
public class TypeObject {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private int id;
    @Column(
            name = "type_name",
            unique = false,
            nullable = false
    )
    private String typeName;
    @Column(
            name = "footnote"
    )
    private String footnote;
    @Column(
            name = "logo_url"
    )
    private String logoUrl;


    @Column(
            name = "short_name"
    )
    private String shortName;

    @Column(name = "active")
    private int active;



    @OneToMany(
            mappedBy = "typeObject",
            fetch = FetchType.EAGER
    )
    @OrderBy("id ASC")
    private List<TypeService> typeServices;

    public TypeObject() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<TypeService> getTypeServices() {
        return this.typeServices;
    }

    public void setTypeServices(List<TypeService> typeServices) {
        this.typeServices = typeServices;
    }

    public String getFootnote() {
        return this.footnote;
    }

    public void setFootnote(String footnote) {
        this.footnote = footnote;
    }

    public String getLogoUrl() {
        return this.logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

}
