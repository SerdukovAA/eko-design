package com.tender.webapp.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Serdukov on 27.07.2015.
 */
public class RegExpCheck {

    public static final Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    public static final Pattern phonePattern = Pattern.compile("^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,12}$");
    public static final Pattern cyrillicNamePattern = Pattern.compile("^[а-яА-ЯЁьъ\\s]{3,20}$");
    public static final Pattern passwordPattern = Pattern.compile("^[a-zA-Z0-9_-]{4,50}$");
    public static final Pattern loginPattern = Pattern.compile("^[a-zA-Z0-9_-]{4,30}$");
    public static final Pattern innPattern = Pattern.compile("^[0-9]{3,20}$");
    public static final Pattern KS_RS_Pattern = Pattern.compile("^[0-9]{20}$");
    public static final Pattern OGRN_Pattern = Pattern.compile("^[0-9]{13}$");




    public static boolean OGRNRegExCheck(String ogrn){
        Matcher matcher = OGRN_Pattern.matcher(ogrn);
        return matcher.matches();
    }


    public static boolean KsRsRegExCheck(String KsRs){
        Matcher matcher = KS_RS_Pattern.matcher(KsRs);
        return matcher.matches();
    }
    public static boolean emailRegExCheck(String email){
        Matcher matcher = emailPattern.matcher(email);
        return matcher.matches();
    }
    public static boolean phoneRegExCheck(String phone){
        Matcher matcher = phonePattern.matcher(phone);
        return matcher.matches();
    }
    public static boolean innRegExCheck(String inn){
        Matcher matcher = innPattern.matcher(inn);
        return matcher.matches();
    }
   public static boolean cyrillicNameRegExCheck(String cyrillicName){
        Matcher matcher = cyrillicNamePattern.matcher(cyrillicName);
        return matcher.matches();
    }
   public static boolean passwordRegExCheck(String password){
        Matcher matcher = passwordPattern.matcher(password);
        return matcher.matches();
   }
   public static boolean loginRegExCheck(String login){
        Matcher matcher = loginPattern.matcher(login);
        return matcher.matches();
   }

    public static boolean fieldRegExCheck(String regEx ,String value){
        Matcher matcher = Pattern.compile(regEx).matcher(value);
        return matcher.matches();
    }




}
