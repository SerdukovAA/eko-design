package com.tender.webapp.models.journal;

import com.tender.webapp.models.Balance;
import com.tender.webapp.models.Offer;
import com.tender.webapp.models.handbook.TrafficType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "cashe_traffic_journal")
public class CashTraffic {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Отражает сумму которая имела место к передвижению по счету клиента. Может быть отрицательной когда сумма списывается
     */
    @Column(name = "sum_amount", unique = false, nullable = false)
    private BigDecimal amount;

    @OneToOne
    @JoinColumn(name = "offer_id")
    private Offer offer;

    @ManyToOne
    @JoinColumn(name = "balance_id")
    private Balance balance;

    @OneToOne
    @JoinColumn(name = "t_id")
    private RefillTransactionJournal refillTransactionJournal;

    /**
     * Это значение баланса после осуществления транзакции по счету исполнителя
     */
    @Column(name = "after_current_value", unique = false, nullable = false)
    private BigDecimal currentValue;

    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;

    @OneToOne
    @JoinColumn(name = "traffic_type_id")
    private TrafficType trafficType;



    /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public BigDecimal getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(BigDecimal currentValue) {
        this.currentValue = currentValue;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }


    public TrafficType getTrafficType() {
        return trafficType;
    }

    public void setTrafficType(TrafficType trafficType) {
        this.trafficType = trafficType;
    }

    public RefillTransactionJournal getRefillTransactionJournal() {
        return refillTransactionJournal;
    }

    public void setRefillTransactionJournal(RefillTransactionJournal refillTransactionJournal) {
        this.refillTransactionJournal = refillTransactionJournal;
    }

}
