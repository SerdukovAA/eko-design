package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.TenderStatus;
import com.tender.webapp.models.handbook.TypeService;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Serdukov on 02.06.2015.
 */
@Entity
@Table(name="tenders")
public class Tender {


    @Id
    @Column(name="tender_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "tender_uniq", unique = false, nullable = true)
    private String tenderUniq;

    @Column(name = "tender_name", unique = false, nullable = false)
    private String tenderName;

    @Column(name = "tender_description",columnDefinition="TEXT", unique = false, nullable = false)
    private String tenderDescription;


    @ManyToOne
    @JoinColumn(name = "type_serv_id")
    private TypeService typeService;

    @Column(name = "date_create_tender", unique = false, nullable = false)
    private Date dateCreate;

    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;

    @OneToMany(mappedBy = "tender", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<SelectorValue> selectorValueList;

    @OneToMany(mappedBy = "tender", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<CommonValue> commonValueList;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tender", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    @OrderBy(value = "id ASC")
    private List<TenderObject> tenderObjects ;

    @OneToMany(mappedBy = "tender", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TenderFile> tenderFileList;



    @Column(name = "date_begin_tender", unique = false, nullable = true)
    private Date dateBegin;

    @Column(name = "date_end_tender", unique = false, nullable = true)
    private Date dateEnd;


    @Column(name = "expected_price", unique = false, nullable = false)
    private BigDecimal expectedPrice;


    @Column(name="is_closed",unique=false)
    private boolean isClosed;

    @Column(name="need_credit",unique=false)
    private boolean needCredit;

    @Column(name="is_owner_close_tender",unique=false)
    private boolean ownerCloseTender;

    @Column(name="is_provider_close_tender",unique=false)
    private boolean providerCloseTender;


    @Column(name = "owner_is_juridical", unique = false, nullable = false)
    private boolean isJuridical;



    @Column(name = "is_correction", unique = false, nullable = false)
    private boolean isCorrection;


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tender")
    private Set<Offer> offerList ;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tender")
    @Fetch(value = FetchMode.SUBSELECT)
    @OrderBy(value = "dateCreate ASC")
    private Set<Message> messageList ;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_owner_id")
    @JsonIgnore
    private User userOwner;

    @ManyToOne
    @JoinColumn(name = "tender_status_id")
    private TenderStatus tenderStatus;


    //расположение тендера
    @Column(name = "tender_place_latitude", unique = false, nullable = false)
    private double tenderPlaceLatitude;

    @Column(name = "tender_place_longitude", unique = false, nullable = false)
    private double tenderPlaceLongitude;

    @Column(name = "full_adress", unique = false)
    private String fullAdress;

    @Column(name = "region_code", unique = false)
    private String regionCode;

    @Column(name = "rayon_code", unique = false)
    private String rayonCode;

    @Column(name = "city_code", unique = false)
    private String cityCode;

    @Column(name = "street", unique = false)
    private String street;


    @Column(name = "house_number", unique = false)
    private String houseNumber;


    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "responsible_info_id")
    private ResponsibleInfo responsibleInfo;


    @Column(name = "have_responsible", nullable = false)
    private boolean haveResponsible;


    @Transient
    private boolean is_my;


    @Transient
    private boolean is_provider;


    @Transient
    private boolean is_guest;


    /*--getters and setters-*/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getTenderUniq() {
        return tenderUniq;
    }

    public void setTenderUniq(String tenderUniq) {
        this.tenderUniq = tenderUniq;
    }

    public String getTenderName() {
        return tenderName;
    }

    public void setTenderName(String tenderName) {
        this.tenderName = tenderName;
    }

    public String getTenderDescription() {
        return tenderDescription;
    }

    public void setTenderDescription(String tenderDescription) {
        this.tenderDescription = tenderDescription;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }

    public List<SelectorValue> getSelectorValueList() {
        return selectorValueList;
    }

    public void setSelectorValueList(List<SelectorValue> selectorValueList) {
        this.selectorValueList = selectorValueList;
    }

    public List<CommonValue> getCommonValueList() {
        return commonValueList;
    }

    public void setCommonValueList(List<CommonValue> commonValueList) {
        this.commonValueList = commonValueList;
    }


    public List<TenderObject> getTenderObjects() {
        return tenderObjects;
    }

    public void setTenderObjects(List<TenderObject> tenderObjects) {
        this.tenderObjects = tenderObjects;
    }

    public boolean isCorrection() {
        return isCorrection;
    }

    public void setIsCorrection(boolean isCorrection) {
        this.isCorrection = isCorrection;
    }

    public double getTenderPlaceLatitude() {
        return tenderPlaceLatitude;
    }

    public void setTenderPlaceLatitude(double tenderPlaceLatitude) {
        this.tenderPlaceLatitude = tenderPlaceLatitude;
    }

    public double getTenderPlaceLongitude() {
        return tenderPlaceLongitude;
    }

    public void setTenderPlaceLongitude(double tenderPlaceLongitude) {
        this.tenderPlaceLongitude = tenderPlaceLongitude;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public BigDecimal getExpectedPrice() {
        return expectedPrice;
    }

    public void setExpectedPrice(BigDecimal expectedPrice) {
        this.expectedPrice = expectedPrice;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setIsClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

    public boolean isOwnerCloseTender() {
        return ownerCloseTender;
    }

    public void setOwnerCloseTender(boolean ownerCloseTender) {
        this.ownerCloseTender = ownerCloseTender;
    }

    public boolean isProviderCloseTender() {
        return providerCloseTender;
    }

    public void setProviderCloseTender(boolean providerCloseTender) {
        this.providerCloseTender = providerCloseTender;
    }

    public boolean isJuridical() {
        return isJuridical;
    }

    public void setIsJuridical(boolean isJuridical) {
        this.isJuridical = isJuridical;
    }

    public Set<Offer> getOfferList() {
        return offerList;
    }

    public void setOfferList(Set<Offer> offerList) {
        this.offerList = offerList;
    }

    public Set<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(Set<Message> messageList) {
        this.messageList = messageList;
    }

    public User getUserOwner() {
        return userOwner;
    }

    public void setUserOwner(User userOwner) {
        this.userOwner = userOwner;
    }

    public TenderStatus getTenderStatus() {
        return tenderStatus;
    }

    public void setTenderStatus(TenderStatus tenderStatus) {
        this.tenderStatus = tenderStatus;
    }


    public TypeService getTypeService() {
        return typeService;
    }

    public void setTypeService(TypeService typeService) {
        this.typeService = typeService;
    }


    public List<TenderFile> getTenderFileList() {
        return tenderFileList;
    }

    public void setTenderFileList(List<TenderFile> tenderFileList) {
        this.tenderFileList = tenderFileList;
    }

    public String getFullAdress() {
        return fullAdress;
    }

    public void setFullAdress(String fullAdress) {
        this.fullAdress = fullAdress;
    }


    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }


    public boolean is_my() {
        return is_my;
    }

    public void setIs_my(boolean is_my) {
        this.is_my = is_my;
    }

    public boolean is_guest() {
        return is_guest;
    }

    public void setIs_guest(boolean is_guest) {
        this.is_guest = is_guest;
    }

    public boolean is_provider() {
        return is_provider;
    }

    public void setIs_provider(boolean is_provider) {
        this.is_provider = is_provider;
    }


    public boolean isNeedCredit() {
        return needCredit;
    }

    public void setNeedCredit(boolean needCredit) {
        this.needCredit = needCredit;
    }


    public ResponsibleInfo getResponsibleInfo() {
        return responsibleInfo;
    }

    public void setResponsibleInfo(ResponsibleInfo responsibleInfo) {
        this.responsibleInfo = responsibleInfo;
    }



    public String getRayonCode() {
        return rayonCode;
    }

    public void setRayonCode(String rayonCode) {
        this.rayonCode = rayonCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }


    public boolean isHaveResponsible() {
        return haveResponsible;
    }

    public void setHaveResponsible(boolean haveResponsible) {
        this.haveResponsible = haveResponsible;
    }
}
