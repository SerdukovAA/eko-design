package com.tender.webapp.models.handbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Serdukov on 14.06.2015.
 */
@Entity
@Table(name = "traffic_type")
public class TrafficType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "traffic_type_name", unique = false, nullable = false)
    private String  trafficTypeName;

    @Column(name = "traffic_type", unique = false, nullable = false)
    private String  trafficType;


    /*--------getters and setters-----*/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTrafficTypeName() {
        return trafficTypeName;
    }

    public void setTrafficTypeName(String trafficTypeName) {
        this.trafficTypeName = trafficTypeName;
    }

    public String getTrafficType() {
        return trafficType;
    }

    public void setTrafficType(String trafficType) {
        this.trafficType = trafficType;
    }

}
