package com.tender.webapp.models.handbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.User;

import javax.persistence.*;

/**
 * Created by Serdukov on 17.01.2016.
 */
@Entity
@Table(name = "user_prefer_region")
public class UserPreferedRegion {



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    /**
     * Коде предпорчитаемого региона
     */
    @Column(name = "region_code", unique = false)
    private String regionCode;

    @Column(name = "rayon_code", unique = false)
    private String rayonCode;
    /**
     * Коде предпорчитаемого города
     */
    @Column(name = "city_code", unique = false)
    private String cityCode;



    //пользователь для кого идет настройка
    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    ///-------------getters and setters------------------------------/


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }


    public String getRayonCode() {
        return rayonCode;
    }

    public void setRayonCode(String rayonCode) {
        this.rayonCode = rayonCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
