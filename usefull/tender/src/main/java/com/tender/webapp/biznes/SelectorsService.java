package com.tender.webapp.biznes;


import com.tender.webapp.controllers.service_bean.ParametersRequest;
import com.tender.webapp.models.handbook.ParameterRelation;

import java.util.List;

/**
 * Created by Serdukov on 05.06.2015.
 */
public interface SelectorsService {

    public List<ParameterRelation> getParametersRelations(List<ParametersRequest> parametersRequestsList) throws Exception;


}
