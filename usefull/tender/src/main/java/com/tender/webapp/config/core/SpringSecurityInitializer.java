package com.tender.webapp.config.core;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.multipart.support.MultipartFilter;

import javax.servlet.ServletContext;

public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {


    CharacterEncodingFilter filter;

    @Override
    protected void beforeSpringSecurityFilterChain(ServletContext servletContext) {
        filter  = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        insertFilters(servletContext,filter);
        insertFilters(servletContext,new MultipartFilter() );
    }

}