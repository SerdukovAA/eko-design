package com.tender.webapp.mail;

import org.apache.log4j.Logger;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;


public class MailManager implements Runnable {

    private String message;
    private String toEmail;
    private String tema;

    private JavaMailSender javaMailSender;
    private static final Logger log = Logger.getLogger(MailManager.class);

    public MailManager(String toEmail, String tema,String message,JavaMailSender javaMailSender){
        this.toEmail = toEmail;
        this.tema = tema;
        this.message = message;
        this.javaMailSender= javaMailSender;

    }

    public void run(){
         try{
             MimeMessage mailMessage = javaMailSender.createMimeMessage();
             MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true, "UTF-8");
             helper.setFrom("UBETEN <info@ubeten.com>");
             helper.setTo(toEmail);
             helper.setSubject(tema);
             helper.setText(message, true);
             javaMailSender.send(mailMessage);
             log.info("Сообщение темой: "+tema+", на адрес "+toEmail+" отправлено успешно" );
           }catch (Exception e){
             e.printStackTrace();
             log.error(" Ошибка отправки email сообщения"+e.getMessage(), e);
           }
    }

}
