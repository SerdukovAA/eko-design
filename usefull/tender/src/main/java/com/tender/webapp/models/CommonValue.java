package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.CategorySelector;
import com.tender.webapp.models.handbook.CommonParameter;

import javax.persistence.*;

/**
 * Created by Serdukov on 09.09.2015.
 */
@Entity
@Table(name = "commons_ten_param_values")
public class CommonValue {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "common_id", unique = false, nullable = false)
    private int commonId;

    @Column(name = "parameter_name", unique = false, nullable = false)
    private String parameterName;

    @Column(name = "value_id", unique = false, nullable = true)
    private int valueId;

    @Column(name = "parameter_value", unique = false, nullable = false)
    private String parameterValue;

    @ManyToOne
    @JoinColumn(name = "tender_id")
    @JsonIgnore
    private Tender tender;

    /*-------------getters and setters----------------------*/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCommonId() {
        return commonId;
    }

    public void setCommonId(int commonId) {
        this.commonId = commonId;
    }


    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }


    public int getValueId() {
        return valueId;
    }

    public void setValueId(int valueId) {
        this.valueId = valueId;
    }


    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }
}

