package com.tender.webapp.models.journal;

import com.tender.webapp.models.Balance;
import com.tender.webapp.models.Offer;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Serdukov on 20.08.2015.
 */

@Entity
@Table(name = "canceled_tenders")
public class CanceledTender {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    @JoinColumn(name = "tender_id")
    private Tender tender;

    @Column(name = "reason", columnDefinition = "TEXT", unique = false, nullable = true)
    private String reason;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "date_canceled", unique = false, nullable = false)
    private Date dateCanceled;


    ///---setters and getters----------------------------------------
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDateCanceled() {
        return dateCanceled;
    }

    public void setDateCanceled(Date dateCanceled) {
        this.dateCanceled = dateCanceled;
    }

}
