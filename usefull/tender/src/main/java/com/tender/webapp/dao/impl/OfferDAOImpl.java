package com.tender.webapp.dao.impl;


import com.tender.webapp.dao.OfferDAO;
import com.tender.webapp.dao.TenderDAO;
import com.tender.webapp.models.Offer;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * Created by serdukov on 01.11.15.
 */
public class OfferDAOImpl implements OfferDAO{


    @Resource
    protected SessionFactory sessionFactory;

    @Autowired
    TenderDAO tenderDAO;


    public Offer getOfferWinByTenderId(int tender_id)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        Criteria sc = session.createCriteria(Offer.class);
        sc.add(Restrictions.eq("tender.id", tender_id));
        sc.add(Restrictions.eq("offerStatus.id", 2));
        sc.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        Offer offer = (Offer) sc.uniqueResult();
        return offer;
    }

}
