package com.tender.webapp.models.journal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "counter")
public class Counter {


    @Id
    private int id;

    @Column(name = "auction_count", unique = false)
    private int auctionCount;

    @Column(name = "total_amount", unique = false)
    private int totalAmount;

    @Column(name = "saved_amount", unique = false)
    private int savedAmount;


    @Column(name = "date_change", unique = false)
    private Date dateChange;






    /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAuctionCount() {
        return auctionCount;
    }

    public void setAuctionCount(int auctionCount) {
        this.auctionCount = auctionCount;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getSavedAmount() {
        return savedAmount;
    }

    public void setSavedAmount(int savedAmount) {
        this.savedAmount = savedAmount;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }

}
