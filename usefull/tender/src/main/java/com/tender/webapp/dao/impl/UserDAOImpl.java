package com.tender.webapp.dao.impl;


import com.tender.webapp.dao.UserDAO;
import com.tender.webapp.models.User;
import com.tender.webapp.models.UserStatus;
import com.tender.webapp.models.handbook.TenderStatus;
import com.tender.webapp.models.handbook.UserParameter;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.Resource;

/**
 * Created by serdukov on 01.11.15.
 */
public class UserDAOImpl implements UserDAO{

    @Resource
    protected SessionFactory sessionFactory;

    public void changePassword(int user_id, String newPassword)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, user_id);
        user.setPassword(newPassword);
        session.update(user);
    }

    public void updateUser(User user)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }

    public void deleteBySelf(int user_id)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class , user_id);
        user.setUserStatus((UserStatus) session.get(UserStatus.class, 3));
        session.update(user);
    }

    public User getUserById(int user_id)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        return (User) session.get(User.class , user_id);
    }

    public User getUserByLogin(String login)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where login = :login and user_status_id in (1,2,5)");
        query.setParameter("login", login);
        return (User) query.uniqueResult();
    }





    public User findUserByEmail(String email)throws Exception {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where private_info_id in (select pi.id from PrivateInfo pi where email = :email) and user_status_id in (1,2,5)");
        query.setParameter("email", email.toLowerCase());
        return (User) query.uniqueResult();
    }


    public User findUserByLogin(String login)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where login =:login and user_status_id in (1,2,5)");
        query.setParameter("login", login.toLowerCase());
        return (User) query.uniqueResult();
    }




}
