package com.tender.webapp.validation;

import com.tender.webapp.biznes.ValidateService;
import com.tender.webapp.controllers.service_bean.ConsumerFormObject;
import com.tender.webapp.models.service.ServerMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Serdukov on 06.06.2015.
 */
@Component
public class RegistrationFormConsumerValidator {


    @Autowired
    ValidateService validateService;


    public void validate(ConsumerFormObject consumerFormObject, ServerMessage serverMessage) throws Exception {


        if (consumerFormObject.isAcceptAgreement() == false) {
            serverMessage.addError(1, "Необходимо согласиться с пользовательским соглашением");
        }

        if (consumerFormObject.getPhoneNumber() == null || consumerFormObject.getPhoneNumber().isEmpty()) {
            serverMessage.addError(2, "Не передан номер телефона");
            if (RegExpCheck.phoneRegExCheck(consumerFormObject.getPhoneNumber())) {
                if (validateService.checkPhoneNumberExist(consumerFormObject.getPhoneNumber())) {
                    serverMessage.addError(10, "Пользователь с таким номером телефона уже зареистрирован в системе");
                }
            } else {
                serverMessage.addError(3, "Передаваемый номер телефона не действителен");
            }
        }

        if (consumerFormObject.getFirstName() == null || consumerFormObject.getFirstName().isEmpty()) {
            serverMessage.addError(4, "Не передано имя пользователя");
            if (!RegExpCheck.cyrillicNameRegExCheck(consumerFormObject.getFirstName())) {
                serverMessage.addError(5, "Передаваемое имя не корректно. Используйте кирилицу");
            }
        }


        if (consumerFormObject.getLastName()== null || consumerFormObject.getLastName().isEmpty()) {
            serverMessage.addError(6, "Не передана фамилия пользователя");
            if (!RegExpCheck.cyrillicNameRegExCheck(consumerFormObject.getLastName())) {
                serverMessage.addError(7, "Передаваемая фамилия не корректна. Используйте кирилицу");
            }
        }


        if (consumerFormObject.getLogin() == null || consumerFormObject.getLogin().isEmpty()) {
            serverMessage.addError(9, "Передан пустой логин");
        } else {
            String login = consumerFormObject.getLogin().trim().toLowerCase();
            if (RegExpCheck.loginRegExCheck(login)) {
                if (validateService.checkLoginExist(login)) {
                    serverMessage.addError(10, "Пользователь с таким логином уже присутствует в системе");
                }
            } else {
                serverMessage.addError(11, "Переданый логин некоректный. Используйте латиницу");
            }
        }


        if (consumerFormObject.getEmail() == null || consumerFormObject.getEmail().isEmpty()) {
            serverMessage.addError(12, "Передан пустой адрес электронной почты");
        } else {
            if (RegExpCheck.emailRegExCheck(consumerFormObject.getEmail())) {
                if (validateService.checkEmailExist(consumerFormObject.getEmail().trim().toLowerCase())) {
                    serverMessage.addError(13, "Пользователь с таким адресом электронной почты уже присутствует в системе");
                }
            } else {
                serverMessage.addError(14, "Переданый email не дейстивтелен");
            }
        }


        if (consumerFormObject.getPhoneCheckID() == null || consumerFormObject.getPhoneCheckID().isEmpty()) {
            serverMessage.addError(15, "Попытка фальсификации проверки телефонного номера");
        }

        if (consumerFormObject.getPhoneNumberCOD() == null || consumerFormObject.getPhoneNumberCOD().isEmpty()) {
            serverMessage.addError(15, "Передан пустой проверочный код телефона");
        } else {
            if (!validateService.checkPhoneCode(consumerFormObject.getPhoneCheckID().trim(), consumerFormObject.getPhoneNumberCOD().trim())) {
                serverMessage.addError(16, "Проверочный код телефонного номера не совпадает с высланным");
            }
        }


//Проверка пароля
        if (consumerFormObject.getPassword() == null || consumerFormObject.getPassword().isEmpty()) {
            serverMessage.addError(17, "Передан пустой пароль");
        } else {
            if (consumerFormObject.getPassword2() == null || consumerFormObject.getPassword2().isEmpty()) {
                serverMessage.addError(18, "Передан пустой проверочный пароль");
            } else {
                if (!RegExpCheck.passwordRegExCheck(consumerFormObject.getPassword())) {
                    serverMessage.addError(19, "Передаваемый пароль некорректный. Используйте латицину");
                } else {
                    if (!consumerFormObject.getPassword().equals(consumerFormObject.getPassword2())) {
                        serverMessage.addError(20, "Проверочный пароль не совпадает с основным");
                    }
                }
            }
        }

    }


}