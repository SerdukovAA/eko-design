package com.tender.webapp.models.service;

import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serdukov on 25.07.2015.
 */
public class ServerMessage {


    //1 - 1000 : errors
    //3000 - 3999 : success
    //4000 - 5000 : special server messages
    private int status;

    private String text;

    private Tender tender;

    private List<Tender> tenderList;

    private List<Error> errors = new ArrayList<Error>();

    private User user;

    private String phoneCheckID;

    private boolean hasErrors = false;


    public void addError(int errorNumber, String errorText){
       this.hasErrors = true;
       Error error = new Error(errorNumber, errorText);
       this.errors.add(error);
    }



   /* ------------getters and setters-------------------*/
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }

    public List<Tender> getTenderList() {
        return tenderList;
    }

    public void setTenderList(List<Tender> tenderList) {
        this.tenderList = tenderList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }


    public boolean isHasErrors() {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors) {
        this.hasErrors = hasErrors;
    }

    public String getPhoneCheckID() {
        return phoneCheckID;
    }

    public void setPhoneCheckID(String phoneCheckID) {
        this.phoneCheckID = phoneCheckID;
    }


}
