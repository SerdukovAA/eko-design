package com.tender.webapp.controllers;


import com.tender.webapp.biznes.ServiceOffer;
import com.tender.webapp.controllers.service_bean.AcceptOfferRequest;
import com.tender.webapp.controllers.service_bean.OfferFormObject;
import com.tender.webapp.controllers.service_bean.OverTenderRequest;
import com.tender.webapp.mail.MailManager;

import com.tender.webapp.models.*;
import com.tender.webapp.models.service.*;
import com.tender.webapp.validation.OfferValidator;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;

/**
 * Created by Serdukov on 14.06.2015.
 */
@Controller
public class OfferController {



    @Resource
    protected SessionFactory sessionFactory;
    @Autowired
    OfferValidator offerValidator;
    @Autowired
    ServiceOffer serviceOffer;

    @Autowired
    ExecutorService executorService;

    private static final Logger log = Logger.getLogger(OfferController.class);


    /**
     * КОнтроллер загрузки нового предложения по тендеру
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/provider_new_offer", method = RequestMethod.POST)
    public @ResponseBody ServerMessage newOffer(@RequestBody OfferFormObject offerFormObject, HttpServletRequest request) throws Exception {
        User user = (User)request.getSession().getAttribute("user");
        ServerMessage serverMessage = new ServerMessage();
        log.info(" id сессии: "+request.getSession().getId()+".Новое предложение по тендеру");
        offerValidator.validate(offerFormObject,serverMessage);
        if(serverMessage.isHasErrors()){
            serverMessage.setStatus(1);
            serverMessage.setText("Переданы некорректные данные в предложении на тендер");
            log.info("Переданы некорректные данные в предложении по тендеру. "+request.getSession().getId());
            for (com.tender.webapp.models.service.Error error:serverMessage.getErrors()){
                log.info(" id сессии: "+request.getSession().getId()+" - "+error.getErrorText());
            }
            return serverMessage;
        }else {

            try {
                serviceOffer.createAndSaveNewOffer(offerFormObject , user);
                serverMessage.setStatus(3000);
                serverMessage.setText("Новый оффер успешно отгружен.");
                log.info(" id сессии: "+request.getSession().getId()+". Новый тендер успешно отгружен.");
                return serverMessage;
            } catch (Exception e) {
                serverMessage.setStatus(3);
                serverMessage.setText("Произошла ошибка при сохранении предложения на сервере");
                serverMessage.addError(1,"Ошибка сервера: " + e.getMessage());
                log.info("Произошла ошибка при сохранении предложения на сервере. id сессии: "+request.getSession().getId()+" - Ошибка сервера: " + e.getMessage());
                return serverMessage;
            }
        }
    }


    /**
     * Контроллер принятия оффера по тендеру
     * @param acceptOfferRequest
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/consumer_accept_offer", method = RequestMethod.POST)
    public @ResponseBody ServerMessage acceptOffer(@RequestBody AcceptOfferRequest acceptOfferRequest,  HttpServletRequest request) throws Exception {
        User user = (User)request.getSession().getAttribute("user");
        ServerMessage serverMessage = new ServerMessage();
        log.info(" id сессии: "+request.getSession().getId()+".Попытка принять предложение по тендеру");
        try {
            serviceOffer.acceptOffer(acceptOfferRequest, user);
            serverMessage.setStatus(3000);
            serverMessage.setText("Предложение успешно принято");
            log.info(" id сессии: "+request.getSession().getId()+". Оффер успешно принят ");
            return serverMessage;
        } catch (Exception e) {
            serverMessage.setStatus(3);
            serverMessage.setText("Произошла ошибка при принятии предложения на сервере");
            serverMessage.addError(1,"Ошибка сервера.");
            log.info("Произошла ошибка при попытке принять предложение на сервере. id сессии: "+request.getSession().getId()+" - Ошибка сервера: " + e.getMessage());
            return serverMessage;
        }
    }




    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }



}









