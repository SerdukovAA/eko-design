package com.tender.webapp.models.journal;

import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;
import com.tender.webapp.models.handbook.RatingType;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Serdukov on 20.08.2015.
 */

@Entity
@Table(name = "rating_journal")
public class RatingJournal {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    @JoinColumn(name = "tender_id")
    private Tender tender;

    @ManyToOne
    @JoinColumn(name = "provider_id")
    private User provider;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "rating", unique = false, nullable = false)
    private int rating;

    @Column(name = "comment", columnDefinition = "TEXT", unique = false, nullable = true)
    private String comment;


    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;

    @OneToOne
    @JoinColumn(name = "rating_type_id")
    private RatingType ratingType;


    ///---setters and getters----------------------------------------

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }

    public User getProvider() {
        return provider;
    }

    public void setProvider(User provider) {
        this.provider = provider;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }


    public RatingType getRatingType() {
        return ratingType;
    }

    public void setRatingType(RatingType ratingType) {
        this.ratingType = ratingType;
    }

}
