package com.tender.webapp.config;

import com.tender.webapp.config.rest.CsrfTokenResponseCookieBindingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


@Autowired
DataSource dataSource;
    @Autowired
    Environment env;

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;//RESTAutenticSuccessHandler

    @Autowired
    private LogoutSuccessHandler logoutSuccessHandler;


    @Autowired
    @Qualifier("customUserDetailsService")
    UserDetailsService userDetailsService;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }


	@Override
	protected void configure(HttpSecurity http) throws Exception {

            http.authorizeRequests()
                .antMatchers("/login", "/info**", "/email_trust/**/**", "/password_recovery/**/**", "/check_order**", "/payment_aviso**", "/registration**").permitAll()
                .antMatchers("/user**", "/accept*", "/upload**", "/consumer*", "/provider*", "/tender_registration").access("hasAnyRole('USER_PROVIDER','USER_CONSUMER','ADMIN')")
                .antMatchers("/admin_**").access("hasAnyRole('ADMIN')")
                .antMatchers("/logout").authenticated();

            http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
            http.formLogin().successHandler(authenticationSuccessHandler);
            http.formLogin().failureHandler(authenticationFailureHandler);

            http.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);

             http.csrf().ignoringAntMatchers("/check_order**", "/payment_aviso**", "/registration**", "/info_**");

            http.csrf().requireCsrfProtectionMatcher(
                new AndRequestMatcher(
                        // Apply CSRF protection to all paths that do NOT match the ones below

                        // We disable CSRF at login/logout, but only for OPTIONS methods
                        new NegatedRequestMatcher(new AntPathRequestMatcher("/login", HttpMethod.OPTIONS.toString())),

                        new NegatedRequestMatcher(new AntPathRequestMatcher("/index.html")),
                        new NegatedRequestMatcher(new AntPathRequestMatcher("/res/**")),
                        new NegatedRequestMatcher(new AntPathRequestMatcher("/sitemap.xml")),
                        new NegatedRequestMatcher(new AntPathRequestMatcher("/robots.txt")),
                        new NegatedRequestMatcher(new AntPathRequestMatcher("/s1/tender_files/**")),
                        new NegatedRequestMatcher(new AntPathRequestMatcher("/email_trust/**/**")),
                        new NegatedRequestMatcher(new AntPathRequestMatcher("/password_recovery/**/**")),
                        new NegatedRequestMatcher(new AntPathRequestMatcher("/registration**")),
                        new NegatedRequestMatcher(new AntPathRequestMatcher("/rest/get*/**"))

                )
             );
            http.addFilterAfter(new CsrfTokenResponseCookieBindingFilter(), CsrfFilter.class); // CSRF tokens handling



		}

}