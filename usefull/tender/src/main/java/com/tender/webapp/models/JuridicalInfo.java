package com.tender.webapp.models;

import com.tender.webapp.models.handbook.TypeService;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Serdukov on 02.06.2015.
 */
@Entity
@Table(name="juridical_info")
public class JuridicalInfo {

    @Id
    @Column(name="juridical_info_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "company_name", unique = false)
    private String companyName;

    @Column(name = "company_discription",columnDefinition="TEXT", unique = false)
    private String companyDescription;


    @Column(name = "inn_file_link", unique = false)
    private String innFileLink;

    @Column(name = "inn", unique = true)
    private String inn;

    @Column(name = "jur_adress", unique = false)
    private String jurAdress;



    @Column(name = "fact_adress", unique = false)
    private String factAdress;




    //локация исполнителя
    @Column(name = "fact_adr_latitude", unique = false, nullable = false)
    private double factAdrLatitude;

    @Column(name = "fact_adr_longitude", unique = false, nullable = false)
    private double factAdrLongitude;

    @Column(name = "city_code", unique = false)
    private String cityCode;

    @Column(name = "region_code", unique = false)
    private String regionCode;

    @Column(name = "rayon_code", unique = false)
    private String rayonCode;

    @Column(name = "street", unique = false)
    private String street;

    @Column(name = "house_number", unique = false)
    private String houseNumber;


    @Column(name = "phone_number_company", unique = false)
    private String phoneNumberCompany;


    @Column(name = "email_company", unique = false)
    private String emailCompany;


    @OneToMany(mappedBy = "juridicalInfo",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderBy(value = "id ASC")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ProviderFilterValues> providerFilterValues;



    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;



    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;




    /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    public String getInnFileLink() {
        return innFileLink;
    }

    public void setInnFileLink(String innFileLink) {
        this.innFileLink = innFileLink;
    }


    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getJurAdress() {
        return jurAdress;
    }

    public void setJurAdress(String jurAdress) {
        this.jurAdress = jurAdress;
    }

    public String getFactAdress() {
        return factAdress;
    }

    public void setFactAdress(String factAdress) {
        this.factAdress = factAdress;
    }

    public String getPhoneNumberCompany() {
        return phoneNumberCompany;
    }

    public void setPhoneNumberCompany(String phoneNumberCompany) {
        this.phoneNumberCompany = phoneNumberCompany;
    }

    public String getEmailCompany() {
        return emailCompany;
    }

    public void setEmailCompany(String emailCompany) {
        this.emailCompany = emailCompany;
    }


    public List<ProviderFilterValues> getProviderFilterValues() {
        return providerFilterValues;
    }

    public void setProviderFilterValues(List<ProviderFilterValues> providerFilterValues) {
        this.providerFilterValues = providerFilterValues;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }


    public double getFactAdrLatitude() {
        return factAdrLatitude;
    }

    public void setFactAdrLatitude(double factAdrLatitude) {
        this.factAdrLatitude = factAdrLatitude;
    }

    public double getFactAdrLongitude() {
        return factAdrLongitude;
    }

    public void setFactAdrLongitude(double factAdrLongitude) {
        this.factAdrLongitude = factAdrLongitude;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }


    public String getRayonCode() {
        return rayonCode;
    }

    public void setRayonCode(String rayonCode) {
        this.rayonCode = rayonCode;
    }

}
