package com.tender.webapp.dao;

import com.tender.webapp.models.PrivateInfo;
import com.tender.webapp.models.User;

/**
 * Created by serdukov on 01.11.15.
 */
public interface PrivateInfoDAO {


    public PrivateInfo findPrivateInfoByPhoneNumber(String phoneNumber) throws Exception;

}
