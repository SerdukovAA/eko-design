package com.tender.webapp.biznes.impl;

import com.tender.webapp.biznes.ServiceMessage;
import com.tender.webapp.comunicators.PosterComunicator;
import com.tender.webapp.controllers.service_bean.NewCommentRequest;
import com.tender.webapp.models.*;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.concurrent.ExecutorService;

/**
 * Created by Serdukov on 05.06.2015.
 */
public class ServiceMessageImpl implements ServiceMessage {

    @Resource
    protected SessionFactory sessionFactory;
    @Autowired
    ExecutorService executorService;
    @Autowired
    JavaMailSender javaMailSender;

    private static final Logger log = Logger.getLogger(ServiceMessageImpl.class);

    public Message createAndSaveNewMessage(NewCommentRequest newCommentRequest, HttpServletRequest  request) throws Exception {
        User user = (User) request.getSession().getAttribute("user");

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            int tender_id = newCommentRequest.getTender_id();
            Message message = new Message();
            message.setDateCreate(new Date());
            Tender tender = (Tender) session.get(Tender.class, tender_id);
            message.setTender(tender);
            message.setText(newCommentRequest.getText().trim());

            message.setUserOwner(user); //владелец комментария

            if (tender.getUserOwner().getUser_id() == user.getUser_id()) {
               message.setTenderOwner(true);
            }else{
               message.setTenderOwner(false);
            }

            boolean isAnsver = newCommentRequest.isAnswer();
            if(isAnsver){
                message.setAnswer(isAnsver);
                int message_id = newCommentRequest.getMessage_id();
                Message mes = (Message) session.get(Message.class, message_id);
                message.setForMessage(mes);
            }else{
                message.setAnswer(isAnsver);
            }
            session.save(message);
            tx.commit();
            log.info(" id сессии: " + request.getSession().getId()+". Комментарий успешно сохранен.");
            try {
                 log.info(" id сессии: " + request.getSession().getId() + ". Оповещает рассылку о поступлении нового коментария");
                 PosterComunicator.posterHanldeRequestNewMessage(message.getId());
            }catch (Exception ex){
                log.error(" id сессии: " + request.getSession().getId() + ". Ошибка оповещения рассылки", ex);
                ex.printStackTrace();
            }


            return message;
        } catch (RuntimeException e) {
            e.printStackTrace();
            try {
                tx.rollback();

            } catch (RuntimeException rbe) {
                log.info(" id сессии: " + request.getSession().getId()+". Не получается откатить транзакцию при сохранении нового комментария");
                throw  new Exception("Не удалось сохранить комментарий");
            }
         log.error(" id сессии: " + request.getSession().getId()+". Не удалось сохранить комментарий", e);
         throw  new Exception("Не удалось сохранить комментарий");
         } finally {
            if (session != null) {
                session.close();
            }
        }
    }


}


