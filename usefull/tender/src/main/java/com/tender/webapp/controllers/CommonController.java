package com.tender.webapp.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * @author Serdjukov A.A. 24.01.2015
 */

@ControllerAdvice
public class CommonController {


    //выставлено в диспетчер сервлете определять не валидные адреса и бросать ошибку, это общий перехватчик для всех контроллеров
    @ExceptionHandler(NoHandlerFoundException.class)
    public String handle(Exception ex) {
        return "redirect:/404";
    }


}
