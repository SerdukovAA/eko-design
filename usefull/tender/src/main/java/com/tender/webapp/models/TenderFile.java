package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Serdukov on 14.06.2015.
 */
@Entity
@Table(name = "tenders_files")
public class TenderFile {

public TenderFile(){}


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(name = "file_name", unique = false, nullable = false)
    private String fileName;


    @Column(name = "file_link", unique = true, nullable = false)
    private String fileLink;

    @Column(name = "file_date_create", unique = false, nullable = false)
    private Date fileDateCreate;

    @ManyToOne
    @JoinColumn(name = "tender_id")
    @JsonIgnore
    private Tender tender;


    @Column(name = "origin_name", unique = false, nullable = true)
    private String originName;

        /*--getters and setters-*/

    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }

    public Date getFileDateCreate() {
        return fileDateCreate;
    }

    public void setFileDateCreate(Date fileDateCreate) {
        this.fileDateCreate = fileDateCreate;
    }

    public String getFileLink() {
        return fileLink;
    }

    public void setFileLink(String fileLink) {
        this.fileLink = fileLink;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }
}
