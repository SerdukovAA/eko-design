package com.tender.webapp.models.handbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(
        name = "selector_options"
)
public class SelectorOption {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private int id;
    @Column(
            name = "option_id",
            unique = false,
            nullable = false
    )
    private int optionId;
    @Column(
            name = "option_name",
            unique = false,
            nullable = false
    )
    private String optionName;
    @ManyToOne
    @JoinColumn(
            name = "cat_sel_id"
    )
    @JsonIgnore
    private CategorySelector categorySelector;
    @Column(
            name = "option_comment",
            unique = false,
            nullable = false
    )
    private String optionComment;
    @Transient
    private boolean checked;

    public SelectorOption() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOptionId() {
        return this.optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }

    public CategorySelector getCategorySelector() {
        return this.categorySelector;
    }

    public void setCategorySelector(CategorySelector categorySelector) {
        this.categorySelector = categorySelector;
    }

    public String getOptionName() {
        return this.optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public boolean isChecked() {
        return this.checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getOptionComment() {
        return this.optionComment;
    }

    public void setOptionComment(String optionComment) {
        this.optionComment = optionComment;
    }
}
