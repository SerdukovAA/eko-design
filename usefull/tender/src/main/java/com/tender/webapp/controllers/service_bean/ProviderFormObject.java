package com.tender.webapp.controllers.service_bean;

import com.tender.webapp.models.Adress;
import com.tender.webapp.models.base.City;
import com.tender.webapp.models.base.Region;

import javax.persistence.Column;

/**
 * Created by Serdukov on 02.06.2015.
 */

public class ProviderFormObject {

    private String firstNameContactPerson;
    private String lastNameContactPerson;
    private String p_login;
    private String emailContactPerson;
    private String phoneNumberContactPerson;
    private String companyName;
    private String inn;
    private String companyDescription;
    private Adress adress;
    private boolean acceptAgreementProvider = false;
    private String recaptchaResponse;

    /*--getters and setters-*/

    public String getFirstNameContactPerson() {
        return firstNameContactPerson;
    }

    public void setFirstNameContactPerson(String firstNameContactPerson) {
        this.firstNameContactPerson = firstNameContactPerson;
    }

    public String getLastNameContactPerson() {
        return lastNameContactPerson;
    }

    public void setLastNameContactPerson(String lastNameContactPerson) {
        this.lastNameContactPerson = lastNameContactPerson;
    }

    public String getP_login() {
        return p_login;
    }

    public void setP_login(String p_login) {
        this.p_login = p_login;
    }

    public String getEmailContactPerson() {
        return emailContactPerson;
    }

    public void setEmailContactPerson(String emailContactPerson) {
        this.emailContactPerson = emailContactPerson;
    }

    public String getPhoneNumberContactPerson() {
        return phoneNumberContactPerson;
    }

    public void setPhoneNumberContactPerson(String phoneNumberContactPerson) {
        this.phoneNumberContactPerson = phoneNumberContactPerson;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    public boolean isAcceptAgreementProvider() {
        return acceptAgreementProvider;
    }

    public void setAcceptAgreementProvider(boolean acceptAgreementProvider) {
        this.acceptAgreementProvider = acceptAgreementProvider;
    }

    public String getRecaptchaResponse() {
        return recaptchaResponse;
    }

    public void setRecaptchaResponse(String recaptchaResponse) {
        this.recaptchaResponse = recaptchaResponse;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }


}
