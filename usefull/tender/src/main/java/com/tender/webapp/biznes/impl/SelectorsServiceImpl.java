package com.tender.webapp.biznes.impl;


import com.tender.webapp.biznes.SelectorsService;
import com.tender.webapp.controllers.service_bean.ParametersRequest;
import com.tender.webapp.models.handbook.ParameterRelation;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serdukov on 05.06.2015.
 */
public class SelectorsServiceImpl implements SelectorsService {

    @Resource
    protected SessionFactory sessionFactory;

    /**
     * +
     * По указанным при регистрации нового тендера селекторам, метод возвращает небходимы поля,
     * которые нужно заполнить для каждого селектора
     *
     * @param parametersRequestsList
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, readOnly = true)
    public List<ParameterRelation> getParametersRelations(List<ParametersRequest> parametersRequestsList) throws Exception {
        List<ParameterRelation> parameterRelations = new ArrayList<ParameterRelation>();
        Session session = sessionFactory.getCurrentSession();
        Query query;
        for (ParametersRequest parametersRequest : parametersRequestsList) {
            query = session.createQuery("from ParameterRelation where selector_id =:selector_id and  option_id = :option_id");
            query.setParameter("selector_id", parametersRequest.getSelectorId());
            query.setParameter("option_id", parametersRequest.getOptionId());
            List<ParameterRelation> parameterRelation = query.list();
            parameterRelations.addAll(parameterRelation);
        }
        return parameterRelations;
    }


}
