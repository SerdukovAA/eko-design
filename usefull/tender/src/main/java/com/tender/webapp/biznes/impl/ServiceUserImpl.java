package com.tender.webapp.biznes.impl;


import com.tender.webapp.biznes.EmailService;
import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.biznes.ValidateService;
import com.tender.webapp.config.ParamsProvider;
import com.tender.webapp.controllers.service_bean.ChangePassRequest;
import com.tender.webapp.controllers.service_bean.ConsumerFormObject;
import com.tender.webapp.controllers.service_bean.ParametersRequest;
import com.tender.webapp.controllers.service_bean.ProviderFormObject;
import com.tender.webapp.dao.PasswordRecoveryListDAO;
import com.tender.webapp.dao.TenderDAO;
import com.tender.webapp.dao.UserDAO;
import com.tender.webapp.models.*;
import com.tender.webapp.models.handbook.TenderStatus;
import com.tender.webapp.models.handbook.UserParameter;
import com.tender.webapp.models.handbook.UserPreferedRegion;
import com.tender.webapp.models.journal.PasswordRecoveryList;
import com.tender.webapp.models.journal.PhoneCheckJournal;
import com.tender.webapp.utils.impl.GenereteHashUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


/**
 * Created by Serdukov on 05.06.2015.
 */
public class ServiceUserImpl implements ServiceUser {

    @Resource
    protected SessionFactory sessionFactory;

    @Autowired
    UserDAO userDAO;

    @Autowired
    TenderDAO tenderDAO;

    @Autowired
    PasswordRecoveryListDAO passwordRecoveryListDAO;

    @Autowired
    EmailService emailService;

    @Autowired
    Environment env;
    private static final Logger log = Logger.getLogger(ServiceUserImpl.class);

    @Autowired
    ValidateService validateService;

    private static final boolean isDevelop = ParamsProvider.isDevelop;

    /**
     * Смена пароля пользователем
     *
     * @param chPass - объект смены пароля
     * @param user   -  пользователь для которого меняется пароль
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void changePassword(ChangePassRequest chPass, User user) throws Exception {
        User userDB = userDAO.getUserById(user.getUser_id());
        if (chPass.getOldPassword().trim().isEmpty() || chPass.getNewPassword().trim().isEmpty())
            throw new Exception("Переданы не все необходимые поля");
        String newPassword = chPass.getNewPassword().trim();
        String oldPassword = chPass.getOldPassword().trim();
        if (!oldPassword.equals(userDB.getPassword())) throw new Exception("Переданный старый пароль не верен");
        userDAO.changePassword(user.getUser_id(), newPassword);
    }


    /**
     * Смена пароля после восстановления, завершающий этап смены пароля
     *
     * @param request
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void changePasswordRecovery(HttpServletRequest request) throws Exception {
        String hash = request.getParameter("hash");
        String password = request.getParameter("password");
        int rec_id = Integer.parseInt(request.getParameter("recovery_id"));
        if (!checkPasswordRecovery(hash, rec_id))
            throw new Exception("Смена пароля не может быть осущствленна так как запрос на смену не найден.");
        PasswordRecoveryList passwordRecoveryList = passwordRecoveryListDAO.getById(rec_id);
        userDAO.changePassword(passwordRecoveryList.getUser().getUser_id(), password);
        passwordRecoveryListDAO.updateDate(passwordRecoveryList);
    }


    /**
     * Удаление пользователя, а точнее перевод его в 3 статус - САМОУДАЛЕННЫЙ
     *
     * @param user_id - id пользователя, которого удаляем
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteBySelf(int user_id) throws Exception {
        //нужно проверить есть ли действующие тендеры у пользователя
        List<Tender> tenderList = tenderDAO.getActivTendersByUserId(user_id);
        if (!tenderList.isEmpty()) throw new Exception("У Вас есть не завершенные тендеры");
        //если нет удаляем пользоватля
        userDAO.deleteBySelf(user_id);
    }

    /**
     * Первый этап востановления пароля по логину или емайл запрашивается существует ли пользовтаель,
     * если существует то начинам процесс восстановления
     *
     * @param loginEmail
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void passwordRecovery(String loginEmail) throws Exception {
        User user = userDAO.findUserByEmail(loginEmail);
        if (user == null) {
            user = userDAO.findUserByLogin(loginEmail);
            if (user == null) {
                throw new Exception("Запрашиваемого логина или адреса электронной почты в системе не найдено");
            } else {
                String hash = GenereteHashUtil.genHash("" + new Date() + loginEmail);
                PasswordRecoveryList pR = passwordRecoveryListDAO.save(user, hash);
                emailService.senMailOnPasswordReset(user.getPrivateInfo().getEmail(), hash, pR.getId());
            }
        } else {
            String hash = GenereteHashUtil.genHash("" + new Date() + loginEmail);
            PasswordRecoveryList pR = passwordRecoveryListDAO.save(user, hash);
            emailService.senMailOnPasswordReset(user.getPrivateInfo().getEmail(), hash, pR.getId());
        }

    }


    /**
     * Проверяем что запись на восстаноление существует и она еще не сипользована.
     * После использования записи ей проставляется date_change
     *
     * @param hash
     * @param rec_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public boolean checkPasswordRecovery(String hash, int rec_id) throws Exception {
        PasswordRecoveryList passwordRecoveryList = passwordRecoveryListDAO.getById(rec_id);
        if (passwordRecoveryList.getCheckHash() == null)
            throw new Exception("Запись на восстанолвение пароля не найдена");
        return passwordRecoveryList.getCheckHash().equals(hash) && passwordRecoveryList.getDateChange() == null;
    }

    /**
     * Возвращаем параметры настройки для юзера. UserParameter - расширяемый класс. Отделен от юзера чтобы при изменениях не трогать юзера
     *
     * @param user_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public UserParameter getProviderParameters(int user_id) throws Exception {
        User user = userDAO.getUserById(user_id);
        if (user.getUserParameter() != null) {
            return user.getUserParameter();
        } else {
            return new UserParameter();
        }
    }

    /**
     * Возвращаем юзера по ид
     *
     * @param user_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public User getUserByUserId(int user_id) throws Exception {
        return userDAO.getUserById(user_id);
    }

    /**
     * Возвращаем юзера по ид
     *
     * @param login
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public User getUserByLogin(String login) throws Exception {
        return userDAO.getUserByLogin(login);
    }


    /**
     * Возвращаем собственника тендера для победителя
     *
     * @param user_id
     * @param tender_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public User getTenderOwnerForWinner(int user_id, int tender_id) throws Exception {

        Session session = sessionFactory.getCurrentSession();

        Query query = session.createQuery("from Tender where tender_id in (select offer.tender from Offer offer where offer.provider.user_id = :user_id and offer.offerStatus.id =2 and offer.tender.id = :tender_id) and tender_status_id = 3 and user_owner_id != :user_id");
        query.setParameter("user_id", user_id);
        query.setParameter("tender_id", tender_id);

        Tender tender = (Tender) query.uniqueResult();

        if (tender != null) return tender.getUserOwner();
        else return null;

    }


    /**
     * Возвращаем параметры настройки категорий участия для исполнителя
     *
     * @param user_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<ProviderFilterValues> getSelectedProvidersFilters(int user_id) throws Exception {
        User user = userDAO.getUserById(user_id);
        if (!user.isProvider()) return new ArrayList<ProviderFilterValues>();
        List<ProviderFilterValues> providerFilterValuesList = user.getJuridicalInfo().getProviderFilterValues();

        if (providerFilterValuesList != null && !providerFilterValuesList.isEmpty()) {
            return providerFilterValuesList;
        } else {
            return new ArrayList<ProviderFilterValues>();
        }
    }


    /**
     * Сервис изменения параметров пользователя
     *
     * @param userParameter
     * @param user
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveMailUserParameters(UserParameter userParameter, User user) throws Exception {
        User userCH = userDAO.getUserById(user.getUser_id());
        //изменяем только условия рассылки, чтобы через данный метод не подсовываи другие изменения
        UserParameter uP = userCH.getUserParameter();
        uP.setMailAll(userParameter.isMailAll());
        uP.setMailOffer(userParameter.isMailOffer());
        uP.setMailTender(userParameter.isMailTender());
        uP.setMailComment(userParameter.isMailComment());
        userCH.setUserParameter(uP);
        userDAO.updateUser(userCH);
    }

    /**
     * Сервис изменения предпочитаемого региона поиска тендеров
     *
     * @param userPreferedRegions
     * @param user
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void saveProviderPreferRegions(List<UserPreferedRegion> userPreferedRegions, User user) throws Exception {
        User userCH = userDAO.getUserById(user.getUser_id());
        Session session = sessionFactory.getCurrentSession();
        for (UserPreferedRegion usPref : userCH.getUserPreferedRegions()) {
            session.delete(usPref);
        }

        List<UserPreferedRegion> userPreferedRegionList = new ArrayList<UserPreferedRegion>();
        for (UserPreferedRegion userPreferedRegion : userPreferedRegions) {
            UserPreferedRegion n = new UserPreferedRegion();

            if(userPreferedRegion.getCityCode().trim().isEmpty()){
                n.setCityCode("0");
            }else{
                n.setCityCode(userPreferedRegion.getCityCode());
            }

            n.setRegionCode(userPreferedRegion.getRegionCode());
            n.setRegionCode(userPreferedRegion.getRegionCode());
            n.setUser(userCH);
            userPreferedRegionList.add(n);
        }

        userCH.setUserPreferedRegions(userPreferedRegionList);
        userDAO.updateUser(userCH);
    }


    /**
     * Изменение фильтров категории участия для исполнителей
     *
     * @param user
     * @param parametersRequestList
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveProviserFiltersParameters(List<ParametersRequest> parametersRequestList, User user) throws Exception {

        User userCH = userDAO.getUserById(user.getUser_id());
        List<ProviderFilterValues> providerFilterValuesList = new ArrayList<ProviderFilterValues>();

        for (ParametersRequest request : parametersRequestList) {
            ProviderFilterValues providerFilterValues = new ProviderFilterValues();
            providerFilterValues.setJuridicalInfo(user.getJuridicalInfo());
            providerFilterValues.setSelectorId(request.getSelectorId());
            providerFilterValues.setValueId(request.getOptionId());
            providerFilterValues.setTypeServiceId(request.getTypeServiceId());
            providerFilterValuesList.add(providerFilterValues);
        }

        for (ProviderFilterValues prFV : userCH.getJuridicalInfo().getProviderFilterValues()) {
            sessionFactory.getCurrentSession().delete(prFV);
        }
        userCH.getJuridicalInfo().setProviderFilterValues(providerFilterValuesList);
        userDAO.updateUser(userCH);
    }


    /**
     * Редактирование информации о существующем исполнителе
     * @param providerFormObject
     * @param user
     * @throws Exception
     */

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void editProvider(ProviderFormObject providerFormObject, User user) throws Exception {


        Session session = sessionFactory.getCurrentSession();

        try {
            user = (User) session.get(User.class, Integer.valueOf(user.getUser_id()));

            if (!user.isProvider())
                throw new Exception("Пользователь не является исполнителем, его нельзя отредактировать в редактировании информации о исполнителях");


            user.setDateChange(new Date());
            String login = providerFormObject.getP_login().trim().toLowerCase();


            if(!user.getLogin().equals(login)){
                if (validateService.checkLoginExist(login)) {
                    throw new Exception("Пользователь с таким логином уже присутствует в базе");
                } else {
                    user.setLogin(login);
                }
            }


            //юридическая информация
            JuridicalInfo juridicalInfo = user.getJuridicalInfo();
            juridicalInfo.setDateChange(new Date());
            juridicalInfo.setCompanyName(providerFormObject.getCompanyName().trim());

            if (providerFormObject.getCompanyDescription() != null && !providerFormObject.getCompanyDescription().trim().isEmpty()) {
                juridicalInfo.setCompanyDescription(providerFormObject.getCompanyDescription().trim());
            }


            juridicalInfo.setFactAdrLatitude(providerFormObject.getAdress().getLatitude());
            juridicalInfo.setFactAdrLongitude(providerFormObject.getAdress().getLongitude());


            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getRegion()!= null && !providerFormObject.getAdress().getRegion().isEmpty()) {
                juridicalInfo.setRegionCode(providerFormObject.getAdress().getRegion());
            }

            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getRayon()!= null && !providerFormObject.getAdress().getRayon().isEmpty()) {
                juridicalInfo.setRayonCode(providerFormObject.getAdress().getRayon());
            }

            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getCity()!= null && !providerFormObject.getAdress().getCity().isEmpty()) {
                juridicalInfo.setCityCode(providerFormObject.getAdress().getCity());
            }

            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getFullAdress()!= null && !providerFormObject.getAdress().getFullAdress().isEmpty()) {
                juridicalInfo.setFactAdress(providerFormObject.getAdress().getFullAdress());
            }

            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getStreet()!= null && !providerFormObject.getAdress().getStreet().isEmpty()) {
                juridicalInfo.setStreet(providerFormObject.getAdress().getStreet());
            }


            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getHouseNumber()!= null && !providerFormObject.getAdress().getHouseNumber().isEmpty()) {
                juridicalInfo.setHouseNumber(providerFormObject.getAdress().getHouseNumber());
            }

            String inn = providerFormObject.getInn().trim();

            if(!juridicalInfo.getInn().equals(inn)){
                if (validateService.checkINNExist(inn)) {
                    throw new Exception("Юр. лицо с таким ИНН уже зарегистрированно в системе");
                }else{
                    juridicalInfo.setInn(inn);
                }
            }


            //контактное лицо
            PrivateInfo privInf = user.getPrivateInfo();
            privInf.setDateChange(new Date());


            String emailContactPerson = providerFormObject.getEmailContactPerson().trim().toLowerCase();

            if(!privInf.getEmail().equals(emailContactPerson)){
                if (validateService.checkEmailExist(emailContactPerson)) {
                    throw new Exception("Пользователь с таким адресом электронной почты уже присутствует в базе");
                }else{
                    privInf.setEmail(emailContactPerson);
                }
            }



            privInf.setFirstName(providerFormObject.getFirstNameContactPerson().trim());
            privInf.setLastName(providerFormObject.getLastNameContactPerson().trim());
            privInf.setPhoneNumber(providerFormObject.getPhoneNumberContactPerson().trim().replaceAll("\\s", ""));

            session.update(user);
            log.info(providerFormObject.toString() + ". Исполнитель успешно отредактирован");
        } catch (RuntimeException e) {
            e.printStackTrace();
            log.error(providerFormObject.toString() + ". Ошибка транзакции при создании и сохранении исполнителя", e);
            throw new Exception(e.getMessage());
        }
    }
    /**
     * Методу создания нового исполнителя по переданному объекту регистрации заказчика
     *
     * @param providerFormObject
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void createAndSaveProvider(ProviderFormObject providerFormObject) throws Exception {

        Session session = sessionFactory.getCurrentSession();
        log.info(providerFormObject.toString() + ". Создаем нового исполнителя.");
        try{

            User user = new User();
            String login = providerFormObject.getP_login().trim().toLowerCase();
            if (validateService.checkLoginExist(login)) {
                throw new Exception("Пользователь с таким логином уже присутствует в базе");
            }

            user.setLogin(login);
            user.setPassword(null);
            user.setDateCreate(new Date());
            user.setBalance(new Balance(new BigDecimal(0.0)));
            user.setJuridical(true);
            user.setProvider(true);

            //(5,'DATA_VALIDATION');
            UserStatus userStatus = (UserStatus) session.get(UserStatus.class, 5);
            user.setUserStatus(userStatus);

            //(2,'USER_PROVIDER');
            UserRole userRole = (UserRole) session.get(UserRole.class, 2);
            user.setUserRole(userRole);

            //основная информация юр лица
            JuridicalInfo juridicalInfo = new JuridicalInfo();
            juridicalInfo.setDateCreate(new Date());
            juridicalInfo.setCompanyName(providerFormObject.getCompanyName().trim());



            //TODO проверка на пустой адрес только один раз

            juridicalInfo.setFactAdrLatitude(providerFormObject.getAdress().getLatitude());
            juridicalInfo.setFactAdrLongitude(providerFormObject.getAdress().getLongitude());


            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getRegion()!= null && !providerFormObject.getAdress().getRegion().isEmpty()) {
                juridicalInfo.setRegionCode(providerFormObject.getAdress().getRegion());
            }

            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getRayon()!= null && !providerFormObject.getAdress().getRayon().isEmpty()) {
                juridicalInfo.setRayonCode(providerFormObject.getAdress().getRayon());
            }

            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getCity()!= null && !providerFormObject.getAdress().getCity().isEmpty()) {
                juridicalInfo.setCityCode(providerFormObject.getAdress().getCity());
            }

            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getFullAdress()!= null && !providerFormObject.getAdress().getFullAdress().isEmpty()) {
                juridicalInfo.setFactAdress(providerFormObject.getAdress().getFullAdress());
            }


            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getStreet()!= null && !providerFormObject.getAdress().getStreet().isEmpty()) {
                juridicalInfo.setStreet(providerFormObject.getAdress().getStreet());
            }


            if (providerFormObject.getAdress()!= null && providerFormObject.getAdress().getHouseNumber()!= null && !providerFormObject.getAdress().getHouseNumber().isEmpty()) {
                juridicalInfo.setHouseNumber(providerFormObject.getAdress().getHouseNumber());
            }



            String inn = providerFormObject.getInn().trim();
            if (validateService.checkINNExist(inn)) {
                throw new Exception("Юр. лицо с таким ИНН уже зарегистрированно в системе");
            }
            juridicalInfo.setInn(inn);

            user.setJuridicalInfo(juridicalInfo);


            //контактное лицо
            PrivateInfo privInf = new PrivateInfo();
            privInf.setDateCreate(new Date());
            String emailContactPerson = providerFormObject.getEmailContactPerson().trim().toLowerCase();
            if (validateService.checkEmailExist(emailContactPerson)) {
                throw new RuntimeException("Пользователь с таким адресом электронной почты уже присутствует в базе");
            }
            privInf.setEmail(emailContactPerson);

            privInf.setFirstName(providerFormObject.getFirstNameContactPerson().trim());
            privInf.setLastName(providerFormObject.getLastNameContactPerson().trim());
            privInf.setPhoneNumber(providerFormObject.getPhoneNumberContactPerson().trim().replaceAll("\\s", ""));
            privInf.setJurContact(true);

            user.setPrivateInfo(privInf);
            session.save(user);
            session.flush();
            emailService.createNewEmailCheckNote(privInf);
            log.info(providerFormObject.toString()+". Исполнитель успешно создан и записан в базу.");
        } catch (RuntimeException e) {
            e.printStackTrace();
            log.error(providerFormObject.toString()+". Ошибка транзакции при создании и сохранении исполнителя", e);
            throw new Exception(e.getMessage());
        }
    }

    /**
     * Метод создает првоерочный код
     * @param phoneNumber
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String getCreateAndCheckPhoneID(String phoneNumber) throws Exception{
        Session session = sessionFactory.getCurrentSession();
        PhoneCheckJournal phoneCheckJournal = new PhoneCheckJournal();
        phoneCheckJournal.setDateRequest(new Date());
        phoneCheckJournal.setUserPhone(phoneNumber.trim().replaceAll("\\s", ""));
        phoneCheckJournal.setIsCheck(false);
        Random rand = new Random();
        int randomNum = rand.nextInt((99999 - 11111) + 1) + 11111;
        phoneCheckJournal.setCheckCode("" + randomNum);
        session.save(phoneCheckJournal);

        ///Отправляем код пользователю смс
        sendSMSCHttpRequest(phoneNumber, "" + randomNum);

        log.info(phoneNumber + " - Сохранение проверочного кода -  успешно");
        return "" + phoneCheckJournal.getId();
    }

    private void sendSMSCHttpRequest(String phoneNumber, String checkCode) throws Exception {

        //если включен режим разработки то отправка не происходит
        if(isDevelop) return;


        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {

            // Creating an instance of HttpPost.
            HttpPost httpost = new HttpPost("https://smsc.ru/sys/send.php");

            MessageDigest md = MessageDigest.getInstance("MD5");
            String str = "Alex_6791";
            md.update(str.getBytes());
            byte byteData[] = md.digest();
            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            String pswMD5 = sb.toString().toLowerCase();


            // Adding all form parameters in a List of type NameValuePair
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("login", "Versteck"));
            nvps.add(new BasicNameValuePair("psw", pswMD5));
            nvps.add(new BasicNameValuePair("phones", phoneNumber));
            nvps.add(new BasicNameValuePair("mes", "UBETEN.COM::Ваш код подтверждения " + checkCode));
            nvps.add(new BasicNameValuePair("sender", "UBETEN.COM"));
            nvps.add(new BasicNameValuePair("charset", "utf-8"));
            nvps.add(new BasicNameValuePair("cost", "0")); //0 обычная, 1 проверочная отправка

            /**
             * UrlEncodedFormEntity encodes form parameters and produce an
             * output like param1=value1&param2=value2
             */
            httpost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));

            // Executing the request.
            CloseableHttpResponse response = httpclient.execute(httpost);
            System.out.println("Response Status line :" + response.getStatusLine());
            try {
                // Do the needful with entity.
                HttpEntity entity = response.getEntity();
                StringWriter writer = new StringWriter();
                IOUtils.copy(entity.getContent(), writer);
                System.out.println("HttpEntity  " + writer.toString());
            } finally {
                // Closing the response
                response.close();
            }
        } finally {
            httpclient.close();
        }

    }

    /**
     * Методу создания нового заказчика по переданному объекту регистрации заказчика
     *
     * @param consumerFormObject
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void createAndSaveConsumer(ConsumerFormObject consumerFormObject) throws Exception {
        try {

            Session session = sessionFactory.getCurrentSession();
            log.info(consumerFormObject.toString() +". Создаем нового заказчика.");

            User user = new User();
            user.setLogin(consumerFormObject.getLogin().trim().toLowerCase());
            user.setPassword(consumerFormObject.getPassword().trim());
            user.setDateCreate(new Date());
            user.setBalance(new Balance(new BigDecimal(0.0)));
            user.setJuridical(consumerFormObject.isJuridical());
            user.setProvider(false);

            //(1,'ACTIVE');
            UserStatus userStatus = (UserStatus) session.get(UserStatus.class, 1);
            user.setUserStatus(userStatus);

            //(3,'USER_CONSUMER');
            UserRole userRole = (UserRole) session.get(UserRole.class, 3);
            user.setUserRole(userRole);

            user.setJuridicalInfo(null);

            PrivateInfo privInf = new PrivateInfo();
            privInf.setDateCreate(new Date());
            privInf.setEmail(consumerFormObject.getEmail().trim().toLowerCase());
            privInf.setFirstName(consumerFormObject.getFirstName().trim());
            privInf.setLastName(consumerFormObject.getLastName().trim());
            privInf.setPhoneNumber(consumerFormObject.getPhoneNumber().trim().replaceAll("\\s",""));
            privInf.setJurContact(false);

            user.setPrivateInfo(privInf);


            if (validateService.checkLoginExist(user.getLogin())) {
                throw new RuntimeException("Пользователь с таким логином уже присутствует в базе");
            }
            if (validateService.checkEmailExist(privInf.getEmail())) {
                throw new RuntimeException("Пользователь с таким адресом электронной почты уже присутствует в базе");
            }
            session.save(user);
            log.info(consumerFormObject.toString()+". Заказчик успешно создан и записан в базу.");
        } catch (RuntimeException e){
            e.printStackTrace();
            log.error(consumerFormObject.toString() +". Ошика при сохранении пользователя в систему ", e);
            throw new Exception(e.getMessage());
        }

    }


    /*  private String saveINN(MultipartFile file) throws Exception {
          File dir = new File(System.getProperty("user.home") + "/tender_files/inn/");
          if (!dir.exists())
              dir.mkdirs();

          String fileName = GenereteHashUtil.genHash(file.getOriginalFilename() + new Date()) + "." + FilenameUtils.getExtension(file.getOriginalFilename());
          if (!file.isEmpty()) {
              try {
                  byte[] bytes = file.getBytes();
                  BufferedOutputStream stream =
                          new BufferedOutputStream(new FileOutputStream(new File(System.getProperty("user.home") + "/tender_files/inn/" + fileName)));
                  stream.write(bytes);
                  stream.close();
                  return env.getProperty("server.id")+"/tender_files/inn/" + fileName;
              } catch (IOException e) {
                  throw new Exception("Ошибка сохранения ИНН. - " + e.getMessage());
              }
          } else {
              throw new Exception("Ошибка сохранения ИНН. Файл пуст.");

          }

      }
  */


    public void adminDeleteUser(int userId) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            User user = (User) session.get(User.class, userId);
            user.setUserStatus((UserStatus) session.get(UserStatus.class, 6));
            session.update(user);
            //блокируем все тендеры пользователя кроме тех что на исполнении
            Query query = session.createQuery("from Tender where user_owner_id = :user_id and tender_status_id != 3");
            query.setParameter("user_id", user.getUser_id());
            List<Tender> tenders = query.list();
            for (int i = 0; i < tenders.size(); i++) {
                Tender tender = tenders.get(i);
                tender.setTenderStatus((TenderStatus) session.get(TenderStatus.class, 5));
                session.update(tender);
            }

            tx.commit();
            log.info("Администратор перевел юзера id=" + userId + " в статус удаленных");
        } catch (RuntimeException e) {
            e.printStackTrace();
            try {
                tx.rollback();
            } catch (RuntimeException rbe) {
                log.error("Не получатся откатить транзакцию при удалении пользователя:", rbe);
            }
            log.error("Ошибка удаление пользователя администратором", e);
            throw new Exception("Выпадаем из транзакции" + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

    public void userDeleteSelf(int userId) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            //переводим юзера в статус самоудаленный
            User user = (User) session.get(User.class, userId);
            user.setUserStatus((UserStatus) session.get(UserStatus.class, 3));
            session.update(user);
            //блокируем все тендеры пользователя кроме тех что на исполнении
            Query query = session.createQuery("from Tender where user_owner_id = :user_id and tender_status_id != 3");
            query.setParameter("user_id", user.getUser_id());
            List<Tender> tenders = query.list();
            for (int i = 0; i < tenders.size(); i++) {
                Tender tender = tenders.get(i);
                tender.setTenderStatus((TenderStatus) session.get(TenderStatus.class, 5));
                session.update(tender);
            }
            tx.commit();
            log.info("Пользователь  id=" + userId + " переведен в статус удаленных");
        } catch (RuntimeException e) {
            e.printStackTrace();
            try {
                tx.rollback();
            } catch (RuntimeException rbe) {
                log.error("Не получатся откатить транзакцию при удалении пользователя:", rbe);
            }
            log.error("Ошибка удаление пользователя самостоятельно", e);
            throw new Exception("Выпадаем из транзакции" + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }














}
