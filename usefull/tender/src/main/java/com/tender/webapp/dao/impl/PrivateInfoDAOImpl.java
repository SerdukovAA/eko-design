package com.tender.webapp.dao.impl;

import com.tender.webapp.dao.PrivateInfoDAO;
import com.tender.webapp.models.PrivateInfo;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.Resource;

/**
 * Created by serdukov on 01.11.15.
 */
public class PrivateInfoDAOImpl implements PrivateInfoDAO {

    @Resource
    protected SessionFactory sessionFactory;


    /**
     * Метод поиска информации о пользователе по номеру телефона
     * @param phoneNumber
     * @return
     */
    public PrivateInfo findPrivateInfoByPhoneNumber(String phoneNumber) throws Exception{
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from PrivateInfo where phone_number = :phone and is_jur_contact=false");
        query.setParameter("phone", phoneNumber.trim().toLowerCase());
        return (PrivateInfo) query.uniqueResult();
    }



}
