package com.tender.webapp.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.UserParameter;
import com.tender.webapp.models.handbook.UserPreferedRegion;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Serdukov on 02.06.2015.
 */
@Entity
@Table(name = "users")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int user_id;

    @Column(name = "login", unique = true, nullable = true)
    private String login;

    @Column(name = "password", unique = false, nullable = true)
    @JsonIgnore
    private String password;

    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;

    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;


    @Column(name = "is_juridical", unique = false, nullable = false)
    private boolean isJuridical;

    @Column(name = "is_provider", unique = false, nullable = false)
    private boolean isProvider;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "private_info_id")
    private PrivateInfo privateInfo;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "juridical_info_id")
    private JuridicalInfo juridicalInfo;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "balance_id")
    @JsonIgnore
    private Balance balance;


    @ManyToOne
    @JoinColumn(name = "user_role_id")
    private UserRole userRole;


    @ManyToOne
    @JoinColumn(name = "user_status_id")
    private UserStatus userStatus;


    @OneToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_param_id")
    private UserParameter userParameter;


    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<UserPreferedRegion> userPreferedRegions;



    /*--getters and setters-*/


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public boolean isJuridical() {
        return isJuridical;
    }

    public void setJuridical(boolean isJuridical) {
        this.isJuridical = isJuridical;
    }


    public boolean isProvider() {
        return isProvider;
    }

    public void setProvider(boolean isProvider) {
        this.isProvider = isProvider;
    }



    public PrivateInfo getPrivateInfo() {
        return privateInfo;
    }

    public void setPrivateInfo(PrivateInfo privateInfo) {
        this.privateInfo = privateInfo;
    }

    public JuridicalInfo getJuridicalInfo() {
        return juridicalInfo;
    }

    public void setJuridicalInfo(JuridicalInfo juridicalInfo) {
        this.juridicalInfo = juridicalInfo;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }


    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }



    public UserParameter getUserParameter() {
        return userParameter;
    }

    public void setUserParameter(UserParameter userParameter) {
        this.userParameter = userParameter;
    }


    public List<UserPreferedRegion> getUserPreferedRegions() {
        return userPreferedRegions;
    }

    public void setUserPreferedRegions(List<UserPreferedRegion> userPreferedRegions) {
        this.userPreferedRegions = userPreferedRegions;
    }

}
