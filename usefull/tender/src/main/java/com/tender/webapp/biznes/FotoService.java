package com.tender.webapp.biznes;


import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * Created by Serdukov on 05.06.2015.
 */
public interface FotoService {

    public Map<String, String> uploadTenderFoto(List<MultipartFile> objectFotos) throws Exception;



}
