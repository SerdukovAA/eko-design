package com.tender.webapp.dao;

import com.tender.webapp.models.Tender;

import java.util.List;

/**
 * Created by serdukov on 01.11.15.
 */
public interface TenderDAO {
    public Tender cancelTender(int user_id, int tender_id)throws Exception;
    public Tender getActiveTenderById(int tender_id)throws Exception;
    public List<Tender> getActivTendersByUserId(int user_id)throws Exception;
    public List<Integer> getACTIVE_TENDER_STATUS();
    public List<Tender> getTendersForProvider(int provider_id)throws Exception;
    public List<Tender> getAllNotMyRunTenders(int my_tender_id);
    public List<Tender> getTendersWinForProvider(int provider_id) throws Exception;
    public List<Tender> getTendersWithOfferProvider(int provider_id) throws Exception;
    public List<Tender> getAllRunTenders(int page_num)throws Exception;
    public Long getAllRunTendersCount()throws Exception;
}
