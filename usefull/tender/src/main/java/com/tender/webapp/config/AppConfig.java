package com.tender.webapp.config;



import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@EnableWebMvc  //Аннотация разрешает использовать MVC
@Configuration //Объявляем конфигурационный класс
@ComponentScan(basePackages ={"com.tender.webapp"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RootConfig.class)
        })//говорит, где искать компоненты проекта.
public class AppConfig extends WebMvcConfigurerAdapter {//WebMvcConfigurerAdapter - абстрактный класс с заглушками, которые надо мы оверайдим




    //Объявление путей ресурсов.
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/index.html").addResourceLocations("/index.html"); //robot.txt
		registry.addResourceHandler("/res/**").addResourceLocations("/res/"); //ресурсы приложения css, js  т.д

		registry.addResourceHandler("/sitemap.xml").addResourceLocations("/sitemap.xml"); //sitemap.xml
		registry.addResourceHandler("/robots.txt").addResourceLocations("/robots.txt"); //robot.txt
        registry.addResourceHandler("/s1/tender_files/**").addResourceLocations("file:///"+System.getProperty("user.home")+"/tender_files/");
    }



    @Bean//этот бин имеет имя по умолчанию, смена имени приведет к отказу
    public MultipartResolver filterMultipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(20300000);
        return multipartResolver;
    }




}