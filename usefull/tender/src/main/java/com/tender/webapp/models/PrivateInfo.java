package com.tender.webapp.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Serdukov on 02.06.2015.
 * Bean represent private person information
 */
@Entity
@Table(name="private_info")
public class PrivateInfo {

    @Id
    @Column(name="private_info_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="first_name",unique=false)
    private String firstName;

    @Column(name="last_name",unique=false)
    private String lastName;

    @Column(name="phone_number",unique=false)
    private String phoneNumber;

    @Column(name="email",unique=false)
    private String email;


    @Column(name="is_jur_contact",unique=false)
    private boolean isJurContact;

    @Column(name="is_email_check",nullable=true)
    private boolean emailCheck;


    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;


    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;


    /*--getters and setters-*/


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean isJurContact() {
        return isJurContact;
    }

    public void setJurContact(boolean isJurContact) {
        this.isJurContact = isJurContact;
    }


    public boolean isEmailCheck() {return emailCheck;}

    public void setEmailCheck(boolean emailCheck) { this.emailCheck = emailCheck; }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }
}
