package com.tender.webapp.models;

import javax.persistence.*;

/**
 * Created by Serdukov on 05.06.2015.
 */
@Entity
@Table(name = "transaction_status")
public class TransactionStatus {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "status", unique = true, nullable = false)
    private String transactionStatus;

    @Column(name = "status_name", unique = true, nullable = false)
    private String statusName;




    /*--------getters and setters-----*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }




}
