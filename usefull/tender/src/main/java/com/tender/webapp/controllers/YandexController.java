package com.tender.webapp.controllers;

import com.tender.webapp.mail.MailManager;
import com.tender.webapp.models.*;
import com.tender.webapp.models.journal.CashTraffic;
import com.tender.webapp.models.journal.RefillTransactionJournal;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.PrintWriter;
import java.io.*;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import javax.servlet.*;
import javax.servlet.http.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by Serdukov on 22.08.2015.
 */
@Controller
public class YandexController {
    @Resource
    protected SessionFactory sessionFactory;

    @Autowired
    ExecutorService executorService;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    Environment env;


    private static final Logger log = Logger.getLogger(YandexController.class);

    @RequestMapping(value = {"/check_order", "/check_order_test"}, method = RequestMethod.POST)
    public void checkOrderTest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        log.info(" id сессии: " + request.getSession().getId() + ". Заходим на проверку /check_order");
        Enumeration enumeration = request.getParameterNames();
        log.info("Переданные Я.кассой параметры.");
        while (enumeration.hasMoreElements()) {
            String parameterName = (String) enumeration.nextElement();
            log.info("par - " + parameterName);
        }

        String responseXML;
        String action = "checkOrder";
        String invoiceId = request.getParameter("invoiceId").trim();
        int user_id = Integer.parseInt(request.getParameter("customerNumber").trim());
        String orderSumAmount = request.getParameter("orderSumAmount").trim();
        String shopSumAmount = request.getParameter("shopSumAmount").trim();
        int orderSumCurrencyPaycash = Integer.parseInt(request.getParameter("orderSumCurrencyPaycash").trim());
        int shopSumCurrencyPaycash = Integer.parseInt(request.getParameter("shopSumCurrencyPaycash").trim());
        String shopId = request.getParameter("shopId").trim();
        int t_uniq = Integer.parseInt(request.getParameter("t_uniq").trim());
        log.info(" id сессии: " + request.getSession().getId() + ". Парметры запроса check_order разобраны");
        if (checkMD5(request)) {
            Session session = sessionFactory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                Query query = session.createQuery("from RefillTransactionJournal where id = :t_uniq and user_id = :user_id and transaction_status_id = 1 ");
                query.setParameter("t_uniq", t_uniq);
                query.setParameter("user_id", user_id);
                RefillTransactionJournal reFT = (RefillTransactionJournal) query.uniqueResult();
                if (reFT == null) {
                    throw new Exception("Не найдено записи на пополнение счета дял данного юзера и T_UNIQ");
                }
                if (orderSumCurrencyPaycash != 643) ///в тестовом режиме 10643
                    throw new Exception("В сервисе используются только рублевые платежи");
                if (!shopId.equals(env.getProperty("yandex.shopId")))
                    throw new Exception("Наш shopId не совпадает в переданном по checkOrder");
                RefillTransactionJournal refillTransactionJournal = new RefillTransactionJournal();
                refillTransactionJournal.setDateCreate(new Date());
                refillTransactionJournal.setInvoiceId(new Long(invoiceId));
                refillTransactionJournal.setOrderSumAmount(new BigDecimal(orderSumAmount));
                refillTransactionJournal.setOrderSumCurrencyPaycash(orderSumCurrencyPaycash);
                refillTransactionJournal.setShopSumAmount(new BigDecimal(shopSumAmount));
                refillTransactionJournal.setShopSumCurrencyPaycash(shopSumCurrencyPaycash);
                refillTransactionJournal.settUniq(reFT);

                refillTransactionJournal.setTransactionStatus((TransactionStatus) session.get(TransactionStatus.class, 2));
                refillTransactionJournal.setUser(reFT.getUser());
                session.save(refillTransactionJournal);
                tx.commit();
                responseXML = buildResponse(action, invoiceId, 0, null);
                log.info(" id сессии: " + request.getSession().getId() + ".CheckOrder успешно");
            } catch (RuntimeException e) {
                e.printStackTrace();
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    log.info(" id сессии: " + request.getSession().getId() + ". Не получается откатить транзакцию CheckOrder");
                }
                log.info(" id сессии: " + request.getSession().getId() + ". Не удалось завершить CheckOrder: " + e.getMessage());
                responseXML = buildResponse(action, invoiceId, 100, "Ошибка на сервере. Не прошла транзакция для CheckOrder");
            } catch (Exception e) {
                responseXML = buildResponse(action, invoiceId, 100, "Ошибка на сервере:" + e.getMessage());
                log.info(" id сессии: " + request.getSession().getId() + "Ошибка на сервере:" + e.getMessage());
            } finally {
                if (session != null) {
                    session.close();
                }
            }
        } else {
            responseXML = buildResponse(action, invoiceId, 1, "Не совпадение md5");
        }
        System.out.println("Response Yandex - " + responseXML);
        log.info("Response Yandex - " + responseXML);
        response.setContentType("application/xml");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print(responseXML);

    }


    @RequestMapping(value = {"/payment_aviso", "/payment_aviso_test"}, method = RequestMethod.POST)
    public void paymentAvisoTest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        log.info(" id сессии: " + request.getSession().getId() + ". Заходим на подтверждение /payment_aviso");
        String responseXML;
        String action = "paymentAviso";
        String invoiceId = request.getParameter("invoiceId").trim();
        int user_id = Integer.parseInt(request.getParameter("customerNumber").trim());
        String orderSumAmount = request.getParameter("orderSumAmount").trim();
        String shopSumAmount = request.getParameter("shopSumAmount").trim();
        int orderSumCurrencyPaycash = Integer.parseInt(request.getParameter("orderSumCurrencyPaycash").trim());
        int shopSumCurrencyPaycash = Integer.parseInt(request.getParameter("shopSumCurrencyPaycash").trim());
        int t_uniq = Integer.parseInt(request.getParameter("t_uniq").trim());
        log.info(" id сессии: " + request.getSession().getId() + ". Парметры запроса payment_aviso разобраны");
        if (checkMD5(request)) {
            Session session = sessionFactory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                Query query = session.createQuery("from RefillTransactionJournal where t_uniq = :t_uniq and user_id = :user_id and transaction_status_id = 2");
                query.setParameter("t_uniq", t_uniq);
                query.setParameter("user_id", user_id);
                RefillTransactionJournal reFT = (RefillTransactionJournal) query.uniqueResult();
                if (reFT == null) throw new Exception("Не найдено записи на проверку дял данного юзера и T_UNIQ");

                RefillTransactionJournal refillTransactionJournal = new RefillTransactionJournal();
                refillTransactionJournal.setDateCreate(new Date());
                refillTransactionJournal.setInvoiceId(new Long(invoiceId));
                refillTransactionJournal.setOrderSumAmount(new BigDecimal(orderSumAmount));
                refillTransactionJournal.setOrderSumCurrencyPaycash(orderSumCurrencyPaycash);
                refillTransactionJournal.setShopSumAmount(new BigDecimal(shopSumAmount));
                refillTransactionJournal.setShopSumCurrencyPaycash(shopSumCurrencyPaycash);
                refillTransactionJournal.settUniq(reFT.gettUniq());

                refillTransactionJournal.setTransactionStatus((TransactionStatus) session.get(TransactionStatus.class, 3));
                refillTransactionJournal.setUser(reFT.getUser());
                session.save(refillTransactionJournal);


                String message = "<html><head><meta charset='UTF-8'></head><body><h2>Ваши деньги зачислены на счет.</h2>" +
                        "<p>Уважаемый, " + reFT.getUser().getPrivateInfo().getFirstName() + " " + reFT.getUser().getPrivateInfo().getLastName() + ". Ваши деньги зачислены на счет</p>" +
                        "<p>Баланс был пополнен на сумму " + orderSumAmount + " руб.</p>" +
                        "</body></html>";
                String tema = "Ваши деньги зачислены на счет сервиса";
                executorService.submit(new MailManager(reFT.getUser().getPrivateInfo().getEmail(), tema, message, javaMailSender));

                //Пополняем баланс исполнителя
                Balance providerBalace = reFT.getUser().getBalance();
                BigDecimal orderSumPlus = new BigDecimal(orderSumAmount);
                providerBalace.setValue(providerBalace.getValue().add(orderSumPlus));
                session.update(providerBalace);

                //добавить в журнал запись
                CashTraffic cashTraffic = new CashTraffic();
                cashTraffic.setBalance(providerBalace);
                cashTraffic.setRefillTransactionJournal(refillTransactionJournal);
                cashTraffic.setDateChange(new Date());
                cashTraffic.setCurrentValue(providerBalace.getValue());
                //вычислить сумму
                cashTraffic.setAmount(orderSumPlus);

                session.save(cashTraffic);
                tx.commit();
                responseXML = buildResponse(action, invoiceId, 0, null);
                log.info(" id сессии: " + request.getSession().getId() + ".PaymentAviso успешно");
            } catch (RuntimeException e) {
                e.printStackTrace();
                try {
                    tx.rollback();
                } catch (RuntimeException rbe) {
                    log.info(" id сессии: " + request.getSession().getId() + ". Не получается откатить транзакцию PaymentAviso");
                }
                log.info(" id сессии: " + request.getSession().getId() + ". Не удалось завершить PaymentAviso: " + e.getMessage());
                responseXML = buildResponse(action, invoiceId, 200, "Ошибка на сервере");
            } catch (Exception e) {
                responseXML = buildResponse(action, invoiceId, 200, "Ошибка на сервере");
                log.info(" id сессии: " + request.getSession().getId() + "Ошибка на сервере:" + e.getMessage());
            } finally {
                if (session != null) {
                    session.close();
                }
            }
        } else {
            responseXML = buildResponse(action, invoiceId, 1, "Не совпадение md5");
        }
        log.info("Response Yandex - " + responseXML);
        response.setContentType("application/xml");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print(responseXML);
    }


    private String buildResponse(String actiionName, String invoiceId, int code, String message) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document document = null;
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument();
            Element checkOrderResponse = document.createElement(actiionName + "Response");
            document.appendChild(checkOrderResponse);
            document.setXmlStandalone(true);

            TimeZone tz = TimeZone.getTimeZone("UTC");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            df.setTimeZone(tz);
            String performedDatetime = df.format(new Date());
            checkOrderResponse.setAttribute("performedDatetime", performedDatetime);

            checkOrderResponse.setAttribute("code", "" + code);
            if (code != 0) {
                if (message != null && !message.isEmpty()) checkOrderResponse.setAttribute("message", message);
            }
            checkOrderResponse.setAttribute("invoiceId", new Long(invoiceId).toString());
            checkOrderResponse.setAttribute("shopId", env.getProperty("yandex.shopId"));
            DOMSource domSource = new DOMSource(document);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(domSource, result);
            writer.flush();
            return writer.toString();
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
            throw new Exception("Ошибка парсинга xml");
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
            throw new Exception("Ошибка конфигурации трансформирования xml");
        } catch (TransformerException e) {
            e.printStackTrace();
            throw new Exception("Ошибка трансформирования xml");
        }

    }


    public boolean checkMD5(HttpServletRequest request) throws NoSuchAlgorithmException {
        StringBuffer sb1 = new StringBuffer();
        sb1.append(request.getParameter("action")).append(";");
        sb1.append(request.getParameter("orderSumAmount")).append(";");
        sb1.append(request.getParameter("orderSumCurrencyPaycash")).append(";");
        sb1.append(request.getParameter("orderSumBankPaycash")).append(";");
        sb1.append(request.getParameter("shopId")).append(";");
        sb1.append(request.getParameter("invoiceId")).append(";");
        sb1.append(request.getParameter("customerNumber")).append(";");
        sb1.append(env.getProperty("yandex.shoppass"));
        String str = sb1.toString();
        System.out.println("String to md5 " + sb1.toString());
        log.info("String to md5 " + sb1.toString());
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(str.getBytes());
        byte byteData[] = md.digest();
        //convert the byte to hex format method 1
        StringBuffer sb2 = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb2.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        System.out.println("Result md5 " + sb2.toString().toUpperCase());
        log.info("Result md5 " + sb2.toString().toUpperCase());
        if (request.getParameter("md5").equals(sb2.toString().toUpperCase())) {
            log.info(" id сессии: " + request.getSession().getId() + ". Проверка MD5 пройдена");
            return true;
        } else {
            log.info(" id сессии: " + request.getSession().getId() + ". Проверка MD5 НЕ пройдена !!!");
            return false;
        }
    }


    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }


}
