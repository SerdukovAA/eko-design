package com.tender.webapp.validation;


import com.tender.webapp.biznes.ValidateService;
import com.tender.webapp.comunicators.ReCaptchaChecker;
import com.tender.webapp.controllers.service_bean.ProviderFormObject;
import com.tender.webapp.models.JuridicalInfo;
import com.tender.webapp.models.PrivateInfo;
import com.tender.webapp.models.User;
import com.tender.webapp.models.service.ServerMessage;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serdukov on 06.06.2015.
 */
@Component
public class RegistrationFormProviderValidator {

    @Autowired
    ValidateService validateService;


    public void validate(ProviderFormObject providerFormObject, ServerMessage serverMessage) throws Exception {


        if (providerFormObject.getFirstNameContactPerson() == null || providerFormObject.getFirstNameContactPerson().trim().isEmpty()) {
            serverMessage.addError(30, "Не передано имя контактоного лица компании");
            if (!RegExpCheck.cyrillicNameRegExCheck(providerFormObject.getFirstNameContactPerson())) {
                serverMessage.addError(31, "Передаваемое имя контактного лица не корректно. Используйте кирилицу");
            }
        }


        if (providerFormObject.getLastNameContactPerson() == null || providerFormObject.getLastNameContactPerson().trim().isEmpty()) {
            serverMessage.addError(32, "Не передана фамилия контактного лица компании");
            if (!RegExpCheck.cyrillicNameRegExCheck(providerFormObject.getLastNameContactPerson())) {
                serverMessage.addError(33, "Передаваемая фамилия контактного лица не корректна. Используйте кирилицу");
            }
        }


        if (providerFormObject.isAcceptAgreementProvider() == false) {
            serverMessage.addError(34, "Необходимо согласиться с пользовательским соглашением");
        }

        if (providerFormObject.getPhoneNumberContactPerson() == null || providerFormObject.getPhoneNumberContactPerson().trim().isEmpty()) {
            serverMessage.addError(35, "Не передан номер телефона для контактного лица");
            if (!RegExpCheck.phoneRegExCheck(providerFormObject.getPhoneNumberContactPerson())) {
                serverMessage.addError(36, "Передаваемый номер телефона не действителен");
            }
        }

        if (providerFormObject.getCompanyName() == null || providerFormObject.getCompanyName().trim().isEmpty()) {
            serverMessage.addError(37, "Не передано наименование компании");
        }


        if (providerFormObject.getInn() == null || providerFormObject.getInn().trim().isEmpty()) {
            serverMessage.addError(40, "Не передан ИНН компании");
        } else {
            if (RegExpCheck.innRegExCheck(providerFormObject.getInn())) {
                if (validateService.checkINNExist(providerFormObject.getInn())) {
                    serverMessage.addError(57, "Юр. лицо с таким ИНН уже зарегистрированно в системе");
                }
            }else{
                serverMessage.addError(57, "ИНН не валидный");
            }
        }




        if(providerFormObject.getAdress() ==null) {
            serverMessage.addError(42, "Не передан адрес компании");
        }else{
            if (providerFormObject.getAdress().getRegion() == null ) {
                serverMessage.addError(42, "Не передан регион фактического адреса компании");
            }

            if (providerFormObject.getAdress().getStreet() == null || providerFormObject.getAdress().getStreet().trim().isEmpty()) {
                serverMessage.addError(42, "Не передана улица фактического адреса компании");
            }

            if (providerFormObject.getAdress().getHouseNumber() == null || providerFormObject.getAdress().getHouseNumber().trim().isEmpty()) {
                serverMessage.addError(42, "Не передан номер дома фактического адреса компании");
            }


            if(providerFormObject.getAdress().getLongitude()==0.0d){
                serverMessage.addError(42, "Не передана Longitude адреса компании");
            }
            if(providerFormObject.getAdress().getLatitude()==0.0d){
                serverMessage.addError(42, "Не передана Latitude адреса компании");
            }


            if (providerFormObject.getAdress().getFullAdress() == null || providerFormObject.getAdress().getFullAdress().trim().isEmpty()) {
                serverMessage.addError(42, "Не передан фактический адрес компании");
            }

        }



        if (providerFormObject.getP_login() == null || providerFormObject.getP_login().isEmpty()) {
            serverMessage.addError(9, "Передан пустой логин");
        } else {

            String login = providerFormObject.getP_login().trim().toLowerCase();
            if (RegExpCheck.loginRegExCheck(login)) {
                if (validateService.checkLoginExist(login)) {
                    serverMessage.addError(10, "Пользователь с таким логином уже присутствует в системе");
                }
            } else {
                serverMessage.addError(11, "Переданый логин некоректный. Используйте латиницу");
            }
        }

        if (providerFormObject.getEmailContactPerson() == null || providerFormObject.getEmailContactPerson().trim().isEmpty()) {
            serverMessage.addError(54, "Не передан адрес электронной почты контактного лица компании");
        } else {
            String email = providerFormObject.getEmailContactPerson().toLowerCase();
            if (RegExpCheck.emailRegExCheck(email)) {
                if (validateService.checkEmailExist(email)) {
                    serverMessage.addError(55, "Контактное лицо с таким адресом электронной почты уже зарегистрированно в системе");
                }
            } else {
                serverMessage.addError(56, "Переданый email не дейстивтелен");
            }
        }
    }
}