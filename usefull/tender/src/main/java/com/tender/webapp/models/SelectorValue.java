package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.CategorySelector;

import javax.persistence.*;

/**
 * Created by Serdukov on 09.09.2015.
 */
@Entity
@Table(name = "selectors_values")
public class SelectorValue {


    @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;


        @Column(name = "selector_id", unique = false, nullable = false)
        private int selectorId;

        @Column(name = "selector_name", unique = false, nullable = false)
        private String selectorName;


        @Column(name = "value_id", unique = false, nullable = true)
        private int valueId;


        @Column(name = "selector_value", unique = false, nullable = false)
        private String selectorValue;

        @ManyToOne
        @JoinColumn(name = "tender_id")
        @JsonIgnore
        private Tender tender;

    /*-------------getters and setters----------------------*/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getSelectorId() {
        return selectorId;
    }

    public void setSelectorId(int selectorId) {
        this.selectorId = selectorId;
    }

    public String getSelectorName() {
        return selectorName;
    }

    public void setSelectorName(String selectorName) {
        this.selectorName = selectorName;
    }



    public int getValueId() {
        return valueId;
    }

    public void setValueId(int valueId) {
        this.valueId = valueId;
    }


    public String getSelectorValue() {
        return selectorValue;
    }

    public void setSelectorValue(String selectorValue) {
        this.selectorValue = selectorValue;
    }

    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }

}

