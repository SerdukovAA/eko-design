package com.tender.webapp.comunicators;

import com.tender.webapp.config.ParamsProvider;
import org.apache.commons.io.IOUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by serdukov on 01.10.15.
 */

public class PosterComunicator {





    private static boolean isDevelop = ParamsProvider.isDevelop;




    private static final Logger log = Logger.getLogger(PosterComunicator.class);



    public static void posterHanldeRequestNewTender(int tender_id) throws Exception {
        //если включен режим разработки то рассылка не происходит
        if(isDevelop) return;
        // Creating an instance of HttpClient.
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            // Creating an instance of HttpPost.
            HttpPost httpost = new HttpPost("http://ubeten.ru:8080/handle_tender");
            // Adding all form parameters in a List of type NameValuePair
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("login", "poster"));
            nvps.add(new BasicNameValuePair("psw", "youAreTheBest123"));
            nvps.add(new BasicNameValuePair("tender_id", ""+tender_id));
            httpost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
            log.info(" Посылаем номер тендрада на опоовещение сервера рассылки "+tender_id);
            CloseableHttpResponse response = httpclient.execute(httpost);
            try {
                // Do the needful with entity.
                HttpEntity entity = response.getEntity();
                StringWriter writer = new StringWriter();
                IOUtils.copy(entity.getContent(), writer);
                log.info("HttpEntity  " + writer.toString());
                System.out.println("HttpEntity  " + writer.toString());
            } finally {
                // Closing the response
                response.close();
            }
        } finally {
            httpclient.close();
        }
    }

    public static void posterHanldeRequestNewOffer(int offer_id) throws Exception {
        //если включен режим разработки то рассылка не происходит
        if(isDevelop) return;
        // Creating an instance of HttpClient.
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            // Creating an instance of HttpPost.
            HttpPost httpost = new HttpPost("http://ubeten.ru:8080/handle_offer");
            // Adding all form parameters in a List of type NameValuePair
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("login", "poster"));
            nvps.add(new BasicNameValuePair("psw", "youAreTheBest123"));
            nvps.add(new BasicNameValuePair("offer_id", ""+offer_id));
            httpost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
            log.info(" Посылаем номер тендрада на опоовещение сервера рассылки "+offer_id);
            CloseableHttpResponse response = httpclient.execute(httpost);
            try {
                // Do the needful with entity.
                HttpEntity entity = response.getEntity();
                StringWriter writer = new StringWriter();
                IOUtils.copy(entity.getContent(), writer);
                log.info("HttpEntity  " + writer.toString());
                System.out.println("HttpEntity  " + writer.toString());
            } finally {
                // Closing the response
                response.close();
            }
        } finally {
            httpclient.close();
        }
    }


public static void posterHanldeRequestNewMessage(int message_id) throws Exception {
        //если включен режим разработки то рассылка не происходит
        if(isDevelop) return;
        // Creating an instance of HttpClient.
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            // Creating an instance of HttpPost.
            HttpPost httpost = new HttpPost("http://ubeten.ru:8080/handle_message");
            // Adding all form parameters in a List of type NameValuePair
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("login", "poster"));
            nvps.add(new BasicNameValuePair("psw", "youAreTheBest123"));
            nvps.add(new BasicNameValuePair("message_id", ""+message_id));
            httpost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
            log.info(" Посылаем номер коментария на опоовещение сервера рассылки "+message_id);
            CloseableHttpResponse response = httpclient.execute(httpost);
            try {
                // Do the needful with entity.
                HttpEntity entity = response.getEntity();
                StringWriter writer = new StringWriter();
                IOUtils.copy(entity.getContent(), writer);
                log.info("HttpEntity  " + writer.toString());
                System.out.println("HttpEntity  " + writer.toString());
            } finally {
                // Closing the response
                response.close();
            }
        } finally {
            httpclient.close();
        }
    }



}
