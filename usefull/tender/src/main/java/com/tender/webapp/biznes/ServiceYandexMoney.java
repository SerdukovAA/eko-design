package com.tender.webapp.biznes;


import com.tender.webapp.controllers.service_bean.ConsumerFormObject;
import com.tender.webapp.controllers.service_bean.ParametersRequest;
import com.tender.webapp.controllers.service_bean.ProviderFormObject;
import com.tender.webapp.controllers.service_bean.YandexTransBegin;
import com.tender.webapp.models.ProviderFilterValues;
import com.tender.webapp.models.User;
import com.tender.webapp.models.handbook.UserParameter;
import com.tender.webapp.models.journal.RefillTransactionJournal;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Serdukov on 05.06.2015.
 */
public interface ServiceYandexMoney {

    public RefillTransactionJournal beginTransaction(YandexTransBegin yandexTransBegin, User user) throws Exception;

}

