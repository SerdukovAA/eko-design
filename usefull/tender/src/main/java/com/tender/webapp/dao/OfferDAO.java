package com.tender.webapp.dao;

import com.tender.webapp.models.Offer;
import com.tender.webapp.models.Tender;

import java.util.List;

/**
 * Created by serdukov on 01.11.15.
 */
public interface OfferDAO {

    public Offer getOfferWinByTenderId(int tender_id)throws Exception;


}
