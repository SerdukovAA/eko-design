package com.tender.webapp.controllers;

import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.models.ServerMessageStatus;
import com.tender.webapp.models.service.ServerMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Serdukov on 4.11.2015.
 */
@Controller
public class PasswordRecoveryController {

    @Autowired
    ServiceUser serviceUser;

    private static final Logger log = Logger.getLogger(PasswordRecoveryController.class);

    /**
     * Инициация процесса востановления пароля
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/info_password_recovery", method = RequestMethod.POST)
    public
    @ResponseBody
    ServerMessage passwordRecovery(@RequestParam("loginMail") String loginMail, HttpServletRequest request) throws Exception {
        if (loginMail == null || loginMail.isEmpty())
            throw new Exception("Не передан идентификатор пользователя для востановления пароля");
        ServerMessage serverMessage = new ServerMessage();
         try {
            serviceUser.passwordRecovery(loginMail);
            serverMessage.setStatus(ServerMessageStatus.SUCCESS_MESSAGE.getId());
            serverMessage.setText("Письмо на восстановление пароля отправлено");
            return serverMessage;
        } catch (Exception ex) {
            ex.printStackTrace();
            serverMessage.setStatus(ServerMessageStatus.ERROR_MESSAGE.getId());
            serverMessage.setText(ex.getMessage());
            log.error("Произошла ошибка при восстановлении пароля на сервере. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", ex);
            return serverMessage;
        }


    }


      /**
     * Смена пароля после востановления
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/info_change_pas_recovery", method = RequestMethod.POST)
    public
    @ResponseBody
    ServerMessage changePassordAfterRecovery(HttpServletRequest request) {
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на смену пароля после восстановления");
        ServerMessage serverMessage = new ServerMessage();
        try {
            if (request.getParameter("hash") == null || request.getParameter("hash").trim().isEmpty())
                throw new Exception("Не передан временный hash-пароль");
            if (request.getParameter("recovery_id") == null || request.getParameter("recovery_id").trim().isEmpty())
                throw new Exception("Не передан id роверки");
            if (request.getParameter("password") == null || request.getParameter("password").trim().isEmpty())
                throw new Exception("Не передан новый пароль");

            serviceUser.changePasswordRecovery(request);
            serverMessage.setStatus(3000);
            serverMessage.setText("Пароль успешно изменен");
            log.info(" id сессии: " + request.getSession().getId() + ". Пароль успешно изменен");
            return serverMessage;
        } catch (Exception ex) {
            serverMessage.setStatus(3);
            serverMessage.setText("Произошла ошибка при смене пароля");
            serverMessage.addError(1, "Ошибка: " + ex.getMessage());
            log.error("Произошла ошибка при смене пароля на сервере. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", ex);
            return serverMessage;
        }

    }

    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }


}
