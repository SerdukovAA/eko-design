package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Serdukov on 02.06.2015.
 */
@Entity
@Table(name="messages")
public class Message {

    @Id
    @Column(name="message_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "tender_id")
    @JsonIgnore
    private Tender tender;

    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;

    @Column(name = "text", unique = false, nullable = false)
    private String text;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id_owner_message")
    private User userOwner;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "for_message_id")
    private Message forMessage;

    @Column(name = "is_answer", unique = false, nullable = false)
    private boolean isAnswer;

    @Column(name = "is_tender_owner", unique = false, nullable = false)
    private boolean isTenderOwner;


     /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUserOwner() {
        ///TODO как то нужно исправить!! обнуляет юзера в сессии!!
        userOwner.setUserStatus(null);
        return userOwner;
    }

    public void setUserOwner(User userOwner) {
        this.userOwner = userOwner;

    }


    public boolean isAnswer() {
        return isAnswer;
    }

    public void setAnswer(boolean isAnswer) {
        this.isAnswer = isAnswer;
    }


    public boolean isTenderOwner() {
        return isTenderOwner;
    }

    public void setTenderOwner(boolean isTenderOwner) {
        this.isTenderOwner = isTenderOwner;
    }

    public Message getForMessage() {
        return forMessage;
    }

    public void setForMessage(Message forMessage) {
        this.forMessage = forMessage;
    }

}

