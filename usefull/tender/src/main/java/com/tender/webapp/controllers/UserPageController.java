package com.tender.webapp.controllers;

import com.tender.webapp.biznes.*;
import com.tender.webapp.controllers.service_bean.ChangePassRequest;
import com.tender.webapp.controllers.service_bean.ParametersRequest;
import com.tender.webapp.controllers.service_bean.YandexTransBegin;
import com.tender.webapp.mail.MailManager;
import com.tender.webapp.models.*;
import com.tender.webapp.models.base.AutoBrand;
import com.tender.webapp.models.handbook.ParameterRelation;
import com.tender.webapp.models.handbook.TypeService;
import com.tender.webapp.models.handbook.UserParameter;
import com.tender.webapp.models.handbook.UserPreferedRegion;
import com.tender.webapp.models.journal.EmailCheckList;
import com.tender.webapp.models.journal.RatingJournal;
import com.tender.webapp.models.journal.RefillTransactionJournal;
import com.tender.webapp.models.service.ServerMessage;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.ExecutorService;

/**
 * Created by Serdukov on 20.06.2015.
 */
@Controller
public class UserPageController {

    @Resource
    protected SessionFactory sessionFactory;
    @Autowired
    ServiceMessage serviceMessage;
    @Autowired
    ServiceTender serviceTender;
    @Autowired
    ServiceUser serviceUser;

    @Autowired
    ServiceYandexMoney serviceYandexMoney;

    @Autowired
    ExecutorService executorService;
    @Autowired
    ServiceOffer serviceOffer;
    @Autowired
    JavaMailSender javaMailSender;

    private static final Random RANDOM = new SecureRandom();
    private static final Logger log = Logger.getLogger(UserPageController.class);


/*TODO слишком много контроллеров, необходимо ввести префикс маппинга user_*, и перенести их в новый контроллер. Это общие контроллеры которые испльзуют и исполнителя и заказчики*/


////------------------------CONSUMERS------------------------------/////



    /**
     * Смена пароля по запросу пользователя
     * @param request
     * @return
     */
    @RequestMapping(value = "/consumer_change_password", method = RequestMethod.POST)
    public @ResponseBody  ServerMessage changePassord(@RequestBody ChangePassRequest changePassRequest, HttpServletRequest request) {
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на смену пароля");
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        ServerMessage serverMessage = new ServerMessage();
        try {
            serviceUser.changePassword(changePassRequest, user);
            serverMessage.setStatus(3000);
            serverMessage.setText("Пароль успешно изменен");
            log.info(" id сессии: "+request.getSession().getId()+". Пароль успешно изменен");
            return serverMessage;
        }   catch (Exception e) {
            serverMessage.setStatus(3);
            serverMessage.setText("Произошла ошибка при смене пароля");
            serverMessage.addError(1,"Ошибка: " + e.getMessage());
            log.error("Произошла ошибка при смене пароля на сервере. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", e);
            return serverMessage;
        }

    }


    /**
     * Контроллер самоудаления пользователя
     * @param request
     * @return
     */
    @RequestMapping(value = "/consumer_delete_byself", method = RequestMethod.POST)
    public  @ResponseBody ServerMessage deleteUserBySelf(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на самоудаление , user_id " + user.getUser_id());
        ServerMessage serverMessage = new ServerMessage();
        try {
            //удаляем пользователя
            serviceUser.deleteBySelf(user.getUser_id());
            serverMessage.setStatus(ServerMessageStatus.SUCCESS_MESSAGE.getId());
            serverMessage.setText("Пользователь успешно удален");
            log.info(" id сессии: "+request.getSession().getId()+". Пользователь успешно удален и переведен в 6 статус");
            return serverMessage;
        } catch (Exception e) {
            serverMessage.setStatus(ServerMessageStatus.ERROR_MESSAGE.getId());
            serverMessage.setText("Произошла ошибка при удалении пользователя");
            serverMessage.addError(1,"Ошибка: " + e.getMessage());
            log.error("Произошла ошибка при удалении пользователя на сервере. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", e);
            return serverMessage;
        }

    }


    /**
     * Возвращаем выигравшее предложение по тендеру
     * @param tender_id
     * @return
     */
    @RequestMapping(value = "/consumer_offer_win_by_tender_id", method = RequestMethod.GET)
    public @ResponseBody Offer getoOfferWinByTenderID(@RequestParam("tender_id") int tender_id, HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос победившего предложения, user_id " + user.getUser_id() + ", для тендера tender_id " + tender_id);
        try{
            return serviceOffer.getoOfferWinByTenderId(tender_id, user.getUser_id());
        }catch (Exception e){
            log.error("Произошла ошибка при запросе юзером победившего предложения. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", e);
            return null;
        }
    }


    /**
     * Сохранение параметров рассылки юзера
     * @param request
     * @return
     */
    @RequestMapping(value = "/provider_save_mail_settings", method = RequestMethod.POST)
    public @ResponseBody  ServerMessage saveMailSettings(@RequestBody UserParameter userParameter, HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на именение параметров рассылки");

        ServerMessage serverMessage = new ServerMessage();
        try {
            serviceUser.saveMailUserParameters(userParameter, user);
            serverMessage.setStatus(ServerMessageStatus.SUCCESS_MESSAGE.getId());
            serverMessage.setText("Настройки рассылки успешно изменены");
            log.info(" id сессии: " + request.getSession().getId() + ". Настройки рассылки успешно изменены");
            return serverMessage;
        } catch (Exception e) {
            e.printStackTrace();
            serverMessage.setStatus(ServerMessageStatus.ERROR_MESSAGE.getId());
            serverMessage.setText("Произошла ошибка при сохранении настроек рассылки");
            serverMessage.addError(1,"Ошибка: " + e.getMessage());
            log.error("Произошла ошибка при сохранении настроек рассылки на сервер. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", e);
            return serverMessage;
        }

    }





    /**
     * Контроллер возвращает тендеры категории участия для исполнителя
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/provider_tenders_for_provider", method = RequestMethod.GET)
    public @ResponseBody List<Tender> getTendersForProvider(HttpServletRequest request) throws  Exception{
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        try {
            if (!user.isProvider()) return new ArrayList<Tender>();
            //реализован через функцию postgreSQL
            return serviceTender.getTendersForProvider(user.getUser_id());
        }catch(Exception ex){
            log.error("Ошибка при запросе тендеров категории участия для исполнителя", ex);
            throw ex;
        }
    }

    /**
     * Возвращаем исполнителю баланс по запросу
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/provider_get_balance", method = RequestMethod.GET)
    public @ResponseBody
    Balance returnBalanceForProvider(HttpServletRequest request) throws Exception{
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        if(user.isProvider() == false) return null;
        User provider = serviceUser.getUserByUserId(user.getUser_id());
        return  provider.getBalance();
    }



    /**
     * Контроллел изменений фильтров категории участия
     * @param parametersRequestList - коллекиция одиночных идентификаторов селектора и выбранное значение селектора
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value ="/provider_set_filter_parameters", method = RequestMethod.POST)
    public @ResponseBody ServerMessage providerSetFilterParameters(@RequestBody List<ParametersRequest> parametersRequestList, HttpServletRequest request) throws Exception {
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на именение параметров рассылки");
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        ServerMessage serverMessage = new ServerMessage();
        try {
            serviceUser.saveProviserFiltersParameters(parametersRequestList, user);
            serverMessage.setStatus(ServerMessageStatus.SUCCESS_MESSAGE.getId());
            serverMessage.setText("Настройки успешно сохранены");
            log.info(" id сессии: " + request.getSession().getId() + ". Настройки фильтров успешно изменены");
            return serverMessage;
        } catch (Exception e) {
            e.printStackTrace();
            serverMessage.setStatus(ServerMessageStatus.ERROR_MESSAGE.getId());
            serverMessage.setText("Произошла ошибка при сохранении настроек рассылки");
            serverMessage.addError(1, "Ошибка: " + e.getMessage());
            log.error("Произошла ошибка при сохранении настроек рассылки на сервер. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", e);
            return serverMessage;
        }

    }


    @RequestMapping(value ="/provider_set_prefer_regions", method = RequestMethod.POST)
    public @ResponseBody ServerMessage providerSetPreferRegions(@RequestBody List<UserPreferedRegion> userPreferedRegions, HttpServletRequest request) throws Exception {
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на именение параметров рассылки");
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        ServerMessage serverMessage = new ServerMessage();
        try {
            serviceUser.saveProviderPreferRegions(userPreferedRegions, user);
            serverMessage.setStatus(ServerMessageStatus.SUCCESS_MESSAGE.getId());
            serverMessage.setText("Настройки успешно сохранены");
            log.info(" id сессии: " + request.getSession().getId() + ". Настройки фильтров успешно изменены");
            return serverMessage;
        } catch (Exception e) {
            e.printStackTrace();
            serverMessage.setStatus(ServerMessageStatus.ERROR_MESSAGE.getId());
            serverMessage.setText("Произошла ошибка при сохранении настроек рассылки");
            serverMessage.addError(1, "Ошибка: " + e.getMessage());
            log.error("Произошла ошибка при сохранении настроек рассылки на сервер. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", e);
            return serverMessage;
        }

    }











    /**
     * Контроллер возвращает выбранные категории участия для исполнителя
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value ="/provider_get_filter_parameters", method = RequestMethod.GET)
    public @ResponseBody  List<ProviderFilterValues> getSelectedProvidersFilters(HttpServletRequest request) throws Exception {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        return serviceUser.getSelectedProvidersFilters(user.getUser_id());
    }



    /**
     * Возращаем собственника тендера для победителя
     * @param tender_id
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/provider_tender_winner_by_tender_id", method = RequestMethod.GET)
    public @ResponseBody User getTenderOwnerForWinner(@RequestParam("tender_id") int tender_id,HttpServletRequest request) throws Exception{
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        User userOwner = serviceUser.getTenderOwnerForWinner(user.getUser_id(), tender_id);
        return userOwner;
    }


    /**
     * Контроллер начала транзакции пополнения счета
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = {"/provider_begin_transaction"}, method = RequestMethod.POST)
    public @ResponseBody RefillTransactionJournal beginMoneyTransaction(@RequestBody YandexTransBegin yandexTransBegin,HttpServletRequest request) throws Exception {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        return  serviceYandexMoney.beginTransaction(yandexTransBegin, user);
    }



     /**
     * Котроллер возвращает персональные настройки для исполнителя (пока что)
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value ="/provider_get_parameter_values", method = RequestMethod.GET)
    public @ResponseBody  UserParameter getProviderParameters(HttpServletRequest request) throws Exception {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        return serviceUser.getProviderParameters(user.getUser_id());
    }


    /**
     * Контроллер возращает список тендеров в который исполнитель участвует
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/provider_tenders_with_offer", method = RequestMethod.GET)
    public @ResponseBody
    List<Tender> getTendersWithOfferProvider(HttpServletRequest request) throws Exception  {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        return serviceTender.getTendersWithOfferProvider(user.getUser_id());
    }


    /**
     * Контроллер возращает список тендеров в который исполнитель победил
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/provider_tenders_win", method = RequestMethod.GET)
    public @ResponseBody List<Tender> getTendersWinForProvider(HttpServletRequest request) throws Exception{
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        return serviceTender.getTendersWinForProvider(user.getUser_id());
    }




    //TODO необходимо вынести в отдельный сервис
    @RequestMapping(value = "/email_trust/{email}/{hash}", method = RequestMethod.GET)
    public String emaiSetTrust(@PathVariable("email") String email,@PathVariable("hash") String hash) throws Exception {

        System.out.println("Email check : email " +email+ "  hash "+ hash);
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            Query query = session.createQuery("from EmailCheckList where user_email = :user_email ORDER BY date_create DESC");
            query.setParameter("user_email", email);
            List<EmailCheckList> emailCheckListS = query.list();
            EmailCheckList emailCheckList = emailCheckListS.get(0);
            if (emailCheckList!=null && emailCheckList.getCheckHash().equals(hash)) {
                emailCheckList.setEmailCheck(true);
                emailCheckList.getPrivateInfo().setEmailCheck(true);
                tx = session.beginTransaction();
                PrivateInfo privateInfo = emailCheckList.getPrivateInfo();
               // session.update(privateInfo);???
                session.update(emailCheckList);

                query = session.createQuery("from User where private_info_id = :private_info_id ");
                query.setParameter("private_info_id", privateInfo.getId());
                User provider = (User) query.uniqueResult();

                if(provider.getUserStatus().getId()==5) {
                    provider.setUserStatus((UserStatus) session.get(UserStatus.class, 1));
                    provider.setPassword(passwordGenerator());
                    session.update(provider);
                    String message = "<html><head><meta charset='UTF-8'></head><body>"+
                            "<div>Уважаемый " + privateInfo.getFirstName() + " " + privateInfo.getLastName() + ". Ваша компания одобрена.</div>" +
                            "<div>Для входа в личный кабинет Вы можете использовать пароль: " + provider.getPassword()+"</div>"
                            +"<p style='font-size:11px;'>*при необходимости пароль может быть изменен в личном кабинете.</p></body></html>";
                    String tema = "Ваша аккаунт одобрен";
                    executorService.submit(new MailManager(privateInfo.getEmail(), tema, message, javaMailSender));
                }
                tx.commit();
                return "redirect:/index.html#/success_email";
            } else {
                return "redirect:/index.html#/error_email";
            }
        }catch (Exception ex){
            log.error("Email check ERROR: ", ex);
            return "redirect:/index.html#/error_email";
        }finally {
            session.close();
        }
    }

    private  String passwordGenerator(){
        String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789@";
        String pw = "";
        for (int i=0; i<8; i++)
        {
            int index = (int)(RANDOM.nextDouble()*letters.length());
            pw += letters.substring(index, index+1);
        }
        return pw;
    }


    /**
     * Контроллер возвращаес собственные тендеры, для заказчика
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/consumer_tenders_for_user_owner", method = RequestMethod.GET)
    public @ResponseBody List<Tender> getTendersForUserOwner(HttpServletRequest request) throws Exception{
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) return null;
        return serviceTender.getTendersForOwner(user.getUser_id());
    }



    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }




}
