package com.tender.webapp.config;

import com.tender.webapp.biznes.*;
import com.tender.webapp.biznes.impl.*;
import com.tender.webapp.config.rest.RESTAuthenticationEntryPoint;
import com.tender.webapp.config.rest.RESTAuthenticationSuccessHandler;
import com.tender.webapp.config.rest.RESTLogoutSuccessHandler;
import com.tender.webapp.dao.*;
import com.tender.webapp.dao.impl.*;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by Serdukov on 06.06.2015.
 */
@Configuration
@EnableScheduling
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.tender.webapp"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class)
        })
@PropertySource("classpath:tenderApp.properties")
public class RootConfig {

    @Autowired
    Environment env;





    //Конфигурация базу данных приложения
    @Bean(name = "dataSource", destroyMethod = "close")
    public javax.sql.DataSource getDataSource() {
        org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUrl(env.getProperty("db.url"));
        ds.setUsername(env.getProperty("db.username"));
        ds.setPassword(env.getProperty("db.password"));
        ds.setInitialSize(5);
        ds.setTestOnBorrow(true);
        ds.setValidationInterval(34000);
        ds.setValidationQuery("SELECT 1");
        ds.setMaxActive(10);
        ds.setMaxIdle(5);
        ds.setMinIdle(1);
        System.out.println("Start DataSource");
        return ds;
    }


    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sfb = new LocalSessionFactoryBean();
        sfb.setDataSource(getDataSource());
        sfb.setPackagesToScan(new String[]{"com.tender.webapp.models"});
        Properties props = new Properties();
        props.setProperty("dialect", "org.hibernate.dialect.PostgreSQLDialect");
        props.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        props.setProperty("hibernate.connection.charSet", "UTF-8");
        props.setProperty("hibernate.connection.characterEncoding", "UTF-8");
        props.setProperty("hibernate.connection.useUnicode", "true");
        props.setProperty("hibernate.connection.isolation", "2");

              sfb.setHibernateProperties(props);
        System.out.println("Start LocalSessionFactoryBean");
        return sfb;
    }





    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", true);
        mailProperties.put("mail.smtp.starttls.enable", true);
        mailProperties.put("mail.smtp.ssl.trust", "smtp.yandex.ru");
        mailProperties.put("mail.transport.protocol", "smtp");
        mailProperties.put("mail.encoding", "UTF-8");
        mailProperties.put("mail.smtp.host", "smtp.yandex.ru");
        mailProperties.put("mail.smtp.port", "465");
        mailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        mailProperties.put("mail.smtp.socketFactory.port", "465");
        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setUsername(env.getProperty("post.email.address"));
        mailSender.setPassword(env.getProperty("post.email.password"));
        System.out.println("Start JavaMailSender");
        return mailSender;
    }


    //Spring manager transaction
    @Bean
    public PlatformTransactionManager txManager(SessionFactory sessionFactory) {
        System.out.println("Start Transaction Manager");
        return new HibernateTransactionManager(sessionFactory);
    }

    //Must register a static PropertySourcesPlaceholderConfigurer bean in either XML or annotation, so that Spring @Value know how to interpret ${}
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    //Пул потоков
    @Bean(destroyMethod = "shutdown")
    public ExecutorService getExecutorService() {
        ExecutorService es = Executors.newFixedThreadPool(10);
        System.out.println("Start Pool Threads");
        return es;
    }

    @Bean
    public ServiceUser getServiceUser() {
        return new ServiceUserImpl();
    }

    @Bean
    public ServiceTender getServiceTender() {
        return new ServiceTenderImpl();
    }

    @Bean
    public ServiceOffer getServiceOffer() {
        return new ServiceOfferImpl();
    }

    @Bean
    public ServiceMessage getServiceMessage() {
        return new ServiceMessageImpl();
    }

    @Bean
    public EmailService getEmailCheckService() {
        return new EmailServiceImpl();
    }

    @Bean
    public ValidateService getValidateService() {
        return new ValidateServiceImpl();
    }
    @Bean
    public SelectorsService getSelectorsService() {
        return new SelectorsServiceImpl();
    }

    @Bean
    public CatalogService getCatalogService() {
        return new CatalogServiceImpl();
    }

    @Bean
    public InfoService getInfoService() {
        return new InfoServiceImpl();
    }

    @Bean
    public FotoService getFotoService() {
        return new FotoServiceImpl();
    }

    @Bean
    public ServiceYandexMoney getServiceYandexMoney() {
        return new ServiceYandexMoneyImpl();
    }


    //---------------DAO implements---------------------------------///

    @Bean
    public TenderStatusDAO getTenderStatusDAO() {
        return new TenderStatusDAOImpl();
    }


   @Bean
    public TenderDAO getTenderDAO() {
        return new TenderDAOImpl();
    }

    @Bean
    public UserDAO getUserDAO() {
        return new UserDAOImpl();
    }

    @Bean
    public OfferDAO getOfferDAO() {
        return new OfferDAOImpl();
    }

    @Bean
    public PasswordRecoveryListDAO getPasswordRecoveryListDAO() {
        return new PasswordRecoveryListDAOImpl();
    }


    @Bean
    public CanceledTenderDAO getCanceledTenderDAO() {
        return new CanceledTenderDAOImpl();
    }

    @Bean
    public PrivateInfoDAO getPrivateInfoDAO() {
        return new PrivateInfoDAOImpl();
    }


    @Bean
    public JuridicalInfoDAO getJuridicalInfoDAO() {
        return new JuridicalInfoDAOImpl();
    }



    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new RESTAuthenticationEntryPoint();
    }

    @Bean
    public SimpleUrlAuthenticationFailureHandler authenticationFailureHandler() {
        return new SimpleUrlAuthenticationFailureHandler();
    }

    @Bean
    public RESTAuthenticationSuccessHandler authenticationSuccessHandler() {
        return new RESTAuthenticationSuccessHandler();
    }


    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new RESTLogoutSuccessHandler();
    }




}
