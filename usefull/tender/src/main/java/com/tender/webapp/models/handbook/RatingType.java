package com.tender.webapp.models.handbook;

import javax.persistence.*;

/**
 * Created by Serdukov on 18.09.2015.
 * заготовка на будущее , будет определять разные типы выставленных рейтингов
 */
@Entity
@Table(name = "rating_type")
public class RatingType {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "rating_type", unique = true, nullable = false)
    private String ratingType;


    @Column(name = "rating_name", unique = true, nullable = false)
    private String  ratingName;



    /*--------getters and setters-----*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getRatingType() {
        return ratingType;
    }

    public void setRatingType(String ratingType) {
        this.ratingType = ratingType;
    }

    public String getRatingName() {
        return ratingName;
    }

    public void setRatingName(String ratingName) {
        this.ratingName = ratingName;
    }



}
