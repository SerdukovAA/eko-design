package com.tender.webapp.models.handbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Serdukov on 14.06.2015.
 */
@Entity
@Table(name = "service_commission")
public class ServiceCommission {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "service_name", unique = false)
    private String  serviceNameProvider;


    @Column(name = "type_service_id", unique = false)
    private int typeServiceId;

    @Column(name = "commission", unique = false)
    private double  commission;


    /*--------getters and setters-----*/


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceNameProvider() {
        return serviceNameProvider;
    }

    public void setServiceNameProvider(String serviceNameProvider) {
        this.serviceNameProvider = serviceNameProvider;
    }

    public int getTypeServiceId() {
        return typeServiceId;
    }

    public void setTypeServiceId(int typeServiceId) {
        this.typeServiceId = typeServiceId;
    }


    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }
}
