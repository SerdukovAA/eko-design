package com.tender.webapp.biznes.impl;


import com.tender.webapp.biznes.EmailService;
import com.tender.webapp.config.ParamsProvider;
import com.tender.webapp.mail.MailManager;
import com.tender.webapp.models.Offer;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;
import com.tender.webapp.models.journal.EmailCheckList;
import com.tender.webapp.models.PrivateInfo;
import com.tender.webapp.utils.impl.GenereteHashUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.concurrent.ExecutorService;


/**
 * Данные методы не делай транзакционными!
 */
public class EmailServiceImpl implements EmailService {

    @Resource
    protected SessionFactory sessionFactory;
    @Autowired
    ExecutorService executorService;
    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    Environment env;



    private static final Logger log = Logger.getLogger(EmailServiceImpl.class);
    private static boolean isDevelop = ParamsProvider.isDevelop;


    /**
     * Метод создает запись на проверку email
     *
     * @param privateInfo
     * @throws Exception
     */

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void createNewEmailCheckNote(PrivateInfo privateInfo) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        log.info(" email " + privateInfo.getEmail() + ". Создание новой записи на проверку email");
        EmailCheckList emailCheckList = new EmailCheckList();
        emailCheckList.setPrivateInfo(privateInfo);
        emailCheckList.setUserEmail(privateInfo.getEmail());
        emailCheckList.setDateCreate(new Date());
        String hash = GenereteHashUtil.genHash("" + new Date() + privateInfo.getEmail());
        emailCheckList.setCheckHash(hash);
        String message = "<html><head><meta charset='UTF-8'></head><body><p>Для подтверждения электронного адреса пройдите по следующей сслыке:</p>" +
                "<p><a href='http://www.ubeten.com/email_trust/" + privateInfo.getEmail() + "/" + hash + "'> Подтвердить электронный адрес : " + privateInfo.getEmail() + "</a></p>" +
                "<p style='font-size:11px;'>*если данное письмо отправлено Вам по ошибке, пожалуйста не отвечайте на него.</p>" +
                "</body></html>";
        String tema = "Подтверждение Email адреса";
        executorService.submit(new MailManager(privateInfo.getEmail(), tema, message, javaMailSender));
        session.save(emailCheckList);
        log.info(" email " + privateInfo.getEmail() + ". Запись на проверку email создана, email отправлен.");
    }

    public void createEmailAboutCredit(Tender tender, User userOwner) throws Exception{
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><meta charset='UTF-8'></head><body><h3>По тендеру необходим кредит</h3>")
                .append("<p>Пользователь user_id - <b>").append(userOwner.getUser_id()).append("</b> Имя пользователя - ")
                .append(userOwner.getPrivateInfo().getFirstName()).append(" ").append(userOwner.getPrivateInfo().getLastName())
                .append("  отметил \"В Кредит\" по тендеру tender_id - ").append(tender.getId())
                .append("<p>Посмотреть тендер - <a href='https://ubeten.com/#/tender/").append(tender.getId()).append("'>").append(tender.getTenderName()).append("</a></p>")
                .append("</body></html>");
        String message = sb.toString();
        String tema = "Тендер "+tender.getTenderName() +" отмечен  - В кредит";
        String email = ParamsProvider.postEmailAddress;
        if(!isDevelop) {
            executorService.submit(new MailManager(email, tema, message, javaMailSender));
        }
    }





    public void senMailOnPasswordReset(String email, String hash, int checkId) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><meta charset='UTF-8'></head><body><h3>Восстановление пароля</h3>")
                .append("<p>Для продолжения и установки нового пароля пройдите по следующей сслыке:</p>")
                .append("<p><a href='http://www.ubeten.com/#/password_recovery_step2/").append(checkId).append("/").append(hash)
                .append("'> Изменить пароль</a></p><p style='font-size:11px;'>*если данное письмо отправлено Вам по ошибке, пожалуйста не отвечайте на него.</p>")
                .append("</body></html>");
        String message = sb.toString();
        String tema = "Смена пароля на Ubeten.com";
        if(!isDevelop) {
            executorService.submit(new MailManager(email, tema, message, javaMailSender));
        }
    }

    public void sendAccepOfferMailToOfferProvider(Tender tender, Offer offer) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><meta charset='UTF-8'></head><body><h2>Поздравляем. Ваша заявка по тендеру победила</h2>" )
                .append("<p>Уважаемый ").append( offer.getProvider().getPrivateInfo().getFirstName()).append(" ")
                .append(offer.getProvider().getPrivateInfo().getLastName())
                .append(", заказчик выбрал именно Ваше предложение</p>")
                .append("<p> Контактная информация заказчика теперь доступна в выгранных Вами тендерах.</p>")
                .append("<p> Ссылка на выигранный тендер  - <a href='https://www.ubeten.com/#/tender/").append(tender.getId()).append("'>")
                .append(tender.getTenderName()).append("</a></p>");

                if(tender.isHaveResponsible()){
                  sb.append("<p>Заказчик указал ответсвенное лицо по тендеру. Дальнейшую работу по тендеру нужно проводить чере него: </p>")
                          .append("<p><b>Ответсвенный :</b> ")
                          .append(tender.getResponsibleInfo().getFirstName()).append(" ").append(tender.getResponsibleInfo().getLastName()).append("</p>")
                          .append("<p><b>Телефон :</b> ").append("+7").append(tender.getResponsibleInfo().getPhoneNumber()).append("</p>")
                          .append("<p><b>Email :</b> ").append(tender.getResponsibleInfo().getEmail()).append("</p>");
                }

                sb.append("</body></html>");

        String message = sb.toString();
        String tema = "Поздравляем. Вы выйграли тендер";
        if(!isDevelop) {
            executorService.submit(new MailManager(offer.getProvider().getPrivateInfo().getEmail(), tema, message, javaMailSender));
        }
    }



}
