package com.tender.webapp.controllers;

import com.tender.webapp.dao.TenderDAO;
import com.tender.webapp.mail.MailManager;

import com.tender.webapp.models.Tender;
import com.tender.webapp.models.handbook.TenderStatus;
import com.tender.webapp.models.User;
import com.tender.webapp.models.UserStatus;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;

/**
 * Created by Serdukov on 21.07.2015.
 */
@Controller
public class AdminController {

    @Autowired
    JavaMailSender javaMailSender;


    @Autowired
    TenderDAO tenderDAO;

    @Autowired
    ExecutorService executorService;

   @Resource
   protected SessionFactory sessionFactory;
   private static final Random RANDOM = new SecureRandom();


    private static final Logger log = Logger.getLogger(AdminController.class);


    @RequestMapping(value = "/admin_show_providers", method = RequestMethod.GET)
    public @ResponseBody List<User> getProviders(@RequestParam("page_num") int page_num) {
        Session session = sessionFactory.openSession();
        page_num--;
        Query query = session.createQuery("from User where isProvider=true  and  user_status_id !=6 ORDER BY date_create");
        query.setFirstResult(0+10*page_num);
        query.setMaxResults(10);
        List<User> providersList = query.list();
        session.close();
        return providersList;
    }

    @RequestMapping(value = "/admin_providers_count", method = RequestMethod.GET)
    public @ResponseBody Long getProvidersCount() {
        Session session = sessionFactory.openSession();
        Criteria crit = session.createCriteria(User.class);
        crit.add(Restrictions.eq("isProvider", true));
        crit.add(Restrictions.ne("userStatus.id", 6));
        crit.setProjection(Projections.rowCount());
        Long res = (Long) crit.uniqueResult();
        session.close();
        return res;
    }

    @RequestMapping(value = "/admin_show_consumers", method = RequestMethod.GET)
    public @ResponseBody List<User> getConsumers(@RequestParam("page_num") int page_num) {
        Session session = sessionFactory.openSession();
        page_num--;
        Query query = session.createQuery("from User where isProvider=false and private_info_id IS NOT NULL  and user_status_id !=6 ORDER BY date_create");
        query.setFirstResult(0 + 10 * page_num);
        query.setMaxResults(10);
        List<User> providersList = query.list();
        session.close();
        return providersList;
    }




    @RequestMapping(value = "/admin_consumers_count", method = RequestMethod.GET)
    public @ResponseBody Long getConsumersCount() {
        Session session = sessionFactory.openSession();
        Criteria crit = session.createCriteria(User.class);
        crit.add(Restrictions.eq("isProvider", false));
        crit.add(Restrictions.ne("userStatus.id", 6));
        crit.setProjection(Projections.rowCount());
        Long res = (Long) crit.uniqueResult();
        session.close();
        return res;
    }





    @RequestMapping(value = "/admin_show_tenders", method = RequestMethod.GET)
     public @ResponseBody List<Tender> getTenders(@RequestParam("page_num") int page_num) throws Exception{
        List<Tender> list =  tenderDAO.getAllRunTenders(page_num);
        return list;
    }



    @RequestMapping(value = "/admin_tenders_count", method = RequestMethod.GET)
    public @ResponseBody Long getTendersCount() throws Exception{
       return tenderDAO.getAllRunTendersCount();
    }





/*
    @RequestMapping(value = "/admin_show_consumers", method = RequestMethod.GET)
    public @ResponseBody List<User> getConsumers(@RequestParam("page_num") int page_num) {
        Session session = sessionFactory.openSession();
        page_num--;
        Query query = session.createQuery("from User where user_status_id = 5");
        query.setFirstResult(0+20*page_num);
        query.setMaxResults(20+20*page_num);
        List<User> providersList = query.list();
        session.close();
        return providersList;
    }

*/











    @RequestMapping(value = "/admin_new_tenders", method = RequestMethod.GET)
    public @ResponseBody List<Tender> getNewTenders(@RequestParam("page_num") int page_num) {
        Session session = sessionFactory.openSession();
        page_num--;
        System.out.println("new tender");
        Query query = session.createQuery("from Tender where tender_status_id = 1");
        query.setFirstResult(0+20*page_num);
        query.setMaxResults(20+20*page_num);
        List<Tender> tenderList = query.list();
        session.close();
        return tenderList;
    }

    @RequestMapping(value = "/admin_ended_tenders", method = RequestMethod.GET)
    public @ResponseBody List<Tender> getEndedTenders(@RequestParam("page_num") int page_num) {
        Session session = sessionFactory.openSession();
        page_num--;
        Query query = session.createQuery("from Tender where tender_status_id = 4");
        query.setFirstResult(0+20*page_num);
        query.setMaxResults(20+20*page_num);
        List<Tender> tenderList = query.list();
        session.close();
        return tenderList;
    }

    @RequestMapping(value = "/admin_new_providers", method = RequestMethod.GET)
    public @ResponseBody List<User> getNewProviders(@RequestParam("page_num") int page_num) {
        Session session = sessionFactory.openSession();
        page_num--;
        Query query = session.createQuery("from User where user_status_id = 5");
        query.setFirstResult(0+20*page_num);
        query.setMaxResults(20+20*page_num);
        List<User> providersList = query.list();
        session.close();
        return providersList;
    }


    @RequestMapping(value = "/admin_get_user_by_tender_id", method = RequestMethod.GET)
    public @ResponseBody User getUserByTenderId(@RequestParam("tender_id") int tender_id) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from User where user_id = (select t.userOwner from Tender t where t.id = :tender_id)");
        query.setParameter("tender_id", tender_id);
        User user = (User) query.uniqueResult();
        session.close();
        return user;
    }

   @RequestMapping(value = "/admin_get_user_by_user_id", method = RequestMethod.GET)
    public @ResponseBody User getUserByUserId(@RequestParam("user_id") int user_id) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from User where user_id =:user_id");
        query.setParameter("user_id", user_id);
        User user = (User) query.uniqueResult();
        session.close();
        return user;
    }




    @RequestMapping(value = "/admin_accept_tender", method = RequestMethod.POST)
    public @ResponseBody String acceptTenderByTenderID(@RequestParam("tender_id") int tender_id) {
        Session session = sessionFactory.openSession();
        System.out.println("accept tender" +tender_id);

        Tender tender = (Tender) session.get(Tender.class, tender_id);
        tender.setTenderStatus((TenderStatus) session.get(TenderStatus.class,2));
        session.update(tender);
        session.flush();
        session.close();
        User user  =tender.getUserOwner();
        //отправка письма
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getPrivateInfo().getEmail());
        mailMessage.setText("Уважаемый " + user.getPrivateInfo().getFirstName() + " " + user.getPrivateInfo().getLastName() + ". Ваша тендер : " +
                tender.getTenderName() + " одобрен модератором. С этого момента Вы можете следить за ним в личном кабинете.");
        mailMessage.setSubject("UBETEN.Ваша тендер одобрен.");
      //  executorService.submit(new MailManager(mailMessage, mailServer));


    return "ok";
    }

    @RequestMapping(value = "/admin_block_tender", method = RequestMethod.POST)
    public @ResponseBody String blockTenderByTenderID(@RequestParam("tender_id") int tender_id,@RequestParam("text") String text) {
        Session session = sessionFactory.openSession();
        System.out.println("block tender" +tender_id);
        System.out.println("block reason:" + text);
        Tender tender = (Tender) session.get(Tender.class, tender_id);
        tender.setTenderStatus((TenderStatus) session.get(TenderStatus.class,5));
        session.update(tender);
        session.flush();
        session.close();
        return "ok";
    }

    @RequestMapping(value = "/admin_block_user", method = RequestMethod.POST)
    public @ResponseBody String blockUserByUserID(@RequestParam("user_id") int user_id,@RequestParam("text") String text) {
        Session session = sessionFactory.openSession();
        System.out.println("block user" +user_id);
        System.out.println("block reason:" + text);
        //todo сообщение пересылается пользователю на email и типо так и так Вы бли запрокированы по причине.
        User user = (User) session.get(User.class, user_id);
        user.setUserStatus((UserStatus) session.get(UserStatus.class, 4));
        session.update(user);
        session.flush();
        session.close();
        return "ok";
    }

    @RequestMapping(value = "/admin_submit_message_for_user", method = RequestMethod.POST)
    public @ResponseBody String submitMessageByUserID(@RequestParam("user_id") int user_id,@RequestParam("text") String text) {
        Session session = sessionFactory.openSession();
        User user = (User) session.get(User.class, user_id);
        //отправка письма
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getPrivateInfo().getEmail());
        mailMessage.setText("Уважаемый " + user.getPrivateInfo().getFirstName() + " " + user.getPrivateInfo().getLastName() + "." + text);
        mailMessage.setSubject("UBETEN.Сообщение от менеджера.");
      //  executorService.submit(new MailManager(mailMessage, mailServer));

       session.close();
        return "ok";
    }


    @RequestMapping(value = "/admin_accept_provider", method = RequestMethod.POST)
    public @ResponseBody String acceptProviderByUserID(@RequestParam("user_id") int user_id) throws Exception {
        Session session = sessionFactory.openSession();
        System.out.println("accept user " +user_id);
        //todo ваши данные подтверждены, так и так вот Ваш логи и пароль для доступа к сервису.

        User user = (User) session.get(User.class, user_id);
        if(user.getUserStatus().getId()!=1) {
            user.setUserStatus((UserStatus) session.get(UserStatus.class, 1));
            user.setPassword(passwordGenerator());
            session.update(user);
            session.flush();
        }
        session.close();

        /*String message = "<html><head><meta charset='UTF-8'></head><body>"+
                "<div>Уважаемый " + user.getPrivateInfo().getFirstName() + " " + user.getPrivateInfo().getLastName() + ". Ваша компания одобрена.</div>" +
                "<div>Для входа в личный кабинет Вы можете использовать: Логин: "+user.getLogin()+" Пароль: " + user.getPassword()+" </div></body></html>";
        String tema = "Ваша аккаунт одобрен";
        executorService.submit(new MailManager(user.getPrivateInfo().getEmail(), tema, message, javaMailSender));*/

        return "ok";
    }



    private  String passwordGenerator(){
        String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789@";
        String pw = "";
        for (int i=0; i<8; i++)
        {
            int index = (int)(RANDOM.nextDouble()*letters.length());
            pw += letters.substring(index, index+1);
        }
        return pw;
    }

    @RequestMapping(value = "/admin_get_tender_by_tender_id", method = RequestMethod.GET)
    public @ResponseBody Tender showTenderById(@RequestParam("tender_id") int tender_id) {
        Session session = sessionFactory.openSession();
        Tender tender = (Tender) session.get(Tender.class, tender_id);
        session.close();
        return tender;
    }

    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }


}
