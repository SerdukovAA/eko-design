package com.tender.webapp.models.service;

/**
 * Created by Serdukov on 26.07.2015.
 */
public class Error {

    private int errorNumber;
    private String errorText;

    public Error(int errorNumber , String errorText){
        this.errorNumber = errorNumber;
        this.errorText = errorText;
    }

    public int getErrorNumber() {
        return errorNumber;
    }

    public void setErrorNumber(int errorNumber) {
        this.errorNumber = errorNumber;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }
}
