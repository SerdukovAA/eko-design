package com.tender.webapp.dao.impl;

import com.tender.webapp.dao.TenderStatusDAO;
import com.tender.webapp.models.handbook.TenderStatus;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by serdukov on 31.10.15.
 */
public class TenderStatusDAOImpl implements TenderStatusDAO {

    @Resource
    protected SessionFactory sessionFactory;

    public TenderStatus getTenderStatusById(int id){
        Session session = sessionFactory.getCurrentSession();
        return (TenderStatus) session.get(TenderStatus.class , id);
    }



}
