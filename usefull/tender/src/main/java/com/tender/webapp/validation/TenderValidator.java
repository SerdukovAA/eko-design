package com.tender.webapp.validation;

import com.tender.webapp.biznes.SelectorsService;
import com.tender.webapp.biznes.ValidateService;
import com.tender.webapp.controllers.service_bean.ParametersRequest;
import com.tender.webapp.controllers.service_bean.TenderFormObject;
import com.tender.webapp.models.*;
import com.tender.webapp.models.handbook.*;
import com.tender.webapp.models.service.ServerMessage;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Serdukov on 06.06.2015.
 */
@Component
public class TenderValidator {


    @Autowired
    SelectorsService selectorsService;


    private static final Logger log = Logger.getLogger(TenderValidator.class);

    public void validate(TenderFormObject tenderFormObject, HttpServletRequest request, ServerMessage serverMessage) {

        try {


            //проверяем ответственных по тендеру
            checkResponseble(tenderFormObject, serverMessage);

            if (tenderFormObject.getTenderName() == null || tenderFormObject.getTenderName().trim().isEmpty()) {
                serverMessage.addError(2, "Не передан заголовок для тендера");
            }

            if (tenderFormObject.getTenderDescription() == null || tenderFormObject.getTenderDescription().trim().isEmpty()) {
                serverMessage.addError(2, "Не передано описание тендера");
            }


            //проверка общих параметров по тендеру
            if (tenderFormObject.getCommonValueList() != null) {
                //проверка общих параметров по тендеру
                for (CommonValue commonValue : tenderFormObject.getCommonValueList()) {
                    if (commonValue.getParameterName().trim().isEmpty())
                        serverMessage.addError(2, "Не передано наименование одного из общих параметров тендера");
                    if (commonValue.getCommonId() == 0)
                        serverMessage.addError(2, "Не передан идентификатор одного из общих параметров тендера");
                    if (commonValue.getParameterValue() == null || commonValue.getParameterValue().trim().isEmpty())
                        serverMessage.addError(2, "Не передан значение одного из общих параметров тендера");
                }
            }


            //проверка селекторов по виду услуги
            if (tenderFormObject.getSelectorValueList() != null) {
                for (SelectorValue selectorValue : tenderFormObject.getSelectorValueList()) {
                    if (selectorValue.getSelectorName().trim().isEmpty())
                        serverMessage.addError(2, "Не передано наименование одного из селекторов категории тендера");
                    if (selectorValue.getSelectorId() == 0)
                        serverMessage.addError(2, "Не передан идентификатор одного из селекторов категории тендера");
                    if (selectorValue.getValueId() == 0)
                        serverMessage.addError(2, "Не передан идентификатор одного из значений селектора категории тендера");
                    if (selectorValue.getSelectorValue().trim().isEmpty())
                        serverMessage.addError(2, "Не передан значение одного из селекторов категории тендера");
                }
            } else {
                serverMessage.addError(2, "Не передан значение одного из селекторов категории тендера. Что то не так.");
            }


            if (tenderFormObject.getTenderFileList() != null) {
                for (TenderFile tenderFile : tenderFormObject.getTenderFileList()) {
                    if (tenderFile.getFileLink().trim().isEmpty())
                        serverMessage.addError(2, "Не передано ссылка на один из дополнительных файлов по тендеру");
                    if (tenderFile.getFileName().trim().isEmpty())
                        serverMessage.addError(2, "Не передан наименование одного из дополнительных файлов по тендеру");
                }
            }


            try {
                BigDecimal exPrice = new BigDecimal(tenderFormObject.getExpectedPrice().replaceAll(" ", ""));
                exPrice = null;
            } catch (Exception e) {
                serverMessage.addError(2, "Ожидаемая цена по тендеру не валидна. Значение должно содержать числовое значение в рублях.");
            }


            if(tenderFormObject.getAdress() ==null) {
                serverMessage.addError(42, "Не передан адрес тендера");
            }else{
                if (tenderFormObject.getAdress().getRegion() == null ) {
                    serverMessage.addError(42, "Не передан регион адреса тендера");
                }

                if (tenderFormObject.getAdress().getStreet() == null || tenderFormObject.getAdress().getStreet().trim().isEmpty()) {
                    serverMessage.addError(42, "Не передана улица адреса тендера");
                }

                if (tenderFormObject.getAdress().getHouseNumber() == null || tenderFormObject.getAdress().getHouseNumber().trim().isEmpty()) {
                    serverMessage.addError(42, "Не передан номер дома адреса тендера");
                }


                if(tenderFormObject.getAdress().getLongitude()==0.0d){
                    serverMessage.addError(42, "Не передана Longitude адреса тендера");
                }
                if(tenderFormObject.getAdress().getLatitude()==0.0d){
                    serverMessage.addError(42, "Не передана Latitude адреса тендера");
                }


                if (tenderFormObject.getAdress().getFullAdress() == null || tenderFormObject.getAdress().getFullAdress().trim().isEmpty()) {
                    serverMessage.addError(42, "Не передан полный адрес  тендера");
                }

            }

















            checkDate(tenderFormObject, serverMessage);


            TypeService typeService = tenderFormObject.getTypeService();
            if (typeService == null) {
                serverMessage.addError(2, "Пеередана не валидная предоставляемая услуга - typeService");
                return;
            } else {
                if (typeService.getCommonParameters() != null) {
                    for (CommonParameter commonParameter : typeService.getCommonParameters()) {
                        if (commonParameter.getValue() == null || commonParameter.getValue().trim().isEmpty())
                            serverMessage.addError(2, "Не передано значение одного из общих параметров тендера");
                    }
                }
            }


            TypeObject typeObject = tenderFormObject.getTypeObject();
            if (typeObject == null) {
                serverMessage.addError(2, "Пеередан не валидный тип объекта тендера - typeObject");
                return;
            }


            //перед валидацией каждого объекта нужно забрать параметры по значениям селекторов и по эти параметра валидировать.
            // используются только не зависимые селекторы поэтому здесь просто


            List<ParametersRequest> parRequest = new ArrayList<ParametersRequest>();

            for (CategorySelector categorySelector : tenderFormObject.getTypeService().getCategorySelectors()) {
                if (categorySelector.getConsumerSelectorType() == "radio" && categorySelector.isDepended() == false) {
                    ParametersRequest parametersRequest = new ParametersRequest();
                    parametersRequest.setSelectorId(categorySelector.getId());
                    parametersRequest.setOptionId(categorySelector.getCheckedOptionId());
                    parRequest.add(parametersRequest);
                }
                if (categorySelector.getConsumerSelectorType() == "checkbox" && categorySelector.isDepended() == false) {

                    for (SelectorOption selectorOption : categorySelector.getSelectorOptions()) {
                        if (selectorOption.isChecked()) {
                            ParametersRequest parametersRequest = new ParametersRequest();
                            parametersRequest.setSelectorId(categorySelector.getId());
                            parametersRequest.setOptionId(selectorOption.getOptionId());
                            parRequest.add(parametersRequest);
                        }
                    }
                }
            }

            //зависимые параметры для объектов задаются на основании основных селекторов
            List<ParameterRelation> parameterRelations = selectorsService.getParametersRelations(parRequest);


            if (tenderFormObject.getTenderObjects() == null) {
                serverMessage.addError(2, "Не передана не один объект по тендеру");
                return;
            }


            for (TenderObject tenderObject : tenderFormObject.getTenderObjects()) {


                if(tenderObject.getObjectFotos() != null) {
                    for (ObjectFoto objectFoto : tenderObject.getObjectFotos()) {
                        if (objectFoto.getFotoLink() == null || objectFoto.getFotoLink().trim().isEmpty()) {
                            serverMessage.addError(2, "Не передана ссылка на фото по объекту тендера");
                        }

                    }
                }


                for (ParameterRelation parameterRelation : parameterRelations) {
                    for (ParameterRelation objectParameterRelation : tenderObject.getParameterRelations()) {
                        if (parameterRelation.getObjectParameter().getId() == objectParameterRelation.getObjectParameter().getId()) {
                            if (parameterRelation.isRequired()) {
                                if (objectParameterRelation.getObjectParameter().getValue() == null || objectParameterRelation.getObjectParameter().getValue().trim().isEmpty()) {
                                    serverMessage.addError(52, "Не передана обязательный параметр тендера");
                                }
                                if (!RegExpCheck.fieldRegExCheck(parameterRelation.getObjectParameter().getParameterRegEx(), objectParameterRelation.getObjectParameter().getValue())) {
                                    serverMessage.addError(53, "Значение переданного параметра (" + parameterRelation.getObjectParameter().getParameterName() + ") для объекта не валидно");
                                }
                            } else {
                                if (!objectParameterRelation.getObjectParameter().getValue().trim().isEmpty()) {
                                    if (!RegExpCheck.fieldRegExCheck(parameterRelation.getObjectParameter().getParameterRegEx(), objectParameterRelation.getObjectParameter().getValue())) {
                                        serverMessage.addError(54, "Значение переданного параметра объекта не валидно");
                                    }
                                }
                            }
                        }
                    }
                }

            }


        } catch (Exception ex) {
            ex.printStackTrace();
            serverMessage.setHasErrors(true);
            log.info(" id сессии: " + request.getSession().getId() + ". Ошибка при валидации нового тендера: " + ex.getMessage());
        }
    }


    private void tenderPlaceCheck(ServerMessage serverMessage, Double coordinate) {

        try {
            Double place = new Double(coordinate);
            place = null;
        } catch (Exception e) {
            serverMessage.addError(2, "Координата " + coordinate + " не валидна");
        }

    }


    private void checkDate(TenderFormObject tenderFormObject, ServerMessage serverMessage) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        sdf.setLenient(false);
        Date dBeng = null;
        Date dEnd = null;

        if (tenderFormObject.getDateBegin() == null || tenderFormObject.getDateBegin().isEmpty()) {
            serverMessage.addError(2, "Не передана дата начала сроков проведения тендера");
        } else {
            try {
                String dateBegin = tenderFormObject.getDateBegin().trim();
                dBeng = sdf.parse(dateBegin);
            } catch (ParseException e) {
                serverMessage.addError(2, "Переданая дата начала сроков проведения тендера не подходит по формату ");
            }
        }

        if (tenderFormObject.getDateEnd() == null || tenderFormObject.getDateEnd().isEmpty()) {
            serverMessage.addError(2, "Не передана дата окончания сроков проведения тендера");
        } else {
            try {
                String dateEnd = tenderFormObject.getDateEnd().trim();
                dEnd = sdf.parse(dateEnd);
            } catch (ParseException e) {
                serverMessage.addError(2, "Переданая дата окончания сроков проведения тендера не подходит по формату ");
            }
        }

        if (dBeng != null && dEnd != null) {
            if (dBeng.compareTo(dEnd) > 0) {
                serverMessage.addError(2, "Дата начала проведения тендера позже даты окончания.");
            }
            dBeng = null;
            dEnd = null;
        }


    }



    //проверка ответственного по тендеру
    private void checkResponseble(TenderFormObject tenderFormObject, ServerMessage serverMessage) {
        if (tenderFormObject.isNeedResponsible()) {

            if (tenderFormObject.getResponsibleInfo() != null) {

                if (tenderFormObject.getResponsibleInfo().getFirstName() == null || tenderFormObject.getResponsibleInfo().getFirstName().trim().isEmpty()) {
                    serverMessage.addError(2, "Не передано имя ответственного по тендеру");
                }

                if (tenderFormObject.getResponsibleInfo().getLastName() == null || tenderFormObject.getResponsibleInfo().getLastName().trim().isEmpty()) {
                    serverMessage.addError(2, "Не передана фамилия ответственного по тендеру");
                }

                if (tenderFormObject.getResponsibleInfo().getPhoneNumber() == null || tenderFormObject.getResponsibleInfo().getPhoneNumber().trim().isEmpty()) {
                    serverMessage.addError(2, "Не передан телефон ответственного по тендеру");
                } else {
                    if (!RegExpCheck.phoneRegExCheck(tenderFormObject.getResponsibleInfo().getPhoneNumber())) {
                        serverMessage.addError(2, "Передан не валидный телефон ответственного по тендеру");
                    }
                }
                if (tenderFormObject.getResponsibleInfo().getEmail() == null || tenderFormObject.getResponsibleInfo().getEmail().trim().isEmpty()) {
                    serverMessage.addError(2, "Не передан email ответственного по тендеру");
                } else {
                    if (!RegExpCheck.emailRegExCheck(tenderFormObject.getResponsibleInfo().getEmail())) {
                        serverMessage.addError(2, "Передан не валидный email ответственного по тендеру");
                    }
                }
            } else {
                serverMessage.addError(2, "Не передан ответственный по тендеру");
            }

            if (tenderFormObject.getResponsibleInfo() != null
                    && tenderFormObject.getResponsibleInfo().getFirstName() != null
                    && tenderFormObject.getResponsibleInfo().getLastName() != null
                    && tenderFormObject.getResponsibleInfo().getPhoneNumber() != null
                    && tenderFormObject.getResponsibleInfo().getEmail() != null) {

                if (!RegExpCheck.emailRegExCheck(tenderFormObject.getResponsibleInfo().getEmail())) {
                    tenderFormObject.setResponsibleInfo(null);
                }

                if (!RegExpCheck.cyrillicNameRegExCheck(tenderFormObject.getResponsibleInfo().getLastName())) {
                    tenderFormObject.setResponsibleInfo(null);
                }


            } else {
                tenderFormObject.setResponsibleInfo(null);
            }
        }
        return;
    }

}