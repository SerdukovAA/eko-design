package com.tender.webapp.biznes.impl;


import com.tender.webapp.biznes.ValidateService;
import com.tender.webapp.dao.JuridicalInfoDAO;
import com.tender.webapp.dao.PrivateInfoDAO;
import com.tender.webapp.dao.UserDAO;
import com.tender.webapp.models.JuridicalInfo;
import com.tender.webapp.models.PrivateInfo;
import com.tender.webapp.models.User;
import com.tender.webapp.models.journal.PhoneCheckJournal;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by Serdukov on 05.06.2015.
 */
public class ValidateServiceImpl implements ValidateService {

    @Resource
    protected SessionFactory sessionFactory;

    @Autowired
    PrivateInfoDAO privateInfoDAO;
    @Autowired
    JuridicalInfoDAO juridicalInfoDAO;

    @Autowired
    UserDAO userDAO;

    /**
     * Проверка что номер телефона уже не зарегистрирован в базе
     * @param phoneNumber
     * @return
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class, readOnly = true)
    public boolean checkPhoneNumberExist(String phoneNumber) throws Exception{
        PrivateInfo phone_check = privateInfoDAO.findPrivateInfoByPhoneNumber(phoneNumber);
        if (phone_check != null) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * Проверка что login уже не используется
     * @param
     * @return
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class, readOnly = true)
    public boolean checkLoginExist(String login) throws Exception{
        User user = userDAO.findUserByLogin(login);
        if (user != null) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * Проверка что email уже не используется
     * @param
     * @return
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class, readOnly = true)
    public boolean checkEmailExist(String email) throws Exception{
        User user = userDAO.findUserByEmail(email);
        if (user != null) {
            return true;
        }else{
            return false;
        }
    }


    /**
     * Проверка что inn уже не используется
     * @param
     * @return
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class, readOnly = true)
    public boolean checkINNExist(String inn) throws Exception{
        User user = juridicalInfoDAO.findJuridicalInfoByInn(inn);
        if (user != null) {
            return true;
        }else{
            return false;
        }
    }





    /**
     * Проверка кода высылаемого на телефон
     * @param
     * @return
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public boolean checkPhoneCode(String checkID,  String code) throws Exception{
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from PhoneCheckJournal where id= :id and check_code = :code");
        query.setParameter("code", code);
        query.setParameter("id", Integer.parseInt(checkID));
        PhoneCheckJournal phoneCheckJournal = (PhoneCheckJournal) query.uniqueResult();
        if(phoneCheckJournal==null){
            return false;
        }else{
            phoneCheckJournal.setIsCheck(true);
            session.update(phoneCheckJournal);
            session.flush();
            return true;
        }
    }


}
