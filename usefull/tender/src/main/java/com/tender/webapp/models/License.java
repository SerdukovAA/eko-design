package com.tender.webapp.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "licenses")
public class License {

public License(){}

    public License(String name,Date date ,String link){
        this.licenseName = name;
        this.licenseLink = link;
        this.licenseDateGiven =date;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "license_name", unique = false, nullable = false)
    private String licenseName;
    @Column(name = "license_link", unique = true, nullable = false)
    private String licenseLink;
    @Column(name = "license_date_given", unique = false, nullable = false)
    private Date licenseDateGiven;


    @ManyToOne
    @JoinColumn(name = "jur_info_id")
    private JuridicalInfo juridicalInfo;

        /*--getters and setters-*/

    public String getLicenseName() {
        return licenseName;
    }

    public void setLicenseName(String licenseName) {
        this.licenseName = licenseName;
    }

    public String getLicenseLink() {
        return licenseLink;
    }

    public void setLicenseLink(String licenseLink) {
        this.licenseLink = licenseLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public JuridicalInfo getJuridicalInfo() {
        return juridicalInfo;
    }

    public void setJuridicalInfo(JuridicalInfo juridicalInfo) {
        this.juridicalInfo = juridicalInfo;
    }
}
