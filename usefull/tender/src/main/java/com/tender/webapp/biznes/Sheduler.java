package com.tender.webapp.biznes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by serdukov on 24.12.15.
 */
@Component
public class Sheduler {

    @Autowired
    InfoService infoService;

    @Scheduled(cron="0 15 21 * * MON,FRI")
    public void counterSumm() {
        infoService.summCounter();
    }
}
