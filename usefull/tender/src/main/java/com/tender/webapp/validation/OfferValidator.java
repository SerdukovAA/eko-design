package com.tender.webapp.validation;

import com.tender.webapp.biznes.ServiceTender;
import com.tender.webapp.controllers.service_bean.OfferFormObject;
import com.tender.webapp.dao.TenderDAO;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.service.ServerMessage;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Serdukov on 06.06.2015.
 */
@Component
public class OfferValidator {


    @Autowired
    ServiceTender serviceTender;

    private static final Logger log = Logger.getLogger(OfferValidator.class);


    public void validate(OfferFormObject offerFormObject, ServerMessage serverMessage) {


        try {


            if(offerFormObject.getAdress() ==null) {
                serverMessage.addError(42, "Не передан адрес предложения");
            }else{
                if (offerFormObject.getAdress().getRegion() == null ) {
                    serverMessage.addError(42, "Не передан регион адреса предложения");
                }

                if (offerFormObject.getAdress().getStreet() == null || offerFormObject.getAdress().getStreet().trim().isEmpty()) {
                    serverMessage.addError(42, "Не передана улица адреса предложения");
                }

                if (offerFormObject.getAdress().getHouseNumber() == null || offerFormObject.getAdress().getHouseNumber().trim().isEmpty()) {
                    serverMessage.addError(42, "Не передан номер дома адреса предложения");
                }

                if(offerFormObject.getAdress().getLongitude()==0.0d){
                    serverMessage.addError(42, "Не передана Longitude адреса предложения");
                }
                if(offerFormObject.getAdress().getLatitude()==0.0d){
                    serverMessage.addError(42, "Не передана Latitude адреса предложения");
                }


                if (offerFormObject.getAdress().getFullAdress() == null || offerFormObject.getAdress().getFullAdress().trim().isEmpty()) {
                    serverMessage.addError(42, "Не передан полный адрес предложения");
                }

            }



            if (offerFormObject.getOfferDescription().trim().isEmpty()) serverMessage.addError(2, "Не передана описание предложения");

            if (offerFormObject.getPrice().trim().isEmpty()) {
                serverMessage.addError(2, "Не передано ценовое предложение");
            } else {
                try {
                    BigDecimal price = new BigDecimal(offerFormObject.getPrice().trim().replaceAll(" ", ""));
                } catch (Exception e) {
                    serverMessage.addError(2, "Предлагаемая цена не валидна. Значение должно содержать сумму в рублях.");
                }
            }


            //////////////////Проверка дат передаваемых сроков предложения////////////////////////////////////////////////////////////
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            sdf.setLenient(false);
            Date dBeng = null;
            Date dEnd = null;

            String dateBegin = offerFormObject.getDateBegin().trim();
            if (dateBegin.isEmpty()) {
                serverMessage.addError(2, "Не передана дата начала сроков выполнения тендера");
            } else {
                try {
                    dBeng = sdf.parse(dateBegin);
                } catch (ParseException e) {
                    serverMessage.addError(2, "Переданая дата начала сроков выполнения тендера не подходит по формату ");
                }
            }

            String dateEnd = offerFormObject.getDateEnd().trim();
            if (dateEnd.isEmpty()) {
                serverMessage.addError(2, "Не передана дата окончания сроков выполнения тендера");
            } else {
                try {
                    dEnd = sdf.parse(dateEnd);
                } catch (ParseException e) {
                    serverMessage.addError(2, "Переданая дата окончания сроков выполнения тендера не подходит по формату ");
                }
            }

            if (dBeng != null && dEnd != null) {
                if (dBeng.compareTo(dEnd) > 0) {
                    serverMessage.addError(2, "Дата начала выполнения тендера позже даты окончания.");
                }
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////


            if (offerFormObject.getTender_id() == 0) {
                serverMessage.addError(2, "Не передан информация, по целевому тендеру");
            } else {

                int tender_id = offerFormObject.getTender_id();

                Tender tender = serviceTender.getUniqTenderByTenderId(tender_id);
                if (tender == null) {
                    serverMessage.addError(2, "Целевого тендера не существует");
                }

                if (tender.getTenderStatus().getId() != 2) {
                    serverMessage.addError(2, "Целевой тендер не ждет предложений");
                }


            }


        } catch (Exception ex) {
            ex.printStackTrace();
            serverMessage.setHasErrors(true);
            log.error(" Ошибка при валидации нового оффера: " + ex.getMessage(), ex);
        }

    }


}