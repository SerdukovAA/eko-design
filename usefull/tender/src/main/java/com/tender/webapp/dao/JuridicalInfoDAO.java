package com.tender.webapp.dao;


import com.tender.webapp.models.User;

/**
 * Created by serdukov on 01.11.15.
 */
public interface JuridicalInfoDAO {


    public User findJuridicalInfoByInn(String inn) throws Exception;

}
