package com.tender.webapp.dao;

import com.tender.webapp.models.handbook.TenderStatus;

/**
 * Created by serdukov on 31.10.15.
 */
public interface TenderStatusDAO {

    public TenderStatus getTenderStatusById(int id);

}
