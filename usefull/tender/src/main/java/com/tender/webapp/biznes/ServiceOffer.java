package com.tender.webapp.biznes;


import com.tender.webapp.controllers.service_bean.AcceptOfferRequest;
import com.tender.webapp.controllers.service_bean.ChangeProviderRequest;
import com.tender.webapp.controllers.service_bean.OfferFormObject;
import com.tender.webapp.models.Offer;
import com.tender.webapp.models.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Created by Serdukov on 05.06.2015.
 */
public interface ServiceOffer {


    public void createAndSaveNewOffer(OfferFormObject offerFormObject, User user) throws Exception;
    public void acceptOffer(AcceptOfferRequest acceptOfferRequest, User user) throws Exception;
    public void changeProvider(ChangeProviderRequest changeProviderRequest, User user) throws Exception;


    public Offer getoOfferWinByTenderId(int tender_id, int user_id) throws Exception;



}
