package com.tender.webapp.config;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by serdukov on 24.01.16.
 */
public class ParamsProvider {


    private static final Properties properties = new Properties();
    private static final String PROPERTIES_FILE_NAME = "tenderApp.properties";

    static {
        try{
            properties.load(ParamsProvider.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME));
        } catch(IOException e){
            System.err.println("Ошибка загруки properties-файла: " + PROPERTIES_FILE_NAME);
            e.printStackTrace();
        }
    }

    //настройки для статичных методов
    public static final boolean isDevelop = new Boolean(properties.getProperty("tender.regime.develop"));
    public static final String siteKey = properties.getProperty("recapcha.siteKey"); //"6LcWMRYTAAAAAHYoidY-XNPVQjhqbt0fHbQ39Z7p";
    public static final String postEmailAddress = properties.getProperty("post.email.address"); //"6LcWMRYTAAAAAHYoidY-XNPVQjhqbt0fHbQ39Z7p";
    public static final String secretKey = properties.getProperty("recapcha.secretKey");// "6LcWMRYTAAAAALTwrXIpaL0G6xJmt4LCmNBWH9sz";




}
