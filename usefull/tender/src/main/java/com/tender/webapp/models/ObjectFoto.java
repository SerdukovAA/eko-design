package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Serdukov on 14.06.2015.
 */
@Entity
@Table(name = "object_fotos")
public class ObjectFoto {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "foto_link", unique = false, nullable = false)
    private String fotoLink;

    @Column(name = "foto_min_link", unique = false)
    private String fotoMinLink;


    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;


    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;

    @ManyToOne
    @JoinColumn(name = "object_id")
    @JsonIgnore
    private TenderObject tenderObject;

   /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFotoLink() {
        return fotoLink;
    }

    public void setFotoLink(String fotoLink) {
        this.fotoLink = fotoLink;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }

    public TenderObject getTenderObject() {
        return tenderObject;
    }

    public void setTenderObject(TenderObject tenderObject) {
        this.tenderObject = tenderObject;
    }


    public String getFotoMinLink() {
        return fotoMinLink;
    }

    public void setFotoMinLink(String fotoMinLink) {
        this.fotoMinLink = fotoMinLink;
    }

}
