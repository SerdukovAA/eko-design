package com.tender.webapp.models.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "city")
@Deprecated
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "city_code")
    private String cityCode;

    @Column(name = "namec40")
    private String cityName;

    @Column(name = "socrc10")
    private String socrName;

    @ManyToOne
    @JoinColumn(name = "code_region")
    @JsonIgnore
    private Region region;

     /*--getters and setters-*/

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }


    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getSocrName() {
        return socrName;
    }

    public void setSocrName(String socrName) {
        this.socrName = socrName;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }


}
