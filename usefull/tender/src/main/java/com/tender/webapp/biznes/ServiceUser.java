package com.tender.webapp.biznes;


import com.tender.webapp.controllers.service_bean.ChangePassRequest;
import com.tender.webapp.controllers.service_bean.ConsumerFormObject;
import com.tender.webapp.controllers.service_bean.ParametersRequest;
import com.tender.webapp.controllers.service_bean.ProviderFormObject;
import com.tender.webapp.models.License;
import com.tender.webapp.models.ProviderFilterValues;
import com.tender.webapp.models.User;
import com.tender.webapp.models.handbook.ParameterRelation;
import com.tender.webapp.models.handbook.UserParameter;
import com.tender.webapp.models.handbook.UserPreferedRegion;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Serdukov on 05.06.2015.
 */
public interface ServiceUser {

    public void createAndSaveConsumer(ConsumerFormObject consumerFormObject) throws Exception;

    public void createAndSaveProvider(ProviderFormObject providerFormObject) throws Exception;
    public void editProvider(ProviderFormObject providerFormObject, User user) throws Exception;


    public void changePassword(ChangePassRequest changePassRequest, User user) throws Exception;
    public UserParameter getProviderParameters(int user_id) throws Exception;
    public List<ProviderFilterValues> getSelectedProvidersFilters(int user_id) throws Exception;
    public User getUserByUserId(int user_id) throws Exception;
    public User getUserByLogin(String login) throws Exception;

    public User getTenderOwnerForWinner(int user_id, int tender_id) throws Exception;


    public void saveProviserFiltersParameters(List<ParametersRequest> parametersRequestList, User user) throws Exception;
    public void saveProviderPreferRegions(List<UserPreferedRegion> userPreferedRegions, User user) throws Exception;

    public String getCreateAndCheckPhoneID(String phoneNumber) throws Exception;

    public void deleteBySelf(int user_id) throws Exception;
    public void passwordRecovery(String loginEmail) throws Exception;
    public boolean checkPasswordRecovery(String hash, int rec_id) throws Exception;
    public void changePasswordRecovery(HttpServletRequest request) throws Exception;
    public void saveMailUserParameters(UserParameter userParameter,User user) throws Exception;
}
