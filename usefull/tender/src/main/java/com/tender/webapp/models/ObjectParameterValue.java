package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.CommonParameter;
import com.tender.webapp.models.handbook.ObjectParameter;

import javax.persistence.*;

/**
 * Created by Serdukov on 09.09.2015.
 */
@Entity
@Table(name = "obj_parameters_values")
public class ObjectParameterValue {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "parameter_id", unique = false, nullable = false)
    private int parameterId;


    @Column(name = "parameter_name", unique = false, nullable = false)
    private String parameterName;

    @Column(name = "post_fix", unique = false, nullable = true)
    private String postFix;



    @Column(name = "value_id", unique = false, nullable = true)
    private int valueId;

    @Column(name = "parameter_value", unique = false, nullable = false)
    private String parameterValue;


    @ManyToOne
    @JoinColumn(name = "object_id")
    @JsonIgnore
    private TenderObject tenderObject;

    /*-------------getters and setters----------------------*/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getParameterId() {
        return parameterId;
    }

    public void setParameterId(int parameterId) {
        this.parameterId = parameterId;
    }


    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }


    public int getValueId() {
        return valueId;
    }

    public void setValueId(int valueId) {
        this.valueId = valueId;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public TenderObject getTenderObject() {
        return tenderObject;
    }

    public void setTenderObject(TenderObject tenderObject) {
        this.tenderObject = tenderObject;
    }


    public String getPostFix() {
        return postFix;
    }

    public void setPostFix(String postFix) {
        this.postFix = postFix;
    }


}

