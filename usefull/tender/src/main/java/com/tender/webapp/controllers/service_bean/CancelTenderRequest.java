package com.tender.webapp.controllers.service_bean;



/**
 * Created by Serdukov on 03.09.2015.
 */

/**
 * Объект для отмены тендера заказчиком
 */
public class CancelTenderRequest {

    private int tender_id = 0;
    private String cancelReasonComment;


/*-------getters and setters------------------------*/


    public String getCancelReasonComment() {
        return cancelReasonComment;
    }

    public void setCancelReasonComment(String cancelReasonComment) {
        this.cancelReasonComment = cancelReasonComment;
    }

    public int getTender_id() {
        return tender_id;
    }

    public void setTender_id(int tender_id) {
        this.tender_id = tender_id;
    }


}
