package com.tender.webapp.config.rest;

import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.config.ParamsProvider;
import com.tender.webapp.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * An authentication success handler implementation adapted to a REST approach.
 */
public class RESTAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	ServiceUser serviceUser;
	private static boolean isDevelop = ParamsProvider.isDevelop;


	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
	                                    Authentication authentication) throws IOException, ServletException {

		clearAuthenticationAttributes(request);


		//самопись
		try{
			System.out.println("Удачно залогинился, прописываем пользователя в сессию ");
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = serviceUser.getUserByLogin(auth.getName());
			HttpSession httpSession = request.getSession();
			if(isDevelop){
				httpSession.setMaxInactiveInterval(1000);
			}else{
				httpSession.setMaxInactiveInterval(4600);
			}
			httpSession.setAttribute("user", user);
		}catch (Exception e){
			System.out.println("почему то не нашли удачно залогиненого пользователя, что быть не может");
			e.printStackTrace();
		}


	}
}
