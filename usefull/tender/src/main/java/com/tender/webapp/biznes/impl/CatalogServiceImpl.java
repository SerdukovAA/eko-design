package com.tender.webapp.biznes.impl;


import com.tender.webapp.biznes.CatalogService;
import com.tender.webapp.models.base.AutoBrand;
import com.tender.webapp.models.base.Region;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Serdukov on 05.06.2015.
 */
public class CatalogServiceImpl implements CatalogService{

    @Resource
    protected SessionFactory sessionFactory;


    /**
     * Возвращаем справочник марок и моделей
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<AutoBrand> getModelBrands() throws Exception{

        Session session = sessionFactory.getCurrentSession();
        Criteria sc = session.createCriteria(AutoBrand.class);
        sc.addOrder(Order.asc("brandName"));
        sc.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

        List<AutoBrand> listAutoBrand = sc.list();

        return listAutoBrand;

    }

     /**
     * Возвращаем справочник моделей и городов
     * возможно нужно будет отвязать города от регионов чтобы ускорить запрос?
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<Region> getRegions() throws Exception{
        Session session = sessionFactory.getCurrentSession();
        Criteria sc = session.createCriteria(Region.class);
        sc.addOrder(Order.asc("regionName"));
        sc.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        List<Region> listRegions = sc.list();
        return listRegions;

    }


}
