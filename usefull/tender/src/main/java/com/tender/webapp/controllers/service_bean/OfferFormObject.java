package com.tender.webapp.controllers.service_bean;

import com.tender.webapp.models.Adress;
import com.tender.webapp.models.base.City;
import com.tender.webapp.models.base.Region;

/**
 * Created by Serdukov on 02.06.2015.
 */

public class OfferFormObject {


    private int tender_id = 0;
    private String offerDescription;

    private Adress adress;

    private String dateBegin;
    private String dateEnd;

    private String price;



    private String street;
    private String houseNumber;





    /*--getters and setters-*/

    public int getTender_id() {
        return tender_id;
    }

    public void setTender_id(int tender_id) {
        this.tender_id = tender_id;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }



    public String getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

}
