
package com.tender.webapp.biznes.impl;

import com.tender.webapp.biznes.EmailService;
import com.tender.webapp.biznes.ServiceTender;
import com.tender.webapp.comunicators.PosterComunicator;
import com.tender.webapp.config.ParamsProvider;
import com.tender.webapp.controllers.service_bean.CancelTenderRequest;
import com.tender.webapp.controllers.service_bean.OverTenderRequest;
import com.tender.webapp.controllers.service_bean.TenderFormObject;
import com.tender.webapp.dao.CanceledTenderDAO;
import com.tender.webapp.dao.OfferDAO;
import com.tender.webapp.dao.TenderDAO;
import com.tender.webapp.dao.UserDAO;
import com.tender.webapp.mail.MailManager;
import com.tender.webapp.models.*;
import com.tender.webapp.models.handbook.*;
import com.tender.webapp.models.journal.RatingJournal;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class ServiceTenderImpl implements ServiceTender {
    @Resource
    protected SessionFactory sessionFactory;
    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    ExecutorService executorService;
    @Autowired
    UserDAO userDAO;

    @Autowired
    OfferDAO offerDAO;
    @Autowired
    TenderDAO tenderDAO;
    @Autowired
    EmailService emailService;
    @Autowired
    CanceledTenderDAO canceledTenderDAO;

    private static boolean isDevelop = ParamsProvider.isDevelop;

    private static final Logger log = Logger.getLogger(ServiceTenderImpl.class);


    /**
     * Сервис по возврату тендера для внутренних нужд
     * @param tender_id
     * @return
     * @throws Exception
     */

    @Transactional( propagation = Propagation.REQUIRED, rollbackFor = {Exception.class}, readOnly = true)
    public Tender getUniqTenderByTenderId(int tender_id) throws Exception {
        Tender tender = tenderDAO.getActiveTenderById(tender_id);
        //если такого тендера нет, то ничего не взращаем, хотя возможно нужно выбрасывать ошибку
        if(tender == null) {
            throw new Exception("Активных тендеров не обнаружено");
        }
        return tender;
    }





    /**
     * Возвращаем тендер, видоизменяя в зависимости от того - кто запрашивает тендер
     * @param user
     * @param tender_id
     * @return
     * @throws Exception
     */
    @Transactional( propagation = Propagation.REQUIRED, rollbackFor = {Exception.class}, readOnly = true)
    public Tender getTenderByTenderId(User user, int tender_id) throws Exception {
        Tender tender = tenderDAO.getActiveTenderById(tender_id);

        //если такого тендера нет, то ничего не взращаем, хотя возможно нужно выбрасывать ошибку
        if(tender == null) {
            throw new Exception("Активных тендеров не обнаружено");
        }

        //если это собственный тендер то ставит отметку что он принадлежит юзеру
        if(user != null && tender.getUserOwner().getUser_id() == user.getUser_id()) {//

            tender.setIs_guest(false);
            tender.setIs_my(true);
            if(tender.getOfferList() != null && tender.getOfferList().size() > 0) {
                for(Offer offer :tender.getOfferList()){
                    User provider = offer.getProvider();
                    provider.setPrivateInfo((PrivateInfo)null);
                    provider.setUserParameter((UserParameter)null);
                }
            }
        }else if(user != null && tender.getUserOwner().getUser_id() != user.getUser_id() && user.getUser_id() != 0 ){
            tender.setIs_guest(false);
            tender.setIs_my(false);
            tender.setUserOwner((User) null);
            if(tender.getOfferList() != null && tender.getOfferList().size() > 0) {
                for(Offer offer :tender.getOfferList()){
                    User provider = offer.getProvider();
                    if(offer.getOfferStatus().getId() == 2 && provider.getUser_id() == user.getUser_id()){ tender.setIs_provider(true);}
                    provider.setPrivateInfo((PrivateInfo)null);
                    provider.setUserParameter((UserParameter)null);
                }
            }
        }else if(user != null && tender.getUserOwner().getUser_id() != user.getUser_id() && user.getUser_id() == 0){
            tender.setIs_guest(false);
            tender.setIs_my(false);
        }else if(user == null){ //значит это просто гость
            tender.setIs_guest(true);
            tender.setIs_my(false);
            tender.setUserOwner((User) null);
            tender.setOfferList( new HashSet<Offer>());
        }
        return tender;
    }

    /**
     * Завершение тендера заказчиком
     * @param overTenderRequest
     * @throws Exception
     */
    @Transactional( propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void consumerOverTender(OverTenderRequest overTenderRequest,User user) throws Exception {

        if(overTenderRequest.getComment().trim().isEmpty())  throw new Exception("Не передана отзыв о исполнителе тендера");

        if(overTenderRequest.getTender_id() == 0) throw new Exception("Не передан целевой тендер");

        if(overTenderRequest.getRating().trim().isEmpty()) throw new Exception("Не передана оценка исполнителя");


        String comment = overTenderRequest.getComment().trim();
        int tender_id = overTenderRequest.getTender_id();
        int rating = Integer.parseInt(overTenderRequest.getRating().trim());

        Session session = sessionFactory.getCurrentSession();

        Tender e = tenderDAO.getActiveTenderById(tender_id);
        if(e.getUserOwner().getUser_id() != user.getUser_id()) {
            throw new Exception("Попытка завершения чужого тендера");
        }

        if(e.getTenderStatus().getId() == 6) {
            throw new Exception("Тендре был отозван");
        }

        if(e.getTenderStatus().getId() == 4 || e.isOwnerCloseTender()) {
            throw new Exception("Тендер уже завершен");
        }

        if(e.getTenderStatus().getId() != 3) {
            throw new Exception("Тендер не выполняется, не исполняемый тендер завершить нельзя");
        }


        Offer offer = offerDAO.getOfferWinByTenderId(tender_id);
        if(offer == null) throw new Exception("Не найден оффер по данному тендеру");

        //если проверки все прошли
        e.setDateChange(new Date());
        e.setOwnerCloseTender(true);

        if(e.isProviderCloseTender()) {
            e.setTenderStatus((TenderStatus)session.get(TenderStatus.class, Integer.valueOf(4)));
        } else {
            String ratingJournal = "<html><head><meta charset=\'UTF-8\'></head><body><p>Уважаемый " + offer.getProvider().getPrivateInfo().getFirstName() + " " + offer.getProvider().getPrivateInfo().getLastName() + "</p>" + "<p>Заказчик подвердил выполнение исполняемого Вами тендера.</p>" + "<p>Наименование тендера " + e.getTenderName() + "</p>" + "<p>Хотим напомнить, что тендер будет закрыт только когда Вы подтвердите его выполнение.</p>" + "</body></html>";
            String tema = "Заказчик подтвердил завершение тендера";
           if(!isDevelop){
                executorService.submit(new MailManager(offer.getProvider().getPrivateInfo().getEmail(), tema, ratingJournal, this.javaMailSender));
            }
        }

        session.update(e);

        RatingJournal ratingJournal1 = new RatingJournal();
        ratingJournal1.setTender(e);
        ratingJournal1.setUser(user);
        ratingJournal1.setRating(rating);
        ratingJournal1.setComment(comment);
        ratingJournal1.setProvider(offer.getProvider());
        ratingJournal1.setDateCreate(new Date());
        session.save(ratingJournal1);

        log.info(" user_id : " + user.getUser_id() + " tender_id " + tender_id+". Тендер успешно завершен.");

    }




    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class},readOnly = true)
    public Tender getMyTenderByTenderId(int user_id, int tender_id) throws Exception {
        Tender tender = this.tenderDAO.getActiveTenderById(tender_id);
        if(tender == null) {
            throw new Exception("Активных тендеров не обнаружено");
        } else if(tender.getUserOwner().getUser_id() != user_id) {
            throw new Exception("Попытка запросить информацию не по своему тендеру");
        } else {
            if(tender.getOfferList() != null && tender.getOfferList().size() > 0) {
                Iterator var4 = tender.getOfferList().iterator();

                while(var4.hasNext()) {
                    Offer offer = (Offer)var4.next();
                    User provider = offer.getProvider();
                    provider.setPrivateInfo((PrivateInfo)null);
                    provider.setUserParameter((UserParameter)null);
                }
            }

            return tender;
        }
    }


    /**
     * Новый метод возвращает тендеры для провайдера используя процедуру фильтрации по категориям участия в базе даных
     * @param provider_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class}, readOnly = true)
    public List<Tender> getTendersForProvider(int provider_id) throws Exception {
      return tenderDAO.getTendersForProvider(provider_id);
    }

    /**
     * Новый метод возвращает тендеры для провайдера используя процедуру фильтрации по категориям участия в базе даных
     * @param provider_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class}, readOnly = true)
    public List<Tender> getTendersWinForProvider(int provider_id) throws Exception {
        return tenderDAO.getTendersWinForProvider(provider_id);
    }


    /**
     * Сервис возвращает тендеры для исполнителя в которых исполнитель принимает участие
     * @param provider_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class}, readOnly = true)
    public List<Tender> getTendersWithOfferProvider(int provider_id) throws Exception {
        return tenderDAO.getTendersWithOfferProvider(provider_id);
    }


    /**
     * Сервис возвращает собственные тендеры для заказчика, предварительно подчищая лишнюю информацию
     * @param user_owner_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class}, readOnly = true)
    public List<Tender> getTendersForOwner(int user_owner_id)throws Exception{
        List<Tender> tenderList  =  tenderDAO.getActivTendersByUserId(user_owner_id);
        //подчищаем лишнюю информацию о исполнителях
        if(tenderList!=null) {
            for (Tender tender : tenderList) {
                if (tender.getOfferList() != null && tender.getOfferList().size() > 0) {
                    for (Offer offer : tender.getOfferList()) {
                        offer.setProvider(null);
                    }
                }
            }
        }
        return tenderList;
    }



    /**
     * Создание нового тендера заказчиком
     * @param tenderFormObject - специфчный объект регистрации тендера
     * @param request
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class} )
    public void createAndSaveNewTender(TenderFormObject tenderFormObject, HttpServletRequest request) throws Exception {



        Session session = this.sessionFactory.getCurrentSession();
        Tender tender = new Tender();
        tender.setTenderName(tenderFormObject.getTenderName());
        tender.setTenderDescription(tenderFormObject.getTenderDescription());
        tender.setDateCreate(new Date());


        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date date = format.parse(tenderFormObject.getDateBegin());
        tender.setDateBegin(date);
        date = format.parse(tenderFormObject.getDateEnd());
        tender.setDateEnd(date);

        tender.setTenderPlaceLatitude(tenderFormObject.getAdress().getLatitude());
        tender.setTenderPlaceLongitude(tenderFormObject.getAdress().getLongitude());

        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getRegion()!= null && !tenderFormObject.getAdress().getRegion().isEmpty()) {
            tender.setRegionCode(tenderFormObject.getAdress().getRegion());
        }

        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getRayon()!= null && !tenderFormObject.getAdress().getRayon().isEmpty()) {
            tender.setRayonCode(tenderFormObject.getAdress().getRayon());
        }

        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getCity()!= null && !tenderFormObject.getAdress().getCity().isEmpty()) {
            tender.setCityCode(tenderFormObject.getAdress().getCity());
        }


        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getStreet()!= null && !tenderFormObject.getAdress().getStreet().isEmpty()) {
            tender.setStreet(tenderFormObject.getAdress().getStreet());
        }

        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getHouseNumber()!= null && !tenderFormObject.getAdress().getHouseNumber().isEmpty()) {
            tender.setHouseNumber(tenderFormObject.getAdress().getHouseNumber());
        }

        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getFullAdress()!= null && !tenderFormObject.getAdress().getFullAdress().isEmpty()) {
            tender.setFullAdress(tenderFormObject.getAdress().getFullAdress());
        }

        User user = (User)request.getSession().getAttribute("user");
        user = (User)session.get(User.class, Integer.valueOf(user.getUser_id()));

        //сохрание ответственного по тендеру
        if(tenderFormObject.isNeedResponsible() && user.isJuridical()){
            ResponsibleInfo responsibleInfo = tenderFormObject.getResponsibleInfo();
            responsibleInfo.setDateCreate(new Date());
            responsibleInfo.setFirstName(responsibleInfo.getFirstName().trim());
            responsibleInfo.setLastName(responsibleInfo.getLastName().trim());
            responsibleInfo.setPhoneNumber(responsibleInfo.getPhoneNumber().trim());
            responsibleInfo.setEmail(responsibleInfo.getEmail().trim());
            tender.setHaveResponsible(true);
            tender.setResponsibleInfo(responsibleInfo);
        }else{
            tender.setHaveResponsible(false);
        }


        tender.setUserOwner(user);
        tender.setIsJuridical(user.isJuridical());

        //нужен ли кредит помечаем
        tender.setNeedCredit(tenderFormObject.isNeedCredit());


        tender.setExpectedPrice(new BigDecimal(tenderFormObject.getExpectedPrice().replaceAll(" ", "")));

        //2 статус - действующий
        TenderStatus tendStat = (TenderStatus)session.get(TenderStatus.class, Integer.valueOf(2));
        tender.setTenderStatus(tendStat);


        TypeService typeService = (TypeService)session.get(TypeService.class, Integer.valueOf(tenderFormObject.getTypeService().getId()));
        tender.setTypeService(typeService);




        List<SelectorValue> selectorValueList =  tenderFormObject.getSelectorValueList();
        for(SelectorValue selectorValue : selectorValueList){
            selectorValue.setTender(tender);
        }
        tender.setSelectorValueList(selectorValueList);



        List<CommonValue> commonValueList= tenderFormObject.getCommonValueList();

        for(CommonValue commonValue : commonValueList){
            commonValue.setTender(tender);
        }
        tender.setCommonValueList(commonValueList);




        List<TenderFile> tenderFiles = tenderFormObject.getTenderFileList();

        for(TenderFile tenderFile : tenderFiles){
            tenderFile.setTender(tender);
            tenderFile.setFileDateCreate(new Date());
        }

        tender.setTenderFileList(tenderFiles);


        List<TenderObject> tenderObjects =  tenderFormObject.getTenderObjects();

        for(TenderObject tenderObject : tenderObjects){
            tenderObject.setTender(tender);
            tenderObject.setDateCreate(new Date());

            TypeObject typeObject = (TypeObject)session.get(TypeObject.class, Integer.valueOf(tenderFormObject.getTypeObject().getId()));
            tenderObject.setTypeObject(typeObject);

            List<ObjectFoto> objectFotos = tenderObject.getObjectFotos();

            for(ObjectFoto objectFoto : objectFotos){
                objectFoto.setTenderObject(tenderObject);
                objectFoto.setDateCreate(new Date());
            }

            List<ObjectParameterValue> ObjectParameterValues= tenderObject.getObjectParameterValues();
            for(ObjectParameterValue objectParameterValue : ObjectParameterValues) {
                objectParameterValue.setTenderObject(tenderObject);
            }

        }
        tender.setTenderObjects(tenderObjects);


        tender.setIsClosed(false);


        log.info(" id сессии: " + request.getSession().getId() + ". Тендер создан, начинаем сохранение");
        session.save(tender);
        session.flush();
        log.info(" id сессии: " + request.getSession().getId() + ". Тендер успешно сохранен.");

        //оповещаем рассылку о поступлении нового тендера
        try {
            log.info(" id сессии: " + request.getSession().getId() + ". Оповещает рассылку о поступлении нового тендера");
            PosterComunicator.posterHanldeRequestNewTender(tender.getId());
        } catch (Exception ex) {
            log.error(" id сессии: " + request.getSession().getId() + ". Ошибка оповещения рассылки", ex);
            ex.printStackTrace();
        }


        //если тендер в кредит то отсылаем письмо самим себе о тендере в кредит
        if(tender.isNeedCredit()){
            try {
                log.info(" id сессии: " + request.getSession().getId() + ". отсылаем письмо самим себе о тендере в кредит");
                emailService.createEmailAboutCredit(tender, user);
            } catch (Exception ex) {
                log.error(" id сессии: " + request.getSession().getId() + ". Ошибка оповещения рассылки", ex);
                ex.printStackTrace();
            }
        }


    }


    /**
     * Редактирование тендера заказчиком
     * @param tenderFormObject - специфчный объект регистрации тендера
     * @param request
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class} )
    public void editExistTender(TenderFormObject tenderFormObject, HttpServletRequest request) throws Exception {
        Session session = sessionFactory.getCurrentSession();

        User user = (User)request.getSession().getAttribute("user");
        user = (User)session.get(User.class, Integer.valueOf(user.getUser_id()));



        if(tenderFormObject.getId() == 0) throw new Exception("Попытка отредактировать не существующий тендер");

        //todo стоит убрать повторяющийся участок кода перевода тендерформы в тендер
        Tender editTender =(Tender) session.get(Tender.class , tenderFormObject.getId());

        if(user.getUser_id() != editTender.getUserOwner().getUser_id() && user.getUser_id() !=0 ){
            throw new Exception("Вы не имеете прав на редактирование данного тендера");
        }


        editTender.setTenderName(tenderFormObject.getTenderName());
        editTender.setTenderDescription(tenderFormObject.getTenderDescription());

        editTender.setDateChange(new Date());

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date date = format.parse(tenderFormObject.getDateBegin());
        editTender.setDateBegin(date);


        date = format.parse(tenderFormObject.getDateEnd());
        editTender.setDateEnd(date);


        editTender.setTenderPlaceLatitude(tenderFormObject.getAdress().getLatitude());
        editTender.setTenderPlaceLongitude(tenderFormObject.getAdress().getLongitude());

        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getRegion()!= null && !tenderFormObject.getAdress().getRegion().isEmpty()) {
            editTender.setRegionCode(tenderFormObject.getAdress().getRegion());
        }

        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getRayon()!= null && !tenderFormObject.getAdress().getRayon().isEmpty()) {
            editTender.setRayonCode(tenderFormObject.getAdress().getRayon());
        }

        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getCity()!= null && !tenderFormObject.getAdress().getCity().isEmpty()) {
            editTender.setCityCode(tenderFormObject.getAdress().getCity());
        }


        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getStreet()!= null && !tenderFormObject.getAdress().getStreet().isEmpty()) {
            editTender.setStreet(tenderFormObject.getAdress().getStreet());
        }


        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getHouseNumber()!= null && !tenderFormObject.getAdress().getHouseNumber().isEmpty()) {
            editTender.setHouseNumber(tenderFormObject.getAdress().getHouseNumber());
        }


        if (tenderFormObject.getAdress()!= null && tenderFormObject.getAdress().getFullAdress()!= null && !tenderFormObject.getAdress().getFullAdress().isEmpty()) {
            editTender.setFullAdress(tenderFormObject.getAdress().getFullAdress());
        }



        //нужен ли кредит помечаем
        boolean needCreditOldValue = editTender.isNeedCredit();
        editTender.setNeedCredit(tenderFormObject.isNeedCredit());
        editTender.setExpectedPrice(new BigDecimal(tenderFormObject.getExpectedPrice().replaceAll(" ", "")));



        for(CommonValue cv : editTender.getCommonValueList()){
            session.delete(cv);
        }


        List<CommonValue> commonValueList = tenderFormObject.getCommonValueList();

        for(CommonValue commonValue : commonValueList){
            commonValue.setTender(editTender);
        }

        editTender.setCommonValueList(commonValueList);

        for(TenderFile tf : editTender.getTenderFileList()){
           tf.setTender(null);
           session.delete(tf);
        }
        editTender.setTenderFileList(null);
        session.flush();


        List<TenderFile> tenderFiles = tenderFormObject.getTenderFileList();
        for(TenderFile tenderFile : tenderFiles){
            tenderFile.setTender(editTender);
            tenderFile.setFileDateCreate(new Date());
        }

        editTender.setTenderFileList(tenderFiles);

        TypeObject typeObject = null;

        for(TenderObject to : editTender.getTenderObjects()){
            typeObject = to.getTypeObject();
            session.delete(to);

        }


        List<TenderObject> tenderObjects =  tenderFormObject.getTenderObjects();
        for(TenderObject tenderObject : tenderObjects){

            tenderObject.setTender(editTender);
            tenderObject.setDateCreate(new Date());

            tenderObject.setTypeObject(typeObject);

            for(ObjectFoto objectFoto : tenderObject.getObjectFotos()){
                objectFoto.setTenderObject(tenderObject);
                objectFoto.setDateCreate(new Date());
            }

            for(ObjectParameterValue objectParameterValue : tenderObject.getObjectParameterValues()){
                objectParameterValue.setTenderObject(tenderObject);
            }


        }

        editTender.setTenderObjects(tenderObjects);
        editTender.setIsClosed(false);

        log.info(" id сессии: " + request.getSession().getId() + ". Тендер тредактирован, начинаем сохранение");
        session.update(editTender);
        session.flush();
        log.info(" id сессии: " + request.getSession().getId() + ". Тендер успешно отредактирован");

        //если тендер не был в кредит но стал в кредит то отсылаем письмо самим себе о тендере в кредит
        if(needCreditOldValue == false && editTender.isNeedCredit()){
            try {
                log.info(" id сессии: " + request.getSession().getId() + ". отсылаем письмо самим себе о тендере в кредит");
                emailService.createEmailAboutCredit(editTender, editTender.getUserOwner());
            } catch (Exception ex) {
                log.error(" id сессии: " + request.getSession().getId() + ". Ошибка оповещения рассылки", ex);
                ex.printStackTrace();
            }
        }


    }










    /**
     * Сервис по отмене тендера
     * @param cancelTenderRequest
     * @param user
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = {Exception.class})
    public void cancelTender(CancelTenderRequest cancelTenderRequest, User user) throws Exception {

        if(cancelTenderRequest.getCancelReasonComment().trim().isEmpty()) throw new Exception("Не передана причина отмены тендера");
        String reason = cancelTenderRequest.getCancelReasonComment().trim();

        if(cancelTenderRequest.getTender_id() == 0) throw new Exception("Не передан целевой отменяемый тендер");
        int tender_id = cancelTenderRequest.getTender_id();

        Tender tender = tenderDAO.cancelTender(user.getUser_id(), tender_id);
        canceledTenderDAO.addNewRow(user, tender, reason);
        log.info(" user_id заказчика: " + user.getUser_id() + ". Тендер id "+tender_id+" успешно отменен.");

    }


    //TODO нужно сравнить методы заверщения тендеров со стороны исполнителя и заказчика. выделить общие хорошие решения и привести к общему виду.
    /**
     * Сервис по отмене тендера исполнителем
     * @param overTenderRequest
     * @param user
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = {Exception.class})
    public void providerOverTender( OverTenderRequest overTenderRequest, User user) throws Exception {
        if(overTenderRequest.getTender_id() == 0)throw new Exception("Не передан целевой тендер");
        int tender_id = overTenderRequest.getTender_id();

        Session session = sessionFactory.getCurrentSession();
        Tender e = (Tender)session.get(Tender.class, Integer.valueOf(tender_id));

        if(e.getTenderStatus().getId() == 6) throw new Exception("Тендер был отозван");

        if(e.getTenderStatus().getId() != 3) throw new Exception("Тендер не выполняется, не исполняемый тендер завершить нельзя");

        if(e.getTenderStatus().getId() != 4 && !e.isProviderCloseTender()) {


            Offer offer = offerDAO.getOfferWinByTenderId(tender_id);
            User provider = offer.getProvider();
            if(provider.getUser_id() != user.getUser_id()) throw new Exception("Вы не являетесь исполнителем по тендеру");

            e.setDateChange(new Date());
            e.setProviderCloseTender(true);
            if(e.isOwnerCloseTender()) {
                e.setTenderStatus((TenderStatus)session.get(TenderStatus.class, Integer.valueOf(4)));
            }
            session.update(e);
        }
    }




}
