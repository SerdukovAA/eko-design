package com.tender.webapp.controllers.service_bean;

/**
 * Created by Serdukov on 02.06.2015.
 */

public class ConsumerFormObject {

    private String lastName;
    private String firstName;



    private String email;
    private String login;
    private String password;
    private String password2;

    private boolean acceptAgreement = false;

    private boolean isJuridical = false;

    private String phoneCheckID;
    private String phoneNumber;
    private String phoneNumberCOD;


    private String recaptchaResponse;


    /*--getters and setters-*/

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public boolean isAcceptAgreement() {
        return acceptAgreement;
    }

    public void setAcceptAgreement(boolean acceptAgreement) {
        this.acceptAgreement = acceptAgreement;
    }

    public boolean isJuridical() {
        return isJuridical;
    }

    public void setIsJuridical(boolean isJuridical) {
        this.isJuridical = isJuridical;
    }

    public String getPhoneCheckID() {
        return phoneCheckID;
    }

    public void setPhoneCheckID(String phoneCheckID) {
        this.phoneCheckID = phoneCheckID;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumberCOD() {
        return phoneNumberCOD;
    }

    public void setPhoneNumberCOD(String phoneNumberCOD) {
        this.phoneNumberCOD = phoneNumberCOD;
    }


    public String getRecaptchaResponse() {
        return recaptchaResponse;
    }

    public void setRecaptchaResponse(String recaptchaResponse) {
        this.recaptchaResponse = recaptchaResponse;
    }
}
