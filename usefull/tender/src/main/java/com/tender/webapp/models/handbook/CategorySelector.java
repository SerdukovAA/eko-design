

package com.tender.webapp.models.handbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(
        name = "category_selectors"
)
public class CategorySelector {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private int id;
    @Column(
            name = "selector_name",
            unique = false,
            nullable = false
    )
    private String selectorName;
    @Column(
            name = "reg_selector_name",
            unique = false,
            nullable = false
    )
    private String regSelectorName;
    @Column(
            name = "parameter_name",
            unique = false,
            nullable = false
    )
    private String parameterName;
    @OneToMany(
            mappedBy = "categorySelector",
            fetch = FetchType.EAGER
    )
    @OrderBy("option_id ASC")
    @Fetch(FetchMode.SUBSELECT)
    private List<SelectorOption> selectorOptions;
    @Column(
            name = "consumer_selector_type",
            unique = false,
            nullable = false
    )
    private String consumerSelectorType;
    @Column(
            name = "provider_selector_type",
            unique = false,
            nullable = false
    )
    private String providerSelectorType;
    @Column(
            name = "selector_comment",
            unique = false,
            nullable = false
    )
    private String selectorComment;
    @Column(
            name = "is_depended",
            unique = false,
            nullable = false
    )
    private boolean depended;
    @OneToMany(
            mappedBy = "parentCategorySelector",
            fetch = FetchType.EAGER
    )
    @OrderBy("id ASC")
    @Fetch(FetchMode.SUBSELECT)
    private List<CategorySelector> dependedSelectors;
    @ManyToOne
    @JoinColumn(
            name = "parent_selector_id"
    )
    @JsonIgnore
    private CategorySelector parentCategorySelector;
    @Column(
            name = "parent_sel_values",
            unique = false
    )
    private String parentSelectorValues;
    @ManyToOne
    @JoinColumn(
            name = "type_serv_id"
    )
    @JsonIgnore
    private TypeService typeService;
    @Transient
    private int checkedOptionId;
    @Transient
    private boolean checked;
    @Transient
    @JsonIgnore
    private boolean invalid;

    public CategorySelector() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSelectorName() {
        return this.selectorName;
    }

    public void setSelectorName(String selectorName) {
        this.selectorName = selectorName;
    }

    public String getRegSelectorName() {
        return this.regSelectorName;
    }

    public void setRegSelectorName(String regSelectorName) {
        this.regSelectorName = regSelectorName;
    }

    public TypeService getTypeService() {
        return this.typeService;
    }

    public void setTypeService(TypeService typeService) {
        this.typeService = typeService;
    }

    public List<SelectorOption> getSelectorOptions() {
        return this.selectorOptions;
    }

    public void setSelectorOptions(List<SelectorOption> selectorOptions) {
        this.selectorOptions = selectorOptions;
    }

    public String getParameterName() {
        return this.parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getConsumerSelectorType() {
        return this.consumerSelectorType;
    }

    public void setConsumerSelectorType(String consumerSelectorType) {
        this.consumerSelectorType = consumerSelectorType;
    }

    public String getProviderSelectorType() {
        return this.providerSelectorType;
    }

    public void setProviderSelectorType(String providerSelectorType) {
        this.providerSelectorType = providerSelectorType;
    }

    public boolean isDepended() {
        return this.depended;
    }

    public void setDepended(boolean depended) {
        this.depended = depended;
    }

    public List<CategorySelector> getDependedSelectors() {
        return this.dependedSelectors;
    }

    public void setDependedSelectors(List<CategorySelector> dependedSelectors) {
        this.dependedSelectors = dependedSelectors;
    }

    public CategorySelector getParentCategorySelector() {
        return this.parentCategorySelector;
    }

    public void setParentCategorySelector(CategorySelector parentCategorySelector) {
        this.parentCategorySelector = parentCategorySelector;
    }

    public String getParentSelectorValues() {
        return this.parentSelectorValues;
    }

    public void setParentSelectorValues(String parentSelectorValues) {
        this.parentSelectorValues = parentSelectorValues;
    }

    public boolean isChecked() {
        return this.checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getCheckedOptionId() {
        return this.checkedOptionId;
    }

    public void setCheckedOptionId(int checkedOptionId) {
        this.checkedOptionId = checkedOptionId;
    }

    public boolean isInvalid() {
        return this.invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    public String getSelectorComment() {
        return this.selectorComment;
    }

    public void setSelectorComment(String selectorComment) {
        this.selectorComment = selectorComment;
    }
}
