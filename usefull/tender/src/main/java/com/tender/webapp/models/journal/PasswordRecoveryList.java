package com.tender.webapp.models.journal;


import com.tender.webapp.models.User;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "password_change_jour")
public class PasswordRecoveryList {




    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //хэш аналог временного пароля
    @Column(name = "check_hash", unique = false, nullable = false)
    private String checkHash;

    //дата когда был запрос на смену пароля
    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;

    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;


    //кому меняем пароль
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;



    /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCheckHash() {
        return checkHash;
    }

    public void setCheckHash(String checkHash) {
        this.checkHash = checkHash;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
