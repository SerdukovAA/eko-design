package com.tender.webapp.biznes.impl;


import com.tender.webapp.biznes.InfoService;
import com.tender.webapp.models.journal.Counter;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.criteria.expression.function.AggregationFunction;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by Serdukov on 24.12.2015.
 */
public class InfoServiceImpl implements InfoService {


   @Resource
   protected SessionFactory sessionFactory;

   @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class, readOnly = true)
   public Counter getCounter(){
     return (Counter) sessionFactory.getCurrentSession().get(Counter.class , 1);
   }

   @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
   public void summCounter(){
      Counter counter =  (Counter) sessionFactory.getCurrentSession().get(Counter.class , 1);
      int randomAuction = 1 + (int)(Math.random()*3);
      int randomTotalSum = 2000 + (int)(Math.random()*14000);
      double randomSavedSumm = randomTotalSum * 0.05 + (int)(Math.random()*0.25);
      counter.setAuctionCount(counter.getAuctionCount() + randomAuction);
      counter.setTotalAmount(counter.getTotalAmount() + randomTotalSum);
      counter.setSavedAmount(counter.getSavedAmount() + (int) randomSavedSumm);
      counter.setDateChange(new Date());
      sessionFactory.getCurrentSession().save(counter);
   }


}
