package com.tender.webapp.config.rest;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CsrfTokenResponseCookieBindingFilter extends OncePerRequestFilter {

	protected static final String REQUEST_ATTRIBUTE_NAME = "_csrf";

	public static final String RESPONSE_COOKIE_NAME = "CSRF-TOKEN";



	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
		throws ServletException, IOException {

		CsrfToken token = (CsrfToken) request.getAttribute(REQUEST_ATTRIBUTE_NAME);

		Cookie cookie = new Cookie(RESPONSE_COOKIE_NAME, token.getToken());
		cookie.setPath("/");

		response.addCookie(cookie);

		filterChain.doFilter(request, response);
	}
}
