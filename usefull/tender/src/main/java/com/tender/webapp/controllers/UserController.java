package com.tender.webapp.controllers;

import com.tender.webapp.biznes.ServiceMessage;
import com.tender.webapp.biznes.ServiceOffer;
import com.tender.webapp.biznes.ServiceTender;
import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.comunicators.ReCaptchaChecker;
import com.tender.webapp.controllers.service_bean.*;
import com.tender.webapp.models.Message;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;
import com.tender.webapp.models.service.*;
import com.tender.webapp.validation.EditFormProviderValidator;
import com.tender.webapp.validation.RegistrationFormProviderValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Serdukov on 20.06.2015.
 */
@Controller
public class UserController {


    @Autowired
    ServiceUser serviceUser;
    @Autowired
    EditFormProviderValidator editFormProviderValidator;



    private static final Logger log = Logger.getLogger(UserController.class);





    /**
     * Запрос на регистрацию нового исполнителя в системе
     * @param providerFormObject
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/provider_edit_info", method = RequestMethod.POST)
    public
    @ResponseBody
    ServerMessage editProviderInfo(@RequestBody ProviderFormObject providerFormObject, HttpServletRequest request) throws Exception{
        String sessionID = request.getSession().getId();
        User user = (User)request.getSession().getAttribute("user");
        ServerMessage serverMessage = new ServerMessage();
        log.info(" id сессии: " + sessionID + ".Регистрация нового исполнителя. Браузер - " + request.getHeader("User-Agent") + ". Компания " + request.getParameter("companyName"));
        editFormProviderValidator.validate(providerFormObject, serverMessage);

        if (serverMessage.isHasErrors()) {
            serverMessage.setStatus(1);
            serverMessage.setText("Переданы некорректные данные при редактировании пользователя.");
            log.info(" id сессии: " + sessionID + ". Переданы некорректные данные при регистрации нового исполнителя");
            for (com.tender.webapp.models.service.Error error : serverMessage.getErrors()) {
                log.info(" id сессии: " + sessionID + " - " + error.getErrorText());
            }
            return serverMessage;
        } else {
            try {
                serviceUser.editProvider(providerFormObject, user);
                serverMessage.setStatus(3000);
                StringBuilder sb = new StringBuilder();
                sb.append("На указанный Вами адрес электронной почты контактного лица, выслана информация")
                        .append("для подтверждения электронного почтового ящика. Вам необходимо подтвердить адрес электронной почты, после")
                        .append("чего Вам будет выдан пароль для входа на сайт");
                serverMessage.setText(sb.toString());
                log.info(" id сессии: " + sessionID + ". Пользователь успешно зарегистрирован.");
                return serverMessage;
            } catch (Exception e) {
                serverMessage.setStatus(3);
                serverMessage.setText("Произошла ошибка при регистрации пользователя на сервере");
                serverMessage.addError(1, "Ошибка сервера: " + e.getMessage());
                log.info(" id сессии: " + sessionID + " - Ошибка сервера: " + e.getMessage());
                return serverMessage;
            }

        }
    }










    /**
     * Обработка ошибок в контроллерах
     * @param exception
     */
    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }




}
