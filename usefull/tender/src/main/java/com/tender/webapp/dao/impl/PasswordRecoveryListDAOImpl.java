package com.tender.webapp.dao.impl;

import com.tender.webapp.dao.PasswordRecoveryListDAO;
import com.tender.webapp.models.User;
import com.tender.webapp.models.journal.PasswordRecoveryList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by serdukov on 4.11.15.
 */
public class PasswordRecoveryListDAOImpl implements PasswordRecoveryListDAO{

    @Resource
    protected SessionFactory sessionFactory;

    /**
     * После инициирования смены пароля создается запись в журнале. Запись будет использована в дальнейших этапах восстановления пароля
     * @param user
     * @param hash
     * @return
     * @throws Exception
     */
    public PasswordRecoveryList save(User user, String hash) throws  Exception {
        Session session = sessionFactory.getCurrentSession();
        PasswordRecoveryList passwordRecoveryList = new PasswordRecoveryList();
        passwordRecoveryList.setUser(user);
        passwordRecoveryList.setCheckHash(hash);
        passwordRecoveryList.setDateCreate(new Date());
        session.save(passwordRecoveryList);
        session.flush();
        return passwordRecoveryList;
    }

    public PasswordRecoveryList getById(int rec_id) throws  Exception {
        Session session = sessionFactory.getCurrentSession();
        PasswordRecoveryList passwordRecoveryList = (PasswordRecoveryList) session.get(PasswordRecoveryList.class, rec_id);
        return passwordRecoveryList;
    }

    public void updateDate(PasswordRecoveryList passwordRecoveryList) throws  Exception{
        Session session = sessionFactory.getCurrentSession();
        passwordRecoveryList.setDateChange(new Date());
        session.update(passwordRecoveryList);
    }


}
