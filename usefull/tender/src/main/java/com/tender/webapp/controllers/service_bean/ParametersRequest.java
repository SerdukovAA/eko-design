package com.tender.webapp.controllers.service_bean;



/** Название так уж повелось. но используется при обмене информации по выбранным селекторам.
 * Created by Serdukov on 03.09.2015.
 */
public class ParametersRequest {

    private int selectorId;
    private int optionId;
    private int typeServiceId;




/*-------getters and setters------------------------*/
    public int getTypeServiceId() {
        return typeServiceId;
    }

    public void setTypeServiceId(int typeServiceId) {
        this.typeServiceId = typeServiceId;
    }

    public int getSelectorId() {
        return selectorId;
    }

    public void setSelectorId(int selectorId) {
        this.selectorId = selectorId;
    }

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }
}
