package com.tender.webapp.controllers.service_bean;

import com.tender.webapp.models.*;
import com.tender.webapp.models.base.City;
import com.tender.webapp.models.base.Region;
import com.tender.webapp.models.handbook.TypeObject;
import com.tender.webapp.models.handbook.TypeService;

import java.util.List;

/**
 * Created by Serdukov on 02.06.2015.
 */

public class TenderFormObject {

    private int id = 0;

    private String tenderName;

    private String tenderDescription;

    private TypeObject typeObject;

    private TypeService typeService;

    private List<CommonValue> commonValueList;

    private List<SelectorValue> selectorValueList;

    private Adress adress;



    //чекбокс который говорит что по тендеру ожидается кредитная поддержка
    //доработка от заказчика за 24.01.2016
    private boolean needCredit;


    private List<TenderFile> tenderFileList;

    private List<TenderObject> tenderObjects;

    private String dateBegin;

    private String dateEnd;

    private String expectedPrice;


    //Доработка. Добавление ответственного по тендеру. Доступно только для юр.лиц
    private ResponsibleInfo responsibleInfo;
    private boolean needResponsible;


    /*--getters and setters-*/

    public String getTenderName() {
        return tenderName;
    }

    public void setTenderName(String tenderName) {
        this.tenderName = tenderName;
    }

    public String getTenderDescription() {
        return tenderDescription;
    }

    public void setTenderDescription(String tenderDescription) {
        this.tenderDescription = tenderDescription;
    }

    public TypeObject getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(TypeObject typeObject) {
        this.typeObject = typeObject;
    }

    public TypeService getTypeService() {
        return typeService;
    }

    public void setTypeService(TypeService typeService) {
        this.typeService = typeService;
    }


    public List<TenderFile> getTenderFileList() {
        return tenderFileList;
    }

    public void setTenderFileList(List<TenderFile> tenderFileList) {
        this.tenderFileList = tenderFileList;
    }

    public List<TenderObject> getTenderObjects() {
        return tenderObjects;
    }

    public void setTenderObjects(List<TenderObject> tenderObjects) {
        this.tenderObjects = tenderObjects;
    }

    public String getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getExpectedPrice() {
        return expectedPrice;
    }

    public void setExpectedPrice(String expectedPrice) {
        this.expectedPrice = expectedPrice;
    }


    public List<SelectorValue> getSelectorValueList() {
        return selectorValueList;
    }

    public void setSelectorValueList(List<SelectorValue> selectorValueList) {
        this.selectorValueList = selectorValueList;
    }


    public List<CommonValue> getCommonValueList() {
        return commonValueList;
    }

    public void setCommonValueList(List<CommonValue> commonValueList) {
        this.commonValueList = commonValueList;
    }


    public boolean isNeedCredit() {
        return needCredit;
    }

    public void setNeedCredit(boolean needCredit) {
        this.needCredit = needCredit;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public ResponsibleInfo getResponsibleInfo() {
        return responsibleInfo;
    }

    public void setResponsibleInfo(ResponsibleInfo responsibleInfo) {
        this.responsibleInfo = responsibleInfo;
    }


    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    public boolean isNeedResponsible() {
        return needResponsible;
    }

    public void setNeedResponsible(boolean needResponsible) {
        this.needResponsible = needResponsible;
    }
}