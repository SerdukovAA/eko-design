package com.tender.webapp.controllers.service_bean;



/**
 * Created by Serdukov on 03.09.2015.
 */

/**
 * Объект для отказа заказчика от исполнителя, после приняти тендера заказчик может отказаться от исполнителя указав причину
 */
public class ChangeProviderRequest {

    private int tender_id = 0;
    private String changeReasonComment;

/*-------getters and setters------------------------*/

    public int getTender_id() {
        return tender_id;
    }

    public void setTender_id(int tender_id) {
        this.tender_id = tender_id;
    }

    public String getChangeReasonComment() {
        return changeReasonComment;
    }

    public void setChangeReasonComment(String changeReasonComment) {
        this.changeReasonComment = changeReasonComment;
    }

}
