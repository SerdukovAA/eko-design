package com.tender.webapp.dao.impl;

import com.tender.webapp.dao.JuridicalInfoDAO;
import com.tender.webapp.models.JuridicalInfo;
import com.tender.webapp.models.PrivateInfo;
import com.tender.webapp.models.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.Resource;

/**
 * Created by serdukov on 01.11.15.
 */
public class JuridicalInfoDAOImpl implements JuridicalInfoDAO{

    @Resource
    protected SessionFactory sessionFactory;

    public User findJuridicalInfoByInn(String inn) throws Exception{
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where juridical_info_id in (select ji.id from JuridicalInfo ji where inn = :inn) and user_status_id in (1,2,5)");
        query.setParameter("inn", inn);
        return (User) query.uniqueResult();
    }

}
