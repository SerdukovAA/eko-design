package com.tender.webapp.controllers;

import com.tender.webapp.biznes.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Serdjukov A.A. 06.01.2015
 */

@Controller
public class InController {

    @Autowired
    ServiceUser serviceUser;



    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String welcomePage() {
        return "redirect:/index.html";
    }

    @RequestMapping(value = {"/rest/secure"}, method = RequestMethod.POST)
    public @ResponseBody Map<String, String> secure() {

        Map<String, String> st = new HashMap<String, String>();
        st.put("key","удача"+new Date());
        return st;
    }




}

