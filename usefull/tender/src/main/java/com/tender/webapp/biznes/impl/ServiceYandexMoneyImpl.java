package com.tender.webapp.biznes.impl;


import com.tender.webapp.biznes.ServiceYandexMoney;
import com.tender.webapp.controllers.service_bean.YandexTransBegin;
import com.tender.webapp.models.TransactionStatus;
import com.tender.webapp.models.User;
import com.tender.webapp.models.journal.RefillTransactionJournal;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Serdukov on 05.06.2015.
 */
public class ServiceYandexMoneyImpl implements ServiceYandexMoney{

    @Resource
    protected SessionFactory sessionFactory;


    private static final Logger log = Logger.getLogger(ServiceYandexMoneyImpl.class);

    @Transactional( propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public RefillTransactionJournal beginTransaction(YandexTransBegin yandexTransBegin, User user) throws Exception{


        BigDecimal orderSumAmount;
        if (yandexTransBegin.getSum().trim().isEmpty()) throw  new Exception("Не передана сумма пополнения");
        orderSumAmount = new BigDecimal(yandexTransBegin.getSum().trim().replaceAll(" ","")+".00");

        System.out.println("Сумма пополениея "+orderSumAmount.toString());

        String paymentType;
        if (yandexTransBegin.getPaymentType().trim().isEmpty()) throw  new Exception("Не передан способ оплаты");
        paymentType = yandexTransBegin.getPaymentType().trim();

        Session session = sessionFactory.getCurrentSession();


        RefillTransactionJournal refillTransactionJournal = new RefillTransactionJournal();
        refillTransactionJournal.setDateCreate(new Date());
        refillTransactionJournal.setOrderSumAmount(orderSumAmount);
        refillTransactionJournal.setOrderSumCurrencyPaycash(643);
        refillTransactionJournal.setTransactionStatus((TransactionStatus) session.get(TransactionStatus.class, 1));
        refillTransactionJournal.setPaymentType(paymentType);
        refillTransactionJournal.setUser(user);

        session.save(refillTransactionJournal);

        log.info(" user_id : " + user.getUser_id() + ".Денежная транзакция -  успешно");
        return  refillTransactionJournal;

    }



}

