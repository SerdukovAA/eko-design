package com.tender.webapp.biznes.impl;

import com.tender.webapp.biznes.EmailService;
import com.tender.webapp.biznes.ServiceOffer;
import com.tender.webapp.comunicators.PosterComunicator;
import com.tender.webapp.controllers.service_bean.AcceptOfferRequest;
import com.tender.webapp.controllers.service_bean.ChangeProviderRequest;
import com.tender.webapp.controllers.service_bean.OfferFormObject;
import com.tender.webapp.dao.OfferDAO;
import com.tender.webapp.dao.TenderDAO;
import com.tender.webapp.mail.MailManager;
import com.tender.webapp.models.*;
import com.tender.webapp.models.handbook.OfferStatus;
import com.tender.webapp.models.handbook.TenderStatus;
import com.tender.webapp.models.handbook.TrafficType;
import com.tender.webapp.models.journal.CashTraffic;
import com.tender.webapp.models.journal.RecallProviderFromTender;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * Created by Serdukov on 05.06.2015.
 */
public class ServiceOfferImpl implements ServiceOffer {

    @Resource
    protected SessionFactory sessionFactory;
    @Autowired
    ExecutorService executorService;

    @Autowired
    EmailService emailService;

    @Autowired
    OfferDAO offerDAO;
    @Autowired
    TenderDAO tenderDAO;

    @Autowired
    JavaMailSender javaMailSender;




    private static final Logger log = Logger.getLogger(ServiceOfferImpl.class);

    /**+
     * Найти победное предложение по тендеру но не передавать информацию о победившем исполнителе
     * @param tender_id
     * @return
     * @throws Exception
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class, readOnly = true)
    public Offer getoOfferWinByTenderId(int tender_id, int user_id) throws Exception{

        //нужно проверить, что тендер активен
        Tender tender = tenderDAO.getActiveTenderById(tender_id);
        if(tender == null) throw new Exception("Активных тендеров не обнаружено");

        //нужно проверить, что тендер пренадлежит пользователю
        if(tender.getUserOwner().getUser_id() != user_id) throw new Exception("Попытка запросить информацию не по своему тендеру");

        Offer offer = offerDAO.getOfferWinByTenderId(tender_id);
        if(offer!=null){
            offer.setProvider(null);
            return offer;
        }else{
            throw new Exception("Для данного тендера (id = "+tender_id+"), победившего предложения не найдено");
        }
    }


    /**
     * Сервис по отмене от ранее выбранного исполнителя
     * @param changeProviderRequest
     * @param user
     * @throws Exception
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public void changeProvider(ChangeProviderRequest changeProviderRequest, User user) throws Exception{


        String reason;
        if (changeProviderRequest.getChangeReasonComment().trim().isEmpty()) throw  new Exception("Не передана причина отказа от исполнителя");
        reason = changeProviderRequest.getChangeReasonComment().trim();

        int tender_id;
        if (changeProviderRequest.getTender_id() == 0)    throw  new Exception("Не передан целевой тендер");
        tender_id = changeProviderRequest.getTender_id();

        Session session = sessionFactory.getCurrentSession();

        Tender tender = (Tender) session.get(Tender.class, tender_id);
        if (tender.getUserOwner().getUser_id() != user.getUser_id())  throw new Exception("Попытка изменений не по своему тендеру");
        if(tender.getTenderStatus().getId() != 3 ) throw new Exception("У тендера не выбран исполнитель ");

        tender.setDateChange(new Date());
        tender.setTenderStatus((TenderStatus) session.get(TenderStatus.class, 2));
        session.update(tender);

        Offer offer = offerDAO.getOfferWinByTenderId(tender_id);
        User provider = offer.getProvider();
        offer.setOfferStatus((OfferStatus) session.get(OfferStatus.class, 1));
        session.update(offer);

      //забираем баланс исполнителя,
        Balance providerBalance = provider.getBalance();

        //нужно понять сколько вернуть денег исполнителю, ведь он более не учавствует в тендере
        Query query2 = session.createQuery("from CashTraffic cf where cf.offer.id = :offer_id and cf.trafficType.id = 1 order by cf.dateChange desc");
        query2.setParameter("offer_id", offer.getId());
        query2.setMaxResults(1);
        CashTraffic cashT = (CashTraffic) query2.uniqueResult();

        //на тему возврата бабла, будет озврат только если было списание. при обнулении баланса просто нужно высавлять другой тип снятия не = 1.
        // Тогда и возврата не будет
        if(cashT != null){
            BigDecimal offerComision =  cashT.getAmount().negate();

            providerBalance.setValue(providerBalance.getValue().add(offerComision));
            session.update(providerBalance);

            //TODO это все еще под вопросом как правильно возвращать бабло.
            // Вприницпе если движения проходят как нужно то все норм. Но когда попросили списать счет клиентаиз минуса, то получилось что исполнителю прибавилась сумма.
            //добавить в журнал запись о возврате бабла
            CashTraffic cashTraffic = new CashTraffic();
            cashTraffic.setBalance(providerBalance);
            cashTraffic.setOffer(offer);
            cashTraffic.setDateChange(new Date());
            cashTraffic.setCurrentValue(providerBalance.getValue());
            cashTraffic.setTrafficType((TrafficType) session.get(TrafficType.class, 2));
            cashTraffic.setAmount(offerComision);

            session.save(cashTraffic);
        }


        //фиксируем смену исполнителя, потом будет сказываться на рейтинге
        RecallProviderFromTender recallProviderFromTender = new RecallProviderFromTender();
        recallProviderFromTender.setReason(reason);
        recallProviderFromTender.setTender(tender);
        recallProviderFromTender.setProvider(provider);
        recallProviderFromTender.setUser(user);
        recallProviderFromTender.setDateCanceled(new Date());

        session.save(recallProviderFromTender);

        log.info(" user_id заказчика : " + user.getUser_id() + ".Заказчки успешно снял исполнителя с выполнения тендера.");

    }









    /**
     * Сервис по загрузке нового оффера
     * @param offerFormObject
     * @param user
     * @throws Exception
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public void createAndSaveNewOffer(OfferFormObject offerFormObject, User user) throws Exception {

       Session session = sessionFactory.getCurrentSession();
       Offer offer = new Offer();
       log.info(" user_id : " + user.getUser_id() + ". Создает новый оффер");

       offer.setDateCreate(new Date());
       offer.setOfferStatus((OfferStatus) session.get(OfferStatus.class, 1));
       offer.setTender((Tender) session.get(Tender.class, offerFormObject.getTender_id()));
       offer.setProvider(user);

       offer.setOfferDescription(offerFormObject.getOfferDescription());



        offer.setOfferPlaceLatitude(offerFormObject.getAdress().getLatitude());
        offer.setOfferPlaceLongitude(offerFormObject.getAdress().getLongitude());

        if (offerFormObject.getAdress()!= null && offerFormObject.getAdress().getRegion()!= null && !offerFormObject.getAdress().getRegion().isEmpty()) {
            offer.setRegionCode(offerFormObject.getAdress().getRegion());
        }

        if (offerFormObject.getAdress()!= null && offerFormObject.getAdress().getRayon()!= null && !offerFormObject.getAdress().getRayon().isEmpty()) {
            offer.setRayonCode(offerFormObject.getAdress().getRayon());
        }

        if (offerFormObject.getAdress()!= null && offerFormObject.getAdress().getCity()!= null && !offerFormObject.getAdress().getCity().isEmpty()) {
            offer.setCityCode(offerFormObject.getAdress().getCity());
        }


        if (offerFormObject.getAdress()!= null && offerFormObject.getAdress().getStreet()!= null && !offerFormObject.getAdress().getStreet().isEmpty()) {
            offer.setStreet(offerFormObject.getAdress().getStreet());
        }

        if (offerFormObject.getAdress()!= null && offerFormObject.getAdress().getHouseNumber()!= null && !offerFormObject.getAdress().getHouseNumber().isEmpty()) {
            offer.setHouseNumber(offerFormObject.getAdress().getHouseNumber());
        }

        if (offerFormObject.getAdress()!= null && offerFormObject.getAdress().getFullAdress()!= null && !offerFormObject.getAdress().getFullAdress().isEmpty()) {
            offer.setFullAdress(offerFormObject.getAdress().getFullAdress());
        }



       offer.setPrice(new BigDecimal(offerFormObject.getPrice().replaceAll(" ", "")));

       DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
       Date date;
       date = format.parse(offerFormObject.getDateBegin().trim());
       offer.setDateBegin(date);

       date = format.parse(offerFormObject.getDateEnd().trim());
       offer.setDateEnd(date);


       log.info(" user_id : " + user.getUser_id() + ". Оффер создан, начинаем сохранение");
       session.save(offer);

       log.info(" user_id : "+user.getUser_id()+". Оффер успешно сохранен.");
            try {
                log.info("Оповещаем рассылку о поступлении нового оффера");
                PosterComunicator.posterHanldeRequestNewOffer(offer.getId());
            }catch (Exception ex){
                log.error("Ошибка оповещения рассылки", ex);
                ex.printStackTrace();
            }
    }

    /**
     * Сервис принятия заказчиком предложения по тендеру
     * @param acceptOfferRequest
     * @param user
     * @throws Exception
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public void acceptOffer(AcceptOfferRequest acceptOfferRequest, User user) throws Exception {

            if(acceptOfferRequest.getTender_id() == 0)  throw  new Exception("Не передан целевой тендер");
            if(acceptOfferRequest.getOffer_id() == 0)  throw  new Exception("Не передан целевой оффер");
            int tender_id = acceptOfferRequest.getTender_id();
            int offer_id = acceptOfferRequest.getOffer_id();

            Session session = sessionFactory.getCurrentSession();

            Tender tender = (Tender) session.get(Tender.class, tender_id);
            if(tender.getTenderStatus().getId()==3) throw new Exception("Тендеру уже выбран исполнитель");
            if(tender.getUserOwner().getUser_id()!=user.getUser_id()) throw new Exception("У заказчика нет прав на принятие оффера по тендеру тендер");

            Offer offer = (Offer) session.get(Offer.class, offer_id);
            if(tender_id != offer.getTender().getId())  throw new Exception("Оффер и тендер не связаны");


            tender.setTenderStatus((TenderStatus) session.get(TenderStatus.class, 3));
            offer.setOfferStatus((OfferStatus) session.get(OfferStatus.class, 2));

            session.update(tender);
            session.update(offer);


            //уведомляем исполнителя о выйгрыше
            emailService.sendAccepOfferMailToOfferProvider(tender,offer);



            //Вычисляем сколько денег он нам должен
            BigDecimal offerPrice = offer.getPrice();
            BigDecimal offerComision =  offerPrice.multiply(new BigDecimal(5)).divide(new BigDecimal(100));


            //забираем баланс исполнителя, списываем коммисию и обновляем
            Balance providerBalance = offer.getProvider().getBalance();
            providerBalance.setValue(providerBalance.getValue().subtract(offerComision));
            session.update(providerBalance);


            //добавить в журнал запись
            CashTraffic cashTraffic = new CashTraffic();
            cashTraffic.setBalance(providerBalance);
            cashTraffic.setOffer(offer);
            cashTraffic.setDateChange(new Date());
            cashTraffic.setCurrentValue(providerBalance.getValue());
            cashTraffic.setTrafficType((TrafficType) session.get(TrafficType.class, 1));
            cashTraffic.setAmount(offerComision.negate());

            session.save(cashTraffic);
    }





}


