package com.tender.webapp.controllers;


import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.comunicators.ReCaptchaChecker;
import com.tender.webapp.config.ParamsProvider;
import com.tender.webapp.controllers.service_bean.ConsumerFormObject;
import com.tender.webapp.controllers.service_bean.ProviderFormObject;
import com.tender.webapp.models.service.Error;
import com.tender.webapp.models.service.ServerMessage;
import com.tender.webapp.validation.RegistrationFormConsumerValidator;
import com.tender.webapp.validation.RegistrationFormProviderValidator;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * Created by Serdukov on 03.06.2015.
 */
@Controller
public class RegistrationController {

    @Autowired
    ServiceUser serviceUser;
    @Autowired
    RegistrationFormConsumerValidator registrationFormConsumerValidator;
    @Autowired
    RegistrationFormProviderValidator registrationFormProviderValidator;
    @Resource
    protected SessionFactory sessionFactory;


    private static boolean isDevelop = ParamsProvider.isDevelop;
    private static final Logger log = Logger.getLogger(RegistrationController.class);


    /**
     * Получение страницы регистрации
     * @return
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationView() {
        return "registration";
    }


    /**
     * Запрос на регистрацию нового заказчика в системе
     * @param consumerFormObject
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/registration_consumer", method = RequestMethod.POST)
    public
    @ResponseBody
    ServerMessage doRegistrationConsumer(@RequestBody ConsumerFormObject consumerFormObject, HttpServletRequest request) throws Exception {
        String sessionID = request.getSession().getId();
        ServerMessage serverMessage = new ServerMessage();
        log.info(" id сессии: " + sessionID + ".Регистрация нового заказчика.");
        registrationFormConsumerValidator.validate(consumerFormObject, serverMessage);

        if (serverMessage.isHasErrors()) {
            serverMessage.setStatus(1);
            serverMessage.setText("Переданы некорректные данные при регистрации пользователя.");
            log.info("Переданы некорректные данные при регистрации нового заказчика. " + sessionID);
            for (Error error : serverMessage.getErrors()) {
                log.info(" id сессии: " + sessionID + " - " + error.getErrorText());
            }
            return serverMessage;
        } else {
            try {
                serviceUser.createAndSaveConsumer(consumerFormObject);
                serverMessage.setStatus(3000);
                serverMessage.setText("Теперь Вы можете пройти в свой <a href='/user_page'>личный кабинет</a> и открыть свой первый тендер");
                log.info(" id сессии: " + sessionID + ". Пользователь успешно зарегистрирован.");
                return serverMessage;
            } catch (Exception e) {
                serverMessage.setStatus(3);
                serverMessage.setText("Произошла ошибка при регистрации пользователя на сервере");
                serverMessage.addError(1, "Ошибка сервера: " + e.getMessage());
                log.info(" id сессии: " + sessionID + " - Ошибка сервера: " + e.getMessage());
                return serverMessage;
            }
        }

    }

    /**
     * Запрос на регистрацию нового исполнителя в системе
     * @param providerFormObject
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/registration_provider", method = RequestMethod.POST)
    public
    @ResponseBody
    ServerMessage doRegistrationProvider(@RequestBody ProviderFormObject providerFormObject, HttpServletRequest request) throws Exception{
        String sessionID = request.getSession().getId();
        ServerMessage serverMessage = new ServerMessage();
        log.info(" id сессии: " + sessionID + ".Регистрация нового исполнителя. Браузер - " + request.getHeader("User-Agent") + ". Компания " + request.getParameter("companyName"));
        registrationFormProviderValidator.validate(providerFormObject, serverMessage);

        if (!providerFormObject.getRecaptchaResponse().equals("secretKeyForTesting") && !ReCaptchaChecker.isResponseValid(request.getRemoteAddr(), providerFormObject.getRecaptchaResponse())){
            serverMessage.addError(33, "Проверка капчи не пройдена");
        }

        if (serverMessage.isHasErrors()) {
            serverMessage.setStatus(1);
            serverMessage.setText("Переданы некорректные данные при регистрации пользователя.");
            log.info(" id сессии: " + sessionID + ". Переданы некорректные данные при регистрации нового исполнителя");
            for (Error error : serverMessage.getErrors()) {
                log.info(" id сессии: " + sessionID + " - " + error.getErrorText());
            }
            return serverMessage;
        } else {
            try {
                serviceUser.createAndSaveProvider(providerFormObject);
                serverMessage.setStatus(3000);
                StringBuilder sb = new StringBuilder();
                sb.append("На указанный Вами адрес электронной почты контактного лица, выслана информация")
                        .append("для подтверждения электронного почтового ящика. Вам необходимо подтвердить адрес электронной почты, после")
                        .append("чего Вам будет выдан пароль для входа на сайт");
                serverMessage.setText(sb.toString());
                log.info(" id сессии: " + sessionID + ". Пользователь успешно зарегистрирован.");
                return serverMessage;
            } catch (Exception e) {
                serverMessage.setStatus(3);
                serverMessage.setText("Произошла ошибка при регистрации пользователя на сервере");
                serverMessage.addError(1, "Ошибка сервера: " + e.getMessage());
                log.info(" id сессии: " + sessionID + " - Ошибка сервера: " + e.getMessage());
                return serverMessage;
            }

        }
    }




    @RequestMapping(value = "/check_phone", method = RequestMethod.GET)
    public @ResponseBody ServerMessage checkCodeConsumer(HttpServletRequest request) throws Exception {
        ServerMessage serverMessage = new ServerMessage();

        String sessionID = request.getSession().getId();
        String phoneNumber;
        if (request.getParameter("phoneNumber").trim().isEmpty())
            serverMessage.addError(1, "Не передан номер телефона для получения проверочного кода");

        if (request.getParameter("captchaResponse").trim().isEmpty())
            serverMessage.addError(2, "Не подверждена капча");

        if (!ReCaptchaChecker.isResponseValid(request.getRemoteAddr(),request.getParameter("captchaResponse").trim()))
            serverMessage.addError(3, "Проверка капчи не пройдена, видимо вы робот :(");

        //todo проверь цену сообщения

        if (serverMessage.isHasErrors()) {
            serverMessage.setStatus(1);
            serverMessage.setText("Переданы некорректные при получении проверочного кода");
            log.info(" id сессии: " + sessionID + ". Переданы некорректные данные при регистрации нового исполнителя");
            for (Error error : serverMessage.getErrors()) {
                log.info(" id сессии: " + sessionID + " - " + error.getErrorText());
            }
            return serverMessage;
        }

        phoneNumber = request.getParameter("phoneNumber").trim();
        System.out.println("/check_phone  " + phoneNumber);
        log.info(" id сессии: " + sessionID + ". Получение проверочного кода для телефона: " + phoneNumber);

        ///подготовка номера к необходимому формату
        phoneNumber = "+7" + phoneNumber.replace("(", "").replace(")", "").replaceAll("-", "").trim().replaceAll("\\s", "");

        serverMessage.setStatus(3000);
        serverMessage.setPhoneCheckID("" + serviceUser.getCreateAndCheckPhoneID(phoneNumber));
        return serverMessage;

    }




    /**
     * Перехват ошибок возникающих в контроллере
     * @param exception
     */
   @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }


}









