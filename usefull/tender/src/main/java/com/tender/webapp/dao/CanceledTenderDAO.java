package com.tender.webapp.dao;

import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;

/**
 * Created by serdukov on 01.11.15.
 */
public interface CanceledTenderDAO {

    public void addNewRow(User user, Tender tender, String reason)throws Exception;


}
