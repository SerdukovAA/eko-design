package com.tender.webapp.controllers;

import com.tender.webapp.biznes.ServiceMessage;
import com.tender.webapp.biznes.ServiceOffer;
import com.tender.webapp.biznes.ServiceTender;
import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.controllers.service_bean.CancelTenderRequest;
import com.tender.webapp.controllers.service_bean.ChangeProviderRequest;
import com.tender.webapp.controllers.service_bean.NewCommentRequest;
import com.tender.webapp.controllers.service_bean.OverTenderRequest;
import com.tender.webapp.models.Message;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;
import com.tender.webapp.models.service.ServerMessage;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Serdukov on 20.06.2015.
 */
@Controller
public class TenderController {

    @Autowired
    ServiceTender serviceTender;
    @Autowired
    ServiceUser serviceUser;
    @Autowired
    ServiceMessage serviceMessage;

    @Autowired
    ServiceOffer serviceOffer;



    private static final Logger log = Logger.getLogger(TenderController.class);


    /**
     * Контроллер запроса на тендер
     * @param request
     * @return
     */
    @RequestMapping(value = "/info_get_tender", method = RequestMethod.GET)
    public @ResponseBody Tender showTenderById(HttpServletRequest request) {
        int tender_id = Integer.parseInt(request.getParameter("tender_id"));
        User user = (User) request.getSession().getAttribute("user");
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос тендера, user_id " + (user != null ? user.getUser_id() : "гостя") + ", tender_id " + tender_id);
        System.out.println(" id сессии: " + request.getSession().getId() + ". Запрос тендера, user_id " + (user!=null?user.getUser_id():"гостя") + ", tender_id " + tender_id);
        try{
            return  serviceTender.getTenderByTenderId(user, tender_id);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Произошла ошибка при запросе юзером тендера. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", e);
            return null;
        }
    }




    /**
     * Контроллер добавления комментария к тендеру
     * @param newCommentRequest
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/add_comment", method = RequestMethod.POST)
    public @ResponseBody Message addConsumerComment(@RequestBody NewCommentRequest newCommentRequest ,HttpServletRequest request ) throws Exception{

        if (newCommentRequest.getText().isEmpty()) {
            throw new Exception("Переданно пустое текстовое сообщение.");
        }
        try {
            return serviceMessage.createAndSaveNewMessage(newCommentRequest, request);
        } catch(Exception e){
            log.info("Произошла ошибка при сохранении тендера на сервере. id сессии: " + request.getSession().getId() + " - Ошибка сервера: " + e.getMessage());
            throw new Exception("Произошла ошибка при сохранении тендера на сервере. id сессии: "+request.getSession().getId()+" - Ошибка сервера: " + e.getMessage());
        }
    }

    /**
     * Контроллер завершения тендера заказчиком.  Тем самым заказчик подверждает что исполнитель выполнил услугу по тендеру
     * @param overTenderRequest
     * @param request
     * @return
     */
    @RequestMapping(value = "/consumer_over_tender", method = RequestMethod.POST)
    public @ResponseBody ServerMessage consOverTender(@RequestBody OverTenderRequest overTenderRequest ,HttpServletRequest request) {
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на завершение тендера от заказчика");
        User user = (User)request.getSession().getAttribute("user");
        ServerMessage serverMessage = new ServerMessage();
        try {
            serviceTender.consumerOverTender(overTenderRequest,user);
            serverMessage.setStatus(3000);
            serverMessage.setText("Тендер успешно завершен");
            log.info(" id сессии: " + request.getSession().getId() + ". Тендер успешно завершен");
            return serverMessage;
        } catch (Exception e) {
            serverMessage.setStatus(3);
            serverMessage.setText("Произошла ошибка при завершении тендера на сервере");
            serverMessage.addError(1, "Ошибка сервера: " + e.getMessage());
            log.info("Произошла ошибка при завершении тендера на сервере. id сессии: " + request.getSession().getId() + " - Ошибка сервера: " + e.getMessage());
            return serverMessage;
        }

    }


    /**
     * Контроллер отказа заказчика от ранее выбранного исполнителя
     * @param changeProviderRequest
     * @param request
     * @return
     */
    @RequestMapping(value = "/consumer_change_provider", method = RequestMethod.POST)
    public @ResponseBody ServerMessage changeProvider (@RequestBody ChangeProviderRequest changeProviderRequest, HttpServletRequest request) {
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на отказ от исполнителя");
        User user = (User)request.getSession().getAttribute("user");
        ServerMessage serverMessage = new ServerMessage();
        try {
            serviceOffer.changeProvider(changeProviderRequest, user);
            serverMessage.setStatus(3000);
            serverMessage.setText("Отказ от исполнителя - успешно");
            log.info(" id сессии: "+request.getSession().getId()+". Отказ от исполнителя - успешно");
            return serverMessage;
        } catch (Exception e) {
            serverMessage.setStatus(3);
            serverMessage.setText("Произошла ошибка при отказе от исполнителя");
            serverMessage.addError(1,"Ошибка сервера: " + e.getMessage());
            log.info("Произошла ошибка при отказе от исполнителя. id сессии: "+request.getSession().getId()+" - Ошибка сервера: " + e.getMessage());
            return serverMessage;
        }

    }

    /**Контроллер отмены тендера по id
     * @param request - запрос от веб-клиента
     * @return - возвращает объект ответа сервера
     */
    @RequestMapping(value = "/consumer_cancel_tender", method = RequestMethod.POST)
    public @ResponseBody  ServerMessage cancelTenderById(@RequestBody CancelTenderRequest cancelTenderRequest, HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на отмену проведения тендера");
        ServerMessage serverMessage = new ServerMessage();
        try {
            serviceTender.cancelTender(cancelTenderRequest, user);
            serverMessage.setStatus(3000);
            serverMessage.setText("Тендер успешно отменен.");
            log.info(" id сессии: "+request.getSession().getId()+". Тендер успешно отменен.");
            return serverMessage;
        } catch (Exception e) {
            serverMessage.setStatus(3);
            serverMessage.setText("Произошла ошибка при отмене тендера на сервере");
            serverMessage.addError(1,"Ошибка сервера: " + e.getMessage());
            log.error("Произошла ошибка при отмене тендера на сервере. id сессии: " + request.getSession().getId() + " - Ошибка сервера: ", e);
            return serverMessage;
        }

    }

    /**
     * Контроллер завершения тендера исполнителем. Для оконательного перевода тендера в статуз завершен/выполнен нужно,
     * чтобы и заказчик и исполнитель подтвердили завршение тендера
     * @param request
     * @return
     */
    @RequestMapping(value = "/provider_over_tender", method = RequestMethod.POST)
    public @ResponseBody ServerMessage provOverTender(@RequestBody OverTenderRequest overTenderRequest , HttpServletRequest request) {
        User user = (User)request.getSession().getAttribute("user");
        log.info(" id сессии: " + request.getSession().getId() + ". Запрос на завершение тендера от исполнителя");
        ServerMessage serverMessage = new ServerMessage();
        try {
            serviceTender.providerOverTender(overTenderRequest, user);
            serverMessage.setStatus(3000);
            serverMessage.setText("Тендер успешно завершен");
            log.info(" id сессии: "+request.getSession().getId()+". Тендер успешно завершен");
            return serverMessage;
        } catch (Exception e) {
            serverMessage.setStatus(3);
            serverMessage.setText("Произошла ошибка при завершении тендера на сервере");
            serverMessage.addError(1,"Ошибка сервера: " + e.getMessage());
            log.info("Произошла ошибка при завершении тендера на сервере. id сессии: " + request.getSession().getId() + " - Ошибка сервера: " + e.getMessage());
            return serverMessage;
        }

    }




    /**
     * Обработка ошибок в контроллерах
     * @param exception
     */
    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }




}
