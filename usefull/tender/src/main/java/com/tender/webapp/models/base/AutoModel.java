package com.tender.webapp.models.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.base.AutoBrand;

import javax.persistence.*;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "auto_models")
public class AutoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "model_ident", unique = false, nullable = false)
    private String modelIdent;


    @ManyToOne
    @JoinColumn(name = "brand_id")
    @JsonIgnore
    private AutoBrand autoBrand;



        /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModelIdent() {
        return modelIdent;
    }

    public void setModelIdent(String modelIdent) {
        this.modelIdent = modelIdent;
    }
    public AutoBrand getAutoBrand() {
        return autoBrand;
    }

    public void setAutoBrand(AutoBrand autoBrand) {
        this.autoBrand = autoBrand;
    }

}
