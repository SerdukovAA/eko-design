package com.tender.webapp.controllers.service_bean;



/**
 * Created by Serdukov on 03.09.2015.
 */

/**
 * Объект для смены пароля пользователя
 */
public class ChangePassRequest {


    private String oldPassword;
    private String newPassword;
    private String newPassword2;


/*-------getters and setters------------------------*/


    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword2() {
        return newPassword2;
    }

    public void setNewPassword2(String newPassword2) {
        this.newPassword2 = newPassword2;
    }

}
