package com.tender.webapp.models.journal;

import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Serdukov on 20.08.2015.
 */

@Entity
@Table(name = "check_phone_journal")
public class PhoneCheckJournal {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(name = "user_phone", unique = false, nullable = false)
    private String userPhone;

    @Column(name = "check_code", unique = false, nullable = false)
    private String checkCode;

    @Column(name = "is_check", nullable = true)
    private boolean isCheck;


    @Column(name = "date_request", unique = false, nullable = true)
    private Date dateRequest;


    ///---setters and getters----------------------------------------


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setIsCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }

    public Date getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(Date dateRequest) {
        this.dateRequest = dateRequest;
    }


}
