package com.tender.webapp.controllers.service_bean;



/**
 * Created by Serdukov on 03.09.2015.
 */

/**
 * Объект для принятия оффера заказчиком.
 */
public class AcceptOfferRequest {


    private int tender_id = 0;
    private int offer_id = 0;



/*-------getters and setters------------------------*/



    public int getTender_id() {
        return tender_id;
    }

    public void setTender_id(int tender_id) {
        this.tender_id = tender_id;
    }

    public int getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(int offer_id) {
        this.offer_id = offer_id;
    }

}
