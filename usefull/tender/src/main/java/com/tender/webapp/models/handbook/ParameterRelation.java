package com.tender.webapp.models.handbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.ObjectParameter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * Класс определят какому селектору какие оции доступны. Относиться к тендерам
 * Created by Serdukov on 11.06.2015.
 */

@Entity
@Table(name = "parameter_relation" ,uniqueConstraints= @UniqueConstraint(columnNames={"selector_id","option_id","parameter_id"}))
public class ParameterRelation {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "selector_id", unique = false, nullable = true)
    private int selectorId;

    @Column(name = "option_id", unique = false, nullable = true)
    private int optionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parameter_id")
    @OrderBy(value = "id ASC")
    private ObjectParameter objectParameter;


    @Column(name = "is_required", unique = false, nullable = true)
    private boolean required;



    /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSelectorId() {
        return selectorId;
    }

    public void setSelectorId(int selectorId) {
        this.selectorId = selectorId;
    }

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }

    public ObjectParameter getObjectParameter() {
        return objectParameter;
    }

    public void setObjectParameter(ObjectParameter objectParameter) {
        this.objectParameter = objectParameter;
    }



    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }


}
