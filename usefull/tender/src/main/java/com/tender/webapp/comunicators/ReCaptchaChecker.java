package com.tender.webapp.comunicators;

import com.tender.webapp.config.ParamsProvider;
import org.apache.log4j.Logger;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Created by serdukov on 01.10.15.
 */
public class ReCaptchaChecker {


    private static final Logger log = Logger.getLogger(ReCaptchaChecker.class);

    private static final String secretKey = ParamsProvider.secretKey;
    private static final String RECAPTCHA_VERIF_URL = "https://www.google.com/recaptcha/api/siteverify";


    public static boolean isResponseValid(String remoteIp, String response) throws Exception {
        RecaptchaResponse recaptchaResponse;
        RestTemplate restTemplate = new RestTemplate();
        try {
            recaptchaResponse = restTemplate.postForObject(RECAPTCHA_VERIF_URL, createBody(secretKey , remoteIp, response), RecaptchaResponse.class);
        } catch (RestClientException e) {
            throw new Exception("Recaptcha API not available due to exception", e);
        }
        return recaptchaResponse.getSuccess();
    }

    private static MultiValueMap<String, String> createBody(String secret, String remoteIp, String response) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("secret", secret);
        map.add("response", response);
        map.add("remoteip", remoteIp);
        return map;
    }

}
