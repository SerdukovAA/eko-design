package com.tender.webapp.models.base;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "region")
@Deprecated
public class Region {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "codec2")
    private String code;

    @Column(name = "namec40")
    private String regionName;


    @Column(name = "socrc10")
    private String socrName;


    @OneToMany(mappedBy = "region", fetch = FetchType.EAGER)
    @OrderBy(value = "namec40 ASC")
    private List<City> cities;
     /*--getters and setters-*/


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getSocrName() {
        return socrName;
    }

    public void setSocrName(String socrName) {
        this.socrName = socrName;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }


}
