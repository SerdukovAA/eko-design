package com.tender.webapp.dao.impl;

import com.tender.webapp.dao.CanceledTenderDAO;
import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;
import com.tender.webapp.models.journal.CanceledTender;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by serdukov on 01.11.15.
 */
public class CanceledTenderDAOImpl implements CanceledTenderDAO{

    @Resource
    protected SessionFactory sessionFactory;


    //для создания новой записи необходимо знать кто отменяет , какой тендер б причину отмены
    public void addNewRow(User user, Tender tender, String reason)throws Exception{
        Session session = sessionFactory.getCurrentSession();
        CanceledTender canceledTender = new CanceledTender();
        canceledTender.setReason(reason);
        canceledTender.setTender(tender);
        canceledTender.setUser(user);
        canceledTender.setDateCanceled(new Date());
        session.save(canceledTender);
    }


}
