package com.tender.webapp.models.handbook;

import javax.persistence.*;

/**
 * Created by Serdukov on 14.06.2015.
 */
@Entity
@Table(name = "offer_status")
public class OfferStatus {



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "offer_status", unique = true, nullable = false)
    private String offer_status;


    @Column(name = "status_name", unique = true, nullable = true)
    private String statusName;


    /*--------getters and setters-----*/


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOffer_status() {
        return offer_status;
    }

    public void setOffer_status(String offer_status) {
        this.offer_status = offer_status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
