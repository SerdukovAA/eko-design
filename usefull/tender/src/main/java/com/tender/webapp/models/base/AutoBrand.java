package com.tender.webapp.models.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.SelectorOption;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Serdukov on 11.06.2015.
 */
@Entity
@Table(name = "auto_brands")
public class AutoBrand {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "brand_name", unique = true, nullable = false)
    private String brandName;


    @OneToMany(mappedBy = "autoBrand", fetch = FetchType.EAGER)
    @OrderBy(value = "model_ident ASC")
    private List<AutoModel> autoModel;

     /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }


    public List<AutoModel> getAutoModel() {
        return autoModel;
    }

    public void setAutoModel(List<AutoModel> autoModel) {
        this.autoModel = autoModel;
    }
}
