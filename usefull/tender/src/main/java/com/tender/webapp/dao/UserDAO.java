package com.tender.webapp.dao;

import com.tender.webapp.models.Tender;
import com.tender.webapp.models.User;
import com.tender.webapp.models.handbook.UserParameter;

/**
 * Created by serdukov on 01.11.15.
 */
public interface UserDAO {

    public void changePassword(int user_id, String newPassword)throws Exception;
    public void deleteBySelf(int user_id)throws Exception;
    public void updateUser(User user)throws Exception;
    public User getUserById(int user_id)throws Exception;
    public User getUserByLogin(String login)throws Exception;
    public User findUserByEmail(String email)throws Exception;
    public User findUserByLogin(String login)throws Exception;


}
