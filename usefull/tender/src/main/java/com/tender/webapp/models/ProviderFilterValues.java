package com.tender.webapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tender.webapp.models.handbook.TypeService;

import javax.persistence.*;

/**
 * Created by root on 13.09.15.
 */

@Entity
@Table(name = "provider_filter_values")
public class ProviderFilterValues {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "selector_id", unique = false, nullable = false)
    private int selectorId;


    @Column(name = "type_service_id", unique = false, nullable = false)
    private int typeServiceId;

    @Column(name = "selector_value_id", unique = false, nullable = true)
    private int valueId;

    @ManyToOne
    @JoinColumn(name = "jur_info_id")
    @JsonIgnore
    private JuridicalInfo juridicalInfo;

/////--------------getters and setters------------------------------///////////////////

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSelectorId() {
        return selectorId;
    }

    public void setSelectorId(int selectorId) {
        this.selectorId = selectorId;
    }



    public int getTypeServiceId() {
        return typeServiceId;
    }

    public void setTypeServiceId(int typeServiceId) {
        this.typeServiceId = typeServiceId;
    }

    public int getValueId() {
        return valueId;
    }

    public void setValueId(int valueId) {
        this.valueId = valueId;
    }

    public JuridicalInfo getJuridicalInfo() {
        return juridicalInfo;
    }

    public void setJuridicalInfo(JuridicalInfo juridicalInfo) {
        this.juridicalInfo = juridicalInfo;
    }


}