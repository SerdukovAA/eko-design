package com.tender.webapp.models;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Отражает текущий баланс пользователя сервиса.
 *
 */
@Entity
@Table(name="balance")
public class Balance {

    public Balance(){

    }

    public Balance(BigDecimal val){
        this.value = val;
        this.dateCreate = new Date();
    }


    @Id
    @Column(name="balance_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "value", unique = false, nullable = false)
    private BigDecimal value;

    @Column(name = "date_create", unique = false, nullable = false)
    private Date dateCreate;

    @Column(name = "date_change", unique = false, nullable = true)
    private Date dateChange;


    /*--getters and setters-*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateChange() {
        return dateChange;
    }

    public void setDateChange(Date dateChange) {
        this.dateChange = dateChange;
    }



}