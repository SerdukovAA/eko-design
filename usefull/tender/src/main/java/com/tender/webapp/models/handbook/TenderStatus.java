package com.tender.webapp.models.handbook;

import javax.persistence.*;

/**
 * Created by Serdukov on 14.06.2015.
 */
@Entity
@Table(name = "tender_status")
public class TenderStatus {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    @Column(name = "tender_status", unique = true, nullable = false)
    private String tenderStatus;


    @Column(name = "status_name", unique = true, nullable = false)
    private String  statusName;



    /*--------getters and setters-----*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getTenderStatus() {
        return tenderStatus;
    }

    public void setTenderStatus(String tenderStatus) {
        this.tenderStatus = tenderStatus;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
