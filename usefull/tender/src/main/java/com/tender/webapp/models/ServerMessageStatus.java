package com.tender.webapp.models;

/**
 * Created by serdukov on 01.11.15.
 */
public enum ServerMessageStatus {

    SUCCESS_MESSAGE(3000,"Удачный результат"),
    ERROR_MESSAGE(3,"Ошибка при выполнении");

    private int id;
    private String status;


     ServerMessageStatus(int id, String status){
        this.id = id;
        this.status = status;
     }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
