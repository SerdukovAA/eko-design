package com.tender.webapp.controllers.service_bean;



/**
 * Created by Serdukov on 03.09.2015.
 */
public class NewCommentRequest {


    private int message_id;
    private String text;



    private boolean isAnswer;
    private int tender_id;



/*-------getters and setters------------------------*/

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isAnswer() {
        return isAnswer;
    }

    public void setIsAnswer(boolean isAnswer) {
        this.isAnswer = isAnswer;
    }

    public int getTender_id() {
        return tender_id;
    }

    public void setTender_id(int tender_id) {
        this.tender_id = tender_id;
    }
}
