package testHib;


import com.tender.webapp.config.AppConfig;
import com.tender.webapp.config.RootConfig;

import com.tender.webapp.controllers.YandexController;
import com.tender.webapp.models.*;
import com.tender.webapp.models.journal.RefillTransactionJournal;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.core.env.Environment;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={RootConfig.class, AppConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class YandexControllersTest {

    MockMvc mockMvc;
    @Autowired
    WebApplicationContext wac;
    @Resource
    protected SessionFactory sessionFactory;



    @Before
    public void setup() {
        // Setup Spring test in webapp-mode (same config as spring-boot)
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }

/*
        @Test
        public void aUser() {
            Session session = sessionFactory.openSession();
            User user = new User();
            user.setDateCreate(new Date());
            user.setBalance(new Balance(new BigDecimal(0.0)));
            user.setJuridical(false);
            user.setProvider(false);
            //(1,'ACTIVE');
            UserStatus userStatus = (UserStatus) session.get(UserStatus.class, 1);
            user.setUserStatus(userStatus);
            //(3,'USER_CONSUMER');
            UserRole userRole = (UserRole) session.get(UserRole.class, 3);
            user.setUserRole(userRole);
            user.setJuridicalInfo(null);
            PrivateInfo privInf = new PrivateInfo();
            privInf.setDateCreate(new Date());
            privInf.setEmail("serd89@mait.ru");
            privInf.setFirstName("Тестовый");
            privInf.setLastName("Пользователь");
            privInf.setPhoneNumber("89267776666");
            privInf.setJurContact(true);
            user.setPrivateInfo(privInf);
            session.save(user);
            RefillTransactionJournal refillTransactionJournal = new RefillTransactionJournal();
            refillTransactionJournal.setDateCreate(new Date());
            refillTransactionJournal.setOrderSumAmount(new BigDecimal(87.6));
            refillTransactionJournal.setOrderSumCurrencyPaycash(643);
            refillTransactionJournal.setTransactionStatus((TransactionStatus) session.get(TransactionStatus.class, 1));
            refillTransactionJournal.setPaymentType("PC");
            refillTransactionJournal.setUser(user);
            session.save(refillTransactionJournal);
            session.close();
        }*/

    @Test
    public void checkOrderT() {
        try {
            MvcResult result = mockMvc.perform(post("https://ubeten.com/check_order_test ")
                    .param("requestDatetime", "2011-05-04T20:38:00.000+04:00")
                    .param("action", "checkOrder")
                    .param("customerNumber", "1")
                    .param("t_uniq", "1")
                    .param("invoiceId", "550055")
                    .param("md5", "C32025453D32FD19EB2B6979C820DFD6")
                    .param("shopId", "47502")
                    .param("orderSumBankPaycash", "1001")
                    .param("paymentType", "PC")
                    .param("orderSumAmount", "87.0")
                    .param("orderSumCurrencyPaycash", "643")
                    .param("shopSumAmount", "86.0")
                    .param("shopSumCurrencyPaycash", "643"))
                    .andExpect(status().isOk()).andExpect(content().contentType("application/xml")).andReturn();

            String content = result.getResponse().getContentAsString();
            System.out.println("result " + content);
        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }

        @Test
        public void paymentAvisoT(){
            try {
                MvcResult result =  mockMvc.perform(post("https://ubeten.com/check_order_test ")
                        .param("requestDatetime", "2011-05-04T20:38:00.000+04:00")
                        .param("action", "paymentAviso")
                        .param("customerNumber", "1")
                        .param("t_uniq", "1")
                        .param("invoiceId", "550055")
                        .param("md5", "3573CCECDA07BBC2E4773EB54946854C")
                        .param("shopId", "47502")
                        .param("orderSumBankPaycash", "1001")
                        .param("paymentType", "PC")
                        .param("orderSumAmount", "87.0")
                        .param("orderSumCurrencyPaycash", "643")
                        .param("shopSumAmount", "86.0")
                        .param("shopSumCurrencyPaycash", "643"))
                        .andExpect(status().isOk()).andExpect(content().contentType("application/xml")).andReturn();

                String content = result.getResponse().getContentAsString();
                System.out.println("result "+content );
            }catch (Exception ex){
                ex.printStackTrace();

            }

    }

  /*  @Test
    public void zUser() {
        Session session = sessionFactory.openSession();
        Query q = session.createQuery("delete CashTraffic where id >= 0");
        q.executeUpdate();
        q = session.createQuery("delete RefillTransactionJournal where id >= 0");
        q.executeUpdate();
        session.delete(session.get(User.class,1));
        session.flush();
        session.close();
    }*/

}