package services;


import com.tender.webapp.biznes.ServiceTender;
import com.tender.webapp.config.AppConfig;
import com.tender.webapp.config.RootConfig;

import com.tender.webapp.dao.TenderDAO;
import com.tender.webapp.models.Tender;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={RootConfig.class, AppConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class ServiceTenderTest {

    @Autowired
    ServiceTender serviceTender;

    @Autowired
    TenderDAO tenderDAO;

/*    @Test
    public void getMyTenderTest(){
        try {
            Tender tender = serviceTender.getMyTenderByTenderId(4,1);
            assertNotNull(tender);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class}, readOnly = true)
    public void callStoredProcce() throws Exception{
        List<Tender> tenderList =  tenderDAO.getTendersForProvider(41);
        assertNotNull(tenderList);
    }*/

    @Test
    public void tenderList() throws Exception{
        List<Tender> tenderList = tenderDAO.getAllRunTenders(1);
        assertEquals(10, tenderList.size());
        tenderList = tenderDAO.getAllRunTenders(2);
        assertEquals(10, tenderList.size());
        tenderList = tenderDAO.getAllRunTenders(3);
        assertEquals(4, tenderList.size());
    }

}