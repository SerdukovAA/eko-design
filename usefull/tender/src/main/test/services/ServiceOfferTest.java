package services;


import com.tender.webapp.biznes.ServiceOffer;
import com.tender.webapp.biznes.ServiceTender;
import com.tender.webapp.config.AppConfig;
import com.tender.webapp.config.RootConfig;
import com.tender.webapp.models.Offer;
import com.tender.webapp.models.Tender;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={RootConfig.class, AppConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServiceOfferTest {

    @Autowired
    ServiceOffer serviceOffer;

    @Test
    public void getoOfferWinByTenderIDTest(){
        try {
            Offer offer = serviceOffer.getoOfferWinByTenderId(1,4);
            assertNotNull(offer);

        }catch (Exception e){
            e.printStackTrace();
        }
    }


}