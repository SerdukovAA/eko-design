package controllers;

import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.config.AppConfig;
import com.tender.webapp.config.RootConfig;
import com.tender.webapp.controllers.TenderRegistartionController;
import com.tender.webapp.controllers.service_bean.TenderFormObject;
import com.tender.webapp.models.*;
import com.tender.webapp.models.base.Region;
import com.tender.webapp.models.handbook.TypeObject;
import com.tender.webapp.models.handbook.TypeService;
import com.tender.webapp.models.service.ServerMessage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by serdukov on 05.04.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
//@Transactional
@ContextConfiguration(classes={RootConfig.class, AppConfig.class})
public class TenderRegistrationController {

    @Autowired
    ServiceUser serviceUser;
    @Resource
    protected SessionFactory sessionFactory;

    @Autowired
    private TenderRegistartionController tenderRegistartionController;


    private HttpServletRequest httpServletRequest;
    private User user;
    private Session session;


    @Before
    public void getUserContext() throws Exception {
        user = serviceUser.getUserByLogin("forpost");
        httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.getSession().setAttribute("user", user);
        session = sessionFactory.openSession();
    }

    //Красный
    @Test
    public void registrNewTenderRed() throws Exception{
        TenderFormObject tenderFormObject = new TenderFormObject();
        tenderFormObject.setTenderName("Junit тестовый тендер. Не валидный.");
        ServerMessage serverMessage =  tenderRegistartionController.doRegistrationTender(tenderFormObject, httpServletRequest);
        Assert.assertEquals("Ожидаем ошибку", 1 ,serverMessage.getStatus());
        System.out.println(serverMessage.toString());

    }


    //Зеленый
    @Test
    public void registrNewTenderGreen() throws Exception{

        TenderFormObject tenderFormObject = new TenderFormObject();
        tenderFormObject.setTenderName("Junit тестовый тендер");
        tenderFormObject.setTenderDescription("Описание валидного Junit тестового сервера");

        TypeObject typeObject =  (TypeObject) session.get(TypeObject.class, 1);
        tenderFormObject.setTypeObject(typeObject);


        TypeService typeService =  (TypeService) session.get(TypeService.class, 4);
        tenderFormObject.setTypeService(typeService);



        SelectorValue sv1 = new SelectorValue();
        sv1.setSelectorId(10);
        sv1.setSelectorName("Параметры аренды");
        sv1.setValueId(2);
        sv1.setSelectorValue("иностранные");


        SelectorValue sv2 = new SelectorValue();
        sv2.setSelectorId(10);
        sv2.setSelectorName("Параметры аренды");
        sv2.setValueId(6);
        sv2.setSelectorValue("со страхованием");



        List<SelectorValue> selectorValueList = new ArrayList<SelectorValue>();
        selectorValueList.add(sv1);
        selectorValueList.add(sv2);

        tenderFormObject.setSelectorValueList(selectorValueList);

        tenderFormObject.setDateBegin("11.11.2014");
        tenderFormObject.setDateEnd("12.11.2015");
        tenderFormObject.setExpectedPrice("2100");
        tenderFormObject.setNeedResponsible(false);


        Adress adress = new Adress();
        adress.setHouseNumber("12");
        adress.setStreet("Новороссийская");
        adress.setRegion("Рязанская область");
        adress.setCity("Касимов");
        adress.setRayon("Касимовский район");
        adress.setFullAdress("Москва, ул Новоссийская22");
        adress.setLongitude(38.43223);
        adress.setLatitude(55.4323);
        tenderFormObject.setAdress(adress);

        TenderObject tenderObject =  new TenderObject();

        ObjectParameterValue objectParameterValue = new ObjectParameterValue();
        objectParameterValue.setParameterId(12);
        objectParameterValue.setParameterName("Возраст водителя");
        objectParameterValue.setParameterValue("12");

        List<ObjectParameterValue> objectParameterValues = new ArrayList<ObjectParameterValue>();


        objectParameterValues.add(objectParameterValue);
        tenderObject.setObjectParameterValues(objectParameterValues);


        ObjectFoto objectFoto = new ObjectFoto();
        objectFoto.setFotoLink("/s1/tender_files/foto_for_tenders/2016-04/1d22853e7db31514ca49ca09256fe1a5c0d64e0d15c803d8b8cb0b3af65b3acd.png");
        objectFoto.setFotoMinLink("/s1/tender_files/foto_for_tenders/2016-04/1d22853e7db31514ca49ca09256fe1a5c0d64e0d15c803d8b8cb0b3af65b3acd_min.png");


        List<ObjectFoto> objectFotos = new ArrayList<ObjectFoto>();
        objectFotos.add(objectFoto);
        tenderObject.setObjectFotos(objectFotos);


        List<TenderObject> tenderObjects =  new ArrayList<TenderObject>();
        tenderObjects.add(tenderObject);

        tenderFormObject.setCommonValueList(new ArrayList<CommonValue>());
        tenderFormObject.setTenderFileList(new ArrayList<TenderFile>());

        tenderFormObject.setTenderObjects(tenderObjects);

        ServerMessage serverMessage =  tenderRegistartionController.doRegistrationTender(tenderFormObject, httpServletRequest);
        Assert.assertEquals("Ожидаем удачный статус", 3000 ,serverMessage.getStatus());
        System.out.println(serverMessage.toString());

    }







}
