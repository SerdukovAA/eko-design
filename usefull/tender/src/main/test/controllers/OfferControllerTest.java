package controllers;

import com.tender.webapp.biznes.CatalogService;
import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.config.AppConfig;
import com.tender.webapp.config.RootConfig;
import com.tender.webapp.controllers.OfferController;
import com.tender.webapp.controllers.RegistrationController;
import com.tender.webapp.controllers.service_bean.OfferFormObject;
import com.tender.webapp.controllers.service_bean.ProviderFormObject;
import com.tender.webapp.controllers.service_bean.TenderFormObject;
import com.tender.webapp.models.Adress;
import com.tender.webapp.models.User;
import com.tender.webapp.models.base.City;
import com.tender.webapp.models.base.Region;
import com.tender.webapp.models.service.ServerMessage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by serdukov on 05.04.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@Transactional
@ContextConfiguration(classes={RootConfig.class, AppConfig.class})
public class OfferControllerTest {

    @Autowired
    ServiceUser serviceUser;
    @Resource
    protected SessionFactory sessionFactory;

    @Autowired
    private OfferController offerController;


    private HttpServletRequest httpServletRequest;
    private User user;
    private Session session;


    @Before
    public void getUserContext() throws Exception {
        user = serviceUser.getUserByLogin("forpost");
        httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.getSession().setAttribute("user", user);
        session = sessionFactory.openSession();
    }


    @Test
    public void registrNewOffer() throws Exception{

        OfferFormObject offerFormObject = new OfferFormObject();

        offerFormObject.setPrice("1 000");


        offerFormObject.setOfferDescription("Описание оффера");
        offerFormObject.setDateBegin("06.04.2016");
        offerFormObject.setDateEnd("13.04.2016");
        offerFormObject.setHouseNumber("15");
        offerFormObject.setStreet("Новокузнецка");


        Adress adress = new Adress();
        adress.setHouseNumber("12");
        adress.setStreet("Новороссийская");
        adress.setRegion("Рязанская область");
        adress.setCity("Касимов");
        adress.setRayon("Касимовский район");
        adress.setFullAdress("Москва, ул Новоссийская22");
        adress.setLongitude(38.43223);
        adress.setLatitude(55.4323);
        offerFormObject.setAdress(adress);


        offerFormObject.setTender_id(5);

        ServerMessage serverMessage =  offerController.newOffer(offerFormObject, httpServletRequest);
        for(com.tender.webapp.models.service.Error er : serverMessage.getErrors()){
            System.out.println("Ошибка  :: "+er.getErrorText());
        }
        Assert.assertEquals("Ожидаем успешно", 3000, serverMessage.getStatus());


    }







}
