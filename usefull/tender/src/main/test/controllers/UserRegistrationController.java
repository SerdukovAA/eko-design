package controllers;

import com.tender.webapp.biznes.CatalogService;
import com.tender.webapp.biznes.ServiceUser;
import com.tender.webapp.config.AppConfig;
import com.tender.webapp.config.RootConfig;
import com.tender.webapp.controllers.RegistrationController;
import com.tender.webapp.controllers.TenderRegistartionController;
import com.tender.webapp.controllers.UserController;
import com.tender.webapp.controllers.service_bean.ProviderFormObject;
import com.tender.webapp.controllers.service_bean.TenderFormObject;
import com.tender.webapp.models.*;
import com.tender.webapp.models.base.Region;
import com.tender.webapp.models.handbook.TypeObject;
import com.tender.webapp.models.handbook.TypeService;
import com.tender.webapp.models.service.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by serdukov on 05.04.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@Transactional
@ContextConfiguration(classes={RootConfig.class, AppConfig.class})
public class UserRegistrationController {

    @Autowired
    ServiceUser serviceUser;

    @Autowired
    CatalogService catalogService;

    @Resource
    protected SessionFactory sessionFactory;

    @Autowired
    UserController userController;


    @Autowired
    RegistrationController registrationController;



    private HttpServletRequest httpServletRequest;
    private User user;





    @Test
    public void providerRegistration() throws Exception {


        List<Region> regionList = catalogService.getRegions();


        httpServletRequest = new MockHttpServletRequest();

        ProviderFormObject providerFormObject = new ProviderFormObject();
        providerFormObject.setFirstNameContactPerson("Алексей2");
        providerFormObject.setLastNameContactPerson("Смекалкин2");
        providerFormObject.setP_login("login_login2");
        providerFormObject.setEmailContactPerson("derf53f@gdff.com");
        providerFormObject.setPhoneNumberContactPerson("(965)444-55-55");
        providerFormObject.setCompanyName("Рога и копыта");
        providerFormObject.setInn("34592374423542");

        providerFormObject.setAcceptAgreementProvider(true);
        providerFormObject.setRecaptchaResponse("secretKeyForTesting");

        Adress adress = new Adress();
        adress.setHouseNumber("12");
        adress.setStreet("Новороссийская");
        adress.setRegion("Рязанская область");
        adress.setCity("Касимов");
        adress.setRayon("Касимовский район");
        adress.setFullAdress("Москва, ул Новоссийская22");
        adress.setLongitude(38.43223);
        adress.setLatitude(55.4323);
        providerFormObject.setAdress(adress);

        ServerMessage serverMessage = registrationController.doRegistrationProvider(providerFormObject, httpServletRequest);
        for(com.tender.webapp.models.service.Error er : serverMessage.getErrors()){
            System.out.println("Ошибка  :: "+er.getErrorText());
        }
        Assert.assertEquals("Ожидаем удачный статус", 3000 ,serverMessage.getStatus());
    }




    @Test
    public void providerEdit() throws Exception {

        user = serviceUser.getUserByLogin("irigos");
        httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.getSession().setAttribute("user", user);

        ProviderFormObject providerFormObject = new ProviderFormObject();
        providerFormObject.setFirstNameContactPerson("Алексей2");
        providerFormObject.setLastNameContactPerson("Смекалкин2");
        providerFormObject.setP_login("irigos2");
        providerFormObject.setEmailContactPerson("de5r2ff@gdf2f22.com");
        providerFormObject.setPhoneNumberContactPerson("(965)444-55-22");
        providerFormObject.setCompanyName("Рога и копыта22");
        providerFormObject.setInn("34592404423992");

        providerFormObject.setAcceptAgreementProvider(true);

        Adress adress = new Adress();
        adress.setHouseNumber("12");
        adress.setStreet("Новороссийская");
        adress.setRegion("Рязанская область");
        adress.setCity("Касимов");
        adress.setRayon("Касимовский район");
        adress.setFullAdress("Москва, ул Новоссийская22");
        adress.setLongitude(38.43223);
        adress.setLatitude(55.4323);
        providerFormObject.setAdress(adress);


        ServerMessage serverMessage =  userController.editProviderInfo(providerFormObject, httpServletRequest);
        for(com.tender.webapp.models.service.Error er : serverMessage.getErrors()){
            System.out.println("Ошибка  :: "+er.getErrorText());
        }
        Assert.assertEquals("Ожидаем удачный статус", 3000 ,serverMessage.getStatus());
    }






}
