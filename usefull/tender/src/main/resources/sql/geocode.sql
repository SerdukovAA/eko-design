ALTER TABLE public.juridical_info ADD COLUMN rayon_code VARCHAR(255);

ALTER TABLE public.tenders ADD COLUMN rayon_code VARCHAR(255);
ALTER TABLE public.tenders ADD COLUMN street VARCHAR(255);
ALTER TABLE public.tenders ADD COLUMN house_number VARCHAR(10);

ALTER TABLE public.offers ADD COLUMN rayon_code VARCHAR(255);


ALTER TABLE public.user_prefer_region ADD COLUMN rayon_code VARCHAR(255);


ALTER TABLE public.user_prefer_region DROP COLUMN region_name;
ALTER TABLE public.user_prefer_region DROP COLUMN city_name;
ALTER TABLE public.user_prefer_region DROP COLUMN region_code;
ALTER TABLE public.user_prefer_region DROP COLUMN city_code;

ALTER TABLE public.user_prefer_region ADD COLUMN region_code VARCHAR(255);
ALTER TABLE public.user_prefer_region ADD COLUMN city_code VARCHAR(255);

Update tenders  set region_code = 'г. Москва' where region_code='77';
Update tenders  set city_code = 'Москва' where region_code='г. Москва';
Update tenders  set city_code='0' where city_code is null;
commit;
