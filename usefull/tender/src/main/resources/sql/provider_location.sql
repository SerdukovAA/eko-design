
ALTER TABLE public.juridical_info ADD COLUMN fact_adr_latitude DOUBLE PRECISION;
ALTER TABLE public.juridical_info ADD COLUMN fact_adr_longitude DOUBLE PRECISION;


ALTER TABLE public.juridical_info ADD COLUMN region_code VARCHAR(255);
ALTER TABLE public.juridical_info ADD COLUMN city_code VARCHAR(255);
ALTER TABLE public.juridical_info ADD COLUMN street VARCHAR(255);
ALTER TABLE public.juridical_info ADD COLUMN house_number VARCHAR(10);



update juridical_info set fact_adr_latitude = 55.7755, fact_adr_longitude = 37.67617199999995, region_code = 77;



---доработка по офферу
ALTER TABLE public.offers ADD COLUMN street VARCHAR(255);
ALTER TABLE public.offers ADD COLUMN house_number VARCHAR(10);



