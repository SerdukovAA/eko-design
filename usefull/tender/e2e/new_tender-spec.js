describe('New tender registration', function() {

    beforeEach(function() {
        browser.get('http://localhost:8080/');
    });



    var path = require('path');


    it('new tender registration', function() {


        element(by.css('a.reg.ng-binding.marginLeft38')).click();

        expect(element(by.model('credentials.username')).isPresent()).toBe(true);
        element(by.model('credentials.username')).sendKeys("forpost");

        expect(element(by.model('credentials.password')).isPresent()).toBe(true);
        element(by.model('credentials.password')).sendKeys("1122");


        element(by.css('button.btn.btn-info.btn-lg')).click();


        element(by.binding('lScope.listTema')).getText().then(function(text) {
            expect(text).toEqual('Тендеры Вашей категории участия');
        });


        element(by.css('a[href="#/new-tender-start"]')).click();

        element(by.css('h2.text-center')).getText().then(function(text) {
            expect(text).toEqual('Разместить новый тендер');
        });



        element.all(by.css('select#typeObject option')).then(function(items) {
            expect(items.length).toBe(2);
            items[1].click();
        });


        element.all(by.css('select#typeService option')).then(function(items) {
            items[3].click();
        });


        element.all(by.css('input[name="rent_type"]')).then(function(items) {
            items[3].click();
        });

        browser.sleep(1000);

        element(by.css('button.next_button')).click();

        browser.sleep(1000);


        //TODO решит проблему с форматом фотографий, jPG не пролазит
        element(by.css('input#fotoMultiInput0')).sendKeys('/home/serdukov/Изображения/logo.png');

        browser.sleep(5000);


    });

//TODO проверить валидации




});



