describe('Ubeten registration', function() {

    beforeEach(function() {
        browser.get('http://localhost:8080/');
    });

/*    it('should load page', function() {
        expect(browser.getTitle()).toEqual('Ubeten.com');
    });*/


    it('provider registration', function() {
        element(by.id('regLinkButton')).click();

        element(by.css('h3')).getText().then(function(text) {
            expect(text).toEqual('Выберите форму для регистрации');
        });

        element(by.css('a[href="#/registration_provider"]')).click();

        //по факту регистрацию не проверить поэтому просто проверяем заполнение формы

        element(by.model('provider.firstNameContactPerson')).sendKeys("Алексей");
        expect(element(by.model('provider.firstNameContactPerson')).isPresent()).toBe(true);
        element(by.model('provider.lastNameContactPerson')).sendKeys("Загорин");
        expect(element(by.model('provider.lastNameContactPerson')).isPresent()).toBe(true);
        element(by.model('provider.p_login')).sendKeys("login_like_a_boss");
        expect(element(by.model('provider.p_login')).isPresent()).toBe(true);
        element(by.model('provider.emailContactPerson')).sendKeys("semen@senn.ru");
        expect(element(by.model('provider.emailContactPerson')).isPresent()).toBe(true);
        element(by.model('provider.phoneNumberContactPerson')).sendKeys("9656666666");
        expect(element(by.model('provider.phoneNumberContactPerson')).isPresent()).toBe(true);
        element(by.model('provider.companyName')).sendKeys("ЗАО РогаИКопыта");
        expect(element(by.model('provider.companyName')).isPresent()).toBe(true);
        element(by.model('provider.inn')).sendKeys("27462374942740");
        expect(element(by.model('provider.inn')).isPresent()).toBe(true);
        element(by.model('provider.factAdress')).sendKeys("Москва, улица Буржуйская");
        expect(element(by.model('provider.factAdress')).isPresent()).toBe(true);

        element(by.model('provider.acceptAgreementProvider')).click();

        element(by.css('button.btn.btn-success.btn-lg')).click();

        $('iframe').click();

        browser.sleep(3000);

    });

//TODO проверить валидации




});



