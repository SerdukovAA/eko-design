﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page session="true" %>
<!DOCTYPE HTML>
<html style=" height: 100%;">
<head>
    <meta charset="UTF-8">
    <title>Вход</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <link href="res/css/bootstrap.min.css" rel="stylesheet">
    <link href="res/css/bootstrap-theme.min.css" rel="stylesheet">
</head>
<body style="height: 100%;  width: auto; background-size: cover; padding: 30px;">

<div class="container text-center" style="margin-top: 20px;">
    <div class="jumbotron">
        <h3>Терминал тех.поддержки ДиКБМ</h3>
    </div>
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
            <form autocomplete="off" class="well well-sm" action="j_spring_security_check" method='POST'>
                <h3>Вход</h3>
                <input type="text" id="login" style="margin-bottom:10px;" class="form-control" name="login" placeholder="Login" autofocus="">
                <input type="password" id="password" style="margin-bottom:10px;"  class="form-control" name="password" placeholder="Password">
                <input class="btn btn-primary" name="submit" type="submit" value="Войти"/>
            </form>
        </div>

        <div class="text-center col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
            <img alt="Улогок инженера ДиКБМ" src="res/img/logo.png" width="130">
        </div>
    </div>
    <c:if test="${not empty error}">
        <div style="color:red;">${error}</div>
    </c:if>
    <c:if test="${not empty msg}">
        <div style="color:green;">${msg}</div>
    </c:if>
</div>
</body>
</html>