﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE HTML>
<html style=" height: 100%;">
<head>
    <meta charset="UTF-8">
    <title>Уголок инженера</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <link href="res/css/bootstrap.min.css" rel="stylesheet">
    <link href="res/css/bootstrap-theme.min.css" rel="stylesheet">
    <link rel="stylesheet" href="res/css/jquery-ui.min.css">
    <link rel="stylesheet" href="res/css/style.css">

</head>
<body ng-app="app" style=" height: 100%;">
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img alt="Улогок инженера ДиКБМ" style="width: 43px; position: absolute;top:2px; left:6px;" src="res/img/logo.png">
           </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="j_spring_security_logout">Выйти</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid" ng-controller="NavigationCtrl">
    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <h5 class="text-center well well-sm"><b>Утилиты</b></h5>
                <li ng-class="{ active: isCurrentPath('/gzip') }"><a href="#/gzip">Работа с GZIP</a></li>
                <li ng-class="{ active: isCurrentPath('/base64') }"><a href="#/base64">Работа с Base64</a></li>
                <li ng-class="{ active: isCurrentPath('/kbm_xml') }"><a href="#/kbm_xml">Запрос КБМ через XML</a></li>

            </ul>
            <ul class="nav nav-sidebar">
                <h5 class="text-center well well-sm"><b>Блобы</b></h5>
                <li ng-class="{ active: isCurrentPath('/blob_xml_request') }"><a href="#/blob_xml_request">Получить XML запрос</a></li>
                <li ng-class="{ active: isCurrentPath('/blob_xml_response') }"><a href="#/blob_xml_response">Получить XML ответ</a></li>
            </ul>

            <ul class="nav nav-sidebar">
                <h5 class="text-center well well-sm"><b>Проверки по е_осаго</b></h5>
                <li ng-class="{ active: isCurrentPath('/eosago_driver_xml') }"><a href="#/eosago_driver_xml">Проверка водителя</a></li>
                <li ng-class="{ active: isCurrentPath('/eosago_ts_xml') }"><a href="#/eosago_ts_xml">Проверка ТС</a></li>
                <li ng-class="{ active: isCurrentPath('/eosago_owner_xml') }"><a href="#/eosago_owner_xml">Проверка собственника </a></li>
                <li ng-class="{ active: isCurrentPath('/eosago_status_driver') }"><a href="#/eosago_status_driver">Статус запроса проверки водителя </a></li>
                <li ng-class="{ active: isCurrentPath('/eosago_status_ts') }"><a href="#/eosago_status_ts">Статус запроса проверки ТС </a></li>
                <li ng-class="{ active: isCurrentPath('/eosago_status_owner') }"><a href="#/eosago_status_owner">Статус запроса проверки собственника </a></li>
            </ul>
            <ul class="nav nav-sidebar">
                <h5 class="text-center well well-sm"><b>Метрики</b></h5>
                <li ng-class="{ active: isCurrentPath('/static_dkbm') }"><a href="#/static_dkbm">ДиКБМ</a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div ng-view>

            </div>
        </div>
    </div>
</div>

<!-----------------------------------Js------------------------------------------------------------------------->
<script src="res/js/lib/jquery-2.1.1.min.js"></script>
<script src="res/js/lib/jquery-ui.min.js"></script>
<script src="res/js/lib/datepicker-ru.js"></script>
<script src="res/js/lib/bootstrap.min.js"></script>


<script src="res/js/lib/angular.min.js"></script>
<script src="res/js/lib/angular-animate.min.js"></script>
<script src="res/js/lib/angular-route.min.js"></script>
<script src="res/ang/app.js"></script>
<script src="res/ang/util/gzip/gzip.js"></script>
<script src="res/ang/util/base64/base64.js"></script>
<script src="res/ang/kbmXml/kbmXml.js"></script>
<script src="res/ang/eosagoTS/eosagoTS.js"></script>
<script src="res/ang/eosagoOwner/eosagoOwner.js"></script>
<script src="res/ang/eosagoDriver/eosagoDriver.js"></script>

<script src="res/ang/statistic/static.js"></script>


<script src="res/ang/statusChecks/ownerStatus/ownerStatus.js"></script>
<script src="res/ang/statusChecks/tsStatus/tsStatus.js"></script>
<script src="res/ang/statusChecks/driverStatus/driverStatus.js"></script>


<script src="res/ang/blobRequest/blobXml.js"></script>





</body>

</html>