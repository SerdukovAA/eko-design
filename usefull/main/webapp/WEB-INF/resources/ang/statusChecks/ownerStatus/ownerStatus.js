/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('EosagoOwnerStatusCtrl', ['$scope', '$http','ServerService',
    function ($scope, $http, ServerService) {

        $scope.eosagoOwnerStatusRequest = {};
        $scope.eosagoOwnerStatusResponse = {};
        $scope.serversEosago = ServerService.getServersEosago();





        $scope.geteosagoOwnerStatusRequestFromXML =  function(invalid){

           // if(invalid) return false;
            $scope.eosagoOwnerStatusResponse.response = "";

            var data = $scope.eosagoOwnerStatusRequest;

            $http({
                method: 'POST',
                url: 'eosago_owner_status_request',
                data: data,
                transformResponse: [function (data) {return data;}]
            }).success(function(data, status, headers, config) {
                    $scope.eosagoOwnerStatusResponse.response = new String(data.trim());
            });

        }


}]);
