/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('EosagoDriverStatusCtrl', ['$scope', '$http','ServerService',
    function ($scope, $http, ServerService) {

        $scope.eosagoDriverStatusRequest = {};
        $scope.eosagoDriverStatusResponse = {};
        $scope.serversEosago = ServerService.getServersEosago();


        $scope.geteosagoDriverStatusRequestFromXML =  function(invalid){

           // if(invalid) return false;
            $scope.eosagoDriverStatusResponse.response = "";

            var data = $scope.eosagoDriverStatusRequest;

            $http({
                method: 'POST',
                url: 'eosago_driver_status_request',
                data: data,
                transformResponse: [function (data) {return data;}]
            }).success(function(data, status, headers, config) {
                    $scope.eosagoDriverStatusResponse.response = new String(data.trim());
            });

        }


}]);
