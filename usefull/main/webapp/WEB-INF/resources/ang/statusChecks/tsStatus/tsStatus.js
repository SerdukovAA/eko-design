/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('EosagoTsStatusCtrl', ['$scope', '$http','ServerService',
    function ($scope, $http, ServerService) {

        $scope.eosagoTsStatusRequest = {};
        $scope.eosagoTsStatusResponse = {};
        $scope.serversEosago = ServerService.getServersEosago();


        $scope.geteosagoTsStatusRequestFromXML =  function(invalid){

           // if(invalid) return false;
            $scope.eosagoTsStatusResponse.response = "";

            var data = $scope.eosagoTsStatusRequest;

            $http({
                method: 'POST',
                url: 'eosago_ts_status_request',
                data: data,
                transformResponse: [function (data) {return data;}]
            }).success(function(data, status, headers, config) {
                    $scope.eosagoTsStatusResponse.response = new String(data.trim());
            });

        }


}]);
