/**
 * Created by serdukovaa on 27.04.2016.
 */
angular.module('app').controller('StaticCtrl', ['$scope', '$http',
    function ($scope, $http) {

        $scope.metrics = {};
        $http.get('get_statistic_dikbm').then(function(http){
            $scope.metrics = http.data;
        });

        $scope.refresh = function(){
            $http.get('refresh_statistic_dikbm').then(function(http){
                $scope.metrics = http.data;
            });
        }


}]);
