/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app', ['ngRoute']).config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.

            when('/static_dkbm', {
                templateUrl: 'res/ang/statistic/staticDKBM.html',
                controller: 'StaticCtrl'
            }).
            when('/gzip', {
                templateUrl: 'res/ang/util/gzip/gzip.html',
                controller: 'GZIPCtrl'
            }).
            when('/base64', {
                templateUrl: 'res/ang/util/base64/base64.html',
                controller: 'Base64Ctrl'
            }).


            when('/kbm_xml', {
                templateUrl: 'res/ang/kbmXml/kbmXml.html',
                controller: 'KbmXmlCtrl',
                resolve: {
                    ServerS: function (ServerService) {
                        return ServerService.promiseKBM;
                    }
                }
            }).
            when('/eosago_ts_xml', {
                templateUrl: 'res/ang/eosagoTS/eosagoTS.html',
                controller: 'EosagoTSXmlCtrl',
                resolve: {
                    ServerS: function (ServerService) {
                        return ServerService.promiseEosago;
                    }
                }
            }).
            when('/eosago_owner_xml', {
                templateUrl: 'res/ang/eosagoOwner/eosagoOwner.html',
                controller: 'EosagoOwnerXmlCtrl',
                resolve: {
                    ServerS: function (ServerService) {
                        return ServerService.promiseEosago;
                    }
                }
            }).


            when('/eosago_driver_xml', {
                templateUrl: 'res/ang/eosagoDriver/eosagoDriver.html',
                controller: 'EosagoDriverXmlCtrl',
                resolve: {
                    ServerS: function (ServerService) {
                        return ServerService.promiseEosago;
                    }
                }
            }).
            when('/eosago_status_owner', {
                templateUrl: 'res/ang/statusChecks/ownerStatus/ownerStatus.html',
                controller: 'EosagoOwnerStatusCtrl',
                resolve: {
                    ServerS: function (ServerService) {
                        return ServerService.promiseEosago;
                    }
                }
            }).
            when('/eosago_status_ts', {
                templateUrl: 'res/ang/statusChecks/tsStatus/tsStatus.html',
                controller: 'EosagoTsStatusCtrl',
                resolve: {
                    ServerS: function (ServerService) {
                        return ServerService.promiseEosago;
                    }
                }
            }).

            when('/eosago_status_driver', {
                templateUrl: 'res/ang/statusChecks/driverStatus/driverStatus.html',
                controller: 'EosagoDriverStatusCtrl',
                resolve: {
                    ServerS: function (ServerService) {
                        return ServerService.promiseEosago;
                    }
                }
            }).
            when('/blob_xml_request', {
                templateUrl: 'res/ang/blobRequest/blobXmlRequest.html',
                controller: 'BlobXmlCtrl',
                resolve: {
                    ServerS: function (ServerService) {
                        return ServerService.promiseDataBase;
                    }
                }
            }).
            when('/blob_xml_response', {
                templateUrl: 'res/ang/blobRequest/blobXmlResponse.html',
                controller: 'BlobXmlCtrl',
                resolve: {
                    ServerS: function (ServerService) {
                        return ServerService.promiseDataBase;
                    }
                }
            }).


            otherwise({
                redirectTo: '/gzip'
            });
    }]);

angular.module('app').config(['$httpProvider', function ($httpProvider) {

    $httpProvider.interceptors.push(function($q) {
        return {
            request: function(request) {
                return request;
            },
            // This is the responseError interceptor
            responseError: function(rejection) {
                if (rejection.status === 401) {
                    window.location = "login?Ваша сессия истекла, необходимо заново ввести логин и пароль";
                }
                if (rejection.status === 405) {
                    window.location = "login?Ваша сессия истекла, необходимо заново ввести логин и пароль";
                }
                return $q.reject(rejection);
            }
        };
    });

}]);


angular.module('app')
    .controller('NavigationCtrl', ['$scope', '$location', function ($scope, $location) {
        $scope.isCurrentPath = function (path) {
            return $location.path() == path;
        };
    }]);


angular.module('app')
    .controller('GeneralCtrl', ['$scope', '$location', function ($scope, $location) {
     $scope.Timer = "321552146321541251254324";
}]);


angular.module('app').factory('ServerService', function($http) {
    var serversKBM = {};
    var serversEosago = {};
    var dataBaseInstances = {};

    var promiseKBM = $http.get('info_kbm_servers').success(function (data) {
        serversKBM = data;
    });

    var promiseEosago = $http.get('info_eosago_servers').success(function (data) {
        serversEosago = data;
    });


    var promiseDataBaseInstance = $http.get('info_data_base_servers').success(function (data) {
        dataBaseInstances = data;
    });

    return {
        promiseKBM: promiseKBM,
        promiseEosago: promiseEosago,
        promiseDataBase: promiseDataBaseInstance,

        getServersEosago : function() {
           return serversEosago;
        },
        getServersKBM : function() {
            return serversKBM;
        },
        getServersDataBase : function() {
            return dataBaseInstances;
        }

    };
});


