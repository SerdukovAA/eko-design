/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('BlobXmlCtrl', ['$scope', '$http','ServerService',
    function ($scope, $http, ServerService) {
        $scope.blobInsRequest = {};
        $scope.blobInsResponse = {};

        $scope.dataBaseInstanceList = ServerService.getServersDataBase();



        $scope.geteBlobRequestFromXML =  function(invalid){

            if(invalid) return false;

            var data =  $scope.blobInsRequest;
            $scope.blobInsResponse.response = "";

            $http({
                method: 'POST',
                url: 'get_blob_xml',
                data: data,
                transformResponse: [function (data) {return data;}]
            })
              .success(function(data, status, headers, config) {
                $scope.blobInsResponse.response = new String(data.trim());
              });
        }


}]);
