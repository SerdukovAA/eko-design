/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('EosagoTSXmlCtrl', ['$scope', '$http','ServerService',
    function ($scope, $http, ServerService) {

        $scope.eosagoTSRequest = {};
        $scope.eosagoTSResponse = {};
        $scope.serversEosago = ServerService.getServersEosago();



        $scope.getEosagoTSRequestFromXML =  function(invalid){

            if(invalid) return false;
            $scope.eosagoTSResponse.response = "";
            $scope.showPreloader =true;
            var data = $scope.eosagoTSRequest;


            $http({
                method: 'POST',
                url: 'eosago_check_ts_from_xml',
                data: data,
                transformResponse: [function (data) {return data;}]
            })
              .success(function(data, status, headers, config) {
                    $scope.showPreloader =false;
               $scope.eosagoTSResponse.response = new String(data.trim());

              });

        }


}]);
