/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('EosagoDriverXmlCtrl', ['$scope', '$http','ServerService',
    function ($scope, $http, ServerService) {

        $scope.eosagoDriverRequest = {};
        $scope.eosagoDriverResponse = {};
        $scope.serversEosago = ServerService.getServersEosago();





        $scope.getEosagoDriverRequestFromXML =  function(invalid){

            if(invalid) return false;
            $scope.eosagoDriverResponse.response = "";
            $scope.showPreloader =true;
            var data = $scope.eosagoDriverRequest;


            $http({
                method: 'POST',
                url: 'eosago_check_driver_from_xml',
                data: data,
                transformResponse: [function (data) {return data;}]
            })
              .success(function(data, status, headers, config) {
                    $scope.showPreloader =false;
               $scope.eosagoDriverResponse.response = new String(data.trim());

              });

        }


}]);
