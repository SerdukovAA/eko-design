/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('KbmXmlCtrl', ['$scope', '$http','ServerService',
    function ($scope, $http, ServerService) {
        $scope.kbmXmlRequest = {};
        $scope.kbmXmlResponse = {};

        $scope.serversKBM = ServerService.getServersKBM();



        $scope.getKBMFromXML =  function(invalid){

            if(invalid) return false;

            var data = $scope.kbmXmlRequest;
            $scope.kbmXmlResponse.response = "";

            $http({
                method: 'POST',
                url: 'kbm_from_xml',
                data: data,
                transformResponse: [function (data) {return data;}]
            })
              .success(function(data, status, headers, config) {
              $scope.kbmXmlResponse.response = new String(data.trim());
              });
        }


}]);
