/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('EosagoOwnerXmlCtrl', ['$scope', '$http','ServerService',
    function ($scope, $http, ServerService) {

        $scope.eosagoOwnerRequest = {};
        $scope.eosagoOwnerResponse = {};
        $scope.serversEosago = ServerService.getServersEosago();





        $scope.getEosagoOwnerRequestFromXML =  function(invalid){

            if(invalid) return false;
            $scope.eosagoOwnerResponse.response = "";
            $scope.showPreloader =true;
            var data = $scope.eosagoOwnerRequest;


            $http({
                method: 'POST',
                url: 'eosago_check_owner_from_xml',
                data: data,
                transformResponse: [function (data) {return data;}]
            })
              .success(function(data, status, headers, config) {
                    $scope.showPreloader =false;
               $scope.eosagoOwnerResponse.response = new String(data.trim());

              });

        }


}]);
