/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('GZIPCtrl', ['$scope', '$http',
    function ($scope, $http) {
        $scope.gzip = {};
        $scope.getGZIP =  function(){
            var data = $scope.gzip.text.trim();
            $http({method: 'POST', url: 'text_to_gzip', params:{text:data},
                   transformResponse: [function (data) {return data;}]
            })
                 .success(function(data, status, headers, config) {
                    $scope.gzip.hash = new String(data.trim());
                 });
        }

        $scope.getText =  function(){
            var data = $scope.gzip.hash.trim();
            $http({method: 'POST', url: 'gzip_to_text', params:{hash:data},
                transformResponse: [function (data) {return data;}]
            })
            .success(function(data, status, headers, config) {
                    $scope.gzip.text = new String(data.trim());
            });
        }

}]);
