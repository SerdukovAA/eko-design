/**
 * Created by serdukovaa on 01.12.2015.
 */
angular.module('app').controller('Base64Ctrl', ['$scope', '$http',
    function ($scope, $http) {
        $scope.base64 = {};
        $scope.getBase64 =  function(){
            var data = $scope.base64.text.trim();
            $http({method: 'POST', url: 'text_to_base', params:{text:data},
                   transformResponse: [function (data) {return data;}]
            })
                 .success(function(data, status, headers, config) {
                    $scope.base64.hash = new String(data.trim());
                 });
        }

        $scope.getText =  function(){
            var data = $scope.base64.hash.trim();
            $http({method: 'POST', url: 'base_to_text', params:{hash:data},
                transformResponse: [function (data) {return data;}]
            })
            .success(function(data, status, headers, config) {
                    $scope.base64.text = new String(data.trim());
            });
        }

}]);
