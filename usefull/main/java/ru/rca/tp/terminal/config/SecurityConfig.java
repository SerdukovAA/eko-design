package ru.rca.tp.terminal.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Configuration
@EnableWebSecurity

public class SecurityConfig extends WebSecurityConfigurerAdapter {





    @Autowired
    public void registerGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("ingr").password("234234").roles("INGENIER");
    }



	@Override
	protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests().antMatchers("/index").access("hasAnyRole('INGENIER')")
                .antMatchers("/","/login", "/res/**").permitAll()
                .anyRequest().authenticated()
                .and()
				.formLogin().loginPage("/login")
                .defaultSuccessUrl("/index")
				.loginProcessingUrl("/j_spring_security_check")
                .usernameParameter("login").passwordParameter("password")
                .failureUrl("/login")
            	.and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/j_spring_security_logout"))
                .deleteCookies("remove")
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/login")
                .and().exceptionHandling().accessDeniedPage("/403");
            http.exceptionHandling().authenticationEntryPoint(new RESTAuthenticationEntryPoint());
            http.csrf().disable();
    }




    public class RESTAuthenticationEntryPoint implements AuthenticationEntryPoint {

        @Override
        public void commence(HttpServletRequest request, HttpServletResponse response,
                             AuthenticationException authException) throws IOException, ServletException {

            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}