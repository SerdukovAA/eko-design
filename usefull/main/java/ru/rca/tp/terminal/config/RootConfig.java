package ru.rca.tp.terminal.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ru.rca.tp.terminal.bilder.AttachmenttBuilder;
import ru.rca.tp.terminal.bilder.XMLDocumentBuilder;
import ru.rca.tp.terminal.bilder.impl.AttachmenttBuilderImpl;
import ru.rca.tp.terminal.bilder.impl.XMLDocumentBuilderImpl;
import ru.rca.tp.terminal.bilder.XMLFormater;
import ru.rca.tp.terminal.bilder.impl.XMLFormaterImpl;
import ru.rca.tp.terminal.services.Base64Service;
import ru.rca.tp.terminal.services.GZIPService;
import ru.rca.tp.terminal.services.impl.Base64ServiceImpl;
import ru.rca.tp.terminal.services.impl.GZIPServiceImpl;


/**
 * Created by Serdukov on 06.06.2015.
 */
@Configuration
@ComponentScan(basePackages = {"ru.rca"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class)
        })
@PropertySource("classpath:app.properties")
public class RootConfig {

    @Autowired
    Environment env;




    @Bean
    public GZIPService getGZIPService() throws Exception {
         return new GZIPServiceImpl();
    }

    @Bean
    public Base64Service getBase64Service() throws Exception {
         return new Base64ServiceImpl();
    }

    @Bean
    public XMLFormater getXMLFormater() {
        return new XMLFormaterImpl();

    }

    @Bean(name="attachmenttBuilder")
    public AttachmenttBuilder getAttachmenttBuilder() {
        return new AttachmenttBuilderImpl();

    }
    @Bean(name="xMLDocumentBuilder")
    public XMLDocumentBuilder getXMLDocumentBuilder() {
        return new XMLDocumentBuilderImpl();

    }


        @Bean(name = "DiKBMRSA1")
        public javax.sql.DataSource getDataSourceDiKBMRSA1() {
            org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
            ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
            ds.setUrl(env.getProperty(".url"));
            ds.setUsername(env.getProperty("oracleDBRSA1.userName"));
            ds.setPassword(env.getProperty("oracleDBRSA1.password"));
            ds.setInitialSize(5);
            ds.setTestOnBorrow(true);
            ds.setValidationInterval(34000);
            ds.setValidationQuery("SELECT 1 FROM DUAL");
            ds.setMaxActive(Integer.valueOf(env.getProperty(".maxThreadsCount")));
            ds.setMaxIdle(5);
            ds.setMinIdle(1);
            return ds;
        }


    @Bean(name = "jdbcTemplateDiKBMRSA1")
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(getDataSourceDiKBMRSA1());
    }





}
