package ru.rca.tp.terminal.config.core;


import ru.rca.tp.terminal.config.RootConfig;
import ru.rca.tp.terminal.config.AppConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { RootConfig.class  };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
        return new Class[] {AppConfig.class  };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}


}