package ru.rca.tp.terminal.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


@EnableWebMvc  //Аннотация разрешает использовать MVC
@Configuration //Объявляем конфигурационный класс
@ComponentScan(basePackages ={"ru.rca"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RootConfig.class)
        })//говорит, где искать компоненты проекта.
@EnableCaching
public class AppConfig extends WebMvcConfigurerAdapter {//WebMvcConfigurerAdapter - абстрактный класс с заглушками, которые надо мы оверайдим

    @Autowired
    Environment env;


	@Bean
	public CacheManager getCacheManager(){
		return  new EhCacheCacheManager(getEhCacheFactory().getObject());
	}
	@Bean
	public EhCacheManagerFactoryBean getEhCacheFactory(){
		EhCacheManagerFactoryBean factoryBean = new EhCacheManagerFactoryBean();
		factoryBean.setConfigLocation(new ClassPathResource("ehcache.xml"));
		factoryBean.setShared(true);
		return factoryBean;
	}


	//Объявление путей ресурсов.
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/res/**").addResourceLocations("/WEB-INF/resources/"); //ресурсы приложения css, js  т.д
        registry.addResourceHandler("files/**").addResourceLocations("file:///"+env.getProperty("file_root")+"/tp_terminal_files/");
    }



	//Настройка "отображений" веб-приложения
	@Bean
	public ViewResolver getViewResolver(){
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/view/");
		resolver.setSuffix(".jsp");
        System.out.println("Start ViewResolver APP");
		return resolver;
	}





}