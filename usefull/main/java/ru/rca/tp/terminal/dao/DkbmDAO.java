package ru.rca.tp.terminal.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by serdukovaa on 27.04.2016.
 */
@Service
public class DkbmDAO {

    @Autowired
    @Qualifier("jdbcTemplateDiKBMRSA1")
    private JdbcTemplate jdbcTemplate;


    @Cacheable(value="statistic", key="'kbm'")
    public Double getAvgKBMCache(){
        return  getAvgKBM();
    }

    @CachePut(value = "statistic", key="'kbm'")
    public Double refreshAvgKBMCache(){
        return getAvgKBM();
    }


    private Double getAvgKBM(){
        String sql = "select AVG(extract(second from diff))avg_seconds from (\n" +
                "   SELECT irj.END_DATE-irj.BEG_DATE AS diff FROM INS_REQUEST_JUR irj\n" +
                "   WHERE irj.BEG_DATE>=SYSDATE-1 ORDER BY irj.BEG_DATE DESC\n" +
                ") WHERE ROWNUM<40";

        Double avg = jdbcTemplate.queryForObject(sql,Double.class);
        return avg;
    }




}
