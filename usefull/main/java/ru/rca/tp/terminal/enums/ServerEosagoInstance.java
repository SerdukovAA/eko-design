package ru.rca.tp.terminal.enums;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by serdukovaa on 28.10.2015.
 */
@JsonFormat(shape= JsonFormat.Shape.OBJECT)
public enum ServerEosagoInstance {


    EOSAGO_PROM  (1, "ПромышлеGOService?wsdl","EOSAGO_PROM"),
    EOSAGO_TCOPY (2, "ТестовcheckSubjectOAGO_TCOPY"),
    EOSAGO_TNEXT (3, "NEXT kSubjectOSAGOServicSAGO_TNEXT");



    private int id;
    private String name;

    @JsonIgnore
    private String password;

    @JsonIgnore
    private String login;

    @JsonIgnore
    private String url;

    private String constant;



    private ServerEosagoInstance(int code, String name, String password, String login, String url, String constant) {
        this.id = code;
        this.name = name;
        this.password = password;
        this.login = login;
        this.url = url;
        this.constant = constant;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getConstant() {
        return constant;
    }

    public void setConstant(String constant) {
        this.constant = constant;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ServerEOSAGOInstance:");
        sb.append("{id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
