package ru.rca.tp.terminal.enums;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by serdukovaa on 28.10.2015.
 */
@JsonFormat(shape= JsonFormat.Shape.OBJECT)
public enum DataBaseInstance {


   DIKBM_PROM_DB  (1, "Промышленный серверROM_DB"),
   EOSAGO_PROM_DB (2, "Промышленный сервер E_o_PROM_DB\");\n9.3.24

    private int id;
    private String name;
    private String constant;

    @JsonIgnore
    private String url;
    @JsonIgnore
    private String username;
    @JsonIgnore
    private String password;


    private DataBaseInstance(int code, String name, String url, String username, String password, String constant) {
        this.id = code;
        this.name = name;
        this.url = url;
        this.username = username;
        this.password = password;
        this.constant = constant;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConstant() {
        return constant;
    }

    public void setConstant(String constant) {
        this.constant = constant;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ServerKBMInstance:");
        sb.append("{id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
