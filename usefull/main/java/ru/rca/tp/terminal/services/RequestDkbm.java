package ru.rca.tp.terminal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rca.tp.terminal.controllers.bean.KBMRequest;
import ru.rca.tp.terminal.services.loaders.KBMRequestLoader;


/**
 * Created by serdukovaa on 18.03.2016.
 */
@Service
public class RequestDkbm{


    @Autowired
    KBMRequestLoader kbmRequestLoader;

    public String requestKbm(KBMRequest kbmRequest) throws Exception {
        return kbmRequestLoader.requestBase64(kbmRequest);
    }


}
