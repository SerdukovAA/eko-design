package ru.rca.tp.terminal.services;

/**
 * Created by serdukovaa on 01.12.2015.
 */
public interface Base64Service {

    public String textToBase64(String text) throws Exception;
    public String base64ToText(String gzipText) throws Exception;

}

