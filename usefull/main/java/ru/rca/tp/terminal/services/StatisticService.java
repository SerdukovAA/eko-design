package ru.rca.tp.terminal.services;

import ru.rca.tp.terminal.controllers.bean.Metric;

import java.util.List;

/**
 * Created by serdukovaa on 27.04.2016.
 */
public interface StatisticService {

    public List<Metric> getAllMetrics(boolean cache);


}
