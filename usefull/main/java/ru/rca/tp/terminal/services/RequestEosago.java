package ru.rca.tp.terminal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rca.tp.terminal.controllers.bean.EosagoRequest;
import ru.rca.tp.terminal.services.loaders.DriverCheckEOSAGOLoader;
import ru.rca.tp.terminal.services.loaders.InsurerCheckEOSAGOLoader;
import ru.rca.tp.terminal.services.loaders.TsCheckEOSAGOLoader;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by serdukovaa on 18.03.2016.
 */
@Service
public class RequestEosago
{

    @Autowired
    InsurerCheckEOSAGOLoader insurerCheckEOSAGOLoader;

    @Autowired
    DriverCheckEOSAGOLoader driverCheckEOSAGOLoader;

    @Autowired
    TsCheckEOSAGOLoader tsCheckEOSAGOLoader;



    public String successDriverCheckLoad(EosagoRequest eosagoRequest) throws Exception {
        Map<String,String> paramlList =  driverCheckEOSAGOLoader.loadRequest(eosagoRequest);
        return driverCheckEOSAGOLoader.statusRequest(paramlList);
    }

    public String successInsurerOwnerCheckLoad(EosagoRequest eosagoRequest) throws Exception {
        Map<String,String> paramlList =  insurerCheckEOSAGOLoader.loadRequest(eosagoRequest);
        return insurerCheckEOSAGOLoader.statusRequest(paramlList);
    }


    public String successTSCheckLoad(EosagoRequest eosagoRequest) throws Exception {
        Map<String,String> paramlList =  tsCheckEOSAGOLoader.loadRequest(eosagoRequest);
        return tsCheckEOSAGOLoader.statusRequest(paramlList);
    }


    public String ownerStatusCheck(EosagoRequest eosagoRequest) throws Exception {
        Map<String,String> paramlList =  new HashMap<String, String>();
        paramlList.put("InsurerID", eosagoRequest.getServerInstance().getLogin());
        paramlList.put("ServoceURL", eosagoRequest.getServerInstance().getUrl());
        paramlList.put("IDCheckInsurerOwner", eosagoRequest.getRequestID());
        return insurerCheckEOSAGOLoader.statusRequest(paramlList);
    }

    public String tsStatusCheck(EosagoRequest eosagoRequest) throws Exception {
        Map<String,String> paramlList =  new HashMap<String, String>();
        paramlList.put("InsurerID", eosagoRequest.getServerInstance().getLogin());
        paramlList.put("ServoceURL", eosagoRequest.getServerInstance().getUrl());
        paramlList.put("IDCheckTS", eosagoRequest.getRequestID());
        return tsCheckEOSAGOLoader.statusRequest(paramlList);
    }


    public String driverStatusCheck(EosagoRequest eosagoRequest) throws Exception {
        Map<String,String> paramlList =  new HashMap<String, String>();
        paramlList.put("InsurerID", eosagoRequest.getServerInstance().getLogin());
        paramlList.put("ServoceURL", eosagoRequest.getServerInstance().getUrl());
        paramlList.put("IDCheckDriver", eosagoRequest.getRequestID());
        return driverCheckEOSAGOLoader.statusRequest(paramlList);
    }


}

