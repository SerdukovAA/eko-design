package ru.rca.tp.terminal.services;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rca.tp.terminal.bilder.XMLFormater;
import ru.rca.tp.terminal.controllers.bean.BlobInsRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.zip.GZIPInputStream;

/**
 * Created by serdukovaa on 04.05.2016.
 */
@Service
public class InsRequestService {



    @Autowired
    XMLFormater xmlFormater;


    public String getRequesrBlob(BlobInsRequest blobInsRequest) throws Exception{

        String response = null;
        String requestSQL = null;
        String blobColumnName = null;

        if(blobInsRequest.getDataBaseInstance().getId() == 1){
            if(blobInsRequest.getRequestType() == BlobInsRequest.RequestType.REQUEST){
              blobColumnName = "REQ_BODY";
              requestSQL = "SELECT REQ_BODY FROM INS_REQUEST_JUR WHERE INS_REQ_ID =?";
            }else{
              blobColumnName = "RESP_BODY";
              requestSQL = "SELECT RESP_BODY FROM INS_REQUEST_JUR WHERE INS_REQ_ID =?";
            }
        }


        if(blobInsRequest.getDataBaseInstance().getId() == 2){
            if(blobInsRequest.getRequestType() == BlobInsRequest.RequestType.REQUEST){
                blobColumnName = "REQ_BODY";
                requestSQL = "SELECT REQ_BODY FROM INS_REQUEST_JOUR WHERE INS_REQ_JOUR_ID =?";
            }else{
                blobColumnName = "RESP_BODY";
                requestSQL = "SELECT RESP_BODY FROM INS_REQUEST_JOUR WHERE INS_REQ_JOUR_ID =?";
            }
        }

        response = getXmlStringFromBlob(blobInsRequest,requestSQL, blobColumnName);

        return response;
    }






    public String getXmlStringFromBlob(BlobInsRequest blobInsRequest, String requestSQL, String blobColumnName) throws Exception {
        String xmlString = "Не получилось";

        PreparedStatement stmt = null;
        Connection connection = null;

        try {
            connection = this.getConnection(blobInsRequest);
            if(connection != null) {
                stmt = null;
                stmt = connection.prepareStatement(requestSQL);
                stmt.setInt(1, Integer.parseInt(blobInsRequest.getRequestId()));
                ResultSet e = stmt.executeQuery();
                e.next();
                Blob blob = e.getBlob(blobColumnName);
                byte[] content = blob.getBytes(1L, (int)blob.length());
                xmlString = IOUtils.toString(new GZIPInputStream(new ByteArrayInputStream(content)), "UTF-8");
                xmlString = xmlFormater.formatStringXML(xmlString);
            } else {
                System.out.println("Failed to make database connection!");
            }
        } catch (Throwable var20) {
            var20.printStackTrace();
        } finally {
            if(stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException var19) {
                    var19.printStackTrace();
                }
            }

            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException var18) {
                    var18.printStackTrace();
                }
            }

            return xmlString;
        }



    }







    private Connection getConnection(BlobInsRequest blobInsRequest) throws SQLException, ClassNotFoundException, IOException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        return DriverManager.getConnection(blobInsRequest.getDataBaseInstance().getUrl(), blobInsRequest.getDataBaseInstance().getUsername(),blobInsRequest.getDataBaseInstance().getPassword());
    }









}
