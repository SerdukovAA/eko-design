package ru.rca.tp.terminal.services.impl;

import org.apache.commons.codec.binary.Base64InputStream;
import org.springframework.security.crypto.codec.Base64;
import ru.rca.tp.terminal.services.Base64Service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Created by serdukovaa on 01.12.2015.
 */
public class Base64ServiceImpl implements Base64Service{

    public String textToBase64(String text) throws Exception {
        byte[] encodedBytes = Base64.encode(text.getBytes("UTF-8"));
        return new String(encodedBytes, Charset.forName("UTF-8"));
    }

    public String base64ToText(String base64Text) throws Exception{
        byte[] bytes=base64Text.getBytes("UTF-8");
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Base64InputStream b64is = new Base64InputStream(bis);
        BufferedReader br = new BufferedReader(new InputStreamReader(b64is, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        b64is.close();
        bis.close();
        return sb.toString();
    }

}

