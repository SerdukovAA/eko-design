package ru.rca.tp.terminal.services.impl;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;
import ru.rca.tp.terminal.services.GZIPService;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by serdukovaa on 01.12.2015.
 */
public class GZIPServiceImpl implements GZIPService {



    public String textToGZIP(String text) throws Exception{
        ByteOutputStream out = new ByteOutputStream();
        Base64OutputStream b64os = new Base64OutputStream(out);
        GZIPOutputStream gzip = new GZIPOutputStream(b64os);
        gzip.write(text.getBytes("UTF-8"));
        gzip.close();
        b64os.close();
        out.toString();
        return out.toString();
    }


    public String gzipToText(String gzipText) throws Exception{
        byte[] bytes=gzipText.getBytes();
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Base64InputStream b64is = new Base64InputStream(bis);
        GZIPInputStream gzis = new GZIPInputStream(b64is);
        BufferedReader br = new BufferedReader(new InputStreamReader(gzis, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        gzis.close();
        b64is.close();
        bis.close();
        return sb.toString().trim();
    }



}

