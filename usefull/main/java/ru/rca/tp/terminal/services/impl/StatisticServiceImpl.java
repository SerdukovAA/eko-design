package ru.rca.tp.terminal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rca.tp.terminal.dao.DkbmDAO;
import ru.rca.tp.terminal.controllers.bean.Metric;
import ru.rca.tp.terminal.services.StatisticService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by serdukovaa on 27.04.2016.
 */
@Service
public class StatisticServiceImpl implements StatisticService {

    @Autowired
    DkbmDAO dkbmDAO;


    public List<Metric> getAllMetrics(boolean cache){
        List<Metric> metrics  = new ArrayList<Metric>();

        Metric m = new Metric();
        m.setName("Среднее время обработки запроса КБМ");
        m.setRazmer("c.");
        m.setDate(new Date());
        if(cache){
            System.out.println("взять из кеша");
            m.setValue(dkbmDAO.getAvgKBMCache());
        }else{
            System.out.println("обновить кеш");
            m.setValue(dkbmDAO.refreshAvgKBMCache());
        }
        m.setControlValue(1.00);
        metrics.add(m);
        return  metrics;
    }





}
