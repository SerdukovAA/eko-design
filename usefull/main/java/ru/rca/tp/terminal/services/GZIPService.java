package ru.rca.tp.terminal.services;

/**
 * Created by serdukovaa on 01.12.2015.
 */
public interface GZIPService {

    public String textToGZIP(String text) throws Exception;
    public String gzipToText(String gzipText) throws Exception;

}

