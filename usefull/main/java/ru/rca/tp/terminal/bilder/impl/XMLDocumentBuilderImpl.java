package ru.rca.tp.terminal.bilder.impl;

import com.rsa.dkbm.ws.Attachment;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import ru.rca.tp.terminal.bilder.XMLDocumentBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * Created by serdukovaa on 28.10.2015.
 */
public class XMLDocumentBuilderImpl implements XMLDocumentBuilder {





    public Document getDocumentFromAtachement(Attachment attachment) throws Exception {
        try {
          //путь до файла корень /xml
          DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
          DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
          Document doc = dBuilder.parse(attachment.getData().getDataSource().getInputStream());
          doc.getDocumentElement().normalize();
          return doc;
      }catch (Exception e){
          e.printStackTrace();
          throw new Exception("Ошибка парсирования xml файла");
      }
    }

    public Document getDocumentFromAtachementGZIP(Attachment attachment) throws Exception {
    try {
            InputStream inputStream = attachment.getData().getDataSource().getInputStream();
            byte[] result =  IOUtils.toByteArray(inputStream);
            GZIPInputStream gzip = new GZIPInputStream(new ByteArrayInputStream(result));

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(gzip);

           doc.getDocumentElement().normalize();
           return doc;
      }catch (Exception e){
          e.printStackTrace();
          throw new Exception("Ошибка парсирования gzip xml файла");
      }
    }


}
