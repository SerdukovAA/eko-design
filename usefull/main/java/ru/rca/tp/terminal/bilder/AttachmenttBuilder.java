package ru.rca.tp.terminal.bilder;

import com.rsa.dkbm.ws.Attachment;
import org.w3c.dom.Document;

/**
 * Created by serdukovaa on 28.10.2015.
 */
public interface AttachmenttBuilder {

    public Attachment getAttachmentFromDocGZIP(Document document) throws Exception;
    public Attachment getAttachmentFromXmlGZIP(String xml) throws Exception;
}
