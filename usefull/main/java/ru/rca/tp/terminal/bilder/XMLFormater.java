package ru.rca.tp.terminal.bilder;

import org.w3c.dom.Document;

/**
 * Created by serdukovaa on 29.10.2015.
 */
public interface XMLFormater {
   public String formatDocument(Document document)throws Exception;
   public String formatStringXML(String xmlstring)throws Exception;
}
