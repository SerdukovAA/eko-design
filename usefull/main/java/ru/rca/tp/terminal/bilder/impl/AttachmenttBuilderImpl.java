package ru.rca.tp.terminal.bilder.impl;


import com.rsa.dkbm.ws.Attachment;
import com.sun.xml.internal.ws.util.ByteArrayDataSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.rca.tp.terminal.bilder.AttachmenttBuilder;


import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.GZIPOutputStream;

/**
 * Created by serdukovaa on 28.10.2015.
 */
public class AttachmenttBuilderImpl implements AttachmenttBuilder {







    /**
     * Считать attachment из файла
     * @param fileName
     * @return attachment
     */
    private Attachment readAttachmentFromFile(String fileName){
        Attachment attachment=null;
        try {
            byte[] bytes = Files.readAllBytes(Paths.get("folderName", fileName));
            ByteArrayDataSource byteArrayDataSource =  new ByteArrayDataSource(bytes,null);
            attachment =  new Attachment();
            attachment.setData(new DataHandler(byteArrayDataSource));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return attachment;
    }





    public Attachment getAttachmentFromDocGZIP(Document document) throws Exception {
        Attachment attachment = new Attachment();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        elementToStream(document.getDocumentElement(), baos);

        byte[] dataToCompress = baos.toByteArray();

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(dataToCompress.length);

        GZIPOutputStream zipStream = new GZIPOutputStream(byteStream);
        zipStream.write(dataToCompress);
        zipStream.close();
        byteStream.close();
        byte[] compressedData = byteStream.toByteArray();
        DataSource ds = new ByteArrayDataSource(compressedData, "text/plain; charset=UTF-8");
        attachment.setData(new DataHandler(ds));
        return attachment;
    }

    public Attachment getAttachmentFromXmlGZIP(String xml) throws Exception {
        Attachment attachment = new Attachment();
        byte[] dataToCompress = xml.getBytes("UTF-8");
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(dataToCompress.length);
        GZIPOutputStream zipStream = new GZIPOutputStream(byteStream);
        zipStream.write(dataToCompress);
        zipStream.close();
        byteStream.close();
        byte[] compressedData = byteStream.toByteArray();
        DataSource ds = new ByteArrayDataSource(compressedData, "text/plain; charset=UTF-8");
        attachment.setData(new DataHandler(ds));
        return attachment;
    }




    private void elementToStream(Element element, OutputStream out) {
        try {
            DOMSource source = new DOMSource(element);
            StreamResult result = new StreamResult(out);
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();
            transformer.transform(source, result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
