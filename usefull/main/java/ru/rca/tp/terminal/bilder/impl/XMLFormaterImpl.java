package ru.rca.tp.terminal.bilder.impl;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import org.w3c.dom.Document;
import ru.rca.tp.terminal.bilder.XMLFormater;

import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by serdukovaa on 29.10.2015.
 */
public class XMLFormaterImpl implements XMLFormater {

    public String formatDocument(Document document) throws Exception {

        Writer out = null;
        try {
            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);
            return out.toString();
        } finally {
            if (out != null) {
                out.close();
            }

        }
    }



    public String formatStringXML(String xmlstring)throws Exception{
        return  xmlstring.replaceAll("><", ">" + System.getProperty("line.separator") + "<");
    }
}
