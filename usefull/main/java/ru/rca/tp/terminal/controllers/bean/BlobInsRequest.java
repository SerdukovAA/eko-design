package ru.rca.tp.terminal.controllers.bean;

import ru.rca.tp.terminal.enums.DataBaseInstance;


/**
 * Created by serdukovaa on 02.12.2015.
 */
public class BlobInsRequest {

    private String requestId;
    private DataBaseInstance dataBaseInstance;
    private String blobColumnName;
    public enum RequestType{REQUEST, RESPONSE};
    private RequestType requestType;

    /*--------------------------getters and setters-----------------------------------------------*/
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public DataBaseInstance getDataBaseInstance() {
        return dataBaseInstance;
    }

    public void setDataBaseInstance(DataBaseInstance dataBaseInstance) {
        this.dataBaseInstance = dataBaseInstance;
    }

    public String getBlobColumnName() {
        return blobColumnName;
    }

    public void setBlobColumnName(String blobColumnName) {
        this.blobColumnName = blobColumnName;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

}
