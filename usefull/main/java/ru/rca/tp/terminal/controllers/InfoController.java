package ru.rca.tp.terminal.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.rca.tp.terminal.enums.DataBaseInstance;
import ru.rca.tp.terminal.enums.ServerEosagoInstance;
import ru.rca.tp.terminal.enums.ServerKBMInstance;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by serdukov on 28.09.15.
 */
@Controller
public class InfoController {


    private static final Logger log = Logger.getLogger(InfoController.class);


    @RequestMapping(value ="info_kbm_servers", method = RequestMethod.GET)
    public @ResponseBody  Object[]  getKBMServers(HttpServletRequest request) throws Exception {
        Object[] servers = ServerKBMInstance.values();
        return servers;
    }


    @RequestMapping(value ="info_eosago_servers", method = RequestMethod.GET)
    public @ResponseBody  Object[]  getEosagoServers(HttpServletRequest request) throws Exception {
        Object[] servers = ServerEosagoInstance.values();
        return servers;
    }

    @RequestMapping(value ="info_data_base_servers", method = RequestMethod.GET)
    public @ResponseBody  Object[] getDBServers(HttpServletRequest request) throws Exception {
        Object[] servers = DataBaseInstance.values();
        return servers;
    }


    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }


}
