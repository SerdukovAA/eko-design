package ru.rca.tp.terminal.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author Serdjukov A.A. 06.01.2015
 */

@Controller
public class InController {

    private static final Logger log = Logger.getLogger(InController.class);


    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String welcomePage() {
        return "redirect:/login";
    }


    @RequestMapping(value = {"index"}, method = RequestMethod.GET)
    public String general_page() {
        return "index";
    }




/////////////////////////////////////////////////боевые страницы
    //Spring Security see this :
    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Неверные логин или пароль.");
        }

        if (logout != null) {
            model.addObject("msg", "Возвращайтесь снова.");
        }
        model.setViewName("login");

        return model;

    }







}

