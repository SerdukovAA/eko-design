package ru.rca.tp.terminal.controllers.bean;

import ru.rca.tp.terminal.enums.ServerEosagoInstance;

/**
 * Created by serdukovaa on 04.05.2016.
 */
public class AsynRequestBase {

    private String xml;

    private String requestID;


    /*--------------------------getters and setters-----------------------------------------------*/

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }


}
