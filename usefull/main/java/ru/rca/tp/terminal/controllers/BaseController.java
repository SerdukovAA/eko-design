package ru.rca.tp.terminal.controllers;


import com.rsa.dkbm.ws.Attachment;
import com.rsa.eosago.ws.impl.CheckSubjectOSAGOServiceImplService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rca.tp.terminal.bilder.AttachmenttBuilder;
import ru.rca.tp.terminal.bilder.XMLDocumentBuilder;
import ru.rca.tp.terminal.controllers.bean.EosagoRequest;
import ru.rca.tp.terminal.bilder.XMLFormater;
import ru.rca.tp.terminal.controllers.bean.KBMRequest;
import ru.rca.tp.terminal.handler.SimpleWsAuthHandler;
import ru.rca.tp.terminal.services.Base64Service;
import ru.rca.tp.terminal.services.GZIPService;
import ru.rca.tp.terminal.services.RequestDkbm;
import ru.rca.tp.terminal.services.RequestEosago;
import ru.rca.tp.terminal.services.loaders.KBMRequestLoader;

import java.io.ByteArrayInputStream;
import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import javax.activation.DataSource;
import java.io.IOException;
import java.net.URL;

/**
 * Created by serdukov on 28.09.15.
 */
@Controller
public class BaseController {



    @Autowired
    GZIPService gzipService;
    @Autowired
    XMLFormater xmlFormater;

    @Autowired
    Base64Service base64Service;

    @Autowired
    RequestDkbm requestDkbm;

    @Autowired
    RequestEosago requestEosago;

    private static final Logger log = Logger.getLogger(BaseController.class);

    @RequestMapping(value ="text_to_gzip", method = RequestMethod.POST)
    public @ResponseBody String  doTextToGzip( @RequestParam("text") String text) throws Exception {
        try {
           return gzipService.textToGZIP(text.trim());
        }catch(IOException e) {
            e.printStackTrace();
            log.error("ExceptionHandler description: ", e);
            return "не удалось";
        }
    }


    @RequestMapping(value ="gzip_to_text", method = RequestMethod.POST)
    public @ResponseBody String  doGzipToText( @RequestParam("hash") String hash) throws Exception {
        try {
            return gzipService.gzipToText(hash.trim());
        }
        catch(IOException e) {
            e.printStackTrace();
            log.error("ExceptionHandler description: ", e);
            return "не удалось";
        }
    }

    @RequestMapping(value ="base_to_text", method = RequestMethod.POST)
    public @ResponseBody String doBaseToText ( @RequestParam("hash") String hash) throws Exception {
        try {
            return base64Service.base64ToText(hash.trim());
        } catch(IOException e) {
            e.printStackTrace();
            log.error("ExceptionHandler description: ", e);
            return "не удалось";
        }
    }


    @RequestMapping(value ="text_to_base", method = RequestMethod.POST)
    public @ResponseBody String  doTextToBase( @RequestParam("text") String text) throws Exception {
        try {
             return base64Service.textToBase64(text.trim());
        }catch(IOException e) {
            e.printStackTrace();
            log.error("ExceptionHandler description: ", e);
            return "не удалось";
        }
    }


/**
 * Запрос КБМ по xml
 * @param kbmRequest
 * @return
 * @throws Exception
 */

    @RequestMapping(value ="kbm_from_xml", method = RequestMethod.POST)
    public @ResponseBody String  doKBMFromXML( @RequestBody KBMRequest kbmRequest) throws Exception {
        try {
            if(kbmRequest.getXml() == null ||kbmRequest.getXml().isEmpty()) return "Передан пустой запрос";
            SimpleWsAuthHandler.setPasswordStr(kbmRequest.getServerInstance().getPassword());
            SimpleWsAuthHandler.setUsernameStr(kbmRequest.getServerInstance().getLogin());
            return requestDkbm.requestKbm(kbmRequest);
        }catch(Exception e) {
            e.printStackTrace();
            log.error("ExceptionHandler description: ", e);
            return "не удалось";
        }
    }


    /**
     * Запрос Проверка водителей по е_osago xml
     * @param eosagoRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value ="eosago_check_driver_from_xml", method = RequestMethod.POST)
    public @ResponseBody String  doCheckDriver( @RequestBody EosagoRequest eosagoRequest) throws Exception {
            if(eosagoRequest.getXml() == null ||eosagoRequest.getXml().isEmpty()) return "Передан пустой запрос";
            SimpleWsAuthHandler.setPasswordStr(eosagoRequest.getServerInstance().getPassword());
            SimpleWsAuthHandler.setUsernameStr(eosagoRequest.getServerInstance().getLogin());
            return  requestEosago.successDriverCheckLoad(eosagoRequest);
    }


    @RequestMapping(value ="eosago_check_owner_from_xml", method = RequestMethod.POST)
    public @ResponseBody String  ownerStatusRequest( @RequestBody EosagoRequest eosagoRequest) throws Exception {
            if(eosagoRequest.getXml() == null ||eosagoRequest.getXml().isEmpty()) return "Передан пустой запрос";
            SimpleWsAuthHandler.setPasswordStr(eosagoRequest.getServerInstance().getPassword());
            SimpleWsAuthHandler.setUsernameStr(eosagoRequest.getServerInstance().getLogin());
            return  requestEosago.successInsurerOwnerCheckLoad(eosagoRequest);
    }



    @RequestMapping(value ="eosago_check_ts_from_xml", method = RequestMethod.POST)
    public @ResponseBody String  doCheckTS( @RequestBody EosagoRequest eosagoRequest) throws Exception {
            if(eosagoRequest.getXml() == null ||eosagoRequest.getXml().isEmpty()) return "Передан пустой запрос";
            SimpleWsAuthHandler.setPasswordStr(eosagoRequest.getServerInstance().getPassword());
            SimpleWsAuthHandler.setUsernameStr(eosagoRequest.getServerInstance().getLogin());
            return  requestEosago.successTSCheckLoad(eosagoRequest);
    }





    @RequestMapping(value ="eosago_owner_status_request", method = RequestMethod.POST)
    public @ResponseBody String doStatusOwnerCheck( @RequestBody EosagoRequest eosagoRequest) throws Exception {
        if(eosagoRequest.getRequestID() == null ||eosagoRequest.getRequestID().isEmpty()) return "Передан пустой запрос";
        SimpleWsAuthHandler.setPasswordStr(eosagoRequest.getServerInstance().getPassword());
        SimpleWsAuthHandler.setUsernameStr(eosagoRequest.getServerInstance().getLogin());
        return  requestEosago.ownerStatusCheck(eosagoRequest);
    }


    @RequestMapping(value ="eosago_ts_status_request", method = RequestMethod.POST)
    public @ResponseBody String doStatusTsCheck( @RequestBody EosagoRequest eosagoRequest) throws Exception {
        if(eosagoRequest.getRequestID() == null ||eosagoRequest.getRequestID().isEmpty()) return "Передан пустой запрос";
        SimpleWsAuthHandler.setPasswordStr(eosagoRequest.getServerInstance().getPassword());
        SimpleWsAuthHandler.setUsernameStr(eosagoRequest.getServerInstance().getLogin());
        return  requestEosago.tsStatusCheck(eosagoRequest);
    }


    @RequestMapping(value ="eosago_driver_status_request", method = RequestMethod.POST)
    public @ResponseBody String doStatusDriverCheck( @RequestBody EosagoRequest eosagoRequest) throws Exception {
        if(eosagoRequest.getRequestID() == null ||eosagoRequest.getRequestID().isEmpty()) return "Передан пустой запрос";
        SimpleWsAuthHandler.setPasswordStr(eosagoRequest.getServerInstance().getPassword());
        SimpleWsAuthHandler.setUsernameStr(eosagoRequest.getServerInstance().getLogin());
        return requestEosago.driverStatusCheck(eosagoRequest);
    }



    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }

}
