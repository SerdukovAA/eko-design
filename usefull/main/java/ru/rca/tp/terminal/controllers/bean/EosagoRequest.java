package ru.rca.tp.terminal.controllers.bean;


import ru.rca.tp.terminal.enums.ServerEosagoInstance;

/**
 * Created by serdukovaa on 02.12.2015.
 */
public class EosagoRequest extends AsynRequestBase {

    private ServerEosagoInstance serverInstance;

    /*--------------------------getters and setters-----------------------------------------------*/
    public ServerEosagoInstance getServerInstance() {
        return serverInstance;
    }

    public void setServerInstance(ServerEosagoInstance serverInstance) {
        this.serverInstance = serverInstance;
    }

}
