package ru.rca.tp.terminal.controllers.bean;

import java.util.Date;

/**
 * Created by serdukovaa on 27.04.2016.
 */
public class Metric {


    private String name;
    private String razmer;
    private Date date;
    private Double value;
    private Double controlValue;


    /*----------------------getters and setters--------------------------------------------*/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getControlValue() {
        return controlValue;
    }

    public void setControlValue(Double controlValue) {
        this.controlValue = controlValue;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getRazmer() {
        return razmer;
    }

    public void setRazmer(String razmer) {
        this.razmer = razmer;
    }

}
