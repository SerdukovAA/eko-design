package ru.rca.tp.terminal.controllers;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.rca.tp.terminal.bilder.XMLFormater;
import ru.rca.tp.terminal.controllers.bean.BlobInsRequest;
import ru.rca.tp.terminal.controllers.bean.KBMRequest;
import ru.rca.tp.terminal.handler.SimpleWsAuthHandler;
import ru.rca.tp.terminal.services.*;

import java.io.IOException;

/**
 * Created by serdukov on 28.09.15.
 */
@Controller
public class BlobController {


    @Autowired
    InsRequestService insRequestService;


    private static final Logger log = Logger.getLogger(BlobController.class);

    @RequestMapping(value ="get_blob_xml", method = RequestMethod.POST)
    public @ResponseBody String  doKBMFromXML(@RequestBody BlobInsRequest blobInsRequest) throws Exception {
        try {
            return insRequestService.getRequesrBlob(blobInsRequest);
        }catch(Exception e) {
            e.printStackTrace();
            log.error("ExceptionHandler description: ", e);
            return "не удалось";
        }
    }


    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }

}
