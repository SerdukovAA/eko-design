package ru.rca.tp.terminal.controllers;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.rca.tp.terminal.controllers.bean.Metric;
import ru.rca.tp.terminal.services.StatisticService;


import java.util.List;

/**
 * Created by serdukov on 28.09.15.
 */
@Controller
public class MetricController {



    @Autowired
    StatisticService statisticService;


    private static final Logger log = Logger.getLogger(MetricController.class);

    @RequestMapping(value ="get_statistic_dikbm", method = RequestMethod.GET)
    public @ResponseBody List<Metric> getStatistic() throws Exception {
        return statisticService.getAllMetrics(true);
    }

    @RequestMapping(value ="refresh_statistic_dikbm", method = RequestMethod.GET)
    public @ResponseBody List<Metric> refreshStatistic() throws Exception {
        return statisticService.getAllMetrics(false);
    }



    @ExceptionHandler(Exception.class)
    public void handleError(Exception exception) {
        exception.printStackTrace();
        log.error("ExceptionHandler description: ", exception);
    }

}
