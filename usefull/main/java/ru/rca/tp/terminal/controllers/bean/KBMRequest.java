package ru.rca.tp.terminal.controllers.bean;

import ru.rca.tp.terminal.enums.ServerKBMInstance;

/**
 * Created by serdukovaa on 02.12.2015.
 */
public class KBMRequest {

    private String xml;

    private ServerKBMInstance serverInstance;


    /*--------------------------getters and setters-----------------------------------------------*/

    public ServerKBMInstance getServerInstance() {
        return serverInstance;
    }

    public void setServerInstance(ServerKBMInstance serverInstance) {
        this.serverInstance = serverInstance;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

}
