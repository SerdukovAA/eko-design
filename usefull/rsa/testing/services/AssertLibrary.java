package ru.rsa.testing.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;
import ru.rsa.testing.xmlutils.XMLFormater;
import ru.rsa.testing.xmlutils.XMLParser;

import static org.junit.Assert.*;

/**
 * Created by serdukovaa on 21.03.2016.
 */
@Service
public class AssertLibrary {

    @Autowired
    XMLParser xmlParser;



    private final String PROJECT_STATUS_SUCCES_LOAD_WITH_CODE = "/PolicyEOSAGOStatusResponse/ErrorList/ErrorInfo[1]/Code";







    /**
     * Проверяется, что ответ от ДиКБМ подтверждает валидность запроса XML
     * @param doc
     * @throws Exception
     */
    public void validPoliceLoadRequest(Document doc) throws Exception{
        System.out.println(XMLFormater.formatDocumentToString(doc));
        String code = xmlParser.gVal(POLICE_SUCCES_LOAD, doc);
        assertTrue(code.equals("3") || code.equals("2") || code.equals("1"));

    }


    public void validLossLoadRequest(Document doc) throws Exception{
        System.out.println(XMLFormater.formatDocumentToString(doc));
        String code = xmlParser.gVal(LOSS_SUCCES_LOAD, doc);
        assertTrue(code.equals("3") || code.equals("2") || code.equals("1"));
    }

    /**
     * Проверка сохраненного ответа - policyStatusRequest, что он успешен
     * @param messageExchange
     * @throws Exception
     */
    public void policySuccesLoad(MessageExchange messageExchange) throws Exception{
        policySuccesLoadWithCode(messageExchange,3);
    }

    /**
     * Проверка сохраненного ответа - policyStatusRequest, что он успешен и статус
     * @param messageExchange
     * @param codes - ожидаемые статусы в порядке следования
     * @throws Exception
     */
    public void policySuccesLoadWithCode(MessageExchange messageExchange, int... codes) throws Exception{
        Document document = messageExchange.getResponse("policyStatusResponse");
        System.out.println(XMLFormater.formatDocumentToString(document));
        assertEquals("Обработан успешно ","3", xmlParser.gVal(POLICE_STATUS_SUCCES_LOAD,document));

        int i = 1;
        for(int c : codes){
            assertEquals("Ожидается загрузка полиса с кодом "+c,""+c, document.getElementsByTagName("Code").item(i).getTextContent());
            i++;
        }


    }


    public void checkTsWithCode(MessageExchange messageExchange,  int... codes) throws Exception{
        Document document = messageExchange.getResponse("tsStatusResponse");
        System.out.println(XMLFormater.formatDocumentToString(document));
        int i = 0;
        for(int c : codes){
            assertEquals("Ожидается проверка ТС  с кодом "+c,""+c, document.getElementsByTagName("Code").item(i).getTextContent());
            i++;
        }
    }



    public void lossSuccesLoadWithCode(MessageExchange messageExchange,  int... codes) throws Exception{
        Document document = messageExchange.getResponse("lossStatusResponse");
        System.out.println(XMLFormater.formatDocumentToString(document));
        int i = 1;
        for(int c : codes){
            assertEquals("Ожидается загрузка полиса с кодом "+c,""+c, document.getElementsByTagName("Code").item(i).getTextContent());
            i++;
        }
    }


    /**
     * Провервка используется при запросе статус и возвращает true  если вернулся не статус об ожидании
     * @param doc
     * @return
     * @throws Exception
     */
    public boolean asserPoliceStatusReady(Document doc) throws Exception{
        //проверить условие
        System.out.println("Проверяем, что статус полиса уже не в обработке");
        System.out.println(XMLFormater.formatDocumentToString(doc));
        String code1 =  xmlParser.gVal(STATUS_POLICY_READY_1, doc);
        String code2 =  xmlParser.gVal(STATUS_POLICY_READY_2, doc);
        if(code1.equals("1") || code1.equals("2") || code2.equals("1") || code2.equals("2")){
            return false;
        }else{
            return true;
        }
    }

    public boolean asserLossStatusReady(Document doc) throws Exception{
        //проверить условие
        System.out.println("Проверяем, что статус убытка уже не в обработке");
        System.out.println(XMLFormater.formatDocumentToString(doc));
        String code1 =  xmlParser.gVal(STATUS_LOSS_READY_1, doc);
        String code2 =  xmlParser.gVal(STATUS_LOSS_READY_2, doc);
        if(code1.equals("1") || code1.equals("2") || code2.equals("1") || code2.equals("2")){
            return false;
        }else{
            return true;
        }
    }





    public void validCheckLoadRequest(Document doc) throws Exception{
        System.out.println(XMLFormater.formatDocumentToString(doc));
        String code =  doc.getElementsByTagName("Code").item(0).getTextContent();
        assertTrue(code.equals("3") || code.equals("2") || code.equals("1"));
    }





    public boolean asserCheckStatusReady(Document responseDoc) throws Exception{
        //проверить условие
        System.out.println("Проверяем, что уже не в обработке");
        System.out.println(XMLFormater.formatDocumentToString(responseDoc));
        Node code =   responseDoc.getElementsByTagName("Code").item(0);
        String codeID = code.getTextContent();
        if(codeID.equals("1")||codeID.equals("2")){
            return false;
        }else{
            return true;
        }
    }



    /**
     * Проверяется результаты на запросы КБМ.
     * @param messageExchange передать тело ответ на запрос КБМ "kbmRequestByFileName"
     * @param kbm - ожидаемый ответ
     * @throws Exception
     */
    public void validKbmRequest(MessageExchange messageExchange, int kbm) throws Exception{
        assertEquals("Возврат КБМ "+kbm,""+kbm, xmlParser.gVal(CALC_RESPONSE_POLICY_KBM, messageExchange.getResponse("kbmRequestByFileName")));
        assertEquals("Запрос валидный ","3", xmlParser.gVal(CALC_RESPONSE_VALID_REQUEST, messageExchange.getResponse("kbmRequestByFileName")));
    }




    public MessageExchange projectSuccesLoadWithCode(MessageExchange messageExchange, int... codes) throws Exception{
        Document document = messageExchange.getResponse("projectStatusResponse");
        System.out.println(XMLFormater.formatDocumentToString(document));
        int i = 0;
        for(int c : codes){
            assertEquals("Ожидается загрузка проекта с кодом "+c,""+c, document.getElementsByTagName("Code").item(i).getTextContent());
            i++;
        }
        return messageExchange;
    }








    public void validEosagoLoadRequest(Document doc) throws Exception{
        System.out.println("Ответ получен. Проверяем, что ЕОСАГО подтвердило  xml ");
        System.out.println(XMLFormater.formatDocumentToString(doc));
        String code =  doc.getElementsByTagName("Code").item(0).getTextContent();
        assertTrue(code.equals("3") || code.equals("2") || code.equals("1"));
    }




    public void policyKaskoSuccesLoad(MessageExchange messageExchange) throws Exception{
        policyKaskoLoadWithCode(messageExchange,3);
    }


    public void policyKaskoLoadWithCode(MessageExchange messageExchange, int... codes) throws Exception{
        Document document = messageExchange.getResponse("kaskoStatusResponse");
        System.out.println(XMLFormater.formatDocumentToString(document));
        int i = 0;
        for(int c : codes){
            assertEquals("Ожидается загрузка полиса с кодом "+c,""+c, document.getElementsByTagName("Code").item(i).getTextContent());
            i++;
        }


    }

    public void validKaskoLoadRequest(Document document) throws Exception{
        System.out.println(XMLFormater.formatDocumentToString(document));
        String code =  document.getElementsByTagName("Code").item(0).getTextContent();
        assertTrue(code.equals("3") || code.equals("2") || code.equals("1"));
    }



}
