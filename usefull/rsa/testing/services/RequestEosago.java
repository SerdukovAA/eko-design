package ru.rsa.testing.services;

import com.rsa.dkbm.ws.Attachment;
import com.rsa.dkbm.ws.impl.KbmToService;
import com.rsa.dkbm.ws.impl.PolicyLossService;

import com.rsa.eosago.ws.impl.CheckSubjectOSAGOService;
import com.rsa.eosago.ws.impl.ProjectPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;
import ru.rsa.testing.bilder.AttachmenttBuilder;
import ru.rsa.testing.bilder.XMLDocumentBuilder;
import ru.rsa.testing.services.loaders.*;
import ru.rsa.testing.xmlutils.XMLFormater;

import java.util.Date;

/**
 * Created by serdukovaa on 18.03.2016.
 */
@Service
public class RequestEosago
{


    @Autowired
    DriverCheckEOSAGOLoader driverCheckEOSAGOLoader;

    @Autowired
    TsCheckEOSAGOLoader tsCheckEOSAGOLoader;

    @Autowired
    InsurerCheckEOSAGOLoader insurerCheckEOSAGOLoader;


    @Autowired
    ProjectEOSAGOLoader projectEOSAGOLoader;


    @Autowired
    ProjectStatusEOSAGOLoader projectStatusEOSAGOLoader;




    public MessageExchange successDriverCheckLoad(String filePath, MessageExchange messageExchange) throws Exception{
        messageExchange = driverCheckEOSAGOLoader.loadRequestByFileName(filePath, messageExchange);
        return driverCheckEOSAGOLoader.statusRequest(messageExchange);
    }

    public MessageExchange successTSCheckLoad(String filePath, MessageExchange messageExchange) throws Exception{
        messageExchange = tsCheckEOSAGOLoader.loadRequestByFileName(filePath, messageExchange);
        return tsCheckEOSAGOLoader.statusRequest(messageExchange);
    }


    public MessageExchange successInsurerOwnerCheckLoad(String filePath, MessageExchange messageExchange) throws Exception{
        messageExchange = insurerCheckEOSAGOLoader.loadRequestByFileName(filePath, messageExchange);
        return insurerCheckEOSAGOLoader.statusRequest(messageExchange);
    }



    public MessageExchange successProjectkLoad(String filePath, MessageExchange messageExchange) throws Exception{
        messageExchange = projectEOSAGOLoader.loadRequestByFileName(filePath, messageExchange);
        return projectEOSAGOLoader.statusRequest(messageExchange);
    }

    public MessageExchange successLoadAcytiveStatusProjectLoad(MessageExchange messageExchange) throws Exception{
        messageExchange = projectStatusEOSAGOLoader.loadRequestByFileName("test/eosago/templates/status_request.xml", messageExchange);
        return projectStatusEOSAGOLoader.statusRequest(messageExchange);
    }


    public MessageExchange successLoadStatusProjectLoad(String filePath, MessageExchange messageExchange) throws Exception{
        messageExchange = projectStatusEOSAGOLoader.loadRequestByFileName(filePath, messageExchange);
        return projectStatusEOSAGOLoader.statusRequest(messageExchange);
    }


}
