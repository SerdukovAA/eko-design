package ru.rsa.testing.services.loaders;

import com.rsa.dkbm.ws.Attachment;
import com.rsa.eosago.ws.impl.CheckSubjectOSAGOService;
import com.rsa.eosago.ws.impl.ObjectExistsException;
import com.rsa.eosago.ws.impl.PolicyLossProcessException;
import com.rsa.eosago.ws.impl.ProjectPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;

import java.util.Date;

/**
 * Created by serdukovaa on 06.04.2016.
 */
@Service
public class ProjectStatusEOSAGOLoader extends AsynRequestLoader {



    @Autowired
    ProjectPolicyService projectPolicyService;


    protected String getRECALL_TEMPLATE_PATH(){
        return "";
    }
    protected String getRECALL_LOG(){
        return "";
    }
    protected String getLOAD_REQUEST_LOG(){
        return  "Запрос на проверку загрузку проекта Е_осаго";
    }
    protected String getSTATUS_LOG(){
        return  "Запрос статуса загрузкит проекта Е_осаго";
    }




    @Override
    protected String builStatusRequest(MessageExchange messageExchange){
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
                .append("<ns2:StatusPolicyEOSAGOStatusRequest xsi:schemaLocation=\"com/rsa/eosago/schema-1.1 StatusPolicyEOSAGOStatusRequest.xsd\" xmlns:ns2=\"com/rsa/eosago/schema-1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">")
                .append("<StatusPolicyTitle>")
                .append("<InsurerID>99700000</InsurerID>")
                .append("<TripNumber>").append(messageExchange.getParameter("TripNumber")).append("</TripNumber>")
                .append("<TripTm>").append(messageExchange.getParameter("TripTm")).append("</TripTm>")
                .append("</StatusPolicyTitle>")
                .append("<StatusPolicyImportId>").append(messageExchange.getParameter("StatusPolicyImportId")).append("</StatusPolicyImportId>")
                .append("</ns2:StatusPolicyEOSAGOStatusRequest>");
        return sb.toString();
    }




    protected void preHandleLoadRequest(Document requsetDocument, MessageExchange messageExchange){
        fillTrips(requsetDocument, messageExchange);
        fiilDrafStatus(requsetDocument, messageExchange);
    }

    private void fiilDrafStatus(Document requsetDocument, MessageExchange messageExchange){
        Node draftPolicyID = requsetDocument.getElementsByTagName("DraftPolicyID").item(0);
        draftPolicyID.setTextContent(messageExchange.getParameter("DraftPolicyID"));
        Node draftPolicyNumberKey = requsetDocument.getElementsByTagName("DraftPolicyNumberKey").item(0);
        draftPolicyNumberKey.setTextContent(messageExchange.getParameter("DraftPolicyNumberKey"));
    }



    @Override
    protected void memoryLoad(Document requsetDocument, MessageExchange messageExchange){
        return;
    }
    @Override
    protected  void recallLoadHandle(Document requsetDocument, MessageExchange messageExchange, Document recalableLoadDocumnet){
        return;
    }

    protected  Attachment serviceLoadGZIP(Attachment request)  throws Exception {
        return  projectPolicyService.setPolicyStatus(request);
    }

    protected  Attachment serviceStatusGZIP(Attachment request){
        return  projectPolicyService.getSetStatusResult(request);
    }

    protected void memoryResponse(Document documentResponse, MessageExchange messageExchange){
        Node statusPolicyImportId =  documentResponse.getElementsByTagName("StatusPolicyImportId").item(0);
        messageExchange.addParameter("StatusPolicyImportId",statusPolicyImportId.getTextContent());
    }

    protected void validateResponse(Document documentResponse) throws Exception {
        assertLibrary.validEosagoLoadRequest(documentResponse);
    }

    protected boolean handelStatusResponse(Document responseStatusDoc, MessageExchange messageExchange) throws Exception {
        messageExchange.addResponse("statusStatusResponse",responseStatusDoc);
        return assertLibrary.asserCheckStatusReady(responseStatusDoc);
    }



}
