package ru.rsa.testing.services.loaders;

import com.rsa.dkbm.ws.Attachment;
import com.rsa.eosago.ws.impl.CheckSubjectOSAGOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;

/**
 * Created by serdukovaa on 06.04.2016.
 */
@Service
public class InsurerCheckEOSAGOLoader extends AsynRequestLoader {



    @Autowired
    CheckSubjectOSAGOService checkSubjectOSAGOService;




    protected String getRECALL_TEMPLATE_PATH(){
        return "";
    }
    protected String getRECALL_LOG(){
        return "";
    }
    protected String getLOAD_REQUEST_LOG(){
        return  "Запрос на проверку INSURER";
    }
    protected String getSTATUS_LOG(){
        return  "Запрос статуса проверки INSURER";
    }




    @Override
    protected String builStatusRequest(MessageExchange messageExchange){
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ns2:InsurerOwnerStatusRequest xmlns:ns2=\"com/rsa/eosago/schema-1.1\">")
                .append("<InsurerID>99700000</InsurerID>")
                .append("<IDCheckInsurerOwner>").append(messageExchange.getParameter("IDCheckInsurerOwner")).append("</IDCheckInsurerOwner>")
                .append("</ns2:InsurerOwnerStatusRequest>");
        return sb.toString();
    }


    @Override
    protected void memoryLoad(Document requsetDocument, MessageExchange messageExchange){
      return;
    }
    @Override
    protected  void recallLoadHandle(Document requsetDocument, MessageExchange messageExchange, Document recalableLoadDocumnet){
      return;
    }

    protected void preHandleLoadRequest(Document requsetDocument, MessageExchange messageExchange){
        return;
    }

    protected  Attachment serviceLoadGZIP(Attachment request) throws Exception{
        return  checkSubjectOSAGOService.loadInsurerOwner(request);
    }

    protected  Attachment serviceStatusGZIP(Attachment request){
        return  checkSubjectOSAGOService.getInsurerOwnerStatus(request);
    }

    protected void memoryResponse(Document documentResponse, MessageExchange messageExchange){
        Node idCheck =   documentResponse.getElementsByTagName("IDCheckInsurerOwner").item(0);
        messageExchange.addParameter("IDCheckInsurerOwner",idCheck.getTextContent());
    }

    protected void validateResponse(Document documentResponse) throws Exception {
        assertLibrary.validCheckLoadRequest(documentResponse);
    }

    protected boolean handelStatusResponse(Document responseStatusDoc, MessageExchange messageExchange) throws Exception {
        messageExchange.addResponse("insurerOwnerStatusResponse",responseStatusDoc);
        return assertLibrary.asserCheckStatusReady(responseStatusDoc);
    }



}
