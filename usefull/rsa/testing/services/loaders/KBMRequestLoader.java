package ru.rsa.testing.services.loaders;

import com.rsa.dkbm.ws.Attachment;
import com.rsa.dkbm.ws.impl.KbmToService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import ru.rsa.testing.beans.MessageExchange;

/**
 * Created by serdukovaa on 06.04.2016.
 */
@Service
public class KBMRequestLoader extends SynhRequestLoader {


    @Autowired
    KbmToService kbmToService;

    protected String getRequestName() {
        return "Запрос КБМ";
    }


    protected Attachment synhRequest(Attachment request) {
        return kbmToService.getKbmTo(request);
    }


    protected void responseHandler(MessageExchange messageExchange, Document documentResponse) {
        messageExchange.addResponse("kbmRequestByFileName", documentResponse);
    }

}
