package ru.rsa.testing.services.loaders;

import com.rsa.bsi.ws.impl.PolicyLossKaskoService;
import com.rsa.dkbm.ws.Attachment;
import com.rsa.eosago.ws.impl.ProjectPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;
import ru.rsa.testing.xmlutils.XMLFormater;

import java.util.Date;

/**
 * Created by serdukovaa on 06.04.2016.
 */
@Service
public class PolicyKaskoLoader extends AsynRequestLoader {


    @Autowired
    PolicyLossKaskoService policyLossKaskoService;


    protected String getRECALL_TEMPLATE_PATH(){
        return "test/bsi/templates/bsi_recall.xml";
    }
    protected String getRECALL_LOG(){
        return "Отзыв полиса КАСКО";
    }
    protected String getLOAD_REQUEST_LOG(){
        return  "Запрос на  загрузку полиса КАСКО";
    }
    protected String getSTATUS_LOG(){
        return  "Запрос статуса загрузки полиса КАСКО";
    }



    protected void validateResponse(Document documentResponse) throws Exception {
        assertLibrary.validKaskoLoadRequest(documentResponse);
    }

    protected void memoryResponse(Document documentResponse, MessageExchange messageExchange) throws Exception {
        System.out.println(XMLFormater.formatDocumentToString(documentResponse));
    }



    protected  Attachment serviceLoadGZIP(Attachment request) throws Exception{
        return  policyLossKaskoService.loadPolicyKasko(request);
    }

    protected  Attachment serviceStatusGZIP(Attachment request){

        return  policyLossKaskoService.getPolicyKaskoStatus(request);
    }



    @Override
    protected String builStatusRequest(MessageExchange messageExchange){
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
                .append("<ns2:PolicyKASKOStatusRequest xmlns:ns2=\"com/rsa/kasko/schema-1.1\">")
                .append("<PolicyTitle>")
                .append("<InsurerID>99700000</InsurerID>")
                .append("<TripNumber>").append(messageExchange.getParameter("TripNumber")).append("</TripNumber>")
                .append("<TripTm>").append(messageExchange.getParameter("TripTm")).append("</TripTm>")
                .append("</PolicyTitle>")
                .append("</ns2:PolicyKASKOStatusRequest>");
        return sb.toString();
    }



    protected void preHandleLoadRequest(Document requsetDocument, MessageExchange messageExchange){
        fillTrips(requsetDocument, messageExchange);
    }




    @Override
    protected void memoryLoad(Document requsetDocument, MessageExchange messageExchange){
        messageExchange.addKaskoPolicy(requsetDocument);
        //подразумевается что только для первой загрузки записываем
        if(messageExchange.getParameter("PolicyID") == null) {
            Node policyID = requsetDocument.getElementsByTagName("PolicyID").item(0);
            messageExchange.addParameter("PolicyID",policyID.getTextContent());
        }
        if(messageExchange.getParameter("DateActionEnd") == null) {
            Node dateActionEnd = requsetDocument.getElementsByTagName("DateActionEnd").item(0);
            messageExchange.addParameter("DateActionEnd", dateActionEnd.getTextContent());
        }
    }


    @Override
    protected  void recallLoadHandle(Document requsetDocument, MessageExchange messageExchange, Document recalableLoadDocumnet){
        fillTrips(requsetDocument, messageExchange);
        Node policyID = requsetDocument.getElementsByTagName("PolicyID").item(0);
        policyID.setTextContent(recalableLoadDocumnet.getElementsByTagName("PolicyID").item(0).getTextContent());
        Node dateRevision = requsetDocument.getElementsByTagName("DateRevision").item(0);
        dateRevision.setTextContent(recalableLoadDocumnet.getElementsByTagName("DateActionEnd").item(0).getTextContent());
    }


    protected boolean handelStatusResponse(Document responseStatusDoc, MessageExchange messageExchange) throws Exception {
        messageExchange.addResponse("kaskoStatusResponse",responseStatusDoc);
        return assertLibrary.asserCheckStatusReady(responseStatusDoc);
    }



}
