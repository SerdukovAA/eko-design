package ru.rsa.testing.services.loaders;

import com.rsa.dkbm.ws.Attachment;
import com.rsa.dkbm.ws.impl.PolicyLossService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;

/**
 * Created by serdukovaa on 06.04.2016.
 */
@Service
public class LossOSAGOLoader extends AsynRequestLoader {


    @Autowired
    PolicyLossService policyLossService;

    protected String getRECALL_TEMPLATE_PATH(){
        return "test/dkbm/templates/loss_recall.xml";
    }
    protected String getRECALL_LOG(){
        return "Отзыв убытка ОСАГО";
    }
    protected String getLOAD_REQUEST_LOG(){
        return  "Запрос на загрузку убытка ОСАГО";
    }

    protected String getSTATUS_LOG(){
        return  "Запрос статуса загрузки убытка ОСАГО";
    }

    @Override
    protected String builStatusRequest(MessageExchange messageExchange){
        StringBuilder sb = new StringBuilder();
        String tripNumberID = messageExchange.getParameter("TripNumber");
        String tripTmID =  messageExchange.getParameter("TripTm");
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><rsa:LossStatusRequest xmlns:schemaLocation=\"com/rsa/dkbm/schema-1.5 LossStatusRequest.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"")
                .append(" xmlns:rsa=\"com/rsa/dkbm/schema-1.5\">")
                .append("<LossTitle>").append("<InsurerID>99700000</InsurerID>")
                .append("<TripNumber>").append(tripNumberID)
                .append("</TripNumber><TripTm>").append(tripTmID)
                .append("</TripTm></LossTitle></rsa:LossStatusRequest>");
        return sb.toString();
    }



    protected void memoryLoad(Document requsetDocument, MessageExchange messageExchange){
        messageExchange.addLoss(requsetDocument);
    }


    protected  Attachment serviceLoadGZIP(Attachment request) throws Exception{
        return   policyLossService.loadLoss(request);
    }

    protected  Attachment serviceStatusGZIP(Attachment request){
        return  policyLossService.getLossStatus(request);
    }

    protected void preHandleLoadRequest(Document requsetDocument, MessageExchange messageExchange){
        fillTrips(requsetDocument, messageExchange);

    }


    protected void memoryResponse(Document documentResponse, MessageExchange messageExchange){
        messageExchange.addResponse("lossLoadDocumentResponse",documentResponse);



    }

    protected void validateResponse(Document documentResponse) throws Exception {
        assertLibrary.validLossLoadRequest(documentResponse);
    }


    protected  void recallLoadHandle(Document requsetDocument, MessageExchange messageExchange, Document recalableLoadDocumnet){
        fillTrips(requsetDocument, messageExchange);

        Node lossID = requsetDocument.getElementsByTagName("LossID").item(0);
        lossID.setTextContent(recalableLoadDocumnet.getElementsByTagName("LossID").item(0).getTextContent());
        //todo  стоит более граммотно подставлять даты
        Node dateRevision = requsetDocument.getElementsByTagName("DateRevision").item(0);
        dateRevision.setTextContent(messageExchange.getParameter("DateActionEnd"));
    }


    protected boolean handelStatusResponse(Document responseStatusDoc, MessageExchange messageExchange) throws Exception {
        messageExchange.addResponse("lossStatusResponse",responseStatusDoc);
        return assertLibrary.asserLossStatusReady(responseStatusDoc);
    }



}
