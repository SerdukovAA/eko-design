package ru.rsa.testing.services.loaders;

import com.rsa.dkbm.ws.Attachment;
import com.rsa.eosago.ws.impl.CheckSubjectOSAGOService;
import com.rsa.eosago.ws.impl.ProjectPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;
import ru.rsa.testing.xmlutils.XMLFormater;

import java.util.Date;

/**
 * Created by serdukovaa on 06.04.2016.
 */
@Service
public class ProjectEOSAGOLoader extends AsynRequestLoader {


    @Autowired
    ProjectPolicyService projectPolicyService;


    protected String getRECALL_TEMPLATE_PATH(){
        return "";
    }
    protected String getRECALL_LOG(){
        return "";
    }
    protected String getLOAD_REQUEST_LOG(){
        return  "Запрос на  загрузку проекта Е_осаго";
    }
    protected String getSTATUS_LOG(){
        return  "Запрос статуса загрузкит проекта Е_осаго";
    }




    protected void validateResponse(Document documentResponse) throws Exception {
        assertLibrary.validEosagoLoadRequest(documentResponse);
    }

    protected void memoryResponse(Document documentResponse, MessageExchange messageExchange){
        Node idCheck =   documentResponse.getElementsByTagName("DraftPolicyImportId").item(0);
        messageExchange.addParameter("DraftPolicyImportId",idCheck.getTextContent());
    }



    protected  Attachment serviceLoadGZIP(Attachment request) throws Exception{
        return  projectPolicyService.loadPolicy(request);
    }

    protected  Attachment serviceStatusGZIP(Attachment request){
        return  projectPolicyService.getPolicyStatus(request);
    }



    @Override
    protected String builStatusRequest(MessageExchange messageExchange){
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
                .append("<ns2:PolicyEOSAGOStatusRequest xsi:schemaLocation=\"com/rsa/eosago/schema-1.1 PolicyEOSAGOStatusRequest.xsd\" xmlns:ns2=\"com/rsa/eosago/schema-1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">")
                .append("<PolicyTitle>")
                .append("<InsurerID>99700000</InsurerID>")
                .append("<TripNumber>").append(messageExchange.getParameter("TripNumber")).append("</TripNumber>")
                .append("<TripTm>").append(messageExchange.getParameter("TripTm")).append("</TripTm>")
                .append("</PolicyTitle>")
                .append("<DraftPolicyImportId>").append(messageExchange.getParameter("DraftPolicyImportId")).append("</DraftPolicyImportId>")
                .append("</ns2:PolicyEOSAGOStatusRequest>");
        return sb.toString();
    }



    protected void preHandleLoadRequest(Document requsetDocument, MessageExchange messageExchange){
        fillTrips(requsetDocument, messageExchange);
        fillDraftProject(requsetDocument, messageExchange);
        fillCheckID(requsetDocument, messageExchange);
    }


    private void fillDraftProject(Document requsetDocument, MessageExchange messageExchange){
        Date d = new Date();
        String tripNumberID = new Long(d.getTime()).toString();
        Node draftPolicyID = requsetDocument.getElementsByTagName("DraftPolicyID").item(0);
        draftPolicyID.setTextContent(tripNumberID);
        messageExchange.addParameter("DraftPolicyID",tripNumberID);
    }

    private void fillCheckID(Document requsetDocument, MessageExchange messageExchange){

        if(messageExchange.getParameter("IDCheckTS") != null) {
            Node iDCheckTS = requsetDocument.getElementsByTagName("IDCheckTS").item(0);
            iDCheckTS.setTextContent(messageExchange.getParameter("IDCheckTS"));
        }
        if(messageExchange.getParameter("IDCheckInsurerOwner") != null) {
            Node iDCheckInsurerOwner = requsetDocument.getElementsByTagName("IDCheckInsurerOwner").item(0);
            iDCheckInsurerOwner.setTextContent(messageExchange.getParameter("IDCheckInsurerOwner"));
        }

        if(messageExchange.getParameter("IDCheckInsurerOwner") != null) {
            Node iDCheckInsurerOwner2 = requsetDocument.getElementsByTagName("IDCheckInsurerOwner").item(1);
            iDCheckInsurerOwner2.setTextContent(messageExchange.getParameter("IDCheckInsurerOwner"));
        }

        if(messageExchange.getParameter("IDCheckDriver") != null){
            Node iDCheckDriver = requsetDocument.getElementsByTagName("IDCheckDriver").item(0);
            iDCheckDriver.setTextContent(messageExchange.getParameter("IDCheckDriver"));
        }

    }






    @Override
    protected void memoryLoad(Document requsetDocument, MessageExchange messageExchange){
      return;
    }
    @Override
    protected  void recallLoadHandle(Document requsetDocument, MessageExchange messageExchange, Document recalableLoadDocumnet){
      return;
    }





    protected boolean handelStatusResponse(Document responseStatusDoc, MessageExchange messageExchange) throws Exception {
        System.out.println("Ответ на запрос статуса загрузки преокта: ");
        System.out.println(XMLFormater.formatDocumentToString(responseStatusDoc));
        messageExchange.addResponse("projectStatusResponse",responseStatusDoc);


       if(assertLibrary.asserCheckStatusReady(responseStatusDoc)){

           messageExchange.addParameter("DraftPolicyImportId", responseStatusDoc.getElementsByTagName("DraftPolicyImportId").getLength() != 0 ? responseStatusDoc.getElementsByTagName("DraftPolicyImportId").item(0).getTextContent():"пусто");
           messageExchange.addParameter("DraftPolicyID", responseStatusDoc.getElementsByTagName("DraftPolicyID").getLength() != 0 ? responseStatusDoc.getElementsByTagName("DraftPolicyID").item(0).getTextContent():"пусто");
           messageExchange.addParameter("DraftPolicySerialKey", responseStatusDoc.getElementsByTagName("DraftPolicySerialKey").getLength() != 0  ? responseStatusDoc.getElementsByTagName("DraftPolicySerialKey").item(0).getTextContent():"пусто");
           messageExchange.addParameter("DraftPolicyNumberKey", responseStatusDoc.getElementsByTagName("DraftPolicyNumberKey").getLength() != 0 ? responseStatusDoc.getElementsByTagName("DraftPolicyNumberKey").item(0).getTextContent():"пусто");

           return true;
       }else{
           return false;
       }
    }



}
