package ru.rsa.testing.services.loaders;

import com.rsa.dkbm.ws.Attachment;
import com.rsa.eosago.ws.impl.CheckSubjectOSAGOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;

/**
 * Created by serdukovaa on 06.04.2016.
 */
@Service
public class TsCheckEOSAGOLoader extends AsynRequestLoader {



    @Autowired
    CheckSubjectOSAGOService checkSubjectOSAGOService;




    protected String getRECALL_TEMPLATE_PATH(){
        return "";
    }
    protected String getRECALL_LOG(){
        return "";
    }
    protected String getLOAD_REQUEST_LOG(){
        return  "Запрос на проверку TS";
    }
    protected String getSTATUS_LOG(){
        return  "Запрос статуса проверки TS";
    }




    @Override
    protected String builStatusRequest(MessageExchange messageExchange){
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ns2:TSStatusRequest xmlns:ns2=\"com/rsa/eosago/schema-1.1\">")
                .append("<InsurerID>99700000</InsurerID>")
                .append("<IDCheckTS>").append(messageExchange.getParameter("IDCheckTS")).append("</IDCheckTS>")
                .append("</ns2:TSStatusRequest>");
        return sb.toString();
    }
    protected void preHandleLoadRequest(Document requsetDocument, MessageExchange messageExchange){
        return;
    }



    @Override
    protected void memoryLoad(Document requsetDocument, MessageExchange messageExchange){
      return;
    }
    @Override
    protected  void recallLoadHandle(Document requsetDocument, MessageExchange messageExchange, Document recalableLoadDocumnet){
      return;
    }


    protected  Attachment serviceLoadGZIP(Attachment request) throws Exception{
        return  checkSubjectOSAGOService.loadVehicle(request);
    }

    protected  Attachment serviceStatusGZIP(Attachment request){
        return  checkSubjectOSAGOService.getVehicleStatus(request);
    }

    protected void memoryResponse(Document documentResponse, MessageExchange messageExchange){
        Node idCheck =   documentResponse.getElementsByTagName("IDCheckTS").item(0);
        messageExchange.addParameter("IDCheckTS",idCheck.getTextContent());
    }

    protected void validateResponse(Document documentResponse) throws Exception {
        assertLibrary.validCheckLoadRequest(documentResponse);
    }

    protected boolean handelStatusResponse(Document responseStatusDoc, MessageExchange messageExchange) throws Exception {
        messageExchange.addResponse("tsStatusResponse",responseStatusDoc);
       // return  false;
        return assertLibrary.asserCheckStatusReady(responseStatusDoc);
    }



}
