package ru.rsa.testing.services.loaders;

import com.rsa.bsi.ws.impl.PolicyLossKaskoService;
import com.rsa.dkbm.ws.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;
import ru.rsa.testing.xmlutils.XMLFormater;

/**
 * Created by serdukovaa on 06.04.2016.
 */
@Service
public class LossKaskoLoader extends AsynRequestLoader {


    @Autowired
    PolicyLossKaskoService policyLossKaskoService;


    protected String getRECALL_TEMPLATE_PATH(){
        return "test/bsi/templates/bsi_loss_recall.xml";
    }
    protected String getRECALL_LOG(){
        return "Отзыв убытка КАСКО";
    }
    protected String getLOAD_REQUEST_LOG(){
        return  "Запрос на загрузку убытка КАСКО";
    }
    protected String getSTATUS_LOG(){
        return  "Запрос статуса загрузки убытка КАСКО";
    }




    protected void validateResponse(Document documentResponse) throws Exception {
        assertLibrary.validKaskoLoadRequest(documentResponse);
    }

    protected void memoryResponse(Document documentResponse, MessageExchange messageExchange) throws Exception {
        System.out.println(XMLFormater.formatDocumentToString(documentResponse));
    }



    protected  Attachment serviceLoadGZIP(Attachment request) throws Exception{
        return  policyLossKaskoService.loadLossKasko(request);
    }

    protected  Attachment serviceStatusGZIP(Attachment request){
        return  policyLossKaskoService.getLossKaskoStatus(request);
    }



    @Override
    protected String builStatusRequest(MessageExchange messageExchange){
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
                .append("<ns2:LossKASKOStatusRequest xmlns:ns2=\"com/rsa/kasko/schema-1.1\">")
                .append("<LossKASKOTitle>")
                .append("<InsurerID>99700000</InsurerID>")
                .append("<TripNumber>").append(messageExchange.getParameter("TripNumber")).append("</TripNumber>")
                .append("<TripTm>").append(messageExchange.getParameter("TripTm")).append("</TripTm>")
                .append("</LossKASKOTitle>")
                .append("</ns2:LossKASKOStatusRequest>");
        return sb.toString();
    }



    protected void preHandleLoadRequest(Document requsetDocument, MessageExchange messageExchange){
        fillTrips(requsetDocument, messageExchange);
    }




    @Override
    protected void memoryLoad(Document requsetDocument, MessageExchange messageExchange){
        messageExchange.addKaskoLoss(requsetDocument);
    }


    @Override
    protected  void recallLoadHandle(Document requsetDocument, MessageExchange messageExchange, Document recalableLoadDocumnet){
        fillTrips(requsetDocument, messageExchange);
        Node lossID = requsetDocument.getElementsByTagName("LossID").item(0);
        lossID.setTextContent(recalableLoadDocumnet.getElementsByTagName("LossID").item(0).getTextContent());

        Node dateRevision = requsetDocument.getElementsByTagName("DateRevision").item(0);
        dateRevision.setTextContent(messageExchange.getParameter("DateActionEnd"));
    }


    protected boolean handelStatusResponse(Document responseStatusDoc, MessageExchange messageExchange) throws Exception {
        messageExchange.addResponse("kaskoLossStatusResponse",responseStatusDoc);
        return assertLibrary.asserCheckStatusReady(responseStatusDoc);
    }



}
