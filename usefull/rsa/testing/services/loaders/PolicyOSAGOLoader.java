package ru.rsa.testing.services.loaders;

import com.rsa.dkbm.ws.Attachment;
import com.rsa.dkbm.ws.impl.PolicyLossService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;

/**
 * Created by serdukovaa on 06.04.2016.
 */
@Service
public class PolicyOSAGOLoader extends AsynRequestLoader {


    @Autowired
    PolicyLossService policyLossService;





    protected String getRECALL_TEMPLATE_PATH(){
        return "test/dkbm/templates/policy_recall.xml";
    }
    protected String getRECALL_LOG(){
      return "Отзыв полиса ОСАГО";
    }
    protected String getLOAD_REQUEST_LOG(){
      return  "Запрос на загрузку полиса ОСАГО";
    }
    protected String getSTATUS_LOG(){
        return  "Запрос статуса загрузки полиса ОСАГО";
    }




    @Override
    protected String builStatusRequest(MessageExchange messageExchange){
        StringBuilder sb = new StringBuilder();
        String tripNumberID = messageExchange.getParameter("TripNumber");
        String tripTmID =  messageExchange.getParameter("TripTm");
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><rsa:PolicyStatusRequest  xmlns:schemaLocation=\"com/rsa/dkbm/schema-1.5 PolicyStatusRequest.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"")
                .append(" xmlns:rsa=\"com/rsa/dkbm/schema-1.5\">")
                .append("<PolicyTitle>")
                .append("<InsurerID>99700000</InsurerID>")
                .append("<TripNumber>").append(tripNumberID)
                .append("</TripNumber><TripTm>").append(tripTmID)
                .append("</TripTm></PolicyTitle></rsa:PolicyStatusRequest>");
        return sb.toString();
    }



    protected void memoryLoad(Document requsetDocument, MessageExchange messageExchange){
        messageExchange.addPolicy(requsetDocument);


        //подразумевается что только для первой загрузки записываем
        if(messageExchange.getParameter("PolicyID") == null) {
            Node policyID = requsetDocument.getElementsByTagName("PolicyID").item(0);
            messageExchange.addParameter("PolicyID",policyID.getTextContent());
        }
        if(messageExchange.getParameter("DateActionEnd") == null) {
            Node dateActionEnd = requsetDocument.getElementsByTagName("DateActionEnd").item(0);
            messageExchange.addParameter("DateActionEnd", dateActionEnd.getTextContent());
        }

    }

    protected  void recallLoadHandle(Document requsetDocument, MessageExchange messageExchange, Document recalableLoadDocumnet){
        fillTrips(requsetDocument, messageExchange);

        Node policyID = requsetDocument.getElementsByTagName("PolicyID").item(0);
        policyID.setTextContent(recalableLoadDocumnet.getElementsByTagName("PolicyID").item(0).getTextContent());

        Node dateCreate = requsetDocument.getElementsByTagName("DateCreate").item(0);
        dateCreate.setTextContent(recalableLoadDocumnet.getElementsByTagName("DateCreate").item(0).getTextContent().substring(0,10));

        Node dateRevision = requsetDocument.getElementsByTagName("DateRevision").item(0);
        dateRevision.setTextContent(recalableLoadDocumnet.getElementsByTagName("DateActionEnd").item(0).getTextContent());

    }


    protected  Attachment serviceLoadGZIP(Attachment request) throws Exception{
        return  policyLossService.loadPolicy(request);
    }

    protected  Attachment serviceStatusGZIP(Attachment request){
        return  policyLossService.getPolicyStatus(request);
    }

    protected void preHandleLoadRequest(Document requsetDocument, MessageExchange messageExchange){
        fillTrips(requsetDocument, messageExchange);
    }


    protected void memoryResponse(Document documentResponse, MessageExchange messageExchange){
        messageExchange.addResponse("policyLoadDocumentResponse",documentResponse);
    }


    protected void validateResponse(Document documentResponse) throws Exception {
        assertLibrary.validPoliceLoadRequest(documentResponse);
    }


    protected boolean handelStatusResponse(Document responseStatusDoc, MessageExchange messageExchange) throws Exception {
        messageExchange.addResponse("policyStatusResponse",responseStatusDoc);
        return assertLibrary.asserPoliceStatusReady(responseStatusDoc);
    }







}
