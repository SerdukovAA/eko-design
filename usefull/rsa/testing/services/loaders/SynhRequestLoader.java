package ru.rsa.testing.services.loaders;

import com.rsa.dkbm.ws.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import ru.rsa.testing.beans.MessageExchange;
import ru.rsa.testing.bilder.AttachmenttBuilder;
import ru.rsa.testing.bilder.XMLDocumentBuilder;
import ru.rsa.testing.xmlutils.XMLFormater;

/**
 * Created by serdukovaa on 06.04.2016.
 */
public abstract class SynhRequestLoader  {


    @Autowired
    AttachmenttBuilder attachmenttBuilder;

    @Autowired
    XMLDocumentBuilder xMLDocumentBuilder;

    protected abstract String getRequestName();


    protected abstract Attachment synhRequest(Attachment request);

    protected abstract void responseHandler(MessageExchange messageExchange, Document documentResponse );



    public MessageExchange requestBase64ByFileName(String filePath, MessageExchange messageExchange) throws Exception {
        if(messageExchange == null) messageExchange = new MessageExchange();
        System.out.println(getRequestName());
        Attachment request = attachmenttBuilder.getBase64AttachmentByFilePath(filePath);
        System.out.println(XMLFormater.formatDocumentToString(xMLDocumentBuilder.getDocumentFromBase64Atachement(request)));

        Attachment response = synhRequest(request);
        Document documentResponse = xMLDocumentBuilder.getDocumentFromBase64Atachement(response);
        System.out.println("Получили ответ: ");
        System.out.println(XMLFormater.formatDocumentToString(documentResponse));

        responseHandler(messageExchange, documentResponse);
        return messageExchange;
    }





}
