package ru.rsa.testing.services.loaders;

import com.rsa.dkbm.ws.Attachment;
import com.rsa.eosago.ws.impl.ObjectExistsException;
import com.rsa.eosago.ws.impl.PolicyLossProcessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.rsa.testing.beans.MessageExchange;
import ru.rsa.testing.bilder.AttachmenttBuilder;
import ru.rsa.testing.bilder.XMLDocumentBuilder;
import ru.rsa.testing.services.AssertLibrary;
import ru.rsa.testing.xmlutils.XMLFormater;

import java.util.Date;

/**
 * Created by serdukovaa on 06.04.2016.
 */
public abstract class AsynRequestLoader {


    @Autowired
    AttachmenttBuilder attachmenttBuilder;

    @Autowired
    XMLDocumentBuilder xMLDocumentBuilder;

    @Autowired
    AssertLibrary assertLibrary;


    protected abstract String getRECALL_TEMPLATE_PATH();
    protected abstract String getRECALL_LOG();
    protected abstract String getSTATUS_LOG();
    protected abstract String getLOAD_REQUEST_LOG();




    protected abstract String builStatusRequest(MessageExchange messageExchange);




    protected abstract Attachment serviceLoadGZIP(Attachment request)  throws Exception;

    protected abstract Attachment serviceStatusGZIP(Attachment request);


    /**
     * Запомнить то что отгружаем перед вызовом serviceLoadGZIP
     * @param requsetDocument
     * @param messageExchange
     */
    protected abstract void memoryLoad(Document requsetDocument, MessageExchange messageExchange);



    /**
     * Предварительная подготовка запроса перед вызовом serviceLoadGZIP
     * @param requsetDocument
     * @param messageExchange
     */

    protected abstract void preHandleLoadRequest(Document requsetDocument, MessageExchange messageExchange);

    /**
     * Предварительная подготовка запроса на отзыв перед вызовом serviceLoadGZIP
     * @param requsetDocument
     * @param messageExchange
     * @param recalableLoadDocumnet
     */
    protected abstract void recallLoadHandle(Document requsetDocument, MessageExchange messageExchange, Document recalableLoadDocumnet);


    /**
     * Сохранить ответ от проверки статуса выгрузки. Вернуть true  если обработка закончилась , т.е. не в процессе ожидания не 1 и не 2 код ошибки
     * @param requsetDocument
     * @param messageExchange
     * @return
     * @throws Exception
     */
    protected abstract boolean handelStatusResponse(Document requsetDocument, MessageExchange messageExchange) throws Exception;


    /**
     * Выполняем необходимые запоминания ответа от serviceLoadGZIP
     * @param documentResponse
     * @param messageExchange
     */
    protected abstract void memoryResponse(Document documentResponse, MessageExchange messageExchange) throws Exception;


    /**
     * Выполняем необзодимые проверки после serviceLoadGZIP
     * @param documentResponse
     * @throws Exception
     */
    protected abstract void validateResponse(Document documentResponse) throws Exception;





    protected MessageExchange recalGZIPDocument(Document requsetDocument, MessageExchange messageExchange) throws Exception{
        Attachment request = attachmenttBuilder.getGZIPAttachmentFromDoc(requsetDocument);
        System.out.println(XMLFormater.formatDocumentToString(xMLDocumentBuilder.getDocumentFromGZIPAtachement(request)));
        Attachment response =  serviceLoadGZIP(request);
        Document documentResponse = xMLDocumentBuilder.getDocumentFromGZIPAtachement(response);
        memoryResponse(documentResponse,messageExchange);
        return messageExchange;
    }




    protected MessageExchange loadGZIPDocument(Document requsetDocument, MessageExchange messageExchange) throws Exception{

        preHandleLoadRequest(requsetDocument, messageExchange);
        memoryLoad(requsetDocument, messageExchange);
        Attachment request = attachmenttBuilder.getGZIPAttachmentFromDoc(requsetDocument);
        System.out.println(XMLFormater.formatDocumentToString(xMLDocumentBuilder.getDocumentFromGZIPAtachement(request)));

        Attachment response =  serviceLoadGZIP(request);
        Document documentResponse = xMLDocumentBuilder.getDocumentFromGZIPAtachement(response);

        validateResponse(documentResponse);
        memoryResponse(documentResponse,messageExchange);

        return messageExchange;
    }


    protected void fillTrips(Document requsetDocument, MessageExchange messageExchange){
        Date d = new Date();
        String tripNumberID = new Long(d.getTime()).toString();
        Node tripNumber = requsetDocument.getElementsByTagName("TripNumber").item(0);
        tripNumber.setTextContent(tripNumberID);
        messageExchange.addParameter("TripNumber",tripNumberID);

        String tripTmID = "2016-01-01T13:06:20Z";
        Node tripTm = requsetDocument.getElementsByTagName("TripTm").item(0);
        tripTm.setTextContent(tripTmID);
        messageExchange.addParameter("TripTm",tripTmID);

    }

    public MessageExchange loadRequestByFileName(String filePath, MessageExchange messageExchange) throws Exception {
        if(messageExchange == null){
            messageExchange = new MessageExchange();
        }
        System.out.println(getLOAD_REQUEST_LOG());
        Document requsetDocument = xMLDocumentBuilder.getDocumentByFilePath(filePath);
        messageExchange = loadGZIPDocument(requsetDocument, messageExchange);
        return messageExchange;
    }




    public MessageExchange recalLoad(MessageExchange messageExchange, Document recalableLoadDocumnet) throws Exception{
        if(messageExchange == null) messageExchange = new MessageExchange();

        System.out.println(getRECALL_LOG());
        //подготовить запрос перед
        Document requsetDocument = xMLDocumentBuilder.getDocumentByFilePath(getRECALL_TEMPLATE_PATH());
        recallLoadHandle(requsetDocument, messageExchange, recalableLoadDocumnet);
        messageExchange = recalGZIPDocument(requsetDocument,messageExchange);
        messageExchange = statusRequest(messageExchange);
        return messageExchange;
    }


    public MessageExchange statusRequest(MessageExchange messageExchange) throws Exception {

        if(messageExchange == null) messageExchange = new MessageExchange();
        System.out.println(getSTATUS_LOG());


        String statusRequestXml = builStatusRequest(messageExchange);



        Attachment statusRequest = attachmenttBuilder.getGZIPAttachmentFromXml(statusRequestXml);
        Attachment responseStatus;
        Document responseStatusDoc;
        int cont = 0 ;
        boolean ready = false;
        long timeSleep = 10000L;


        do{
            System.out.println("Делаем "+cont+" запрос статуса загрузки");
            System.out.println(XMLFormater.formatDocumentToString(xMLDocumentBuilder.getDocumentFromGZIPAtachement(statusRequest)));
            responseStatus = serviceStatusGZIP(statusRequest);
            responseStatusDoc =  xMLDocumentBuilder.getDocumentFromGZIPAtachement(responseStatus);

            ready = handelStatusResponse(responseStatusDoc,messageExchange);

            if(!ready){
                try {
                    Thread.sleep(timeSleep);
                    System.out.println("Повторный запрос статуса загрузки через "+timeSleep+" милисекунд..."+cont);
                    timeSleep = timeSleep + 4000L;
                    cont++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(cont>20){
                System.out.println("Более 20 запросов статуса загрузки");
                return messageExchange;
            }
        } while(!ready);

        return messageExchange;
    }


}
