package ru.rsa.testing.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import ru.rsa.testing.beans.MessageExchange;
import ru.rsa.testing.services.loaders.*;

/**
 * Created by serdukovaa on 18.03.2016.
 */
@Service
public class RequestBsi {


   @Autowired
   PolicyKaskoLoader policyKaskoLoader;

   @Autowired
    LossKaskoLoader lossKaskoLoader;




    //Комплексный метод на загрузку
    public MessageExchange successKaskoPolicyLoad(String filePath, MessageExchange messageExchange) throws Exception{
        messageExchange = policyKaskoLoader.loadRequestByFileName(filePath, messageExchange);
        return policyKaskoLoader.statusRequest(messageExchange);
    }


    public MessageExchange successKaskoLossLoad(String filePath, MessageExchange messageExchange) throws Exception{
        messageExchange = lossKaskoLoader.loadRequestByFileName(filePath, messageExchange);
        return lossKaskoLoader.statusRequest(messageExchange);
    }




    //Комплексный метод на отзыв полисов каско
    public MessageExchange successKaskoPolicyRecall(MessageExchange messageExchange, Document policy) throws Exception{
        messageExchange = policyKaskoLoader.recalLoad(messageExchange, policy);
        return policyKaskoLoader.statusRequest(messageExchange);
   }

    //Комплексный метод на отзыв полисов каско
    public MessageExchange successKaskoLossRecall(MessageExchange messageExchange, Document policy) throws Exception{
        messageExchange = lossKaskoLoader.recalLoad(messageExchange, policy);
        return lossKaskoLoader.statusRequest(messageExchange);
    }



}
