package ru.rsa.testing.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import ru.rsa.testing.beans.MessageExchange;
import ru.rsa.testing.services.loaders.KBMRequestLoader;
import ru.rsa.testing.services.loaders.LossOSAGOLoader;
import ru.rsa.testing.services.loaders.PolicyOSAGOLoader;

/**
 * Created by serdukovaa on 18.03.2016.
 */
@Service
public class RequestDkbm{


    @Autowired
    KBMRequestLoader kbmRequestLoader;

    @Autowired
    LossOSAGOLoader lossOSAGOLoader;

    @Autowired
    PolicyOSAGOLoader policyOSAGOLoader;




    public MessageExchange kbmRequestByFileName(String filePath, MessageExchange messageExchange) throws Exception {
        return kbmRequestLoader.requestBase64ByFileName(filePath, messageExchange);
    }


    //Комплексный метод на загрузку
    public MessageExchange successPolicyLoad(String filePath, MessageExchange messageExchange) throws Exception{
        messageExchange = policyOSAGOLoader.loadRequestByFileName(filePath, messageExchange);
        return policyOSAGOLoader.statusRequest(messageExchange);
    }

    //Комплексный метод на загрузку
    public MessageExchange successLossLoad(String filePath, MessageExchange messageExchange) throws Exception{
        messageExchange = lossOSAGOLoader.loadRequestByFileName(filePath, messageExchange);
        return lossOSAGOLoader.statusRequest(messageExchange);
    }

    //Комплексный метод на отзыв полисов осаго
    public MessageExchange successPolicyRecall(MessageExchange messageExchange, Document policy) throws Exception{
        messageExchange = policyOSAGOLoader.recalLoad(messageExchange, policy);
        return policyOSAGOLoader.statusRequest(messageExchange);
   }

    //Комплексный метод на отзыв убытков осаго
    public MessageExchange successLossRecall(MessageExchange messageExchange, Document loss) throws Exception{
        messageExchange = lossOSAGOLoader.recalLoad(messageExchange, loss);
        return lossOSAGOLoader.statusRequest(messageExchange);
    }



}
