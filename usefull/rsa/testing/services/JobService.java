package ru.rsa.testing.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;
import ru.rsa.testing.beans.MessageExchange;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by serdukovaa on 11.04.2016.
 */
@Service
public class JobService {

    @Autowired
    @Qualifier("")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    TaskExecutor taskExecutor;



    private void asynchStartJob() {
        //creates a DB thread pool
        this.taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                startJob();
            }
        });
    }

   //Данный метод блокирует поток, поэтому возможно нет необходимости дожидаться ответа в цикле. Но пока цикл оставляем
    private void startJob(){
        final String plsql = "" +
                " DECLARE " +
               " +
                " END;";
        Connection connection = null;
        try {
            connection = jdbcTemplate.getDataSource().getConnection();
            CallableStatement callableSt = connection.prepareCall(plsql);
            callableSt.setString(1, null);

            callableSt.execute();

        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(connection != null)
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }



    private String getLastJobDate(){
        String last = jdbcTemplate.queryForObject("SELECT EOAD' AND lSC) WHERE ROUM =1", String.class);
        return last;
    }


    /**
     * Процедура запускает JOB и ждет его окончания
     * @throws Exception
     */
    public void waitJobDB2() throws Exception{
        String lastTime = getLastJobDate();
        System.out.println("Последний  завершился "+lastTime);
        asynchStartJob();
        System.out.println(" запущен, входим в цикл ожидания");
        boolean loaded = false;
        while(!loaded){
            String currentJobDate = getLastJobDate();
            System.out.println("Последний  " + lastTime + " текущий  "+currentJobDate);
            if(!lastTime.equals(currentJobDate)){
                loaded = true;
            }else{
                try {
                    Thread.sleep(20*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }





}
