package ru.rsa.testing.config;

import com.rsa.bsi.ws.impl.PolicyLossKaskoService;
import com.rsa.bsi.ws.impl.PolicyLossKaskoServiceImplService;
import com.rsa.dkbm.ws.impl.KbmToService;
import com.rsa.dkbm.ws.impl.KbmToServiceImplService;
import com.rsa.dkbm.ws.impl.PolicyLossService;
import com.rsa.dkbm.ws.impl.PolicyLossServiceImplService;

import com.rsa.eosago.ws.impl.CheckSubjectOSAGOService;
import com.rsa.eosago.ws.impl.CheckSubjectOSAGOServiceImplService;
import com.rsa.eosago.ws.impl.ProjectPolicyService;
import com.rsa.eosago.ws.impl.ProjectPolicyServiceImplService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.rsa.testing.handler.SimpleWsAuthHandler;

import java.net.URL;

/**
 * Created by serdukovaa on 26.10.2015.
 */

@Configuration
@ComponentScan({"ru.rsa.*"})
@PropertySource("application-${profile}.properties")
public class SpringConfig {

    @Autowired
    public Environment evn;




    ///----------------клиенты на серверы--------------------------------////
    @Bean(name="kbmToService")
    public KbmToService getKbmToService() throws Exception {
        URL url  = new URL(evn.getProperty("configuration.kbmToServiceUrl"));
        KbmToServiceImplService sis = new KbmToServiceImplService(url);
        return sis.getKbmToServiceImplPort();
    }


    @Bean(name="policyLossService")
    public PolicyLossService getPolicyLossService() throws Exception {
        URL url  = new URL(evn.getProperty("configuration.PolicyLossServiceUrl"));
        PolicyLossServiceImplService sis = new PolicyLossServiceImplService(url);
        return sis.getPolicyLossServiceImplPort();
    }


    @Bean(name="checkEosagoService")
    public CheckSubjectOSAGOService getCheckEosagoService() throws Exception {
        URL url  = new URL(evn.getProperty("configuration.checkEosagoServiceUrl"));
        CheckSubjectOSAGOServiceImplService sis = new CheckSubjectOSAGOServiceImplService(url);
        return sis.getCheckSubjectOSAGOServiceImplPort();
    }

    @Bean(name="projectPolicyService")
    public ProjectPolicyService getProjectPolicyService() throws Exception {
        URL url  = new URL(evn.getProperty("configuration.projectPolicyService"));
        ProjectPolicyServiceImplService sis = new ProjectPolicyServiceImplService(url);
        return sis.getProjectPolicyServiceImplPort();
    }


    @Bean(name="policyLossKaskoService")
    public PolicyLossKaskoService getPolicyLossKaskoService() throws Exception {
        URL url  = new URL(evn.getProperty("configuration.PolicyLossKaskoServiceUrl"));
        PolicyLossKaskoServiceImplService sis = new PolicyLossKaskoServiceImplService(url);
        return sis.getPolicyLossKaskoServiceImplPort();
    }




    ///----------------сервисы--------------------------------////
    @Bean(name="simpleWsAuthHandler")
    public SimpleWsAuthHandler getSimpleWsAuthHandler(){
        SimpleWsAuthHandler.setPasswordStr(evn.getProperty("testPassword"));
        SimpleWsAuthHandler.setUsernameStr(evn.getProperty("testLogin"));
        return new SimpleWsAuthHandler();
    }






    ///----------------Базы данных--------------------------------////
    //1. Настроить пул
    @Bean(name = "BD2")
    public javax.sql.DataSource getDataSourceBD2() {
        org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
        ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        ds.setUrl(evn.getProperty("oracleDB2.url"));
        ds.setUsername(evn.getProperty("oracleDB2.userName"));
        ds.setPassword(evn.getProperty("oracleDB2.password"));
        ds.setInitialSize(5);
        ds.setTestOnBorrow(true);
        ds.setValidationInterval(34000);
        ds.setValidationQuery("SELECT 1 FROM DUAL");
        ds.setMaxActive(Integer.valueOf(evn.getProperty("oracleDB2.maxThreadsCount")));
        ds.setMaxIdle(5);
        ds.setMinIdle(1);
        return ds;
    }


    @Bean(name = "jdbcTemplateBD2")
    public JdbcTemplate getJdbcTemplateBD2() {
        return new JdbcTemplate(getDataSourceBD2());
    }


    @Bean(name = "taskExecutor")
    public TaskExecutor getTaskExecutor() {
        ThreadPoolTaskExecutor t = new ThreadPoolTaskExecutor();
        t.setMaxPoolSize(5);
        t.setQueueCapacity(5);
        t.setMaxPoolSize(5);
        return t;
    }




}