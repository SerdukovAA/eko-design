package ru.rsa.testing.beans;
import groovy.util.slurpersupport.GPathResult;
import org.w3c.dom.Document;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by serdukovaa on 20.03.2016.
 */
public class MessageExchange {


    //коллекция ответов после запросов
    private Map<String,Document> responseMap = new ConcurrentHashMap<String, Document>();
    //коллекция параметров, используемых между запросами
    private Map<String,String> parametersMap = new ConcurrentHashMap<String,String>();



    //загруженные во время тестирования убытки
    private  Set<Document> losses = Collections.synchronizedSet(new HashSet<Document>());
    private  Set<Document> policies = Collections.synchronizedSet(new HashSet<Document>());
    private  Set<Document> kaskoPolicies = Collections.synchronizedSet(new HashSet<Document>());



    private  Set<Document> kaskoLosss = Collections.synchronizedSet(new HashSet<Document>());

    /////-------------------getters and setters-------------------------------//////

    public String getParameter(String key) {
        return parametersMap.get(key);
    }

    public void addParameter(String key, String parameter) {
        if(parametersMap.get(key) != null){
            parametersMap.remove(key);
        }
        parametersMap.put(key, parameter);
        return;
    }

    public Document getResponse(String key) {
        return responseMap.get(key);
    }


    public synchronized Set<Document> getPolicies() {
        return policies;
    }

    public synchronized Set<Document> getKaskoPolicies() { return kaskoPolicies;  }

    public synchronized Set<Document> getLosses() {
        return losses;
    }

    public Set<Document> getKaskoLosss() {
        return kaskoLosss;
    }

    public void addResponse(String key, Document response) {
        if(responseMap.get(key) != null){
            responseMap.remove(key);
        }
        responseMap.put(key, response);
        return;
    }

    public void addLoss(Document loss) {
        this.losses.add(loss);
    }
    public void addPolicy(Document policy) {
        this.policies.add(policy);
    }
    public void addKaskoPolicy(Document policy) {
        this.kaskoPolicies.add(policy);
    }
    public void addKaskoLoss(Document loss) {
        this.kaskoLosss.add(loss);
    }



}
