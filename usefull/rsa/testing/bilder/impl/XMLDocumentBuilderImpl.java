package ru.rsa.testing.bilder.impl;


import com.rsa.dkbm.ws.Attachment;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import ru.rsa.testing.bilder.XMLDocumentBuilder;
import ru.rsa.testing.procces.FileManager;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * Created by serdukovaa on 28.10.2015.
 */
@Service
public class XMLDocumentBuilderImpl implements XMLDocumentBuilder {

    @Autowired
    FileManager fileManager;

    public InputStream getInputStrimFromBase64Atachement(Attachment attachment) throws Exception{
        try {
          return attachment.getData().getDataSource().getInputStream();
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Ошибка парсирования base64 в InputStrim");
        }
    }

    public InputStream getInputStrimFromGZIPAtachement(Attachment attachment) throws Exception{
        try {
            InputStream inputStream = attachment.getData().getDataSource().getInputStream();
            byte[] result =  IOUtils.toByteArray(inputStream);
            GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (result));
            return gzip;
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Ошибка парсирования GZIP в InputStrim");
        }
    }

    public Document getDocumentByFilePath(String pathToFile) throws Exception{
        try {
            //путь до файла корень /xml/
            InputStream inputStream = new ByteArrayInputStream(fileManager.getXmlFileBytesByPath(pathToFile));
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputStream);
            doc.getDocumentElement().normalize();
            return doc;
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Ошибка парсирования xml файла");
        }
    }


    public Document getDocumentFromBase64Atachement(Attachment attachment) throws Exception{
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(attachment.getData().getDataSource().getInputStream());
            doc.getDocumentElement().normalize();
            return doc;
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Ошибка парсирования base64 в xml файла");
        }
    }

    public Document getDocumentFromGZIPAtachement(Attachment attachment) throws Exception{
        try {
            InputStream inputStream = attachment.getData().getDataSource().getInputStream();
            byte[] result =  IOUtils.toByteArray(inputStream);
            GZIPInputStream  gzip = new GZIPInputStream (new ByteArrayInputStream (result));
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(gzip);
            doc.getDocumentElement().normalize();
            return doc;
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Ошибка парсирования gzip в xml файла");
        }
    }




}
