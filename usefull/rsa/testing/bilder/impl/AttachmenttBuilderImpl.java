package ru.rsa.testing.bilder.impl;


import com.rsa.dkbm.ws.Attachment;
import com.sun.xml.ws.util.ByteArrayDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.rsa.testing.bilder.AttachmenttBuilder;
import ru.rsa.testing.procces.FileManager;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by serdukovaa on 28.10.2015.
 */
@Service
public class AttachmenttBuilderImpl implements AttachmenttBuilder {


   @Autowired
   FileManager fileManager;

    public  Attachment getBase64AttachmentByFilePath(String pathToFile) throws Exception{
        Attachment attachment = new Attachment();
        byte[] data = fileManager.getXmlFileBytesByPath(pathToFile);
        DataSource ds = new ByteArrayDataSource(data, "text/plain; charset=UTF-8");
        attachment.setData(new DataHandler(ds));
        return attachment;
    }

    public Attachment getGZIPAttachmentByFilePath(String pathToFile) throws Exception{
        Attachment attachment = new Attachment();
        byte[] dataToCompress = fileManager.getXmlFileBytesByPath(pathToFile);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(dataToCompress.length);
        GZIPOutputStream zipStream = new GZIPOutputStream(byteStream);
        zipStream.write(dataToCompress);
        zipStream.close();
        byteStream.close();
        byte[] compressedData = byteStream.toByteArray();
        DataSource ds = new ByteArrayDataSource(compressedData, "text/plain; charset=UTF-8");
        attachment.setData(new DataHandler(ds));
        return attachment;
    }

    public Attachment getGZIPAttachmentFromDoc(Document document) throws Exception{
        Attachment attachment = new Attachment();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        elementToStream(document.getDocumentElement(), baos);
        byte[] dataToCompress = baos.toByteArray();
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(dataToCompress.length);
        GZIPOutputStream zipStream = new GZIPOutputStream(byteStream);
        zipStream.write(dataToCompress);
        zipStream.close();
        byteStream.close();
        byte[] compressedData = byteStream.toByteArray();
        DataSource ds = new ByteArrayDataSource(compressedData, "text/plain; charset=UTF-8");
        attachment.setData(new DataHandler(ds));
        return attachment;
    }

    public Attachment getGZIPAttachmentFromXml(String xml) throws Exception{
        Attachment attachment = new Attachment();
        byte[] dataToCompress = xml.getBytes();
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(dataToCompress.length);
        GZIPOutputStream zipStream = new GZIPOutputStream(byteStream);
        zipStream.write(dataToCompress);
        zipStream.close();
        byteStream.close();
        byte[] compressedData = byteStream.toByteArray();
        DataSource ds = new ByteArrayDataSource(compressedData, "text/plain; charset=UTF-8");
        attachment.setData(new DataHandler(ds));
        return attachment;
    }

    private void elementToStream(Element element, OutputStream out) {
        try {
            DOMSource source = new DOMSource(element);
            StreamResult result = new StreamResult(out);
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();
            transformer.transform(source, result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



}
