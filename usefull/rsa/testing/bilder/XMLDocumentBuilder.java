package ru.rsa.testing.bilder;
import com.rsa.dkbm.ws.Attachment;
import org.w3c.dom.Document;

import java.io.InputStream;

/**
 * Created by serdukovaa on 28.10.2015.
 */
public interface XMLDocumentBuilder {
    public InputStream getInputStrimFromBase64Atachement(Attachment attachment) throws Exception;
    public InputStream getInputStrimFromGZIPAtachement(Attachment attachment) throws Exception;
    public Document getDocumentFromBase64Atachement(Attachment attachment) throws Exception;
    public Document getDocumentFromGZIPAtachement(Attachment attachment) throws Exception;
    public Document getDocumentByFilePath(String pathToFile) throws Exception;
}
