package ru.rsa.testing.bilder;

import com.rsa.dkbm.ws.Attachment;
import org.w3c.dom.Document;

/**
 * Created by serdukovaa on 28.10.2015.
 */
public interface AttachmenttBuilder {
    public Attachment getBase64AttachmentByFilePath(String pathToFile) throws Exception;
    public Attachment getGZIPAttachmentByFilePath(String pathToFile) throws Exception;
    public Attachment getGZIPAttachmentFromDoc(Document document) throws Exception;
    public Attachment getGZIPAttachmentFromXml(String xml) throws Exception;

}
