package ru.rsa.testing.xmlutils;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by serdukovaa on 29.10.2015.
 */
@Service
public class XMLFormater {

    public static String formatDocumentToString(Document document) throws Exception {

        Writer out = null;
        try {
            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);
            return out.toString();
        } finally {
            if (out != null) {
                out.close();
            }

        }
    }
}
