package ru.rsa.testing.xmlutils;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

/**
 * Created by serdukovaa on 21.03.2016.
 */
@Service
public class XMLParser {

    public String gVal(String uri, Document doc) throws Exception{
        //todo возможна утечка памяти
        XPath xPath =  XPathFactory.newInstance().newXPath();
        return xPath.compile(uri).evaluate(doc);
    }

}
