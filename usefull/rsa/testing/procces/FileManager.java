package ru.rsa.testing.procces;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by serdukovaa on 18.03.2016.
 */
@Service
public class FileManager{


    public byte[] getXmlFileBytesByPath(String filePath) throws IOException{
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource(filePath).getFile());
        Path path = Paths.get(file.getAbsolutePath());
        return Files.readAllBytes(path);
    }

}
